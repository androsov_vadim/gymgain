package com.vaisoft.health.gymgain.schedule;

import com.vaisoft.health.gymgain.schedule.validator.PreciseWorkoutsValidator;
import com.vaisoft.health.gymgain.schedule.validator.SimpleWorkoutsValidator;

import org.joda.time.DateTime;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestFlexibleSchedule extends TestSchedule {

    /**
     1. Ежедневные тренировки - критический уровень каждый день без занятий
     a. Неделю идут подряд - без сгораний
     b. Критический уровень - только в дни без тренировок
     */
    @Test
    public void case1() {
        DateTime firstWeekStart = MON_START;
        DateTime secondWeekStart = MON_START.plusDays(7);
        DateTime upTo = SUN[1];

        List<DateTime> workouts = generateDates(firstWeekStart, 7, 1);
        workouts.addAll(generateDates(secondWeekStart, 7, 2));

        Schedule.Flexible schedule = createEveryDaySchedule();
        FlexibleAnalyserGenerator generator = new FlexibleAnalyserGenerator(schedule);

        int step = getStep(schedule);
        List<DateTime> shouldBurn = generateDates(secondWeekStart.plusDays(1), 6, 2);
        PreciseWorkoutsValidator burnValidator = new PreciseWorkoutsValidator(shouldBurn);
        validateBurnouts(firstWeekStart, upTo, generator, workouts, burnValidator, step);

        List<DateTime> shouldBeCritical = generateDates(secondWeekStart.plusDays(1), 6, 2);
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(firstWeekStart, upTo, generator, workouts, criticalsValidator);
    }

    /**
     2. Три раза в неделю
     a. На первой неделе три занятия (понедельник, пятница, суббота)
     b. На второй неделе одно занятие в понедельник. Сгорает в воскресенье. Критический с пятницы
     */
    @Test
    public void case2() {
        List<DateTime> workouts = createDateList(new DateTime[] {MON[0], FRI[0], SAT[0], MON[1]});

        Schedule.Flexible schedule = createTimesPerWeeksSchedule(3, 1);
        FlexibleAnalyserGenerator generator = new FlexibleAnalyserGenerator(schedule);

        int step = getStep(schedule);
        List<DateTime> shouldBurn = createDateList(new DateTime[] {SUN[1]});
        PreciseWorkoutsValidator burnValidator = new PreciseWorkoutsValidator(shouldBurn);
        validateBurnouts(MON_START, SUN[1], generator, workouts, burnValidator, step);

        List<DateTime> shouldBeCritical = createDateList(new DateTime[] {FRI[1], SAT[1], SUN[1]});
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(MON_START, SUN[1], generator, workouts, criticalsValidator);
    }

    /**
     3. 10 раз в месяц, одно занятие в понедельник 1-й недели и одно в понедельник 2-й
     a. Критический уровень с субботы по воскресенье 1-й недели и со вторника 2-й
     b. 5 тренировок сгорает
     */
    @Test
    public void case3() {
        DateTime upTo = upToEndOfMonth();
        List<DateTime> workouts = createDateList(new DateTime[] {MON[0], MON[1]});

        Schedule.Flexible schedule = createTimesAMonthSchedule(10);
        FlexibleAnalyserGenerator generator = new FlexibleAnalyserGenerator(schedule);

        int step = getStep(schedule);
        SimpleWorkoutsValidator burnValidator = new SimpleWorkoutsValidator(5);
        validateBurnouts(MON_START, upTo, generator, workouts, burnValidator, step);

        List<DateTime> shouldBeCritical = createDateList(new DateTime[] {SAT[0], SUN[0]});
        shouldBeCritical.addAll(generateDates(TUE[1], upTo));
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(MON_START, upTo, generator, workouts, criticalsValidator);
    }

    /**
     4. 10 раз в месяц, вообще без занятий. Критический уровень начиная с понедельника
     */
    @Test
    public void case4() {
        DateTime upTo = upToEndOfMonth();
        List<DateTime> workouts = EMPTY_DAYS;

        Schedule.Flexible schedule = createTimesAMonthSchedule(10);
        FlexibleAnalyserGenerator generator = new FlexibleAnalyserGenerator(schedule);

        int step = getStep(schedule);
        SimpleWorkoutsValidator burnValidator = new SimpleWorkoutsValidator(7);
        validateBurnouts(MON_START, upTo, generator, workouts, burnValidator, step);

        List<DateTime> shouldBeCritical = generateDates(WEN[0], upTo);
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(MON_START, upTo, generator, workouts, criticalsValidator);
    }

    /**
     5. 5 раз в 2 недели без сгораний
     a. Занятия понедельник, суббота, понедельник, вторник воскресенье. Не сгорает ничего
     b. Критические даты - 1-я неделя четверг-пятница, вторая неделя - суббота
     */
    @Test
    public void case5() {
        DateTime thirdWeekStart = MON_START.plusDays(14);
        List<DateTime> workouts = createDateList(new DateTime[] {MON[0], SAT[0], MON[1], TUE[1], SUN[1]});

        Schedule.Flexible schedule = createTimesPerWeeksSchedule(3, 2);
        FlexibleAnalyserGenerator generator = new FlexibleAnalyserGenerator(schedule);

        int step = getStep(schedule);
        PreciseWorkoutsValidator burnValidator = PreciseWorkoutsValidator.empty();
        validateBurnouts(MON_START, thirdWeekStart, generator, workouts, burnValidator, step);

        PreciseWorkoutsValidator criticalsValidator = PreciseWorkoutsValidator.empty();
        validateCriticals(MON_START, thirdWeekStart, generator, workouts, criticalsValidator);
    }

    /**
     6. Три раза в день
     a. В первый день ничего не горит, но критический уровень
     б. Во второй день три сгорания за первый день
     */
    @Test
    public void case6() {
        List<DateTime> workouts = EMPTY_DAYS;

        Schedule.Flexible schedule = createTimesPerDaysSchedule(3, 1);
        FlexibleAnalyserGenerator generator = new FlexibleAnalyserGenerator(schedule);

        int step = getStep(schedule);
        List<DateTime> shouldBurn = createDateList(new DateTime[] {TUE[0]});
        PreciseWorkoutsValidator burnValidator = new PreciseWorkoutsValidator(shouldBurn);
        validateBurnouts(MON[0], TUE[0], generator, workouts, burnValidator, step);

        List<DateTime> shouldBeCritical = createDateList(new DateTime[] {MON[0], TUE[0]});
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(MON[0], TUE[0], generator, workouts, criticalsValidator);
    }

    /**
     * bugfix
     * План - 5 раз в неделю, запланировано 2017-04-17
     * Тренировки
     * 1) 2017-04-20T06:35:01.664+03 = 1492659301664 (1492659500527)
     * 2) 2017-04-21T07:19:38.751+03 = 1492748378751 (1492748493556)
     * Результат - 21-го ничего сгореть не должно, т.к. в этот день была тренировка
     */
    @Test
    public void case7() {
        List<DateTime> workouts = new ArrayList<>();
        workouts.add(new DateTime(2017, 04, 20, 6, 35));
        workouts.add(new DateTime(2017, 04, 21, 7, 19));

        Schedule.Flexible schedule = createTimesPerWeeksSchedule(5, 1);
        FlexibleAnalyserGenerator generator = new FlexibleAnalyserGenerator(schedule);

        int step = getStep(schedule);

        List<DateTime> shouldBurn = EMPTY_DAYS;
        PreciseWorkoutsValidator burnValidator = new PreciseWorkoutsValidator(shouldBurn);

        DateTime created = new DateTime(2017, 04, 19, 13, 00);
        DateTime checkout = new DateTime(2017, 04, 21, 19, 00);
        validateBurnouts(created, checkout, generator, workouts, burnValidator, step);
    }

    /**
     * bugfix
     * План - 5 раз в две недели, запланировано 2017-04-17
     * Тренировки
     * 1) 2017-04-18T13:54:12.664+03 = 1492512852664 (1492515997946)
     * 2) 2017-04-20T13:26:09.575+03 = 1492683969575 (1492687068912)
     * Результат - 21-го ничего сгореть не должно, т.к. все по плану, 22-го тоже даже без тренировки
     */
    @Test
    public void case8() {
        List<DateTime> workouts = new ArrayList<>();
        workouts.add(new DateTime(2017, 04, 18, 13, 54));
        workouts.add(new DateTime(2017, 04, 20, 13, 26));

        Schedule.Flexible schedule = createTimesPerWeeksSchedule(5, 2);
        FlexibleAnalyserGenerator generator = new FlexibleAnalyserGenerator(schedule);

        int step = getStep(schedule);

        List<DateTime> shouldBurn = EMPTY_DAYS;
        PreciseWorkoutsValidator burnValidator = new PreciseWorkoutsValidator(shouldBurn);

        DateTime created = new DateTime(2017, 04, 17, 13, 00);
        DateTime checkout = new DateTime(2017, 04, 21, 19, 00);
        validateBurnouts(created, checkout, generator, workouts, burnValidator, step);
    }
    //-------------------------------------------------------------------------------------

    private int getStep(Schedule.Flexible schedule) {
        int estimate = schedule.getDaysEstimate();
        if (estimate > 1) {
            return estimate - 1;
        }
        return 1;
    }

    private DateTime upToEndOfMonth() {
        return MON_START.plusDays(31).withDayOfMonth(1).minusDays(1);
    }

    private static List<DateTime> generateDates(DateTime start, int period, int interval) {
        List<DateTime> result = new ArrayList<>();
        DateTime current = new DateTime(start);
        for (int i = 0; i < period; i += interval) {
            result.add(current);
            current = current.plusDays(interval);
        }
        return result;
    }

    private static List<DateTime> generateDates(DateTime start, DateTime finish) {
        List<DateTime> result = new ArrayList<>();
        DateTime current = new DateTime(start);
        while (current.isBefore(finish)) {
            result.add(current);
            current = current.plusDays(1);
        }
        result.add(finish);
        return result;
    }

    private Schedule.Flexible createEveryDaySchedule() {
        Schedule.Flexible schedule = new Schedule.Flexible();
        schedule.period = Schedule.PeriodType.DAY;
        schedule.periodsCount = 1;
        schedule.times = 1;
        return schedule;
    }

    private Schedule.Flexible createTimesPerWeeksSchedule(int times, int weeks) {
        Schedule.Flexible schedule = new Schedule.Flexible();
        schedule.period = Schedule.PeriodType.WEEK;
        schedule.periodsCount = weeks;
        schedule.times = times;
        return schedule;
    }

    private Schedule.Flexible createTimesPerDaysSchedule(int times, int days) {
        Schedule.Flexible schedule = new Schedule.Flexible();
        schedule.period = Schedule.PeriodType.DAY;
        schedule.periodsCount = days;
        schedule.times = times;
        return schedule;
    }

    private Schedule.Flexible createTimesAMonthSchedule(int times) {
        Schedule.Flexible schedule = new Schedule.Flexible();
        schedule.period = Schedule.PeriodType.MONTH;
        schedule.periodsCount = 1;
        schedule.times = times;
        return schedule;
    }

    private static class FlexibleAnalyserGenerator implements AnalyserGenerator
    {
        public FlexibleAnalyserGenerator(Schedule.Flexible flexible) {
            mFlexible = flexible;
        }

        @Override
        public AbstractScheduleAnalyser generate(DateTime date, DateTime lastUpdatedDate, DateTime lastAnalysed) {
            return new FlexibleAnalyser(date, lastUpdatedDate, mFlexible, lastAnalysed);
        }

        private Schedule.Flexible mFlexible;
    }

}
