package com.vaisoft.health.gymgain.sync.generator;

import com.vaisoft.health.gymgain.data.BodyMeasure;
import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.sync.Generator;
import com.vaisoft.health.gymgain.utils.Utils;

import java.util.ArrayList;

public class BodyLogGenerator extends Generator<BodyMeasure.LogEntry> {

    public BodyLogGenerator() {
        super(BodyMeasure.LogEntry.class);
    }

    @Override
    protected BodyMeasure.LogEntry createItem(Knowledge target, int id, boolean cloud) {
        BodyMeasure.LogEntry entry = new BodyMeasure.LogEntry();
        entry.date = Utils.now();
        entry.items = new ArrayList<>();
        entry.items.add(createItem(getId(id), Unit.weight, Unit.kilogram, id, true));
        target.addBodyLog(null, entry);
        return entry;
    }

    @Override
    protected void deleteItem(Knowledge target, BodyMeasure.LogEntry item) {
        target.deleteBodyLog(null, item);
    }

    @Override
    protected int getValidItemsCount(Knowledge target) {
        return target.getBodyMeasurer().getValidLogSize();
    }

    private String getId(int id) {
        return "log #" + id;
    }

    private BodyMeasure.LogItem createItem(String object, Unit general, Unit unit, long count, boolean shouldGrow) {
        BodyMeasure.LogItem item = new BodyMeasure.LogItem();
        item.object = object;
        item.unitGeneral = general;
        item.value = new Value(unit, count, shouldGrow, Value.Status.Applied);
        return item;
    }

    @Override
    protected boolean isRawItem(Knowledge target, BodyMeasure.LogEntry entry) {
        BodyMeasure measurer = target.getBodyMeasurer();
        int size = measurer.getRawLogSize();
        for (int i = 0; i < size; ++i) {
            BodyMeasure.LogEntry targetEntry = measurer.getLogEntryRaw(i);
            if (entry == targetEntry) {
                return true;
            }
        }
        return false;
    }
}
