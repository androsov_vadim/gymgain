package com.vaisoft.health.gymgain.schedule.validator;


import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class PreciseWorkoutsValidator implements WorkoutsValidator {

    public PreciseWorkoutsValidator(List<DateTime> workouts) {
        mData = workouts;
    }

    public static PreciseWorkoutsValidator empty() {
        List<DateTime> data = new ArrayList<>();
        return new PreciseWorkoutsValidator(data);
    }

    @Override
    public void assertWorkouts(List<DateTime> workouts, String hint) {
        assertEquals(hint + " (precise)", mData, workouts);
    }

    @Override
    public WorkoutsValidator clamp(DateTime start, DateTime finish) {
        List<DateTime> data = Utils.clampDates(mData, start, finish);
        return new PreciseWorkoutsValidator(data);
    }

    private List<DateTime> mData;
}
