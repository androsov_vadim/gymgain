package com.vaisoft.health.gymgain.data;

import com.vaisoft.health.gymgain.sync.generator.WorkoutGenerator;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class WorkoutTest {
    @Test
    public void testBurnOut() {
        WorkoutGenerator generator = new WorkoutGenerator(Source.Type.Training);
        Workout workout = generator.addLocal(0);
        assertFalse(workout.isBurnedOut());
        workout.testBurnOut();
        assertTrue(workout.isBurnedOut());
    }
}
