package com.vaisoft.health.gymgain.sync.generator;

import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.data.Source;
import com.vaisoft.health.gymgain.data.Training;
import com.vaisoft.health.gymgain.sync.Generator;

import static junit.framework.TestCase.assertNotNull;

public class RecordGenerator extends Generator<Record> {
    public RecordGenerator(Source.Type base) {
        super(Record.class);
        mBase = base;
    }

    public RecordGenerator(Generator source, Source.Type base) {
        super(source, Record.class);
        mBase = base;
    }

    @Override
    protected Record createItem(Knowledge target, int id, boolean cloud) {
        Record record = null;
        if (mBase == Source.Type.Exercise) {
            Exercise exercise = SportGenerator.create(target, id);
            onItemCreated(this, exercise, id, cloud);
            record = new Record(exercise);
        } else if (mBase == Source.Type.Training) {
            Training training = TrainingGenerator.create(this, target, id, cloud);
            onItemCreated(this, training, id, cloud);
            record = new Record(training);
        }
        assertNotNull(record);
        target.addRecord(record);
        return record;
    }

    @Override
    protected boolean isRawItem(Knowledge target, Record item) {
        Record record = target.getRecordByKey(item.getSource().getKey());
        return record != null;
    }

    @Override
    protected void deleteItem(Knowledge target, Record item) {
        target.deleteRecord(null, item);
    }

    @Override
    protected int getValidItemsCount(Knowledge target) {
        return target.getRecords().size();
    }

    private Source.Type mBase;
}
