package com.vaisoft.health.gymgain.schedule;

import org.joda.time.DateTime;

public interface AnalyserGenerator {
    AbstractScheduleAnalyser generate(DateTime date, DateTime lastUpdatedDate, DateTime lastAnalysed);
}
