package com.vaisoft.health.gymgain.sync.test;

import com.vaisoft.health.gymgain.sync.Generator;
import com.vaisoft.health.gymgain.sync.MergeTest;
import com.vaisoft.health.gymgain.sync.generator.TrainingGenerator;

public class TrainingMergeTest extends MergeTest {
    @Override
    protected Generator createGenerator() {
        return new TrainingGenerator();
    }
}
