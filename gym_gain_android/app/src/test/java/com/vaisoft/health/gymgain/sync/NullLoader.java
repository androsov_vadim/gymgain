package com.vaisoft.health.gymgain.sync;

import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class NullLoader implements JSONLoader {
    @Override
    public JSONObject loadObject(String id, Utils.FileTarget target) {
        return null;
    }

    @Override
    public JSONArray loadArray(String id, Utils.FileTarget target) {
        return null;
    }
}
