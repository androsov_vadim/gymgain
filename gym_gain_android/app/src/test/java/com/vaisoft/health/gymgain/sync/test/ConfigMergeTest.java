package com.vaisoft.health.gymgain.sync.test;

import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.sync.Generator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConfigMergeTest {

    //Local config is newer, separate initialization
    @Test
    public void case1() {
        Knowledge localKnowledge = Generator.createKnowledge();
        Knowledge cloudKnowledge = Generator.createKnowledge();
        Config local = localKnowledge.getConfig();
        Config cloud = cloudKnowledge.getConfig();

        updateSeparately(local);
        local.merge(cloud);

        assertSi(cloud);
        assertUs(local);
    }

    //Local config is newer, bundle initialization
    @Test
    public void case2() {
        Knowledge localKnowledge = Generator.createKnowledge();
        Knowledge cloudKnowledge = Generator.createKnowledge();
        Config local = localKnowledge.getConfig();
        Config cloud = cloudKnowledge.getConfig();

        updateBundle(local);
        local.merge(cloud);

        assertSi(cloud);
        assertUs(local);
    }

    //Cloud config is newer, separate initialization
    @Test
    public void case3() {
        Knowledge localKnowledge = Generator.createKnowledge();
        Knowledge cloudKnowledge = Generator.createKnowledge();
        Config local = localKnowledge.getConfig();
        Config cloud = cloudKnowledge.getConfig();

        updateSeparately(cloud);
        local.merge(cloud);

        assertUs(cloud);
        assertUs(local);
    }

    //Cloud config is newer, bundle initialization
    @Test
    public void case4() {
        Knowledge localKnowledge = Generator.createKnowledge();
        Knowledge cloudKnowledge = Generator.createKnowledge();
        Config local = localKnowledge.getConfig();
        Config cloud = cloudKnowledge.getConfig();

        updateBundle(cloud);
        local.merge(cloud);

        assertUs(cloud);
        assertUs(local);
    }

    private void assertUs(Config target) {
        assertEquals(target.getMinReps(), 100);
        assertEquals(target.getBodyUnits(), Unit.inch);
        assertEquals(target.getDistanceUnits(), Unit.foot);
        assertEquals(target.getWeightUnits(), Unit.pound);
    }

    private void assertSi(Config target) {
        assertEquals(target.getMinReps(), Config.DEFAULT_MIN_REPS);
        assertEquals(target.getBodyUnits(), Unit.centimeter);
        assertEquals(target.getDistanceUnits(), Unit.meter);
        assertEquals(target.getWeightUnits(), Unit.kilogram);
    }

    private void updateSeparately(Config target) {
        target.setMinReps(100);
        target.setBodyUnits(Unit.inch);
        target.setDistanceUnits(Unit.foot);
        target.setWeightUnits(Unit.pound);
    }

    private void updateBundle(Config target) {
        target.setMinReps(100);
        target.useUsUnits(false);
    }
}
