package com.vaisoft.health.gymgain.sync.generator;

import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.data.Lifecycle;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.data.Training;
import com.vaisoft.health.gymgain.sync.Generator;

import java.util.Collections;
import java.util.List;

public class TrainingGenerator extends Generator<Training> {
    public TrainingGenerator() {
        super(Training.class);
    }

    public TrainingGenerator(Generator source) {
        super(source, Training.class);
    }

    @Override
    protected Training createItem(Knowledge target, int id, boolean cloud) {
        Training training = create(this, target, id, cloud);
        return training;
    }

    public static Training create(Generator source, Knowledge target, int id, boolean cloud) {
        String newId = getId(id);
        Training training = target.getTraining(newId);
        boolean trainingCreated = false;
        if (training == null) {
            training = new Training();
            training.setId(getId(id));
            trainingCreated = true;
        }

        Exercise exercise = SportGenerator.create(target, id);
        onItemCreated(source, exercise, id, cloud);
        List<String> exercises = Collections.singletonList(exercise.getId());
        training.addExercises(exercises);

        if (trainingCreated) {
            target.saveTraining(null, training, null);
        }
        return training;
    }

    private static String getId(int id) {
        return "tr#" + id;
    }

    @Override
    protected boolean isRawItem(Knowledge target, Training item) {
        Training training = target.getTrainingRaw(item.getLifecycleUniqueId());
        return training != null;
    }

    @Override
    protected Training getLocalItem(int id) {
        Lifecycle item = super.getLocalItem(id);
        if (item == null) {
            return null;
        }
        return getLocalTarget().getTrainingRaw(item.getLifecycleUniqueId());
    }

    @Override
    protected Training getCloudItem(int id) {
        Lifecycle item = super.getCloudItem(id);
        if (item == null) {
            return null;
        }
        return getCloudTarget().getTrainingRaw(item.getLifecycleUniqueId());
    }

    @Override
    protected void deleteItem(Knowledge target, Training item) {
        Record record = target.getRecordFor(item);
        long stamp = item.testGetUpdatedTime();
        target.deleteTraining(null, item.getId());
        if ((record != null) && record.isDeleted()) {
            onItemUpdated(record, stamp);
        }
    }

    @Override
    protected int getValidItemsCount(Knowledge target) {
        return target.getTrainings().size();
    }
}
