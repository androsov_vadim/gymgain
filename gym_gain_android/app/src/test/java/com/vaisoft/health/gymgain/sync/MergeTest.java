package com.vaisoft.health.gymgain.sync;

import org.junit.Test;

public abstract class MergeTest {

    protected abstract Generator createGenerator();

    /**
     * Добавление нового элемента
     * (0 + 1) + (0 + 2) = (0 + 1) + 2
     */
    @Test
    public void case1() {
        Generator generator = createGenerator();

        generator.addLocal(0);
        generator.addLocal(1);

        generator.addCloud(0);
        generator.addCloud(2);

        generator.mergeAndValidateResult(new int[] {0, 1}, new int[] {2});
    }

    /**
     * Обновление элемента
     * (0 + 1) + (0 + 1') = (0) + (1')
     */
    @Test
    public void case2() {
        Generator generator = createGenerator();

        generator.addLocal(0);
        generator.addLocal(1);

        generator.addCloud(0);
        generator.addCloud(1);
        generator.updateCloud(1);

        generator.mergeAndValidateResult(new int[] {0}, new int[] {1});
    }

    /**
     * Удаление элемента в облаке
     * (0 + 1) + (0 + -1) = (0) + ()
     */
    @Test
    public void case3() {
        Generator generator = createGenerator();

        generator.addLocal(0);
        generator.addLocal(1);

        generator.addCloud(0);
        generator.addCloud(1);
        generator.deleteCloud(1);

        generator.mergeAndValidateResult(new int[] {0}, new int[] {});
    }

    /**
     * Удаление локального элемента
     * 0 приходит из облака, так как локальный элемент удален бесследно
     * (--0 + -1) + (0 + 1) = () + (0)
     */
    @Test
    public void case4() {
        Generator generator = createGenerator();

        generator.addLocal(0);
        generator.addLocal(1);
        generator.deleteLocal(0, false);
        generator.deleteLocal(1, true);

        generator.addCloud(0);
        generator.addCloud(1);

        generator.mergeAndValidateResult(new int[] {}, new int[] {0});
    }

    /**
     * Более новый элемент имеет приоритет
     * (0' + -1'' + 2') + (0'' + 1' + 2) = (2) + (0)
     */
    @Test
    public void case5() {
        Generator generator = createGenerator();

        generator.addLocal(0);
        generator.addLocal(1);
        generator.addLocal(2);
        generator.updateLocal(0);
        generator.updateLocal(1);
        generator.updateLocal(2);
        generator.deleteLocal(1, true);

        generator.addCloud(0);
        generator.addCloud(1);
        generator.addCloud(2);
        generator.updateCloud(0);
        generator.updateCloud(0);
        generator.updateCloud(1);

        generator.mergeAndValidateResult(new int[] {2}, new int[] {0});
    }
}
