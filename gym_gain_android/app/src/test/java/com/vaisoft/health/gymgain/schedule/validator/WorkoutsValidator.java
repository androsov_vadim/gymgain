package com.vaisoft.health.gymgain.schedule.validator;


import org.joda.time.DateTime;

import java.util.List;

public interface WorkoutsValidator {
    void assertWorkouts(List<DateTime> workouts, String hint);
    WorkoutsValidator clamp(DateTime start, DateTime finish);
}
