package com.vaisoft.health.gymgain.compare;

import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.data.Workout;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Sets {

    @Test
    public void moreRepsSameWeightBetter() {
        Workout.Set set1 = createRepsWeight(10, 50);
        Workout.Set set2 = createRepsWeight(11, 50);
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void moreWeightSameRepsBetter() {
        Workout.Set set1 = createRepsWeight(10, 50);
        Workout.Set set2 = createRepsWeight(10, 51);
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void bitLessWeightSameRepsSoSoNormal() {
        Workout.Set set1 = createRepsWeight(10, 50);
        Workout.Set set2 = createRepsWeight(10, 48);
        assertCompare(set1, set2, Progress.Estimate.SOSO, Config.Estimation.NORMAL);
    }

    @Test
    public void moreWeightLessRepsBetterIfFitsMinReps() {
        Workout.Set set1 = createRepsWeight(10, 50);
        Workout.Set set2 = createRepsWeight(8, 51);
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void moreWeightLessRepsWorseIfNotFitsMinReps() {
        Workout.Set set1 = createRepsWeight(10, 50);
        Workout.Set set2 = createRepsWeight(7, 51);
        assertCompare(set1, set2, Progress.Estimate.BAD, Config.Estimation.STRICT);
    }

    @Test
    public void moreWeightLessRepsOkIfFitPlannedRepsNotMinReps() {
        Workout.Set set1 = createRepsWeight(10, 50);
        Workout.Set set2 = createRepsWeight(7, 51);
        set2.plannedReps = 7;
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void oneParameterHugeProgressOvercomesTwoLess() {
        Workout.Set set1 = createRepsWeightTime(10, 50, 1000);
        Workout.Set set2 = createRepsWeightTime(9, 49, 200);
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void repsNotFitOvercomesAnyOtherGrow() {
        Workout.Set set1 = createRepsWeightTime(10, 50, 1000);
        Workout.Set set2 = createRepsWeightTime(7, 100, 200);
        assertCompare(set1, set2, Progress.Estimate.BAD, Config.Estimation.STRICT);
    }

    @Test
    public void slowerSameDistanceIsBad() {
        Workout.Set set1 = createTimeDistance(1000, 500);
        Workout.Set set2 = createTimeDistance(1050, 500);
        assertCompare(set1, set2, Progress.Estimate.BAD, Config.Estimation.STRICT);
    }

    @Test
    public void slowerABitSameDistanceIsSoSONormal() {
        Workout.Set set1 = createTimeDistance(1000, 500);
        Workout.Set set2 = createTimeDistance(1050, 500);
        assertCompare(set1, set2, Progress.Estimate.SOSO, Config.Estimation.NORMAL);
    }

    @Test
    public void fasterSameDistanceIsGood() {
        Workout.Set set1 = createTimeDistance(1050, 500);
        Workout.Set set2 = createTimeDistance(1000, 500);
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void littleSlowerMuchMoreDistanceIsGood() {
        Workout.Set set1 = createTimeDistance(1000, 500);
        Workout.Set set2 = createTimeDistance(1050, 1000);
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void oneSecondMatters() {
        Workout.Set set1 = createTimeDistance(30 + 14 * 60, 650);
        Workout.Set set2 = createTimeDistance(29 + 14 * 60, 650);
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void bitWorseSoSoNormal() {
        Workout.Set set1 = createTimeDistance(30 + 14 * 60, 650);
        Workout.Set set2 = createTimeDistance(15 + 14 * 60, 650);
        assertCompare(set1, set2, Progress.Estimate.SOSO, Config.Estimation.NORMAL);
    }

    @Test
    public void lessWeightButRepsUpToMinimal() {
        Workout.Set set1 = createRepsWeight(7, 50);
        Workout.Set set2 = createRepsWeight(8, 10);
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void biggerDistanceOvercomesTimeIfNoMoreParameters() {
        Workout.Set set1 = createTimeDistance(1000, 500);
        Workout.Set set2 = createTimeDistance(2000, 501);
        assertCompare(set1, set2, Progress.Estimate.GOOD, Config.Estimation.STRICT);
    }

    @Test
    public void sameRepsZeroWeightIsSoso() {
        Workout.Set set1 = createRepsWeight(8, 0);
        Workout.Set set2 = createRepsWeight(8, 0);
        assertCompare(set1, set2, Progress.Estimate.SOSO, Config.Estimation.STRICT);
    }

    //-----------------------------------------------------------------------------------

    private void assertCompare(Workout.Set set1, Workout.Set set2, Progress.Estimate expectedResult, Config.Estimation estimation) {
        Config config = Config.test(8, estimation);
        Progress progressSets = set2.compareTo(set1, config);
        assertEquals(expectedResult, progressSets.getEstimate());
        Workout.Entry entry1 = createEntry(set1);
        Workout.Entry entry2 = createEntry(set2);
        Progress progressEntries = entry2.compareTo(entry1, 0, config);
        assertEquals(expectedResult, progressEntries.getEstimate());
    }

    private Workout.Entry createEntry(Workout.Set set) {
        Workout.Entry result = new Workout.Entry();
        Workout.ExerciseLogItem item = new Workout.ExerciseLogItem();
        item.exercise = "test";
        item.sets.add(set);
        result.log.add(item);
        return result;
    }

    private Workout.Set createRepsWeight(long reps, long weight) {
        Workout.Set set = new Workout.Set();
        Value weightValue = new Value(Unit.kilogram, weight, true, Value.Status.Applied);
        Value repsValue = new Value(Unit.reps, reps, true, Value.Status.Applied);
        set.result.put(Unit.weight, weightValue);
        set.result.put(Unit.reps, repsValue);
        return set;
    }

    private Workout.Set createRepsWeightTime(long reps, long weight, long time) {
        Workout.Set set = new Workout.Set();
        Value weightValue = new Value(Unit.kilogram, weight, true, Value.Status.Applied);
        Value repsValue = new Value(Unit.reps, reps, true, Value.Status.Applied);
        Value timeValue = new Value(Unit.time, time, false, Value.Status.Applied);
        set.result.put(Unit.weight, weightValue);
        set.result.put(Unit.reps, repsValue);
        set.result.put(Unit.time, timeValue);
        return set;
    }

    private Workout.Set createTimeDistance(long time, long distance) {
        Workout.Set set = new Workout.Set();
        Value timeValue = new Value(Unit.time, time, false, Value.Status.Applied);
        Value distanceValue = new Value(Unit.meter, distance, true, Value.Status.Applied);
        set.result.put(Unit.time, timeValue);
        set.result.put(Unit.distance, distanceValue);
        return set;
    }
}
