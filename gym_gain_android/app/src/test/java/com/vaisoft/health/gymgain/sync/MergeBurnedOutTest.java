package com.vaisoft.health.gymgain.sync;

import com.vaisoft.health.gymgain.data.Source;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.sync.generator.WorkoutGenerator;

import org.junit.Test;

public class MergeBurnedOutTest {

    /**
     * При мерже обычное занятие затирает сгоревшее: сгоревший - локальный
     */
    @Test
    public void case1() {
        WorkoutGenerator generator = new WorkoutGenerator(Source.Type.Training);
        Workout workoutLocal = generator.addLocal(0);
        workoutLocal.testBurnOut();
        generator.addCloud(0);
        generator.mergeAndValidateResult(new int[]{}, new int[]{0});
    }

    /**
     * При мерже обычное занятие затирает сгоревшее: сгоревший - облачный
     */
    @Test
    public void case2() {
        WorkoutGenerator generator = new WorkoutGenerator(Source.Type.Training);
        generator.addLocal(0);
        Workout workoutCloud = generator.addCloud(0);
        workoutCloud.testBurnOut();
        generator.mergeAndValidateResult(new int[]{0}, new int[]{});
    }

    /**
     * При мерже сгоревших остается только локальный вариант
     */
    @Test
    public void case3() {
        WorkoutGenerator generator = new WorkoutGenerator(Source.Type.Training);
        Workout workoutLocal = generator.addLocal(0);
        workoutLocal.testBurnOut();
        Workout workoutCloud = generator.addCloud(0);
        workoutCloud.testBurnOut();
        generator.mergeAndValidateResult(new int[]{0}, new int[]{});
    }
}
