package com.vaisoft.health.gymgain.sync;

import androidx.core.util.Pair;

import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.data.Lifecycle;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public abstract class Generator<T extends Lifecycle> {

    protected abstract T createItem(Knowledge target, int id, boolean cloud);
    protected abstract boolean isRawItem(Knowledge target, T item);
    protected abstract void deleteItem(Knowledge target, T item);
    protected abstract int getValidItemsCount(Knowledge target);

    public Generator(Class<T> theClass) {
        mLocalItems = new ArrayList<>();
        mCloudItems = new ArrayList<>();
        mLocal = createKnowledge();
        mCloud = createKnowledge();
        mClass = theClass;
    }

    public Generator(Generator source, Class<T> theClass) {
        mLocalItems = source.mLocalItems;
        mCloudItems = source.mCloudItems;
        mLocal = source.mLocal;
        mCloud = source.mCloud;
        mClass = theClass;
    }

    private T addItem(Knowledge target, int id, boolean cloud) {
        T item = createItem(target, id, cloud);
        onItemCreated(this, item, id, cloud);
        return item;
    }

    public static void onItemCreated(Generator source, Lifecycle item, int id, boolean cloud) {
        item.testSetCreatedTime(1 + id);
        if (cloud) {
            item.testSetCloud();
            source.registerCloudItem(id, item);
        } else {
            source.registerLocalItem(id, item);
        }
    }

    public static void onItemUpdated(Lifecycle item, long stamp) {
       item.testSetUpdatedTime(stamp + sUpdateStep);
    }

    public T addLocal(int id) {
        T item = addItem(mLocal, id, false);
        registerLocalItem(id, item);
        return item;
    }

    public void registerLocalItem(int id, Lifecycle item) {
        if (isRegistered(mLocalItems, item)) {
            return;
        }
        mLocalItems.add(new Pair<>(id, item));
    }

    public T addCloud(int id) {
        T item = addItem(mCloud, id, true);
        registerCloudItem(id, item);
        return item;
    }

    public void registerCloudItem(int id, Lifecycle item) {
        if (isRegistered(mCloudItems, item)) {
            return;
        }
        mCloudItems.add(new Pair<>(id, item));
    }

    private boolean isRegistered(List<Pair<Integer, Lifecycle>> items, Lifecycle item) {
        for (Pair<Integer, Lifecycle> record: items) {
            if (record.second == item) {
                return true;
            }
        }
        return false;
    }

    public void mergeAndValidateResult(int[] localIds, int[] cloudIds) {
        mLocal.merge(mCloud);
        validate(localIds, cloudIds);
    }

    protected T getLocalItem(Class<T> theClass, int id) {
        return getItem(theClass, mLocalItems, id);
    }

    protected T getLocalItem(int id) {
        return getLocalItem(mClass, id);
    }

    protected T getCloudItem(Class<T> theClass, int id) {
        return getItem(theClass, mCloudItems, id);
    }

    protected T getCloudItem(int id) {
        return getCloudItem(mClass, id);
    }

    private T getItem(Class<T> theClass, List<Pair<Integer, Lifecycle>> items, int id) {
        for (Pair<Integer, Lifecycle> item: items) {
            if (item.first.equals(id) && (item.second.getClass().equals(theClass))) {
                return (T)item.second;
            }
        }
        return null;
    }

    public void updateCloud(int id) {
        T item = getCloudItem(id);
        long time = item.testGetUpdatedTime();
        item.testSetUpdatedTime(time + sUpdateStep);
    }

    public void updateLocal(int id) {
        T item = getLocalItem(id);
        long time = item.testGetUpdatedTime();
        item.testSetUpdatedTime(time + sUpdateStep);
    }

    public void deleteCloud(int id) {
        T item = getCloudItem(id);
        long time = item.testGetUpdatedTime();
        deleteItem(mCloud, item);
        item.testSetUpdatedTime(time + sUpdateStep);
    }

    public void deleteLocal(int id, boolean cloudAware) {
        T item = getLocalItem(id);
        if (cloudAware) {
            item.testSetCloud();
        }
        long time = item.testGetUpdatedTime();
        deleteItem(mLocal, item);
        if (cloudAware) {
            assertTrue(isRawItem(mLocal, item));
            item.testSetUpdatedTime(time + sUpdateStep);
        } else {
            assertFalse(isRawItem(mLocal, item));
            mLocalItems.remove(id);
        }
    }

    public static Knowledge createKnowledge() {
        Knowledge knowledge = new Knowledge();
        JSONLoader loader = new NullLoader();
        knowledge.init(loader);
        return knowledge;
    }

    // это проверка только локального хранилища - сколько пришло из облака, а сколько было
    public void validate(int[] localIds, int[] cloudIds) {
        assertEquals(localIds.length + cloudIds.length, getValidItemsCount(mLocal));
        for (int i: localIds) {
            T item = getLocalItem(i);
            assertFalse(item.isDeleted());
            assertTrue(isRawItem(mLocal, item));
        }
        for (int i: cloudIds) {
            T item = getCloudItem(i);
            assertFalse(item.isDeleted());
            assertTrue(isRawItem(mLocal, item));
        }
    }

    public void validateLocalSourceOnly(int[] localIds) {
        validate(localIds, new int[] {});
    }

    public void validateNoItems() {
        validate(new int[] {}, new int[] {});
    }

    protected Knowledge getCloudTarget() {
        return mCloud;
    }

    protected Knowledge getLocalTarget() {
        return mLocal;
    }

    private List<Pair<Integer, Lifecycle>> mLocalItems;
    private List<Pair<Integer, Lifecycle>> mCloudItems;
    private Knowledge mLocal;
    private Knowledge mCloud;
    private Class<T> mClass;

    private static final int sUpdateStep = 1000;
}
