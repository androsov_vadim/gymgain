package com.vaisoft.health.gymgain.sync.test;

import com.vaisoft.health.gymgain.sync.MergeTest;
import com.vaisoft.health.gymgain.sync.generator.BodyLogGenerator;
import com.vaisoft.health.gymgain.sync.Generator;

public class BodyLogMergeTest extends MergeTest {

    @Override
    protected Generator createGenerator() {
        return new BodyLogGenerator();
    }
}
