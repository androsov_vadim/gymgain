package com.vaisoft.health.gymgain.sync.generator;

import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.data.Lifecycle;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.sync.Generator;

public class SportGenerator extends Generator<Exercise> {

    public SportGenerator() {
        super(Exercise.class);
    }

    @Override
    protected Exercise createItem(Knowledge target, int id, boolean cloud) {
        Exercise exercise = create(target, id);
        return exercise;
    }

    public static Exercise create(Knowledge target, int id) {
        String newId = getId(id);
        Exercise exercise = target.getExercise(newId);
        if (exercise != null) {
            return exercise;
        }
        exercise = new Exercise();
        exercise.setId(newId);
        target.saveExercise(null, exercise, null);
        return  exercise;
    }

    private static String getId(int id) {
        return "ex#" + id;
    }

    @Override
    protected Exercise getLocalItem(int id) {
        Lifecycle item = super.getLocalItem(id);
        if (item == null) {
            return null;
        }
        return getLocalTarget().getExerciseRaw(item.getLifecycleUniqueId());
    }

    @Override
    protected Exercise getCloudItem(int id) {
        Lifecycle item = super.getCloudItem(id);
        if (item == null) {
            return null;
        }
        return getCloudTarget().getExerciseRaw(item.getLifecycleUniqueId());
    }

    @Override
    protected boolean isRawItem(Knowledge target, Exercise item) {
        Exercise exercise = target.getExerciseRaw(item.getLifecycleUniqueId());
        return exercise != null;
    }

    @Override
    protected void deleteItem(Knowledge target, Exercise item) {
        Record record = target.getRecordFor(item);
        long stamp = item.testGetUpdatedTime();
        target.deleteExercise(null, item.getId());
        if ((record != null) && record.isDeleted()) {
            onItemUpdated(record, stamp);
        }
    }

    @Override
    protected int getValidItemsCount(Knowledge target) {
        return target.getExercises().size();
    }
}
