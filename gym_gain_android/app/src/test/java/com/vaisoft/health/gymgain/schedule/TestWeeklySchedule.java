package com.vaisoft.health.gymgain.schedule;

import com.vaisoft.health.gymgain.schedule.validator.PreciseWorkoutsValidator;

import org.joda.time.DateTime;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestWeeklySchedule extends TestSchedule {

    /**
     ПСП. Заводим запись во вторник. Не занимаемся вообще
     a. Выгорает в пятницу (критический уровень - со среды каждый день)
     b. Выходим на следующую неделю с отрицательным бонусом - сгорает понедельник, среда и пятница
     */
    @Test
    public void case1() {
        WeeklyAnalyserGenerator psp = createWeekDaysGenerator(new int[] {1, 3, 5});
        DateTime created = TUE[0];
        DateTime upTo = SUN[1];

        List<DateTime> shouldBurn = new ArrayList<>();
        shouldBurn.add(FRI[0]);
        shouldBurn.addAll(wholeWeekToDays(psp.getSchedule(), 1));
        PreciseWorkoutsValidator burnValidator = new PreciseWorkoutsValidator(shouldBurn);
        validateBurnouts(created, upTo, psp, EMPTY_DAYS, burnValidator, 5);

        List<DateTime> shouldBeCritical = new ArrayList<>();
        shouldBeCritical.addAll(createDateList(new DateTime[] {WEN[0], THU[0], FRI[0], SAT[0], SUN[0]}));
        shouldBeCritical.addAll(wholeWeekToDays(1));
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(created, upTo, psp, EMPTY_DAYS, criticalsValidator);
    }

    /**
     ПСП. Заводим во вторник. Одно занятие в пятницу
     a. На первой неделе не сгорает ничего (Критический уровень среда, четверг)
     b. На второй сгорает среда и пятница (Критический уровень - с понедельника до упора)
     */
    @Test
    public void case2() {
        WeeklyAnalyserGenerator psp = createWeekDaysGenerator(new int[] {1, 3, 5});
        List<DateTime> workouts = createDateList(new DateTime[] {FRI[0]});
        DateTime created = TUE[0];
        DateTime upTo = SUN[1];

        List<DateTime> shouldBurn = new ArrayList<>();
        shouldBurn.addAll(createDateList(new DateTime[] {WEN[1], FRI[1]}));
        PreciseWorkoutsValidator burnValidator = new PreciseWorkoutsValidator(shouldBurn);
        validateBurnouts(created, upTo, psp, workouts, burnValidator, 5);

        List<DateTime> shouldBeCritical = new ArrayList<>();
        shouldBeCritical.addAll(createDateList(new DateTime[] {WEN[0], THU[0]}));
        shouldBeCritical.addAll(wholeWeekToDays(1));
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(created, upTo, psp, workouts, criticalsValidator);
    }

    /**
     ПСП Заведено в понедельник
     a. Занятия всю первую неделю (нет критических дней)
     b. На второй пропущена пятница - все ОК - выходим с минусом на третью неделю (критические дни: с пятницы до конца недели)
     */
    @Test
    public void case3() {
        WeeklyAnalyserGenerator psp = createWeekDaysGenerator(new int[] {1, 3, 5});
        DateTime created = MON[0];
        DateTime upTo = SUN[1];

        List<DateTime> workouts = createDateList(new DateTime[] {
                MON[0], WEN[0], FRI[0],
                MON[1], WEN[1]});

        List<DateTime> shouldBurn = new ArrayList<>();
        PreciseWorkoutsValidator burnValidator = new PreciseWorkoutsValidator(shouldBurn);
        validateBurnouts(created, upTo, psp, workouts, burnValidator, 6);

        List<DateTime> shouldBeCritical = new ArrayList<>();
        shouldBeCritical.addAll(createDateList(new DateTime[] {FRI[1], SAT[1], SUN[1]}));
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(created, upTo, psp, workouts, criticalsValidator);
    }

    /**
     ПСП Понедельник
     a. Занимаемся в понедельник-пятницу, минус на следующую неделю не переходит (Критические дни - среда, четверг)
     b. Вторая неделя - сгорают среда и пятница (Критический уровень - с понедельника)
     */
    @Test
    public void case4() {
        WeeklyAnalyserGenerator psp = createWeekDaysGenerator(new int[] {1, 3, 5});
        List<DateTime> workouts = createDateList(new DateTime[] {
                MON[0], FRI[0]});
        DateTime created = MON[0];
        DateTime upTo = SUN[1];

        List<DateTime> shouldBurn = new ArrayList<>();
        shouldBurn.add(WEN[1]);
        shouldBurn.add(FRI[1]);
        PreciseWorkoutsValidator burnValidator = new PreciseWorkoutsValidator(shouldBurn);
        validateBurnouts(created, upTo, psp, workouts, burnValidator, 6);

        List<DateTime> shouldBeCritical = new ArrayList<>();
        shouldBeCritical.addAll(createDateList(new DateTime[] {WEN[0], THU[0]}));
        shouldBeCritical.addAll(wholeWeekToDays(1));
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(created, upTo, psp, workouts, criticalsValidator);
    }

    /**
     ПСП Понедельник
     a. На первой неделе 4 занятия (пн, вт, ср, пт)
     b. На второй неделе 1 занятие (пн), но ничего но сгорает благодаря запасу с прошлой
     c. Критический уровень со среды второй недели
     */
    @Test
    public void case5() {
        WeeklyAnalyserGenerator psp = createWeekDaysGenerator(new int[] {1, 3, 5});
        List<DateTime> workouts = createDateList(new DateTime[] {
                MON[0], TUE[0], WEN[0], FRI[0], MON[1]});
        DateTime created = MON[0];
        DateTime upTo = SUN[1];

        PreciseWorkoutsValidator burnValidator = PreciseWorkoutsValidator.empty();
        validateBurnouts(created, upTo, psp, workouts, burnValidator, 6);

        List<DateTime> shouldBeCritical = new ArrayList<>();
        shouldBeCritical.addAll(createDateList(new DateTime[] {WEN[1], THU[1],
                FRI[1], SAT[1], SUN[1]}));
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(created, upTo, psp, workouts, criticalsValidator);
    }

    /**
     ПСП
     a) Первая неделя - понедельник, вторник, пятница - ни сгораний ни критический дней
     б) Вторая неделя - вторник, среда, четверг - ничего не сгорает, критический день - понедельник
     */
    @Test
    public void case6() {
        WeeklyAnalyserGenerator psp = createWeekDaysGenerator(new int[] {1, 3, 5});
        List<DateTime> workouts = createDateList(new DateTime[] {
                MON[0], TUE[0], FRI[0], TUE[1], WEN[1], THU[1]});
        DateTime created = MON[0];
        DateTime upTo = SUN[1];

        PreciseWorkoutsValidator burnValidator = PreciseWorkoutsValidator.empty();
        validateBurnouts(created, upTo, psp, workouts, burnValidator, 6);

        List<DateTime> shouldBeCritical = new ArrayList<>();
        shouldBeCritical.addAll(createDateList(new DateTime[] {MON[1]}));
        PreciseWorkoutsValidator criticalsValidator = new PreciseWorkoutsValidator(shouldBeCritical);
        validateCriticals(created, upTo, psp, workouts, criticalsValidator);
    }

    private static WeeklyAnalyserGenerator createWeekDaysGenerator(int[] data) {
        Set<Integer> schedule = new HashSet<>();
        for (int i : data) {
            schedule.add(i);
        }
        return new WeeklyAnalyserGenerator(schedule);
    }

    private static List<DateTime> wholeWeekToDays(Set<Integer> data, int weekIndex) {
        List<DateTime> result = new ArrayList<>();
        for (int i: data) {
            result.add(WEEK[i - 1][weekIndex]);
        }
        return result;
    }

    private static List<DateTime> wholeWeekToDays(int weekIndex) {
        List<DateTime> result = new ArrayList<>();
        for (DateTime[] weekDay: WEEK) {
            result.add(weekDay[weekIndex]);
        }
        return result;
    }

    private static class WeeklyAnalyserGenerator implements AnalyserGenerator
    {
        public WeeklyAnalyserGenerator(Set<Integer> weekDays) {
            mSchedule = weekDays;
        }

        @Override
        public AbstractScheduleAnalyser generate(DateTime date, DateTime lastUpdatedDate, DateTime lastAnalysed) {
            return new WeeklyAnalyser(date, lastUpdatedDate, mSchedule, lastAnalysed);
        }

        public Set<Integer> getSchedule() {
            return mSchedule;
        }

        private Set<Integer> mSchedule;
    }

    private static DateTime[][] WEEK = {MON, TUE, WEN, THU, FRI, SAT, SUN};
}
