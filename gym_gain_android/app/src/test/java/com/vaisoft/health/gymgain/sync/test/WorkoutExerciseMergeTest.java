package com.vaisoft.health.gymgain.sync.test;

import com.vaisoft.health.gymgain.data.Source;
import com.vaisoft.health.gymgain.sync.Generator;
import com.vaisoft.health.gymgain.sync.MergeTest;
import com.vaisoft.health.gymgain.sync.generator.WorkoutGenerator;

public class WorkoutExerciseMergeTest extends MergeTest {
    @Override
    protected Generator createGenerator() {
        return new WorkoutGenerator(Source.Type.Exercise);
    }
}
