package com.vaisoft.health.gymgain.sync.generator;

import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.data.Source;
import com.vaisoft.health.gymgain.data.Training;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.sync.Generator;

import java.util.List;
import java.util.Map;

import static junit.framework.TestCase.assertNotNull;

public class WorkoutGenerator extends Generator<Workout> {
    public WorkoutGenerator(Source.Type base) {
        super(Workout.class);
        mBase = base;
    }

    public WorkoutGenerator(Generator source, Source.Type base) {
        super(source, Workout.class);
        mBase = base;
    }

    @Override
    protected Workout createItem(Knowledge target, int id, boolean cloud) {
        Workout workout = null;
        if (mBase == Source.Type.Exercise) {
            Exercise exercise = SportGenerator.create(target, id);
            onItemCreated(this, exercise, id, cloud);
            workout = new Workout(exercise);
        } else if (mBase == Source.Type.Training) {
            Training training = TrainingGenerator.create(this, target, id, cloud);
            onItemCreated(this, training, id, cloud);
            workout = new Workout(training);
        }
        assertNotNull(workout);
        target.start(null, workout);
        int index = target.getActiveWorkoutId(workout);
        target.finish(null, index, workout);
        return workout;
    }

    @Override
    protected boolean isRawItem(Knowledge target, Workout item) {
        List<Workout> workouts = target.getFinishedWorkoutsByKey(item.getSource().getKey());
        return workouts.indexOf(item) != -1;
    }

    @Override
    protected void deleteItem(Knowledge target, Workout item) {
        target.deleteFinishedWorkout(null, item);
    }

    @Override
    protected int getValidItemsCount(Knowledge target) {
        int count = 0;
        for (Map.Entry<String, List<Workout>> entry : target.getFinishedWorkouts().entrySet()) {
            for (Workout workout: entry.getValue()) {
                if (!workout.isDeleted()) {
                    ++count;
                }
            }
        }
        return count;
    }

    private Source.Type mBase;
}
