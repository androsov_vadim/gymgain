package com.vaisoft.health.gymgain.sync.test;

import com.vaisoft.health.gymgain.data.Lifecycle;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.data.Source;
import com.vaisoft.health.gymgain.data.Training;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.sync.Generator;
import com.vaisoft.health.gymgain.sync.generator.RecordGenerator;
import com.vaisoft.health.gymgain.sync.generator.SportGenerator;
import com.vaisoft.health.gymgain.sync.generator.TrainingGenerator;
import com.vaisoft.health.gymgain.sync.generator.WorkoutGenerator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ComplexMergeTest {

    /**
     * Удаленное локальное упражнение должно восстановиться, если выживает привязанная к нему Record
     */
    @Test
    public void case1() {
        Generator source = new SportGenerator();
        Generator record = new RecordGenerator(source, Source.Type.Exercise);
        testContainerRecoverTarget(source, record, true);
    }

    /**
     * Удаленная локальная тренировка должна восстановиться, если выживает привязанная к ней Record
     */
    @Test
    public void case2() {
        Generator source = new TrainingGenerator();
        Generator record = new RecordGenerator(source, Source.Type.Training);
        testContainerRecoverTarget(source, record, true);
    }

    /**
     * Более новый локальный Record упражнения не дает удалить упражнение стимулом из облака
     */
    @Test
    public void case3() {
        Generator source = new SportGenerator();
        Generator record = new RecordGenerator(source, Source.Type.Exercise);
        testContainerBlockTargetRemoval(source, record, true);
    }

    /**
     * Более новый локальный Record тренировки не дает удалить упражнение стимулом из облака
     */
    @Test
    public void case4() {
        Generator source = new TrainingGenerator();
        Generator record = new RecordGenerator(source, Source.Type.Training);
        testContainerBlockTargetRemoval(source, record, true);
    }

    /**
     * Удаленное локальное упражнение восстанавливается, если его использует Workout
     */
    @Test
    public void case5() {
        Generator source = new SportGenerator();
        Generator workout = new WorkoutGenerator(source, Source.Type.Exercise);
        testContainerRecoverTarget(source, workout, false);
    }

    /**
     * Более новый Workout не дает удалить упражнение стимулом из облака
     */
    @Test
    public void case6() {
        Generator source = new SportGenerator();
        Generator workout = new WorkoutGenerator(source, Source.Type.Exercise);
        testContainerBlockTargetRemoval(source, workout, false);
    }

    /**
     * Удаленное локальное упражнение восстанавливается, если его использует тренировка
     */
    @Test
    public void case7() {
        Generator source = new SportGenerator();
        Generator training = new TrainingGenerator(source);
        testContainerRecoverTarget(source, training, false);
    }

    /**
     * Более новая тренировка не дает удалить упражнение стимулом из облака
     */
    @Test
    public void case8() {
        Generator source = new SportGenerator();
        Generator training = new TrainingGenerator(source);
        testContainerBlockTargetRemoval(source, training, false);
    }

    /**
     * Удаленное локальное упражнение должно восстановиться, если выживает привязанная к нему ЧЕРЕЗ ТРЕНИРОВКУ Record
     */
    @Test
    public void case_implicit1() {
        Generator exercises = new SportGenerator();
        Generator trainings = new TrainingGenerator(exercises);
        Generator records = new RecordGenerator(exercises, Source.Type.Training);

        records.addCloud(0);
        records.updateCloud(0);
        records.updateCloud(0);//update x2 to be newer than delete timestamp

        records.addLocal(0);
        exercises.validateLocalSourceOnly(new int[] {0});
        records.validateLocalSourceOnly(new int[] {0});

        exercises.updateLocal(0);
        exercises.deleteLocal(0, true);
        trainings.deleteLocal(0, true); //cascade delete record using training
        exercises.validateNoItems();
        records.validateNoItems();

        records.mergeAndValidateResult(new int[] {}, new int[] {0});
        exercises.validateLocalSourceOnly(new int[] {0});
    }

    /**
     * Более новый локальный Record ТРЕНИРОВКИ не дает удалить СВЯЗАННОЕ УПРАЖНЕНИЕ стимулом из облака
     */
    @Test
    public void case_implicit2() {
        Generator exercises = new SportGenerator();
        Generator trainings = new TrainingGenerator(exercises);
        Generator records = new RecordGenerator(exercises, Source.Type.Training);

        records.addCloud(0);

        records.addLocal(0);
        records.updateLocal(0);
        records.updateLocal(0);//update x2 to be newer than delete timestamp

        exercises.validateLocalSourceOnly(new int[] {0});
        records.validateLocalSourceOnly(new int[] {0});

        exercises.updateCloud(0);
        exercises.deleteCloud(0);
        trainings.deleteCloud(0);

        records.mergeAndValidateResult(new int[] {0}, new int[] {});
        exercises.validateLocalSourceOnly(new int[] {0});

    }

    /**
     * Удаленное упражнение было переименовано, но локальная запись новее - она должна обновиться
     */
    @Test
    public void case_rename1() {
        Generator exercises = new SportGenerator();
        testRecordTargetCloudRename(exercises, Source.Type.Exercise);
    }

    /**
     * Удаленная тренировка была переименовано, но локальная запись новее - она должна обновиться
     */
    @Test
    public void case_rename2() {
        Generator trainings = new TrainingGenerator();
        testRecordTargetCloudRename(trainings, Source.Type.Training);
    }

    /**
     * Локальное упражнение было переименовано, но удаленная запись новее - она должна обновиться
     */
    @Test
    public void case_rename3() {
        Generator exercises = new SportGenerator();
        testRecordTargetLocalRename(exercises, Source.Type.Exercise);
    }

    /**
     * Локальная тренировка была переименовано, но удаленная запись новее - она должна обновиться
     */
    @Test
    public void case_rename4() {
        Generator trainings = new TrainingGenerator();
        testRecordTargetLocalRename(trainings, Source.Type.Training);
    }

    /**
     * Удаленное упражнение было переименовано, но локальный воркаут новее - он должен обновиться
     */
    @Test
    public void case_rename5() {
        Generator exercises = new SportGenerator();
        testWorkoutContentCloudRename(exercises, Source.Type.Exercise);
    }

    /**
     * Удаленная тренировка была переименовано, но локальный воркаут новее - он должен обновиться
     */
    @Test
    public void case_rename6() {
        Generator trainings = new TrainingGenerator();
        testWorkoutContentCloudRename(trainings, Source.Type.Training);
    }

    /**
     * Локальное упражнение было переименовано, но облачный воркаут новее - он должен обновиться
     */
    @Test
    public void case_rename7() {
        Generator exercises = new SportGenerator();
        testWorkoutContentLocalRename(exercises, Source.Type.Exercise);
    }

    /**
     * Локальная тренировка была переименовано, но удаленный воркаут новее - он должен обновиться
     */
    @Test
    public void case_rename8() {
        Generator trainings = new TrainingGenerator();
        testWorkoutContentLocalRename(trainings, Source.Type.Training);
    }

    /**
     * Удаленное упражнение было переименовано, но локальная тренировка новее - она должна обновиться
     */
    @Test
    public void case_rename9() {
        Generator exercises = new SportGenerator();
        Generator trainings = new TrainingGenerator(exercises);

        exercises.addLocal(0);
        Lifecycle exerciseCloud = exercises.addCloud(0);

        Training trainingLocal = (Training) trainings.addLocal(0);
        trainings.addCloud(0);

        exerciseCloud.setId("newName");
        exercises.updateCloud(0);
        trainings.updateLocal(0);

        trainings.mergeAndValidateResult(new int[] {0}, new int[] {}); //должна остаться только локлальная
        exercises.validate(new int[] {}, new int[] {0}); //а тут наоборот - только облачное
        assertTrue(trainingLocal.usesExercise(exerciseCloud.getId()));
    }

    /**
     * Локальное упражнение было переименовано, но удаленная тренировка новее - она должна обновиться
     */
    @Test
    public void case_rename10() {
        Generator exercises = new SportGenerator();
        Generator trainings = new TrainingGenerator(exercises);

        Lifecycle exerciseLocal = exercises.addLocal(0);
        exercises.addCloud(0);

        trainings.addLocal(0);
        Training trainingCloud = (Training) trainings.addCloud(0);

        exerciseLocal.setId("newName");
        exercises.updateLocal(0);
        trainings.updateCloud(0);

        trainings.mergeAndValidateResult(new int[] {}, new int[] {0}); //должна остаться только облачная
        exercises.validate(new int[] {0}, new int[] {}); //а тут наоборот - только локальное
        assertTrue(trainingCloud.usesExercise(exerciseLocal.getId()));
    }

    //----------------------------------------------------------------------------------------

    private void testRecordTargetCloudRename(Generator target, Source.Type type) {
        Generator records = new RecordGenerator(target, type);

        target.addLocal(0);
        Lifecycle targetCloud = target.addCloud(0);

        Record recordLocal = (Record) records.addLocal(0);
        records.addCloud(0);

        target.updateCloud(0);
        records.updateLocal(0);
        targetCloud.setId("newName"); //rename after update as update relies on id which was changed

        records.mergeAndValidateResult(new int[] {0}, new int[] {}); //должна остаться только локлальная
        target.validate(new int[] {}, new int[] {0}); //а тут наоборот - только облачное
        assertEquals(targetCloud.getId(), recordLocal.getSource().getId());
    }

    private void testRecordTargetLocalRename(Generator target, Source.Type type) {
        Generator records = new RecordGenerator(target, type);

        Lifecycle targetLocal = target.addLocal(0);
        target.addCloud(0);

        records.addLocal(0);
        Record recordCloud = (Record) records.addCloud(0);

        target.updateLocal(0);
        records.updateCloud(0);
        targetLocal.setId("newName"); //rename after update as update relies on id which was changed

        records.mergeAndValidateResult(new int[] {}, new int[] {0}); //должна остаться только облачная
        target.validate(new int[] {0}, new int[] {}); //а тут наоборот - только локальное
        assertEquals(targetLocal.getId(), recordCloud.getSource().getId());
    }

    private void testWorkoutContentCloudRename(Generator target, Source.Type type) {
        Generator workouts = new WorkoutGenerator(target, type);

        target.addLocal(0);
        Lifecycle targetCloud = target.addCloud(0);

        Workout workoutLocal = (Workout) workouts.addLocal(0);
        workouts.addCloud(0);

        target.updateCloud(0);
        workouts.updateLocal(0);
        targetCloud.setId("newName"); //rename after update as update relies on id which was changed

        workouts.mergeAndValidateResult(new int[] {0}, new int[] {}); //должна остаться только локлальная
        target.validate(new int[] {}, new int[] {0}); //а тут наоборот - только облачное
        assertTrue(workoutLocal.getSource().getId().equals(targetCloud.getId()));
        if (type == Source.Type.Exercise) {
            assertTrue(workoutLocal.usesExercise(targetCloud.getId()));
        }
    }

    private void testWorkoutContentLocalRename(Generator target, Source.Type type) {
        Generator workouts = new WorkoutGenerator(target, type);

        Lifecycle targetLocal = target.addLocal(0);
        target.addCloud(0);

        workouts.addLocal(0);
        Workout workoutCloud = (Workout) workouts.addCloud(0);

        target.updateLocal(0);
        workouts.updateCloud(0);
        targetLocal.setId("newName"); //rename after update as update relies on id which was changed

        workouts.mergeAndValidateResult(new int[] {}, new int[] {0}); //должна остаться только облачная
        target.validate(new int[] {0}, new int[] {}); //а тут наоборот - только локальное
        assertTrue(workoutCloud.getSource().getId().equals(targetLocal.getId()));
        if (type == Source.Type.Exercise) {
            assertTrue(workoutCloud.usesExercise(targetLocal.getId()));
        }
    }

    private void testContainerRecoverTarget(Generator source, Generator container, boolean cascade) {
        container.addCloud(0);
        container.updateCloud(0);
        container.updateCloud(0);//update x2 to be newer than delete timestamp

        container.addLocal(0);
        source.validateLocalSourceOnly(new int[] {0});
        container.validateLocalSourceOnly(new int[] {0});

        if (!cascade) {
            container.deleteLocal(0, true);
        }
        source.updateLocal(0);
        source.deleteLocal(0, true);
        source.validateNoItems();
        container.validateNoItems();

        container.mergeAndValidateResult(new int[] {}, new int[] {0});
        source.validateLocalSourceOnly(new int[] {0});
    }

    private void testContainerBlockTargetRemoval(Generator source, Generator container, boolean cascade) {
        container.addCloud(0);

        container.addLocal(0);
        container.updateLocal(0);
        container.updateLocal(0);//update x2 to be newer than delete timestamp

        source.validateLocalSourceOnly(new int[] {0});
        container.validateLocalSourceOnly(new int[] {0});

        if (!cascade) {
            container.deleteCloud(0);
        }
        source.updateCloud(0);
        source.deleteCloud(0);

        container.mergeAndValidateResult(new int[] {0}, new int[] {});
        source.validateLocalSourceOnly(new int[] {0});
    }

}
