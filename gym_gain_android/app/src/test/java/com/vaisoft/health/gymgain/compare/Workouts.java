package com.vaisoft.health.gymgain.compare;

import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.data.Workout;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class Workouts {

    @Test
    public void useSummaryForWeightRepsPatternEquals() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createWeightRepsSet(7, 10));
        sets1.add(createWeightRepsSet(8, 10));
        sets1.add(createWeightRepsSet(5, 10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createWeightRepsSet(15, 10));
        sets2.add(createWeightRepsSet(5, 10));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.BAD, progress.getEstimate());
    }

    @Test
    public void useSummaryForWeightRepsPatternSingle() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createWeightRepsSet(10, 10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createWeightRepsSet(10, 10));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.SOSO, progress.getEstimate());
    }

    @Test
    public void useSummaryForWeightRepsPatternMultiple() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createWeightRepsSet(100, 10));
        sets1.add(createWeightRepsSet(10, 10));
        sets1.add(createWeightRepsSet(10, 10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createWeightRepsSet(10, 10));
        sets2.add(createWeightRepsSet(11, 10));
        sets2.add(createWeightRepsSet(11, 10));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.GOOD, progress.getEstimate());
    }

    @Test
    public void useSummaryForWeightRepsPatternMultipleBitWorseStrict() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createWeightRepsSet(100, 10));
        sets1.add(createWeightRepsSet(10, 10));
        sets1.add(createWeightRepsSet(10, 10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createWeightRepsSet(99, 10));
        sets2.add(createWeightRepsSet(10, 10));
        sets2.add(createWeightRepsSet(10, 9));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.BAD, progress.getEstimate());
    }

    @Test
    public void useSummaryForWeightRepsPatternMultipleBitWorseNormal() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createWeightRepsSet(100, 10));
        sets1.add(createWeightRepsSet(10, 10));
        sets1.add(createWeightRepsSet(10, 10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createWeightRepsSet(99, 10));
        sets2.add(createWeightRepsSet(10, 10));
        sets2.add(createWeightRepsSet(10, 9));

        Config config = Config.test(8, Config.Estimation.NORMAL);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.SOSO, progress.getEstimate());
    }

    @Test
    public void useSummaryForDistancePatternEquals() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createDistanceSet(7));
        sets1.add(createDistanceSet(8));
        sets1.add(createDistanceSet(5));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createDistanceSet(15));
        sets2.add(createDistanceSet(5));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.SOSO, progress.getEstimate());
    }

    @Test
    public void useSummaryForDistancePatternSingle() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createDistanceSet(10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createDistanceSet(10));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.SOSO, progress.getEstimate());
    }

    @Test
    public void useSummaryForDistancePatternMultiple() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createDistanceSet(100));
        sets1.add(createDistanceSet(10));
        sets1.add(createDistanceSet(10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createDistanceSet(10));
        sets2.add(createDistanceSet(11));
        sets2.add(createDistanceSet(11));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.BAD, progress.getEstimate());
    }

    @Test
    public void useSummaryForTimeDistancePatternEquals() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createDistanceTimeSet(10, 7));
        sets1.add(createDistanceTimeSet(10, 8));
        sets1.add(createDistanceTimeSet(10, 5));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createDistanceTimeSet(15, 12));
        sets2.add(createDistanceTimeSet(15, 8));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.SOSO, progress.getEstimate());
    }

    @Test
    public void useSummaryForTimeDistancePatternSingle() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createDistanceTimeSet(10, 10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createDistanceTimeSet(10, 10));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.SOSO, progress.getEstimate());
    }

    @Test
    public void useSummaryForTimeDistancePatternMultiple() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createDistanceTimeSet(100, 10));
        sets1.add(createDistanceTimeSet(10, 10));
        sets1.add(createDistanceTimeSet(10, 10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createDistanceTimeSet(10, 10));
        sets2.add(createDistanceTimeSet(11, 10));
        sets2.add(createDistanceTimeSet(11, 10));

        Config config = Config.test(8, Config.Estimation.STRICT);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.BAD, progress.getEstimate());
    }

    @Test
    public void useSummaryForTimeDistancePatternMultipleBitWorseNormal() {
        Exercise exercise = createExercise();

        Workout workout1 = new Workout(exercise);
        List<Workout.Set> sets1 = getSets(workout1);
        sets1.add(createDistanceTimeSet(100, 10));
        sets1.add(createDistanceTimeSet(10, 10));
        sets1.add(createDistanceTimeSet(10, 10));


        Workout workout2 = new Workout(exercise);
        List<Workout.Set> sets2 = getSets(workout2);
        sets2.add(createDistanceTimeSet(100, 10));
        sets2.add(createDistanceTimeSet(9, 10));
        sets2.add(createDistanceTimeSet(9, 12));

        Config config = Config.test(8, Config.Estimation.NORMAL);
        Progress progress = workout2.compareTo(workout1, config);
        assertEquals(Progress.Estimate.SOSO, progress.getEstimate());
    }


    //-------------------------------------------------------------------------

    private List<Workout.Set> getSets(Workout workout) {
        return workout.getEntry(0).log.get(0).sets;
    }

    private Workout.Set createWeightRepsSet(long weight, long reps) {
        Workout.Set set = new Workout.Set();
        set.result.put(Unit.weight, new Value(Unit.kilogram, weight, true, Value.Status.Applied));
        set.result.put(Unit.reps, new Value(Unit.reps, reps, true, Value.Status.Applied));
        return set;
    }

    private Workout.Set createDistanceTimeSet(long distance, long time) {
        Workout.Set set = new Workout.Set();
        set.result.put(Unit.distance, new Value(Unit.meter, distance, true, Value.Status.Applied));
        set.result.put(Unit.time, new Value(Unit.time, time, false, Value.Status.Applied));
        return set;
    }

    private Workout.Set createDistanceSet(long distance) {
        Workout.Set set = new Workout.Set();
        set.result.put(Unit.distance, new Value(Unit.meter, distance, true, Value.Status.Applied));
        return set;
    }

    private Exercise createExercise() {
        Exercise exercise = new Exercise();
        exercise.setId("test");
        exercise.addParameter(Unit.time);
        exercise.addParameter(Unit.distance);
        return exercise;
    }
}
