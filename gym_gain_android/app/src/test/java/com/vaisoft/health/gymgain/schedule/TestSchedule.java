package com.vaisoft.health.gymgain.schedule;

import com.vaisoft.health.gymgain.schedule.validator.WorkoutsValidator;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class TestSchedule {

    protected void validateCriticals(DateTime created, DateTime upTo, AnalyserGenerator generator,
                                   List<DateTime> workouts, WorkoutsValidator shouldBeCritical) {
        List<DateTime> critical = analyseOnCritical(created, upTo, generator, workouts);
        shouldBeCritical.assertWorkouts(critical, "Criticals");
    }

    private List<DateTime>analyseOnCritical(DateTime start, DateTime finish, AnalyserGenerator generator,
                                             List<DateTime> workouts) {
        assertTrue(start.isBefore(finish) || start.equals(finish));

        List<DateTime> result = new ArrayList<>();
        DateTime currentDate = new DateTime(start);
        DateTime lastAnalysed = new DateTime(start);
        while (currentDate.isBefore(finish) || currentDate.equals(finish)) {
            AbstractScheduleAnalyser analyser = generator.generate(currentDate, start, lastAnalysed);
            List<DateTime> filteredWorkouts = Utils.clampDates(workouts, analyser.getHistoryStart(), currentDate);
            WorkoutHealth health = analyser.analyseHealth(filteredWorkouts);
            if (health.showFeed()) {
                result.add(currentDate);
            }
            lastAnalysed = new DateTime(currentDate);
            currentDate = currentDate.plusDays(1);
        }
        return result;
    }

    protected void  validateBurnouts(DateTime created, DateTime upTo, AnalyserGenerator generator,
                                  List<DateTime> workouts, WorkoutsValidator shouldBurn, int maxStep) {
        List<DateTime> burnoutsDaily = analyseOnBurnouts(created, upTo, generator, workouts, 1);
        shouldBurn.assertWorkouts(burnoutsDaily, "Burnouts every day");

        if (maxStep >= 7) {
            List<DateTime> burnoutsWeekly = analyseOnBurnouts(created, upTo, generator, workouts, 7);
            shouldBurn.assertWorkouts(burnoutsWeekly, "Burnouts every week");
        }

        if (maxStep >= Days.daysBetween(created, upTo).getDays()) {
            AbstractScheduleAnalyser analyser = generator.generate(upTo, created, created);
            WorkoutsValidator shouldBurnOnce = shouldBurn.clamp(analyser.getFirstDateToAnalyse(), upTo);
            if (shouldBurnOnce != null) {
                List<DateTime> burnoutsOnce = analyseOnceOnBurnouts(created, upTo, generator, workouts);
                shouldBurnOnce.assertWorkouts(burnoutsOnce, "Burnouts once");
            }
        }
    }

    private List<DateTime> analyseOnBurnouts(DateTime start, DateTime finish, AnalyserGenerator generator,
                                             List<DateTime> workouts, int step) {
        Collections.reverse(workouts);//workouts should be in descending order
        List<DateTime> result = new ArrayList<>();
        DateTime currentDate = new DateTime(start);
        DateTime lastAnalysed = new DateTime(start);
        while (currentDate.isBefore(finish)) {
            currentDate = currentDate.plusDays(step);
            if (currentDate.isAfter(finish)) {
                currentDate = finish;
            }
            List<DateTime> burned = analyseOnBurnouts(currentDate, start, lastAnalysed, generator, workouts);
            result.addAll(burned);
            lastAnalysed = new DateTime(currentDate);
        }
        return result;
    }

    private List<DateTime> analyseOnceOnBurnouts(DateTime lastUpdated, DateTime current, AnalyserGenerator generator,
                                                 List<DateTime> workouts) {
        return analyseOnBurnouts(current, lastUpdated, lastUpdated, generator, workouts);
    }

    private List<DateTime> analyseOnBurnouts(DateTime current, DateTime lastUpdated,
                                             DateTime lastAnalysed, AnalyserGenerator generator,
                                             List<DateTime> workouts) {
        assertTrue(current.isAfter(lastUpdated) || current.equals(lastUpdated));
        assertTrue(current.isAfter(lastAnalysed) || current.equals(lastAnalysed));

        AbstractScheduleAnalyser analyser = generator.generate(current, lastUpdated, lastAnalysed);
        List<DateTime> filteredWorkouts = Utils.clampDates(workouts, analyser.getHistoryStart(), current);
        List<DateTime> burned = analyser.analyseBurnout(filteredWorkouts);
        return burned;
    }

    protected static DateTime date(int day) {
        return new DateTime(2016, 9, day, 0, 0);
    }

    protected static DateTime[] add(DateTime[] source, int addDays) {
        DateTime[] result = new DateTime[source.length];
        for (int i = 0; i < source.length; i++) {
            result[i] = source[i].plusDays(addDays);
        }
        return result;
    }

    protected static List<DateTime> createDateList(DateTime[] data) {
        List<DateTime> result = new ArrayList<>();
        for (DateTime date: data) {
            result.add(date);
        }
        return result;
    }

    protected static List<DateTime> EMPTY_DAYS = new ArrayList<>();
    protected static DateTime MON_START = date(5);

    protected static DateTime[] MON = {
            MON_START, MON_START.plusDays(7), MON_START.plusDays(14),
            MON_START.plusDays(21), MON_START.plusDays(28)};
    protected static DateTime[] TUE = add(MON, 1);
    protected static DateTime[] WEN = add(TUE, 1);
    protected static DateTime[] THU = add(WEN, 1);
    protected static DateTime[] FRI = add(THU, 1);
    protected static DateTime[] SAT = add(FRI, 1);
    protected static DateTime[] SUN = add(SAT, 1);
}
