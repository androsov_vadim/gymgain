package com.vaisoft.health.gymgain.schedule.validator;


import org.joda.time.DateTime;
import static org.junit.Assert.assertEquals;

import java.util.List;

public class SimpleWorkoutsValidator implements WorkoutsValidator {

    public SimpleWorkoutsValidator(int workoutsCount) {
        mData = workoutsCount;
    }

    @Override
    public void assertWorkouts(List<DateTime> workouts, String hint) {
        assertEquals(hint + " (count only)", mData, workouts.size());
    }

    @Override
    public WorkoutsValidator clamp(DateTime start, DateTime finish) {
        return null;
    }

    private int mData;
}
