package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.utils.Utils;

public class ExerciseResultEditor extends LinearLayout implements BoostEditor.OnValueChangedListener {
    public ExerciseResultEditor(Context context) {
        super(context);
        init();
    }

    public ExerciseResultEditor(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ExerciseResultEditor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View.inflate(getContext(), R.layout.exercise_result_editor, this);

        mCaptions[0] = (TextView) findViewById(R.id.edit_reps_caption);
        mCaptions[1] = (TextView) findViewById(R.id.edit_weight_caption);
        mCaptions[2] = (TextView) findViewById(R.id.edit_time_caption);
        mCaptions[3] = (TextView) findViewById(R.id.edit_distance_caption);

        mMarkers[0] = findViewById(R.id.marker_reps);
        mMarkers[1] = findViewById(R.id.marker_weight);
        mMarkers[2] = findViewById(R.id.marker_time);
        mMarkers[3] = findViewById(R.id.marker_distance);
    }

    public void init(String exerciseId, Coach coach, boolean active) {
        Exercise exercise = coach.getExercise(exerciseId);

        mEditors[0] = initEditor(R.id.edit_reps, Unit.reps, coach, exercise);
        mEditors[1] = initEditor(R.id.edit_weight, Unit.weight, coach, exercise);
        mEditors[2] = initEditor(R.id.edit_time, Unit.time, coach, exercise);
        mEditors[3] = initEditor(R.id.edit_distance, Unit.distance, coach, exercise);

        initTabs(coach);
        mActiveWorkout = active;
        setValue(null, null, coach);
    }

    private void initTabs(Coach coach) {
        int visibleIndex = 0;
        for (int i = 0; i < mEditors.length; i++) {
            TextView tab = mCaptions[i];
            View marker = mMarkers[i];
            BoostEditor editor = mEditors[i];
            if (editor == null) {
                View parent = (View) marker.getParent();
                parent.setVisibility(GONE);
                continue;
            }
            Tag tag = (Tag) editor.getTag();
            String tabText = getTabText(tag.parameter, coach);
            tab.setText(Utils.html(tabText));
            int color = 0;
            if (visibleIndex % 2 == 0) {
                color = ContextCompat.getColor(getContext(), R.color.exercise_parameter_even);
            } else {
                color = ContextCompat.getColor(getContext(), R.color.exercise_parameter_odd);
            }
            marker.setBackgroundColor(color);
            visibleIndex++;
        }
    }

    private String getTabText(Unit parameter, Coach coach) {
        StringBuilder result = new StringBuilder();
        result.append("<b>");
        result.append(coach.getLocalized(parameter));
        result.append("</b>");
        Unit unit = coach.getConfig().getUnits(parameter);
        if (unit != parameter) {
            Utils.appendHint(result, coach.getLocalized(unit));
        }
        return result.toString();
    }

    public void setValue(Workout.Set value, Workout.Set previousValue, Coach coach) {
        boolean setChanged = (mCurrentSet != value) || (value == null);
        mCurrentSet = value;
        for (int i = 0; i < mEditors.length; i++) {
            BoostEditor editor = mEditors[i];
            if (editor != null) {
                applyEditorValue(editor, value, previousValue, setChanged);
            }
        }
        if (mActiveWorkout) {
            showWeightRepsHint(previousValue, coach);
        } else {
            showWeightRepsHint(value, coach);
        }
    }

    private void showWeightRepsHint(Workout.Set value, Coach coach) {
        if (value == null) {
            return;
        }
        if ((mEditors[0] == null) || (mEditors[1] == null) ||
                (mEditors[2] != null) || (mEditors[3] != null)) {
            return;
        }
        Value weight = value.getValueForCategory(Unit.weight);
        if (weight == null) {
            return;
        }
        Value repsValue = value.getValueForCategory(Unit.reps);
        if (repsValue == null) {
            return;
        }
        long reps = repsValue.get(Unit.reps);
        showWeightRepsHint((int) reps, coach);
    }

    private void showWeightRepsHint(int reps, Coach coach) {
        Config config = coach.getConfig();
        String tabText = getTabText(Unit.weight, coach);
        StringBuilder weightCaption = new StringBuilder(tabText);
        if (reps < config.getMinReps()) {
            appendHint(weightCaption, Progress.Estimate.BAD, R.string.hint_less_weight);
        } else if (config.getMaxReps() > 0) {
            if (reps > config.getMaxReps()) {
                appendHint(weightCaption, Progress.Estimate.GOOD, R.string.hint_more_weight);
            } else {
                appendHint(weightCaption, Progress.Estimate.SOSO, R.string.hint_same_weight);
            }
        }

        String html = weightCaption.toString();
        mCaptions[1].setText(Utils.html(html));
    }

    private void appendHint(StringBuilder target, Progress.Estimate estimate, int text) {
        target.append(" > ");
        appendColored(target, estimate, text);
    }

    private void appendColored(StringBuilder target, Progress.Estimate estimate, int text) {
        target.append("<small>");
        target.append("<font color='");
        target.append(Utils.getEstimateColorHtml(estimate, getContext()));
        target.append("'>");
        target.append(getContext().getString(text));
        target.append("</font>");
        target.append("</small>");
    }

    private BoostEditor initEditor(int id, Unit parameter, Coach coach, Exercise exercise) {
        BoostEditor editor = (BoostEditor) findViewById(id);
        if (exercise.isParameter(parameter)) {
            editor.setVisibility(VISIBLE);
        } else {
            editor.setVisibility(GONE);
            return null;
        }
        mExercise = exercise.getId();

        Tag tag = (Tag) editor.getTag();
        if (tag == null) {
            tag = new Tag(parameter, coach);
            editor.setTag(tag);
            editor.setOnValueChangedListener(this);
        }

        return editor;
    }

    private void applyEditorValue(BoostEditor editor, Workout.Set value, Workout.Set previousValue, boolean setChanged) {
        Tag tag = (Tag) editor.getTag();
        boolean initialized = false;
        long rawValue = 0;
        Value.Status status = Value.Status.Undefined;
        if (value != null) {
            rawValue = value.plannedReps;
            Value initialValue = value.getValue(tag.unit);
            if (initialValue != null) {
                rawValue = initialValue.get(tag.unit);
                status = initialValue.getStatus();
                initialized = true;
            }
        }
        if (!initialized) {
            rawValue = getValue(previousValue, tag.unit);
        }
        if (setChanged) {
            long previous = 0;
            if (initialized) {
                previous = getValue(previousValue, tag.unit);
            } else {
                previous = rawValue;
            }
            editor.setPreviousValue(previous);
        }
        editor.setValue(tag.unit, rawValue, status);
    }

    private long getValue(Workout.Set set, Unit unit) {
        if (set != null) {
            Value initialValue = set.getValue(unit);
            if (initialValue != null) {
                return initialValue.get(unit);
            }
        }
        return 0;
    }

    public void setOnValueChangedListener(OnValueChangedListener listener) {
        mListener = listener;
    }

    @Override
    public void onValueChanged(BoostEditor source, long value, Value.Status status) {
        if (mListener == null) {
            return;
        }
        Tag tag = (Tag) source.getTag();
        mListener.onChanged(mExercise, tag.parameter, value, status);
    }

    public interface OnValueChangedListener {
        void onChanged(String exerciseId, Unit unit, long value, Value.Status status);
    }

    private class Tag {
        public Unit parameter;
        public Unit unit;

        public Tag(Unit parameter, Coach coach) {
            this.parameter = parameter;
            unit = coach.getConfig().getUnits(parameter);
        }
    }

    private BoostEditor[] mEditors = new BoostEditor[4];
    private TextView[] mCaptions = new TextView[4];
    private View[] mMarkers = new View[4];
    private OnValueChangedListener mListener;
    private String mExercise;
    private boolean mActiveWorkout;
    private Workout.Set mCurrentSet;
}
