package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import com.vaisoft.health.gymgain.R;

public class ConfirmDialog extends DialogFragment
{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle bundle = getArguments();
        int message = bundle.getInt(MESSAGE);
        int caption = bundle.getInt(CAPTION);
        final int code = bundle.getInt(CODE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        
        builder.setTitle(caption);
        builder.setMessage(message);
        builder.setIcon(R.drawable.ic_help_outline_black_24dp);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                listener.onConfirmed(code);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                listener.onCanceled(code);
            }
        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public interface Listener {
        boolean onConfirmed(int code);
        boolean onCanceled(int code);
    }

    public static final String MESSAGE = "message";
    public static final String CAPTION = "caption";
    public static final String CODE = "code";
    public static final String TAG = "ConfirmDialog";
}
