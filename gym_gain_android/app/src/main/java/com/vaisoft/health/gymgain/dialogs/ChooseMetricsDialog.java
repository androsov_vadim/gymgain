package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.main.MainActivity;

public class ChooseMetricsDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.metric_units);
        builder.setIcon(R.drawable.ic_settings_black_24dp);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                chooseMetrics();
            }
        });
        View view = createInterface();
        builder.setView(view);

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;
    }

    private void chooseMetrics() {
        boolean si = mChooser.getCheckedRadioButtonId() == R.id.units_si;
        ((MainActivity) getActivity()).chooseMetrics(si);
    }

    @Override
    public void onStart() {
        super.onStart();
        AlertDialog dialog = (AlertDialog) getDialog();

        mChooser = (RadioGroup) dialog.findViewById(R.id.units_choose);

        final Button buttonOK = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        buttonOK.setEnabled(mChooser.getCheckedRadioButtonId() != -1);

        mChooser.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                buttonOK.setEnabled(true);
            }
        });
    }

    private View createInterface() {
        return View.inflate(getActivity(), R.layout.choose_metrics_dialog, null);
    }

    private RadioGroup mChooser;
}
