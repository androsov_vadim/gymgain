package com.vaisoft.health.gymgain.sync;

import android.content.Context;
import android.util.Log;

import com.vaisoft.health.gymgain.coach.Coach;

import org.joda.time.DateTime;

import java.util.List;

public class BackgroundCloudUpdater implements Cloud.SyncFinishListener {
    public BackgroundCloudUpdater(Context context, Coach coach, boolean autoApplyIncomingData) {
        mAutoApplyIncomingData = autoApplyIncomingData;
        mPendingIncoming = false;
        mCloud = new Cloud(context);
        mCoach = coach;
        mCloud.setListener(this);
    }

    public void update() {
        mDone = false;
        sync();
        try {
            synchronized (mWaiter) {
                while (!mDone) {
                    mWaiter.wait();
                }
            }
        } catch (InterruptedException e) {
            Log.e(getClass().getName(), "Background cloud sync interrupted");
        }
    }

    private void sync() {
        mConnecting = true;
        mCloud.requestDriveConnection();
    }

    @Override
    public void onOperationCompleted(boolean dataTransferred) {
        if (mCloud.isFinishedSuccessfully()) {
            if (mConnecting) {
                mConnecting = false;
                connectedToCloud();
            } else {
                if (dataTransferred) {
                    info("Background sync OK: data transferred");
                    mCoach.getConfig().onCloudSync();
                } else {
                    info("Background sync OK: data NOT transferred");
                }
                finish();
            }
        } else {
            Log.e(getClass().getName(), "Background cloud sync failed");
            finish();
        }
    }

    private void info(String text) {
        Log.i(getClass().getName(), text);
    }

    private void connectedToCloud() {
        DateTime local = mCoach.getCloudInfo().getLastSyncDate();
        DateTime remote = mCloud.getInfo().getLastSyncDate();
        if (mCoach.getConfig().nothingToSync(local, remote)) {
            info("Nothing to sync");
            mCoach.getConfig().onCloudSync();
            finish();
            return;
        }
        SyncType syncType = getSyncType(local, remote);
        applySync(syncType);
    }

    private void applySync(SyncType syncType) {
        switch (syncType) {
            case LocalToCloud:
                List<String> files = mCoach.getFilesToSync();
                mCloud.localToCloud(files, mCoach);
                break;
            case CloudToLocal:
                if (checkApplyIncoming()) {
                    mCloud.cloudToLocal(mCoach);
                }
                break;
            case Merge:
                if (checkApplyIncoming()) {
                    mCloud.merge(mCoach);
                }
                break;
        }
    }

    private boolean checkApplyIncoming() {
        if (mAutoApplyIncomingData) {
            info("Auto apply");
            return true;
        }
        info("Pending update");
        finish();
        mPendingIncoming = true;
        return false;
    }

    private SyncType getSyncType(DateTime local, DateTime remote) {
        if (remote == null) {
            return SyncType.LocalToCloud;
        }
        if (local == null) {
            if (!mCoach.getConfig().isDataModifiedAfterCloudSync()) {
                return SyncType.CloudToLocal;
            }
        } else if (local.equals(remote)) { //changed assumed 'true' because other case covered by Config.nothingToSync
            return SyncType.LocalToCloud;
        }
        boolean changed = mCoach.getConfig().isDataModifiedAfterCloudSync();
        if (!changed) {
            return SyncType.CloudToLocal;
        }
        return SyncType.Merge;
    }

    private void finish() {
        mCloud.setListener(null);
        mCloud.close();
        mCloud = null;
        mDone = true;
        synchronized (mWaiter) {
            mWaiter.notify();
        }
    }

    public boolean hasPendingIncomingData() {
        return mPendingIncoming;
    }

    private enum SyncType {
        LocalToCloud,
        CloudToLocal,
        Merge
    }

    public interface Listener {
        void onIncomingData();
    }

    private Cloud mCloud;
    private boolean mAutoApplyIncomingData;
    private boolean mPendingIncoming;
    private Coach mCoach;
    private boolean mConnecting;
    private final Object mWaiter = new Object();
    private boolean mDone;
}
