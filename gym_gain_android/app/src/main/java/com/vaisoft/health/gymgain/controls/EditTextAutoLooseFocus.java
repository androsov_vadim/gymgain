package com.vaisoft.health.gymgain.controls;


import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;

import com.vaisoft.health.gymgain.R;

public class EditTextAutoLooseFocus extends androidx.appcompat.widget.AppCompatEditText {

    public EditTextAutoLooseFocus(Context context) {
        super(context);
    }

    public EditTextAutoLooseFocus(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextAutoLooseFocus(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        boolean result = super.dispatchKeyEvent(event);
        if (keyCode == KeyEvent.KEYCODE_BACK &&
                event.getAction() == KeyEvent.ACTION_UP) {
            getRootView().findViewById(R.id.foo).requestFocus();
        }
        return result;
    }
}
