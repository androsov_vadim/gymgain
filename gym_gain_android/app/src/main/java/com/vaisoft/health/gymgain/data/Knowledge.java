package com.vaisoft.health.gymgain.data;

import android.content.Context;
import androidx.core.util.Pair;
import android.util.Log;

import com.vaisoft.health.gymgain.sync.JSONLoader;
import com.vaisoft.health.gymgain.sync.JSonLoaderFile;
import com.vaisoft.health.gymgain.sync.Merger;
import com.vaisoft.health.gymgain.sync.Stamp;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Knowledge  {

    public Knowledge() {
    }

    public void init(Context context) {
        JSONLoader loader = new JSonLoaderFile(context);
        loadCloudInfo(context, loader);
        init(loader);
    }

    public void init(JSONLoader loader) {
        mConfig = Config.load(loader);
        mBody = Body.load(loader);
        mSport = Sport.load(loader, mBody);
        loadTrainings(loader);
        loadRecords(loader);
        mBodyMeasure = BodyMeasure.load(loader);
        loadActiveWorkouts(loader);
        loadFinishedWorkouts(loader);
    }

    public void save(Context context) {
        mBody.save(context);
        mSport.saveAllExercises(context);
        saveAllTrainings(context);
        saveRecords(context);
        saveBodyLog(context);
        saveActiveWorkouts(context);
        saveFinishedWorkouts(context);
        saveConfig(context);
    }

    public List<String> getFilesToSync() {
        List<String> files = new ArrayList<>();

        Body.addSourceFilesToList(files);
        Sport.addSourceFilesToList(files);
        Config.addSourceFilesToList(files);
        Training.addSourceFilesToList(files);
        Record.addSourceFilesToList(files);
        BodyMeasure.addSourceFilesToList(files);
        Workout.addSourceFilesToList(files);

        return files;
    }

    public boolean merge(Knowledge knowledge) {
        boolean changes = false;

        List<Exercise> deletedExercises = mSport.getDeletingExercises(knowledge.mSport);
        List<Training> deletedTrainings = Merger.getDeletingItems(mTrainings, knowledge.mTrainings);
        List<Pair<Lifecycle, Lifecycle>> renamed = new ArrayList<>();

        changes = !deletedExercises.isEmpty();
        changes = changes || (!deletedTrainings.isEmpty());

        boolean isChanges = mBody.merge(knowledge.mBody);
        changes = changes || isChanges;

        isChanges =  mConfig.merge(knowledge.mConfig);
        changes = changes || isChanges;

        isChanges = mBodyMeasure.merge(knowledge.mBodyMeasure);
        changes = changes || isChanges;

        isChanges = mSport.merge(knowledge.mSport, renamed);
        changes = changes || isChanges;

        isChanges = mergeTrainings(knowledge, renamed);
        changes = changes || isChanges;

        isChanges = mergeRecords(knowledge);
        changes = changes || isChanges;

        isChanges = mergeWorkouts(knowledge);
        changes = changes || isChanges;


        if ((!deletedExercises.isEmpty()) || (!deletedTrainings.isEmpty())) {
            fixRecords(deletedExercises, deletedTrainings);
        }
        if (!deletedExercises.isEmpty()) {
            fixExercises(deletedExercises);
        }
        if (!renamed.isEmpty()) {
            applyRenames(renamed);
        }

        return changes;
    }

    private void applyRenames(List<Pair<Lifecycle, Lifecycle>> renamed) {
        for (Pair<Lifecycle, Lifecycle> item: renamed) {
            if (item.first instanceof Exercise) {
                applyExerciseRename(null, item.second.getId(), item.first.getId());
            } else if (item.first instanceof Training) {
                applyTrainingRename(null, item.second.getId(), item.first.getId());
            } else {
                Log.e(getClass().getName(), "Unexpected rename(" + item.first.getClass().getName() +
                    ": " + item.first.getId() + " -> " + item.second.getId());
            }
        }
    }

    private void fixExercises(List<Exercise> deletedExercises) {
        for (Exercise exercise: deletedExercises) {
            if (isExerciseInUse(exercise.getId())) {
                mSport.undeleteExercise(exercise);
            }
        }
    }

    private void fixRecords(List<Exercise> deletedExercises, List<Training> deletedTrainings) {
        for (Map.Entry<String, Record> iRecord : mRecords.entrySet()) {
            Record record = iRecord.getValue();
            if (record.isDeleted()) {
                continue;
            }
            String id = record.getSource().getId();
            if (record.isExercise()) {
                Exercise exercise = getExercise(id);
                if (exercise == null) {
                    fixRecordForExercise(record, id, deletedExercises);
                }
            } else if (record.isTraining()) {
                Training training = getTraining(id);
                if (training == null) {
                    fixRecordForTraining(record, id, deletedTrainings);
                }
            }
        }

    }

    private static Exercise extractExercise(List<Exercise> exercises, String id) {
        Exercise result = null;
        for (Exercise exercise: exercises) {
            if (exercise.getId().equals(id)) {
                result = exercise;
                break;
            }
        }
        if (result != null) {
            exercises.remove(result);
        }
        return result;
    }

    private void fixRecordForExercise(Record record, String id, List<Exercise> deletedExercises) {
        Exercise toFix = extractExercise(deletedExercises, id);
        if (toFix != null) {
            mSport.undeleteExercise(toFix);
            record.invalidateExercise(toFix.getId(), id);
        }
    }

    private static Training extractTraining(List<Training> trainings, String id) {
        Training result = null;
        for (Training training: trainings) {
            if (training.getId().equals(id)) {
                result = training;
                break;
            }
        }
        if (result != null) {
            trainings.remove(result);
        }
        return result;
    }

    private void fixRecordForTraining(Record record, String id, List<Training> deletedTrainings) {
        Training toFix = extractTraining(deletedTrainings, id);
        if (toFix != null) {
            undeleteTraining(toFix);
            record.invalidateTraining(toFix.getId(), id);
        }
    }

    private void undeleteTraining(Training deletedTraining) {
        boolean replaced = false;
        for (int i = 0; i < mTrainings.size(); ++i) {
            Training training = mTrainings.get(i);
            if (training.getId().equals(deletedTraining.getId())) {
                mTrainings.set(i, deletedTraining);
                replaced = true;
                break;
            }
        }
        if (!replaced) {
            mTrainings.add(deletedTraining);
        }
    }

    private boolean mergeWorkouts(Knowledge knowledge) {
        boolean changed = false;
        for (Map.Entry<String, List<Workout>> entry : knowledge.mFinishedWorkouts.entrySet()) {
            List<Workout> local = mFinishedWorkouts.get(entry.getKey());
            if (local == null) {
                mFinishedWorkouts.put(entry.getKey(), entry.getValue());
                changed = true;
            } else {
                Merger.Statistics statistics = new Merger.Statistics();
                mergeBurnedOutWorkouts(local, entry.getValue());
                List<Workout> merged = Merger.merge(local, entry.getValue(), statistics);
                mFinishedWorkouts.put(entry.getKey(), merged);
                if (statistics.isChanged()) {
                    changed = true;
                }
            }
        }
        return changed;
    }

    private void mergeBurnedOutWorkouts(List<Workout> local, List<Workout> cloud) {
        List<Workout> burnedOutLocal = getBurnedOutWorkouts(local);
        List<Workout> burnedOutCloud = getBurnedOutWorkouts(cloud);
        dropSameDay(cloud, burnedOutCloud, local);
        dropSameDay(local, burnedOutLocal, cloud);
    }

    private void dropSameDay(List<Workout> affect, List<Workout> vulnerable, List<Workout> source) {
        List<Workout> toRemove = new ArrayList<>();
        for (Workout sourceWorkout: source) {
            for (Workout vulnerableWorkout: vulnerable) {
                if (Utils.isEqualDay(sourceWorkout.getFinishedDate(), vulnerableWorkout.getFinishedDate())) {
                    toRemove.add(vulnerableWorkout);
                }
            }
        }
        affect.removeAll(toRemove);
    }

    private List<Workout> getBurnedOutWorkouts(List<Workout> source) {
        List<Workout> result = new ArrayList<>();
        for (Workout workout: source) {
            if (workout.isBurnedOut()) {
                result.add(workout);
            }
        }
        return result;
    }

    private boolean mergeRecords(Knowledge knowledge) {
        boolean changed = false;
        for (Map.Entry<String, Record> iRecord : knowledge.mRecords.entrySet()) {
            Record cloudRecord = iRecord.getValue();
            Record localRecord = getRecordByKey(cloudRecord.getSource().getKey());
            if ((localRecord == null) || (cloudRecord.isNewer(localRecord))) {
                mRecords.put(iRecord.getKey(), cloudRecord);
                changed = true;
            }
        }
        return changed;
    }

    private boolean mergeTrainings(Knowledge knowledge, List<Pair<Lifecycle, Lifecycle>> renamed) {
        Merger.Statistics statistics = new Merger.Statistics();
        mTrainings = Merger.merge(mTrainings, knowledge.mTrainings, statistics);
        if (statistics.isChanged()) {
            statistics.getRenamed(renamed);
            invalidateTrainingMap();
            return true;
        }
        return false;
    }

    private void invalidateTrainingMap() {
        mTrainingsMap.clear();
        for (Training training: mTrainings) {
            addTrainingToMap(training);
        }
    }

    private void loadCloudInfo(Context context, JSONLoader loader) {
        mCloudInfo = Stamp.load(loader);
        try {
            saveCloudInfo(context);
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    private void loadFinishedWorkouts(JSONLoader loader) {
        Workout.load(loader, Workout.FINISHED_FILE_NAME, new Workout.OnLoadedListener() {
            @Override
            public void onLoaded(Workout workout) {
                List<Workout> target = getFinishedWorkoutsByKey(workout.getSource().getKey());
                target.add(workout);
            }
        });
    }

    private void loadActiveWorkouts(JSONLoader loader) {
        Workout.load(loader, Workout.ACTIVE_FILE_NAME, new Workout.OnLoadedListener() {
            @Override
            public void onLoaded(Workout workout) {
                mActiveWorkouts.add(workout);
            }
        });
    }

    private void loadTrainings(JSONLoader loader) {
        Training.load(loader, new Training.OnLoadedListener() {
            @Override
            public void onLoaded(Training training) {
                addTraining(training);
            }
        });
    }

    private void saveAllTrainings(Context context) {
        Training.saveAll(context, mTrainings);
    }

    public Exercise getExercise(String id) {
        return mSport.getExercise(id);
    }

    public Exercise getExerciseRaw(long id) {
        return mSport.getExerciseRaw(id);
    }

    public Exercise getExerciseByKey(String key) {
        return mSport.getExerciseByKey(key);
    }

    public Training getTraining(String id) {
        for (Training training: mTrainings) {
            if (training.isDeleted()) {
                continue;
            }
            if (training.getId().equals(id)) {
                return training;
            }
        }
        return null;
    }

    public Training getTrainingRaw(long id) {
        for (Training training: mTrainings) {
            if (training.isSameObject(id)) {
                return training;
            }
        }
        return null;
    }

    public List<Training> getTrainings() {
        List<Training> result = new ArrayList<>();
        for (Training training: mTrainings) {
            if (!training.isDeleted()) {
                result.add(training);
            }
        }
        return result;
    }

    public Training getTrainingByKey(String key) {
        return mTrainingsMap.get(key);
    }

    public boolean isWorkoutSource(String key) {
        return mTrainingsMap.containsKey(key) || mSport.isExercise(key);
    }

    public List<String> getAffectedByMusclesParts(List<String> muscles) {
        return mBody.getPartsForMuscles(muscles);
    }

    public List<String> getAffectedParts(Exercise exercise) {
        return getAffectedByMusclesParts(exercise.getMuscles());
    }

    public List<String> getAllParts() {
        return mBody.getAllParts();
    }

    public List<String> getAffectedByExercisesParts(List<String> exercises) {
        List<String> muscles = getAffectedByExercisesMuscles(exercises);
        return getAffectedByMusclesParts(muscles);
    }

    public List<String> getAffectedByExercisesMuscles(List<String> exercises) {
        Set<String>  muscles = new HashSet<>();
        for (String ex: exercises) {
            Exercise exercise = mSport.getExercise(ex);
            if (exercise != null) {
                List<String> exMuscles = exercise.getMuscles();
                muscles.addAll(exMuscles);
            } else {
                Log.e(getClass().getName(), "Non-existent exercise: " + ex);
            }
        }
        List<String> result = new ArrayList<>();
        result.addAll(muscles);
        return result;
    }

    public List<String> getAllMuscles() {
        return mBody.getFilteredMuscles(null);
    }

    public ArrayList<String> getFilteredMuscles(Exercise excludeFrom) {
        return mBody.getFilteredMuscles(excludeFrom.getMuscles());
    }

    public String getBodyPartForMuscle(String muscle) {
        return mBody.getPartForMuscle(muscle);
    }

    public List<String> getMusclesForPart(String bodyPart) {
        Body.Part part = mBody.getPart(bodyPart);
        if (part != null) {
            return part.getMuscles();
        } else {
            Log.e(getClass().getName(), "Body part not found: " + bodyPart);
            return  new ArrayList<>();
        }
    }

    public List<String> getMusclesForExercises(List<String> exercises) {
        Set<String>  parts = new HashSet<>();
        for (String ex: exercises) {
            Exercise exercise = mSport.getExercise(ex);
            List<String> exMuscles = exercise.getMuscles();
            parts.addAll(exMuscles);
        }
        List<String> result = new ArrayList<>();
        result.addAll(parts);
        return result;
    }

    public void saveExercise(Context context, Exercise exercise, String previousId) {
        if (previousId != null) {
            applyExerciseRename(context, exercise.getId(), previousId);
        }
        mSport.saveExercise(context, exercise, previousId);
        setDataModified(context, true);
    }

    private void applyExerciseRename(Context context, String id, String previousId) {
        boolean recordChanged = renameExerciseInRecord(id, previousId);
        boolean trainingChanged = renameExerciseInTraining(id, previousId);
        boolean finishedWorkoutsChanged = renameExerciseInFinishedWorkouts(id, previousId);
        boolean activeWorkoutsChanged = renameExerciseInWorkouts(mActiveWorkouts, id, previousId);
        if (context != null) {
            if (recordChanged) {
                saveRecords(context);
            }
            if (trainingChanged) {
                saveAllTrainings(context);
            }
            if (finishedWorkoutsChanged) {
                saveFinishedWorkouts(context);
            }
            if (activeWorkoutsChanged) {
                saveActiveWorkouts(context);
            }
        }
    }

    private boolean renameExerciseInFinishedWorkouts(String id, String previousId) {
        boolean changes = false;
        List<Workout> newKeyWorkouts = null;
        String key = Source.getKey(Source.Type.Exercise, previousId);
        for (Map.Entry<String, List<Workout>> entry : mFinishedWorkouts.entrySet()) {
            List<Workout> workouts = entry.getValue();
            if ((workouts != null) && (!workouts.isEmpty())) {
                if (renameExerciseInWorkouts(workouts, id, previousId)) {
                    if (entry.getKey().equals(key)) {
                        newKeyWorkouts = workouts;
                    }
                    changes = true;
                }
            }
        }
        if (newKeyWorkouts != null) {
            String newKey = Source.getKey(Source.Type.Exercise, id);
            mFinishedWorkouts.put(newKey, newKeyWorkouts);
            mFinishedWorkouts.remove(key);
        }
        return changes;
    }

    private boolean renameTrainingInFinishedWorkouts(String id, String previousId) {
        String key = Source.getKey(Source.Type.Training, previousId);
        List<Workout> workouts = mFinishedWorkouts.get(key);
        if ((workouts != null) && (!workouts.isEmpty())) {
            renameTrainingInWorkouts(workouts, id, previousId);
            String newKey = Source.getKey(Source.Type.Training, id);
            mFinishedWorkouts.put(newKey, workouts);
            mFinishedWorkouts.remove(key);
             return true;
        }
        return false;
    }

    private boolean renameTrainingInWorkouts(List<Workout> workouts, String trainingId, String previousId) {
        boolean changed = false;
        for (Workout workout: workouts) {
            if (!workout.isDeleted()) {
                if (workout.renameTraining(trainingId, previousId)) {
                    workout.onUpdated();
                    changed = true;
                }
            }
        }
        return changed;
    }

    private boolean renameExerciseInWorkouts(List<Workout> workouts, String exerciseId, String previousId) {
        boolean changes = false;
        for (Workout workout: workouts) {
            if (!workout.isDeleted()) {
                if (workout.renameExercise(exerciseId, previousId)) {
                    workout.onUpdated();
                    changes = true;
                }
            }
        }
        return changes;
    }

    private boolean renameExerciseInTraining(String id, String previousId) {
        boolean changes = false;
        for (Training training: mTrainings) {
            if (!training.isDeleted()) {
                if (training.renameExercises(id, previousId)) {
                    changes = true;
                }
            }
        }
        return changes;
    }

    private boolean renameExerciseInRecord(String id, String previousId) {
        String key = Source.getKey(Source.Type.Exercise, previousId);
        Record record = mRecords.get(key);
        if (record != null) {
            boolean changed = record.invalidateExercise(id, previousId);
            if (changed) {
                mRecords.remove(key);
                addRecord(record);
                return true;
            } else {
                Log.e(getClass().getName(), "Exercise rename in record fail:" +
                        previousId + " -> " + id);
            }
        }
        return false;
    }

    public void deleteExercise(Context context, String exerciseId) {
        String key = mSport.deleteExercise(context, exerciseId);
        if (key != null) {
            deleteRecord(context, key);
            setDataModified(context, true);
        }
    }

    public void saveTraining(Context context, Training training, String previousId) {
        if (previousId != null) {
            applyTrainingRename(context, training.getId(), previousId);
        }
        if (previousId == null) {
            addTraining(training);
            training.onCreated();
        } else {
            insertTraining(training, previousId);
            training.onUpdated();
        }
        saveAllTrainings(context);
        setDataModified(context, true);
    }

    private void applyTrainingRename(Context context, String id, String previousId) {
        boolean recordChanged = renameTrainingInRecord(id, previousId);
        boolean finishedWorkoutsChanged = renameTrainingInFinishedWorkouts(id, previousId);
        boolean activeWorkoutsChanged = renameTrainingInWorkouts(mActiveWorkouts, id, previousId);
        if (context != null) {
            if (recordChanged) {
                saveRecords(context);
            }
            if (finishedWorkoutsChanged) {
                saveFinishedWorkouts(context);
            }
            if (activeWorkoutsChanged) {
                saveActiveWorkouts(context);
            }
        }
    }

    private void insertTraining(Training training, String previousId) {
        if (!training.getId().equals(previousId)) {
            removeTrainingFromMap(previousId);
        }
        for (int i = 0; i < mTrainings.size(); i++) {
            Training checkTraining = mTrainings.get(i);
            if (checkTraining.getId().equals(previousId)) {
                mTrainings.set(i, training);
                addTrainingToMap(training);
                break;
            }
        }
    }

    private boolean renameTrainingInRecord(String id, String previousId) {
        String key = Source.getKey(Source.Type.Training, previousId);
        Record record = mRecords.get(key);
        if (record != null) {
            boolean changed = record.invalidateTraining(id, previousId);
            if (changed) {
                mRecords.remove(key);
                addRecord(record);
                return true;
            } else {
                Log.e(getClass().getName(), "Training rename in record fail:" +
                        previousId + " -> " + id);
            }
        }
        return false;
    }

    private String deleteTrainingObject(String trainingId) {
        for (int i = 0; i < mTrainings.size(); i++) {
            Training training = mTrainings.get(i);
            if (training.getId().equals(trainingId)) {
                if (!training.markAsDeleted()) {
                    mTrainings.remove(i);
                }
                removeTrainingFromMap(trainingId);
                return Source.getKey(training);
            }
        }
        return null;
    }

    public void deleteTraining(Context context, String trainingId) {
        String key = deleteTrainingObject(trainingId);
        if (key != null) {
            deleteRecord(context, key);
            saveAllTrainings(context);
            setDataModified(context, true);
        }
    }

    private void addTraining(Training training) {
        if (isTrainingIdFree(training.getId())) {
            mTrainings.add(training);
            addTrainingToMap(training);
        } else {
            insertTraining(training, training.getId());
        }
    }

    private boolean isTrainingIdFree(String id) {
        for (Training training: mTrainings) { //including deleted
            if (training.getId().equals(id)) {
                return false;
            }
        }
        return true;
    }

    private void removeTrainingFromMap(String id) {
        String key = Source.getKey(Source.Type.Training, id);
        mTrainingsMap.remove(key);
    }

    private void addTrainingToMap(Training training) {
        if (!training.isDeleted()) {
            String key = Source.getKey(training);
            mTrainingsMap.put(key, training);
        }
    }

    public Body getBody() {
        return mBody;
    }

    public List<String> getExercisesForBodyPart(Training training, String id) {
        List<String> result = new ArrayList<>();
        for (String exId: training.getExercises()) {
            Exercise ex = mSport.getExercise(exId);
            if (getAffectedParts(ex).contains(id)) {
                result.add(exId);
            }
        }
        return result;
    }

    private void loadRecords(JSONLoader loader) {
        Record.load(loader, new Record.OnLoadedListener() {
            @Override
            public void onLoaded(Record record) {
                addRecord(record);
            }
        });
    }

    public void saveRecords(Context context) {
        if (context != null) {
            Record.saveAll(context, mRecords);
        }
    }

    public void addBodyLog(Context context, BodyMeasure.LogEntry entry) {
        mBodyMeasure.log(entry);
        saveBodyLog(context);
        setDataModified(context, true);
    }

    public void renameBodyLogItem(Context context, String source, String target) {
        mBodyMeasure.renameItem(source, target);
        saveBodyLog(context);
        setDataModified(context, true);
    }

    public void updateBodyLog(Context context, List<BodyMeasure.LogItem> items, BodyMeasure.LogEntry entry) {
        mBodyMeasure.log(items, entry);
        saveBodyLog(context);
        setDataModified(context, true);
    }

    public void deleteBodyLog(Context context, BodyMeasure.LogEntry entry) {
        mBodyMeasure.unlog(entry);
        saveBodyLog(context);
        setDataModified(context, true);
    }

    private void saveBodyLog(Context context) {
        if (context != null) {
            mBodyMeasure.saveLog(context);
        }
    }

    public Record getRecordByKey(String key) {
        return mRecords.get(key);
    }

    public List<Record> getRecords() {
        List<Record> result = new ArrayList<>();
        for (Map.Entry<String, Record> iRecord : mRecords.entrySet()) {
            Record record = iRecord.getValue();
            if (!record.isDeleted()) {
                result.add(record);
            }
        }
        return result;
    }

    public Record createRecord(Exercise exercise) {
        Record newRecord = new Record(exercise);
        addRecord(newRecord);
        return newRecord;
    }

    public Record createRecord(Training training) {
        Record newRecord = new Record(training);
        addRecord(newRecord);
        return newRecord;
    }

    public Record createExerciseRecord(String id) {
        Record newRecord = Record.createExercise(id);
        addRecord(newRecord);
        return newRecord;
    }

    public Record createTrainingRecord(String id) {
        Record newRecord = Record.createTraining(id);
        addRecord(newRecord);
        return newRecord;
    }

    public Record getRecordForExercise(String id) {
        List<Record> records = getRecords();
        for (Record record : records) {
            if (record.isForExercise(id)) {
                return record;
            }
        }
        return null;
    }

    public Record getRecordFor(Exercise exercise) {
        return getRecordForExercise(exercise.getId());
    }

    public Record getRecordForTraining(String id) {
        List<Record> records = getRecords();
        for (Record record : records) {
            if (record.isForTraining(id)) {
                return record;
            }
        }
        return null;
    }

    public Record getRecordFor(Training training) {
        return getRecordForTraining(training.getId());
    }

    public void addRecord(Record record) {
        mRecords.put(record.getSource().getKey(), record);
    }

    public void deleteRecord(Context context, Record record) {
        if (!record.markAsDeleted()) {
            mRecords.remove(record.getSource().getKey());
        }
        saveRecords(context);
        setDataModified(context, true);
    }

    public void deleteRecord(Context context, String key) {
        Record record = getRecordByKey(key);
        if (record != null) {
            deleteRecord(context, record);
        }
    }

    public void start(Context context, Workout workout) {
        workout.start();
        addActiveWorkout(workout);
        boolean burnoutDeleted = deleteBurnout(workout);
        if (context != null) {
            if (burnoutDeleted) {
                saveFinishedWorkouts(context);
            }
            saveActiveWorkouts(context);
            setDataModified(context, true);
        }
    }

    private boolean deleteBurnout(Workout workout) {
        List<Workout> target = getFinishedWorkoutsByKey(workout.getSource().getKey());
        if ((target == null) || target.isEmpty()) {
            return false;
        }
        DateTime date = workout.getStartedDate();
        for (int i = 0; i < target.size(); ++i) {
            Workout targetWorkout = target.get(i);
            DateTime targetDate = targetWorkout.getStartedDate();
            if (!Utils.isAfterOrEqualDay(targetDate, date)) {
                return false;
            }
            if (targetWorkout.isBurnedOut() && Utils.isEqualDay(targetDate, date)) {
                target.remove(i);
                return true;
            }
        }
        return false;
    }

    private void addFinishedWorkout(Workout workout) {
        List<Workout> target = getFinishedWorkoutsByKey(workout.getSource().getKey());
        workout.onCreated();
        target.add(0, workout);
    }

    private void addActiveWorkout(Workout workout) {
        mActiveWorkouts.add(0, workout);
    }

    public void finish(Context context, int workoutIndex, Workout workout) {
        mActiveWorkouts.remove(workoutIndex); //remove by index as value was cloned
        finish(workout);
        if (context != null) {
            onWorkoutsFinished(context);
        }
    }

    private void finish(Workout workout) {
        workout.finish();
        addFinishedWorkout(workout);
    }

    protected void onWorkoutsFinished(Context context) {
        saveFinishedWorkouts(context);
        saveActiveWorkouts(context);
        setDataModified(context, true);
    }

    private void saveActiveWorkouts(Context context) {
        JSONArray objects = new JSONArray();
        try {
            for (Workout workout: mActiveWorkouts) {
                JSONObject workoutObject = new JSONObject();
                workout.save(workoutObject);
                objects.put(workoutObject);
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
        Utils.saveJSON(context, Workout.ACTIVE_FILE_NAME, objects);
    }

    public void onActiveWorkoutUpdated(Context context, int index, Workout workout) {
        mActiveWorkouts.set(index, workout);
        saveActiveWorkouts(context);
        setDataModified(context, true);
    }

    public void onFinishedWorkoutUpdated(Context context, int index, Workout workout) {
        List<Workout> target = getFinishedWorkoutsByKey(workout.getSource().getKey());
        target.set(index, workout);
        workout.onUpdated();
        saveFinishedWorkouts(context);
        setDataModified(context, true);
    }

    public void onFinishedWorkoutsChanged(Context context) {
        saveFinishedWorkouts(context);
        setDataModified(context, true);
    }

    private void saveFinishedWorkouts(Context context) {
        JSONArray objects = new JSONArray();
        try {
            for (Map.Entry<String, List<Workout>> entry : mFinishedWorkouts.entrySet()) {
                for (Workout workout: entry.getValue()) {
                    JSONObject workoutObject = new JSONObject();
                    workout.save(workoutObject);
                    objects.put(workoutObject);
                }
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
        Utils.saveJSON(context, Workout.FINISHED_FILE_NAME, objects);
    }

    public void deleteActiveWorkout(Context context, Workout workout) {
        mActiveWorkouts.remove(workout);
        saveActiveWorkouts(context);
        setDataModified(context, true);
    }

    public void deleteFinishedWorkout(Context context, Workout workout) {
        if (!workout.markAsDeleted()) {
            List<Workout> target = getFinishedWorkoutsByKey(workout.getSource().getKey());
            target.remove(workout);
        }
        if (context != null) {
            saveFinishedWorkouts(context);
            setDataModified(context, true);
        }
    }

    public Workout getActiveWorkout(int workoutId) {
        return mActiveWorkouts.get(workoutId);
    }

    public List<Workout> getActiveWorkouts() {
        return mActiveWorkouts;
    }

    public Map<String, List<Workout>> getFinishedWorkouts() {
        return mFinishedWorkouts;
    }

    public List<Workout> getFinishedWorkoutsByKey(String key) {
        List<Workout> workouts = mFinishedWorkouts.get(key);
        if (workouts == null) {
            workouts = new ArrayList<>();
            mFinishedWorkouts.put(key, workouts);
        }
        return workouts;
    }

    public static  Workout getPreviousWorkout(List<Workout> workouts, int index) {
        for (int i = index + 1; i < workouts.size(); i++) {
            Workout target = workouts.get(i);
            if (target.isDeleted()) {
                continue;
            }
            if (target.isResults()) {
                return target;
            }
        }
        return null;
    }

    public int getActiveWorkoutId(Workout workout) {
        return mActiveWorkouts.indexOf(workout);
    }

    public void saveConfig(Context context) {
        mConfig.save(context);
    }

    public Config getConfig() {
        return mConfig;
    }

    public BodyMeasure getBodyMeasurer() {
        return mBodyMeasure;
    }

    private void saveCloudInfo(Context context) throws JSONException {
        mCloudInfo.save(context);
    }

    public void onCloudSynchronized(Context context, Stamp stamp) {
        mCloudInfo = stamp;
        try {
            saveCloudInfo(context);
            setDataModified(context, false);
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    public void setDataModified(Context context, boolean modified) {
        mConfig.setDataModifiedAfterCloudSync(modified);
        if (context != null) {
            saveConfig(context);
        }
    }

    public Stamp getCloudInfo() {
        return mCloudInfo;
    }

    public int autoFinishActiveWorkouts(Context context) {
        List<Workout> finished = new ArrayList<>();

        for (Workout workout: mActiveWorkouts) {
            if (workout.lastsDays() > 1) {
                finished.add(workout);
            }
        }
        if (!finished.isEmpty()) {
            for (Workout toFinish : finished) {
                mActiveWorkouts.remove(toFinish);
                finish(toFinish);
            }
            onWorkoutsFinished(context);
        }
        return finished.size();
    }

    public List<Exercise> getExercises() {
        return mSport.getExercises();
    }

    public void enableCloud() {
        mSport.enableCloud();
        enableCloudRecords();
        enableCloudTrainings();
        mBodyMeasure.enableCloud();
        enableCloudFinishedWorkouts();
    }

    public void enableCloudAndSave(Context context) {
        if (mSport.enableCloud()) {
            mSport.saveAllExercises(context);
        }
        if (enableCloudRecords()) {
            saveRecords(context);
        }
        if (enableCloudTrainings()) {
            saveAllTrainings(context);
        }
        if (mBodyMeasure.enableCloud()) {
            mBodyMeasure.saveLog(context);
        }
        if (enableCloudFinishedWorkouts()) {
            saveFinishedWorkouts(context);
        }
    }

    private boolean enableCloudFinishedWorkouts() {
        boolean wasEnabled = false;
        for (Map.Entry<String, List<Workout>> entry : mFinishedWorkouts.entrySet()) {
            for (Workout workout: entry.getValue()) {
                if (workout.enableCloud()) {
                    wasEnabled = true;
                }
            }
        }
        return wasEnabled;
    }

    private boolean enableCloudTrainings() {
        boolean wasEnabled = false;
        for (Training training: mTrainings) {
            if (training.enableCloud()) {
                wasEnabled = true;
            }
        }
        return wasEnabled;
    }

    private boolean enableCloudRecords() {
        boolean wasEnabled = false;
        for (Map.Entry<String, Record> iRecord : mRecords.entrySet()) {
            Record record = iRecord.getValue();
            if (record.enableCloud()) {
                wasEnabled = true;
            }
        }
        return wasEnabled;
    }

    public boolean isExerciseInUse(String exerciseId) {
        for (Training training: mTrainings) {
            if (training.isDeleted()) {
                continue;
            }
            if (training.usesExercise(exerciseId)) {
                return true;
            }
        }
        for (Map.Entry<String, List<Workout>> entry : mFinishedWorkouts.entrySet()) {
            List<Workout> workouts = entry.getValue();
            if ((workouts != null) && (!workouts.isEmpty())) {
                if (isExerciseInUse(workouts, exerciseId)) {
                    return true;
                }
            }
        }
        return isExerciseInUse(mActiveWorkouts, exerciseId);
    }

    private boolean isExerciseInUse(List<Workout> workouts, String exerciseId) {
        for (Workout workout: workouts) {
            if (workout.isDeleted()) {
                continue;
            }
            if (workout.usesExercise(exerciseId)) {
                return true;
            }
        }
        return false;
    }

    private Sport mSport;
    private Body mBody;
    private Config mConfig;
    private List<Training> mTrainings = new ArrayList<>();
    private Map<String, Record> mRecords = new HashMap<>();
    private Map<String, Training> mTrainingsMap = new HashMap<>();
    private BodyMeasure mBodyMeasure;
    private List<Workout> mActiveWorkouts = new ArrayList<>();
    private Map<String, List<Workout>> mFinishedWorkouts = new HashMap<>();
    private Stamp mCloudInfo;
}
