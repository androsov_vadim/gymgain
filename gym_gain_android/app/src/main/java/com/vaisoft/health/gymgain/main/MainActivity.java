package com.vaisoft.health.gymgain.main;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.view.GravityCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.billing.Billing;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.coach.CoachService;
import com.vaisoft.health.gymgain.controls.CoachFragment;
import com.vaisoft.health.gymgain.controls.CoachListFragment;
import com.vaisoft.health.gymgain.controls.RotateFriendlyPagerAdapter;
import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.dialogs.AddBoosterDialog;
import com.vaisoft.health.gymgain.dialogs.ChooseMetricsDialog;
import com.vaisoft.health.gymgain.dialogs.ConfirmDialog;
import com.vaisoft.health.gymgain.dialogs.RateDialog;
import com.vaisoft.health.gymgain.edit.BodyLogActivity;
import com.vaisoft.health.gymgain.edit.CoachActivity;
import com.vaisoft.health.gymgain.edit.ExerciseDetailActivity;
import com.vaisoft.health.gymgain.edit.TrainingDetailActivity;
import com.vaisoft.health.gymgain.settings.SettingsActivity;
import com.vaisoft.health.gymgain.sync.BackgroundCloudUpdater;
import com.vaisoft.health.gymgain.utils.AboutActivity;
import com.vaisoft.health.gymgain.utils.Utils;

public class MainActivity extends CoachActivity
        implements SearchView.OnQueryTextListener,
                    RateDialog.Listener,
                    ConfirmDialog.Listener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            sActiveTab = savedInstanceState.getInt(ACTIVE_TAB, UNDEFINED_TAB);
        } else {
            Intent intent = getIntent();
            if (intent != null) {
                sActiveTab = intent.getIntExtra(ACTIVE_TAB, sActiveTab);
            }
        }
    }

    private void initAds() {
        mInterstitialAd = new InterstitialAd(this);
        String unitId = getString(R.string.ads_unit_id);
        mInterstitialAd.setAdUnitId(unitId);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });

        requestNewInterstitial();
    }

    private boolean isInitialized() {
        return mPageAdapter != null;
    }

    @Override
    public void onCoachConnected() {
        setEditTarget(null);
        boolean firstRun = false;
        if (isInitialized()) {
            getCoach().invalidateAllLists();
            CoachListFragment fragment = getActiveDataFragment();
            if (fragment != null) {
                fragment.update();
            }
        } else {
            initInterface();
            firstRun = doInitialActions();
        }
        if (!firstRun) {
            quickUpdateCoach();
        }
    }

    @Override
    protected void onPause() {
        mIsActive = false;
        super.onPause();
    }

    @Override
    protected void onResume() {
        mIsActive = true;
        super.onResume();
        if (isCoach() && (getCoach().getConfig().isFirstRun())) {
            return;
        }
        CoachService.scheduleUpdates(this);
        Intent intent = getIntent();
        boolean adsOrRateShown = false;
        if (intent != null) {
            boolean updatePurchases = intent.getBooleanExtra(UPDATE_PURCHASES, false);
            if (updatePurchases) {
                if ((mBilling != null) && mBilling.isBound()) {
                    mBilling.update();
                }
            } else {
                adsOrRateShown = showAdsOrRate();
            }
        } else {
            adsOrRateShown = showAdsOrRate();
        }
        if (!adsOrRateShown) {
            quickUpdateCoach();
        }
    }

    private void quickUpdateCoach() {
        if (!mIsActive) {
            return;
        }
        Coach coach = getCoach();
        if (coach != null) {
            coach.updateCloud(this, new BackgroundCloudUpdater.Listener() {
                @Override
                public void onIncomingData() {
                    askApplyIncomingData();
                }
            });
        }
    }

    private void askApplyIncomingData() {
        if (!mIsActive) {
            return;
        }
        confirm(R.string.confirm_apply_incoming_cloud, R.string.confirm_incoming_cloud, 0);
    }

    @Override
    public boolean onConfirmed(int code) {
        if (!mIsActive) {
            return false;
        }
        updateCloudApplyIncoming();
        return true;
    }

    @Override
    public boolean onCanceled(int code) {
        Coach coach = getCoach();
        if (coach != null) {
            coach.getConfig().onCloudSync();
        }
        return true;
    }

    private boolean showAdsOrRate() {
        if (timeToShowRateMe()) {
            showRateMe();
            return true;
        } else {
            return showAds();
        }
    }

    private void showRateMe() {
        RateDialog dialog = new RateDialog();
        dialog.show(getSupportFragmentManager(), "RateDialog");
    }

    private boolean showAds() {
        if (mInterstitialAd == null) {
            return false;
        }
        if (timeToShowAds()) {
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                return true;
            }
        }
        return false;
    }

    private boolean timeToShowAds() {
        if (!isCoach()) {
            return false;
        }
        return getCoach().getConfig().isTimeForAds();
    }

    private boolean timeToShowRateMe() {
        if (!isCoach()) {
            return false;
        }
        return getCoach().getConfig().isTimeToShowRateMe();
    }

    @Override
    public void onRateMeNow() {
        onRateMeNever();
        Utils.openStore(this);
    }

    @Override
    public void onRateMeLater() {
        Coach coach = getCoach();
        if (coach != null) {
            coach.getConfig().rateMeLater(this);
        }
    }

    @Override
    public void onRateMeNever() {
        Coach coach = getCoach();
        if (coach != null) {
            coach.getConfig().rateMeDisable(this);
        }
    }

    private boolean doInitialActions() {
        boolean chooseMetricsDialogShown = showChooseMetricsDialog();
        mBilling = new Billing(this, false, new Billing.ReadyListener() {
            @Override
            public void onBillingReady() {
                applyBilling();
            }
        });
        mBilling.bind();
        return chooseMetricsDialogShown;
    }

    private void applyBilling() {
        if (mBilling.isOk()) {
            int theme = mBilling.getTheme();
            if (applyTheme(theme)) {
                recreate();
                return;
            }
        }
        if (mBilling.isAdsEnabled()) {
            initAds();
        } else {
            mInterstitialAd = null;
        }
        if (mBilling.doShowShopButton()) {
            mShopButton.show();
        }
    }

    private boolean showChooseMetricsDialog() {
        Config cfg = getCoach().getConfig();
        if (!cfg.isFirstRun()) {
            return false;
        }
        cfg.save(this);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("ChooseMetricsDialog");
        if (fragment != null) {
            return true;
        }
        ChooseMetricsDialog dialog = new ChooseMetricsDialog();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(dialog, "ChooseMetricsDialog");
        ft.commitAllowingStateLoss();
        return true;
    }

    private void initInterface() {
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        ab.setHomeAsUpIndicator(R.drawable.ic_menu);
        ab.setDisplayHomeAsUpEnabled(true);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        setupDrawerContent(navigationView);

        initTabs();
        initAddButton();
        initShopButton();
    }

    private void initAddButton() {
        mAddButton = findViewById(R.id.add_entity);
        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddEntityDialog();
            }
        });
    }

    private void initShopButton() {
        mShopButton = findViewById(R.id.shop);
        mShopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startShopActivity();
            }
        });
    }

    private void initTabs() {
        ViewPager viewPager = findViewById(R.id.viewpager);
        mPageAdapter = new Adapter(getSupportFragmentManager(), getResources());
        viewPager.setAdapter(mPageAdapter);
        initTabs(viewPager);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (sActiveTab != UNDEFINED_TAB) {
            outState.putInt(ACTIVE_TAB, sActiveTab);
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int activeTab = savedInstanceState.getInt(ACTIVE_TAB, UNDEFINED_TAB);
        activateTab(activeTab);
    }

    public void chooseMetrics(boolean si) {
        Config config = getCoach().getConfig();
        if (si) {
            config.useSiUnits(true);
        } else {
            config.useUsUnits(true);
        }
        config.save(this);
        showAddEntityDialog();
    }

    public void showAddEntityDialog() {
        switch (sActiveTab) {
            case TAB_FEED:
                ((FeedFragment)getActiveDataFragment()).showAddActionDialog();
                break;
            case TAB_TRAINING:
                startActivity(new Intent(this, TrainingDetailActivity.class));
                break;
            case TAB_EXERCISE:
                startActivity(new Intent(this, ExerciseDetailActivity.class));
                break;
        }
    }

    private void initTabs(ViewPager viewPager) {
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        if (sActiveTab != UNDEFINED_TAB) {
            activateTab(tabLayout, sActiveTab);
        } else {
            sActiveTab = TAB_FEED;
        }

        tabLayout.addOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        super.onTabSelected(tab);
                        sActiveTab = tab.getPosition();
                        onTabObtainsFocus();
                        if (mSearchItem != null) {
                            mSearchItem.collapseActionView();
                        }
                    }
                });
    }

    private void onTabObtainsFocus() {
        switch (sActiveTab) {
            case TAB_FEED:
            case TAB_TRAINING:
            case TAB_EXERCISE:
                getActiveDataFragment().resetFilter();
                setAddButtonVisible(true);
                break;
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (sActiveTab) {
            case TAB_FEED:
            case TAB_TRAINING:
            case TAB_EXERCISE:
                getActiveDataFragment().onContextMenu(item.getItemId());
                break;
        }
        return super.onContextItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        FloatingActionButton fab = findViewById(R.id.add_entity);
        if (fab != null) {
            fab.setOnClickListener(null);
        }
        if (mBilling != null) {
            mBilling.unbind();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        mSearchItem = menu.findItem(R.id.menu_item_search);
        SearchView searchView = (SearchView) mSearchItem.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        switch (sActiveTab)
        {
            case TAB_FEED:
            case TAB_TRAINING:
            case TAB_EXERCISE:
                getActiveDataFragment().applyFilter(newText);
                return  true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.menu_settings:
                startSettingsActivityConfig();
                return true;
            case R.id.menu_cloud:
                startSettingsActivityCloud();
                return true;
            case R.id.menu_about:
                startAboutActivity();
                return true;
            case R.id.menu_shop:
                startShopActivity();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void startSettingsActivityCloud() {
        Intent intent = SettingsActivity.getStartIntentCloud(this);
        startActivity(intent);
    }

    private void startSettingsActivityConfig() {
        Intent intent = SettingsActivity.getStartIntentConfig(this);
        startActivity(intent);
    }

    private void startBodyLogActivity() {
        Intent intent = new Intent(this, BodyLogActivity.class);
        startActivity(intent);
    }

    void startAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    void startShopActivity() {
        Billing.startShop(this);
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_body_log:
                                startBodyLogActivity();
                                break;
                            case R.id.nav_add_entity:
                                showAddEntityDialog();
                                break;
                            case R.id.nav_add_booster:
                                showAddBoosterDialog();
                                break;
                            case R.id.nav_settings:
                                startSettingsActivityConfig();
                                break;
                            case R.id.nav_cloud:
                                startSettingsActivityCloud();
                                break;
                            case R.id.nav_about:
                                startAboutActivity();
                                break;
                            case R.id.nav_shop:
                                startShopActivity();
                                break;
                            case R.id.nav_tutorial:
                                Utils.openTutorial(MainActivity.this);
                                break;
                            case R.id.nav_social:
                                Utils.openSocial(MainActivity.this);
                                break;
                            default:
                                Log.w(getClass().getName(), "Unknown drawer command: " + menuItem.getTitle());
                        }
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });
    }

    public void showAddBoosterDialog() {
        AddBoosterDialog dialog = new AddBoosterDialog();
        dialog.show(getSupportFragmentManager(), "AddBoosterDialog");
    }

    public void activateTab(int tabIndex) {
        final TabLayout tabLayout = findViewById(R.id.tabs);
        if (tabLayout != null) {
            activateTab(tabLayout, tabIndex);
        } else {
            Log.e(getClass().getName(), "Tab layout not found");
        }
    }

    private void activateTab(TabLayout tabLayout, int tabIndex) {
        TabLayout.Tab tab = tabLayout.getTabAt(tabIndex);
        tab.select();
    }

    private CoachListFragment getActiveDataFragment() {
        Fragment fragment = mPageAdapter.getFragment(sActiveTab);
        return (CoachListFragment) fragment;
    }

    private void setAddButtonVisible(boolean addButtonShouldBeVisible) {
        boolean visible = mAddButton.getVisibility() == View.VISIBLE;
        if (visible == addButtonShouldBeVisible) {
            return;
        }
        if (addButtonShouldBeVisible) {
            mAddButton.show();
        } else {
            mAddButton.hide();
        }
    }

    private class Adapter extends RotateFriendlyPagerAdapter {

        public Adapter(FragmentManager fm, Resources resources) {
            super(fm);
            mResources = resources;
        }

        @Override
        public CoachFragment getItem(int position) {
            switch (position) {
                case TAB_FEED:
                    return new FeedFragment();
                case TAB_TRAINING:
                    return new TrainingListFragment();
                case TAB_EXERCISE:
                    return new ExcerciseListFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return TAB_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            int rText = 0;
            switch (position) {
                case TAB_FEED:
                    rText = R.string.tab_feed;
                    break;
                case TAB_TRAINING:
                    rText = R.string.tab_trainings;
                    break;
                case TAB_EXERCISE:
                    rText = R.string.tab_excercises;
                    break;
            }
            return mResources.getString(rText);
        }

        private final Resources mResources;
    }


    private void requestNewInterstitial() {
        AdRequest.Builder builder = new AdRequest.Builder();
        //builder.addTestDevice(AdRequest.DEVICE_ID_EMULATOR);
        String keywordsList = getString(R.string.ads_keywords);
        if ((keywordsList != null) && (!keywordsList.isEmpty())) {
            String keywords[] = keywordsList.split(" ");
            for (String keyword: keywords) {
                builder.addKeyword(keyword);
            }
        }

        AdRequest adRequest = builder.build();
        mInterstitialAd.loadAd(adRequest);
    }


    private DrawerLayout mDrawerLayout;
    private MenuItem mSearchItem;
    private Adapter mPageAdapter;
    private FloatingActionButton mAddButton;
    private FloatingActionButton mShopButton;
    private InterstitialAd mInterstitialAd;
    private Billing mBilling;
    private boolean mIsActive;

    public static final String ACTIVE_TAB = "activeTab";
    public static final String UPDATE_PURCHASES = "updatePurchases";


    private static final int UNDEFINED_TAB = -1;
    private static int sActiveTab = UNDEFINED_TAB;

    public   static final int TAB_FEED = 0;
    public   static final int TAB_TRAINING = 1;
    public   static final int TAB_EXERCISE = 2;
    private  static final int TAB_COUNT = 3;
}
