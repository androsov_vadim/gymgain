package com.vaisoft.health.gymgain.controls;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.edit.ExerciseDetailActivity;
import com.vaisoft.health.gymgain.utils.Utils;

public class ExerciseParameter extends LinearLayout {
    public ExerciseParameter(Context context) {
        super(context);
        init(context);
    }

    public ExerciseParameter(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ExerciseParameter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        View.inflate(context, R.layout.excercise_parameter, this);
        mOn = (CheckBox) findViewById(R.id.parameter);
        mOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (mListener != null) {
                    mListener.onEnabled(isChecked);
                }
                setTargetSelectEnabled(isChecked);
                setSelected(isChecked);
            }
        });
        mSpinnerInitialized = false;
        mTargetValue = (TextView) findViewById(R.id.target_value);
        mTarget = (Spinner) findViewById(R.id.target);
        mTarget.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!mSpinnerInitialized) {
                    mSpinnerInitialized = true;
                    return;
                }
                if (mListener != null) {
                    mListener.onTarget(Value.getTargetForIndex(position));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setTargetSelectEnabled(boolean isEnabled) {
        mTarget.setEnabled(isEnabled);
        mTargetValue.setClickable(isEnabled);
        mTargetValue.setEnabled(isEnabled);
    }

    public void setEditable(boolean editable) {
        mOn.setClickable(editable);
        if (!editable) {
            setTargetSelectEnabled(false);
        }
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public void init(boolean isOn, Value value, final Unit parameter) {
        mOn.setText(getActivity().localize(parameter.toString()));
        mOn.setChecked(isOn);
        setTargetSelectEnabled(isOn);
        setSelected(isOn);
        mTarget.setSelection(Value.getTargetIndex(value.shouldGrow()));
        setTargetSelectEnabled(isOn);
        mTargetValue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().showTargetEditDialog(parameter);
            }
        });
        update(value);
    }

    public void update(Value value) {
        if (value.isZero()) {
            mTargetValue.setText(R.string.set_target);
        } else {
            StringBuilder text = new StringBuilder(getActivity().getString(R.string.exercise_target));
            text.append(": ");
            text.append(value.toString());
            text.append(' ');
            text.append(getActivity().localize(value.unit().toString()));
            mTargetValue.setText(text.toString());
        }
    }

    private ExerciseDetailActivity getActivity() {
        Context context = getContext();
        return (ExerciseDetailActivity) Utils.getActivity(context);
    }

    public interface Listener {
        void onEnabled(boolean enabled);
        void onTarget(boolean grow);
    }

    private CheckBox mOn;
    private Spinner mTarget;
    private TextView mTargetValue;
    private boolean mSpinnerInitialized;
    private Listener mListener;
}
