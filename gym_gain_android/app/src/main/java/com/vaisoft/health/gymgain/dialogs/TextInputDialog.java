package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.text.InputType;
import android.widget.EditText;

import com.vaisoft.health.gymgain.R;

public class TextInputDialog extends DialogFragment
{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Bundle bundle = getArguments();
        int title = bundle.getInt(TITLE);
        final int code = bundle.getInt(CODE);
        builder.setTitle(title);

        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_TEXT_FLAG_AUTO_CORRECT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        String defaultValue = bundle.getString(VALUE);
        if (defaultValue != null) {
            input.setText(defaultValue);
        }
        builder.setView(input);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                listener.onTextInput(code, input.getText().toString());
            }
        });
        builder.setNegativeButton(R.string.cancel, null);

        return builder.create();
    }

    public interface Listener {
        void onTextInput(int code, String text);
    }

    public static final String TITLE = "title";
    public static final String VALUE = "value";
    public static final String CODE = "code";
}
