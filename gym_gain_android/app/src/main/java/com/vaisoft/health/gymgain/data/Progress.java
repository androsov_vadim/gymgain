package com.vaisoft.health.gymgain.data;

public class Progress {

    public static Progress stable() {
        return new Progress(Estimate.SOSO, Change.STABLE);
    }

    public static Progress initial() {
        return new Progress(Estimate.GOOD, Change.STABLE);
    }

    public static Progress positiveSummaryIndex(int index) {
        if (index == 0) {
            return stable();
        }
        return changed(index > 0, true);
    }

    public static Progress changed(boolean grow, boolean shouldGrow) {
        Change change = grow ? Change.MORE : Change.LESS;
        Estimate estimate = (grow == shouldGrow) ? Estimate.GOOD : Estimate.BAD;
        return new Progress(estimate, change);
    }

    public static Progress failedGrowth () {
        return changed(false, true);
    }

    public static Progress okGrowth () {
        return changed(true, true);
    }

    private Progress(Estimate estimate, Change change) {
        mEstimate = estimate;
        mChange = change;
    }

    public Estimate getEstimate() {
        return mEstimate;
    }

    public Change getChange() {
        return mChange;
    }

    public enum Estimate {
        GOOD, SOSO, BAD
    }

    public enum Change {
        MORE, STABLE, LESS
    }

    private Estimate mEstimate;
    private Change mChange;
}

