package com.vaisoft.health.gymgain.controls;

import android.view.View;
import android.widget.TextView;

import com.vaisoft.health.gymgain.utils.Ticker;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;

public class TextTimeTicker implements Ticker.Listener {
    public TextTimeTicker(TextView textView) {
        mTextView = textView;
        reset();
    }

    public void reset() {
        stop();
        mTextView.setText(getElapsedFormat(0));
    }

    public void setInitialDate(DateTime initialDate) {
        mInitialDate = initialDate;
        tick();
    }

    public void start() {
        if (mTicker == null) {
            mTicker = new Ticker(this, 1000);
        }
        mTicker.start();
    }

    public void stop() {
        if (mTicker != null) {
            mTicker.stop();
        }
    }

    private static String getElapsedFrom(DateTime date) {
        long diff = (Utils.now().getMillis() - date.getMillis());
        return  Utils.getElapsedFormat(diff, true, false);
    }

    private static String getElapsedFormat(long diff) {
        return Utils.getElapsedFormat(diff, true, false);
    }

    public void setInterval(DateTime startedDate, DateTime finishedDate) {
        mTextView.setText(Utils.getElapsedFormat(startedDate, finishedDate, true));
    }

    @Override
    public void tick() {
        mTextView.setText(getElapsedFrom(mInitialDate));
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        mTextView.setOnClickListener(onClickListener);
    }

    private DateTime mInitialDate;
    private Ticker mTicker;
    private TextView mTextView;
}
