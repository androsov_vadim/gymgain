package com.vaisoft.health.gymgain.data;

import android.text.InputType;
import android.util.Log;

import com.vaisoft.health.gymgain.utils.Utils;

public enum Unit {
    centimeter,
    inch,
    meter,
    foot,
    kilogram,
    pound,
    size,
    weight,
    distance,
    reps,
    time,
    undefined;

    public static Unit[] parametersOrder = {reps, weight, distance, time};

    public Unit getCategory() {
        switch (this) {
            case centimeter:
            case inch:
                return size;
            case meter:
            case foot:
                return distance;
            case kilogram:
            case pound:
                return weight;
            default:
                return this;
        }
    }

    public void setRule(Config.UnitRule rule) {
        mRule = rule;
    }

    public String toString(long value) {
        if (this == time) {
            return Utils.getElapsedFormat(value, false, true);
        }
        if (mRule.isTextFormat()) {
            return mRule.toText(value);
        }
        return getCategory().toString(value);
    }

    public long parseString(String text) {
        try {
            if (this == time) {
                return Utils.parseElapsedFormat(text);
            }
            if (mRule.isTextFormat()) {
                return mRule.parseText(text);
            }
            return getCategory().parseString(text);
        } catch (NumberFormatException e) {
            Log.e(getClass().getName(), e.getMessage());
            return 0;
        }
    }

    public int getValueInputType() {
        if (this == time) {
            return InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_NORMAL;
        }
        if (mRule.isTextFormat()) {
            return mRule.getValueInputType();
        }
        return getCategory().getValueInputType();
    }

    public int[] getStep() {
        if (mRule.step != null) {
            return mRule.step;
        }
        if (getCategory() != undefined) {
            return getCategory().getStep();
        }
        return null;
    }

    public long toInternalFormat(double source) {
        if ((mRule != null) && mRule.isTextFormat()) {
            return mRule.toInternalFormat(source);
        }
        if (getCategory() != undefined) {
            return getCategory().toInternalFormat(source);
        }
        return -1;
    }

    private Config.UnitRule mRule;
}
