package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import com.vaisoft.health.gymgain.R;

public class MusclesDialog extends MultySelectDialog {
    @Override
    protected int configDialogCreate(AlertDialog.Builder builder) {
        return R.string.select_muscles;
    }
}
