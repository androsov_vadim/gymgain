package com.vaisoft.health.gymgain.data;

import android.util.Log;

import com.vaisoft.health.gymgain.sync.JSONLoader;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Workout extends Lifecycle implements Cloneable {

    public Workout(Exercise exercise) {
        this(Source.createExercise(exercise.getId()));
        mPlan.add(new Entry(mSource.getId()));
    }

    public Workout(Training training) {
        this(Source.createTraining(training.getId()));
        for (int i = 0; i < training.getPlanSize(); i++) {
            Training.PlanEntry stageContent = training.getPlanItemContent(i);
            add(stageContent.getItems(), stageContent.getPrecisePlan(), stageContent.getComment());
        }
    }

    public Workout(Source source) {
        mSource = source.clone();
    }

    public static Workout burnOut(Source source, DateTime date, String comment) {
        Workout workout = new Workout(source);
        workout.mStartedDate = new DateTime(date);
        workout.setFinishedDate(date);
        workout.mComment = comment;
        return workout;
    }

    private Workout(JSONObject source) throws JSONException {
        mSource = new Source(source);
        mStartedDate = new DateTime(source.getLong(FIELD_STARTED_DATE));
        loadLifecycle(source);
        if (!isDeleted()) {
            if (source.has(FIELD_COMMENT)) {
                mComment = source.getString(FIELD_COMMENT);
            }
            if (source.has(FIELD_FINISHED_DATE)) {
                mFinishedDate = new DateTime(source.getLong(FIELD_FINISHED_DATE));
            }
            JSONArray planArray = source.getJSONArray(FIELD_PLAN);
            for (int i = 0; i < planArray.length(); i++) {
                JSONObject entryObject = planArray.getJSONObject(i);
                Entry entry = new Entry(entryObject);
                mPlan.add(entry);
            }
        }
    }

    public static void load(JSONLoader loader, String fileName, OnLoadedListener listener) {
        JSONArray workouts = loader.loadArray(fileName, Utils.FileTarget.FILE_TARGET_DYNAMIC);
        if (workouts == null) {
            return;
        }
        try {
            for (int i=0; i < workouts.length(); i++) {
                JSONObject theObject = workouts.getJSONObject(i);
                Workout workout = new Workout(theObject);
                listener.onLoaded(workout);
            }
        } catch (JSONException e) {
            Log.e(Workout.class.getName(), e.getMessage());
        }
    }

    public static void addSourceFilesToList(List<String> list) {
        list.add(FINISHED_FILE_NAME);
    }

    public Workout(Workout workout) {
        mSource = workout.mSource.clone();
        mComment = workout.getComment();
        if (workout.mStartedDate != null) {
            mStartedDate = new DateTime(workout.mStartedDate);
        }
        if (workout.mFinishedDate != null) {
            mFinishedDate = new DateTime(workout.mFinishedDate);
        }
        for (Entry entry: workout.mPlan) {
            Entry newEntry = new Entry(entry);
            mPlan.add(newEntry);
        }
    }

    private void add(List<String> exercises, int[] plan, String comment) {
        Entry entry = new Entry(exercises, plan, comment);
        mPlan.add(entry);
    }

    public void save(JSONObject workoutObject) throws JSONException {
        mSource.save(workoutObject);
        workoutObject.put(FIELD_STARTED_DATE, mStartedDate.getMillis());
        saveLifecycle(workoutObject);
        if (!isDeleted()) {
            if (mComment != null) {
                workoutObject.put(FIELD_COMMENT, mComment);
            }
            if (mFinishedDate != null) {
                workoutObject.put(FIELD_FINISHED_DATE, mFinishedDate.getMillis());
            }
            JSONArray planArray = new JSONArray();
            for (Entry entry : mPlan) {
                JSONObject entryObject = new JSONObject();
                entry.save(entryObject);
                planArray.put(entryObject);
            }
            workoutObject.put(FIELD_PLAN, planArray);
        }
    }

    public Source getSource() {
        return mSource;
    }

    public int getItemsCount() {
        return mPlan.size();
    }

    public Workout.Entry getEntry(int set) {
        return mPlan.get(set);
    }

    public Workout.Entry getSameEntry(Workout.Entry target) {
        for (Entry entry: mPlan) {
            if ((entry.log.size() == target.log.size()) && entry.containsExercisesOf(target)) {
                return entry;
            }
        }
        return null;
    }

    public List<String> getLogContent(int set) {
        List<ExerciseLogItem> log = mPlan.get(set).log;
        List<String> result = new ArrayList<>();
        for (ExerciseLogItem logItem: log) {
            result.add(logItem.exercise);
        }
        return result;
    }

    public void start() {
        mStartedDate = Utils.now();
    }

    public DateTime getStartedDate() {
        return new DateTime(mStartedDate);
    }

    public void finish() {
        setFinishedDate(Utils.now());
        if (mPlan != null) {
            for (Entry entry: mPlan) {
                entry.applyActiveItems();
            }
        }
     }

    public void setFinishedDate(DateTime date) {
        mFinishedDate = new DateTime(date);
    }

    public DateTime getFinishedDate() {
        return new DateTime(mFinishedDate);
    }

    @Override
    public Workout clone() {
       return new Workout(this);
    }

    public Workout createBlank() {
        Workout workout = new Workout(mSource);
        for (Entry entry: mPlan) {
            Entry newEntry = entry.createBlank();
            workout.mPlan.add(newEntry);
        }
        return workout;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        mComment = comment;
    }

    public Progress compareTo(Workout previous, Config config) {
        if (!isResults()) {
            return Progress.failedGrowth();
        }
        if (previous == null) {
             return Progress.initial();
        }
        int index = 0;
        for (Entry entry: mPlan) {
            Entry previousEntry = previous.getSameEntry(entry);
            index += getIndexChangeForEntries(entry, previousEntry, config);
        }
        return Progress.positiveSummaryIndex(index);
    }

    public ProgressStatistics getProgressStatistics(Workout previous, Config config) {
        ProgressStatistics result = new ProgressStatistics();
        for (Entry entry: mPlan) {
            Entry previousEntry = previous.getSameEntry(entry);
            int index = getIndexChangeForEntries(entry, previousEntry, config);
            updateStatictics(result, entry, index);
        }
        return result;
    }

    private void updateStatictics(ProgressStatistics statistics, Entry entry, int index) {
        switch (index) {
            case 0:
                addExercisesAsSoso(statistics, entry);
                break;
            case 1:
                addExercisesAsGood(statistics, entry);
                break;
            case -1:
                addExercisesAsBad(statistics, entry);
                break;
        }
    }

    private void addExercisesAsSoso(ProgressStatistics statistics, Entry entry) {
        for (ExerciseLogItem item: entry.log) {
            statistics.addSoso(item.exercise);
        }
    }

    private void addExercisesAsGood(ProgressStatistics statistics, Entry entry) {
        for (ExerciseLogItem item: entry.log) {
            statistics.addGood(item.exercise);
        }
    }

    private void addExercisesAsBad(ProgressStatistics statistics, Entry entry) {
        for (ExerciseLogItem item: entry.log) {
            statistics.addBad(item.exercise);
        }
    }

    private static int getIndexChangeForEntries(Entry current, Entry previous, Config config) {
        if ((previous != null) && previous.isResults()) {
            if (!current.isResults()) {
                return -1;
            }
            if (current.isSummaryEstimation()) {
                current = current.getSummary();
            }
            if (previous.isSummaryEstimation()) {
                previous = previous.getSummary();
            }
            Progress progress = current.compareTo(previous, config);
            if (progress.getEstimate() == Progress.Estimate.GOOD) {
                return 1;
            } else if (progress.getEstimate() == Progress.Estimate.BAD) {
                return -1;
            }
        } else if (current.isResults()) {
            return 1;
        }
        return 0;
    }

    public double lastsDays() {
        double diff = Utils.now().getMillis() - mStartedDate.getMillis();
        return diff / (24 * 60 * 60 * 1000);
    }

    public DateTime getEffectiveDate() {
        if (getFinishedDate() == null) {
            return getStartedDate();
        } else {
            return getFinishedDate();
        }
    }

    public boolean isResults() {
        for (Entry entry : mPlan) {
            if (entry.isResults()) {
                return true;
            }
        }
        return false;
    }

    public boolean isBurnedOut() {
        return mPlan.isEmpty();
    }

    public void testBurnOut() {
        mPlan.clear();
    }

    public boolean usesExercise(String exerciseId) {
        for (Entry entry: mPlan) {
            for (ExerciseLogItem item: entry.log) {
                if (item.exercise.equals(exerciseId)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void applyActiveValuesExcept(Entry exceptEntry) {
        for (Entry entry: mPlan) {
            if (entry == exceptEntry) {
                continue;
            }
            for (ExerciseLogItem item: entry.log) {
                for (Set set: item.sets) {
                    set.applyActiveValues(false);
                }
            }
        }
    }

    public int indexIn(List<Workout> workouts) {
        int index = 0;
        for (Workout workout: workouts) {
            if ((!workout.isDeleted()) && workout.getStartedDate().equals(getStartedDate())) {
                return index;
            }
            ++index;
        }
        return -1;
    }

    public boolean renameExercise(String exerciseId, String previousId) {
        boolean changes = false;
        if (mSource.isForExercise(previousId)) {
            mSource = Source.createExercise(exerciseId);
            changes = true;
        }
        for (Entry entry: mPlan) {
            for (ExerciseLogItem item: entry.log) {
                if (item.exercise.equals(previousId)) {
                    item.exercise = exerciseId;
                    changes = true;
                }
            }
        }
        return changes;
    }

    public boolean renameTraining(String trainingId, String previousId) {
        if (mSource.isForTraining(previousId)) {
            mSource = Source.createTraining(trainingId);
            return true;
        }
        return false;
    }

    //--------------------------------------------------------------------------------------------------

    public static class Set implements Cloneable {

        public Map<Unit, Value> result = new HashMap<>();
        public int plannedReps;

        public Set() {

        }

        public boolean isActiveValue() {
            for (Map.Entry<Unit, Value> entry : result.entrySet()) {
                if (entry.getValue().isActive()) {
                    return true;
                }
            }
            return false;
        }

        public void apply(Set set) {
            for (Map.Entry<Unit, Value> entry : set.result.entrySet()) {
                result.put(entry.getKey(), entry.getValue().clone());
            }
        }

        public boolean isResults() {
            return !result.isEmpty();
        }

        public Set(Set source) {
            plannedReps = source.plannedReps;
            for (Map.Entry<Unit, Value> entry : source.result.entrySet()) {
                result.put(entry.getKey(), entry.getValue().clone());
            }
        }

        public java.util.Set<Value> applyActiveValues(boolean returnApplied) {
            java.util.Set<Value> applied = null;
            if (returnApplied) {
                applied =  new HashSet<>();
            }
            for (Map.Entry<Unit, Value> entry : result.entrySet()) {
                Value value = entry.getValue();
                if (value.isActive()) {
                    value.apply();
                    if (returnApplied) {
                        applied.add(value);
                    }
                } else if (!value.isApplied()) {
                    value.apply();
                }
            }
            return applied;
        }

        public void activateValues(java.util.Set<Value> applied) {
            for (Value appliedValue: applied) {
                Unit unit = appliedValue.unit();
                Value value = result.get(unit);
                if (value == null) {
                    Value newValue = new Value(unit, 0, appliedValue.shouldGrow(), Value.Status.Progress);
                    result.put(unit, newValue);
                }
            }
        }

        public Set(JSONObject source) throws JSONException {
            plannedReps = source.getInt(FIELD_PLANNED);
            if (source.has(FIELD_RESULTS)) {
                JSONObject resultsObject = source.getJSONObject(FIELD_RESULTS);
                Iterator<String> keys = resultsObject.keys();
                while(keys.hasNext()){
                    String metric = keys.next();
                    JSONObject valueObject = resultsObject.getJSONObject(metric);
                    Value value = new Value(valueObject);
                    result.put(Unit.valueOf(metric), value);
                }
            }
        }

        public void save(JSONObject target) throws JSONException {
            target.put(FIELD_PLANNED, plannedReps);
            if ((result != null)) {
                JSONObject resultsObject = new JSONObject();
                for (Map.Entry<Unit, Value> entry : result.entrySet()) {
                    JSONObject valueObject = new JSONObject();
                    entry.getValue().save(valueObject);
                    resultsObject.put(entry.getKey().toString(), valueObject);
                }
                target.put(FIELD_RESULTS, resultsObject);
            }
        }

        public Set createBlank() {
            Set newSet = new Set();
            newSet.plannedReps = plannedReps;
            return newSet;
        }

        @Override
        public Set clone() {
            Set set = new Set();
            set.plannedReps = plannedReps;
            for (Map.Entry<Unit, Value> entry : result.entrySet()) {
                set.result.put(entry.getKey(), entry.getValue().clone());
            }
            return set;
        }

        public Progress compareTo(Set previousSet, Config config) {
            if (previousSet == null) {
                return Progress.initial();
            }

            Progress result = compareRepsWeight(previousSet, config);
            if (result != null) {
                return result;
            }

            result = compareTimeDistance(previousSet);
            if (result != null) {
                return result;
            }

            int index = getCompareIndex(previousSet, config);
            if (index == 0) {
                return Progress.stable();
            }

            return Progress.changed(index > 0, true);
        }

        private Progress compareTimeDistance(Set previousSet) {
            Unit timeUnits = getUnitForParameter(Unit.time);
            Unit distanceUnits = getUnitForParameter(Unit.distance);
            if ((distanceUnits != null) && (timeUnits != null)) {
                if (result.size() == 2) {
                    return getTimeDistancePatternIndex(previousSet, timeUnits, distanceUnits);
                }
            }
            return null;
        }

        private Progress getTimeDistancePatternIndex(Set previousSet, Unit timeUnits, Unit distanceUnits) {
            return getPatternIndex(previousSet, distanceUnits, timeUnits);
        }

        private Progress compareRepsWeight(Set previousSet, Config config) {
            Unit repsUnits = getUnitForParameter(Unit.reps);
            Unit weightUnits = getUnitForParameter(Unit.weight);
            if ((weightUnits != null) && (repsUnits != null)) {
                int minReps = config.getMinReps();
                if (!isMinRepsOk(minReps)) {
                    return Progress.failedGrowth();//current set does not have enough reps
                }
                if (!previousSet.isMinRepsOk(minReps)) { //current set have enough reps but previous doesn't
                    return Progress.okGrowth();
                }
                if (result.size() == 2) {
                    return getRepsWeightPatternIndex(previousSet, repsUnits, weightUnits, config);
                }
            }
            return null;
        }

        private int getCompareIndex(Set previousSet, Config config) {
            float percent = 0;
            for (Map.Entry<Unit, Value> entry : result.entrySet()) {
                Value current = entry.getValue();
                Value previous = getSameValue(current, previousSet);
                if (previous == null) {
                    percent += 1;
                    continue;
                }
                Unit unit = current.unit();
                long diff = current.get(unit) - previous.get(unit);
                if (diff != 0) {
                    float diffPercent = Math.abs((float)diff) / current.get(unit);
                    Progress progress = Progress.changed(diff > 0, current.shouldGrow());
                    if (progress.getEstimate() == Progress.Estimate.GOOD) {
                        percent += diffPercent;
                    } else if (progress.getEstimate() == Progress.Estimate.BAD) {
                        percent -= diffPercent;
                    }
                }
            }
            if (percent == 0) {
                return 0;
            }
            return percent > 0 ? 1 : -1;
        }

        public boolean isMinRepsOk(int minReps) {
            Unit repsUnits = getUnitForParameter(Unit.reps);
            if (repsUnits != null) {
                if (plannedReps != 0) {
                    minReps = plannedReps;
                }
                Value reps = getValue(repsUnits);
                if (reps.get(repsUnits) < minReps) { //not enough reps: progress-
                    return false;
                }
            }
            return true;
        }

        private Progress getRepsWeightPatternIndex(Set previousSet, Unit repsUnits, Unit weightUnits, Config config) {
            int minReps = config.getMinReps();
            if (plannedReps != 0) {
                minReps = plannedReps;
            }
            Value reps = getValue(repsUnits);
            if (reps.get(repsUnits) < minReps) { //not enough reps: progress-
                return Progress.failedGrowth();
            }
            return getPatternIndex(previousSet, weightUnits, repsUnits);
        }

        private Progress getPatternIndex(Set previousSet, Unit primaryUnits, Unit secondaryUnits) {
            Value primary = getValue(primaryUnits);
            Value previousPrimary = getSameValue(primary, previousSet);
            if (previousPrimary == null) { //no previous value: progress +
                return Progress.okGrowth();
            }
            Value secondary = getValue(secondaryUnits);
            Value previousSecondary = getSameValue(secondary, previousSet);
            if (previousSecondary == null) { //no previous value: progress +
                return Progress.okGrowth();
            }
            Progress result = getNonStableResult(primaryUnits, primary, previousPrimary);
            if (result != null) {
                return result;
            }
            result = getNonStableResult(secondaryUnits, secondary, previousSecondary);
            if (result != null) {
                return result;
            }
            return Progress.stable();
        }

        private Progress getNonStableResult(Unit units, Value current, Value previous) {
            long currentValue = current.get(units);
            long previousValue = previous.get(units);
            if (currentValue == previousValue) {
                return null;
            }
            return Progress.changed(currentValue > previousValue, current.shouldGrow());
        }

        private Unit getUnitForParameter(Unit parameter) {
            Value value = result.get(parameter);
            if (value != null) {
                return value.unit();
            }
            return null;
        }

        public static Value getSameValue(Value value, Set source) {
            Unit unit = value.unit();
            Value previousValue = source.getValue(unit);
            if (previousValue != null) {
                long prevValue = previousValue.get(unit);
                boolean shouldGrow = value.shouldGrow();
                Value sameValue = new Value(unit, prevValue, shouldGrow, Value.Status.Undefined);
                return sameValue;
            }
            return null;
        }

        public boolean isComplete(List<Unit> exerciseParameters) {
            for (Unit parameter: exerciseParameters) {
                if (!result.containsKey(parameter)) {
                    return false;
                }
            }
            return true;
        }

        public Value getValue(Unit unit) {
            return result.get(unit.getCategory());
        }

        public Value getValueForCategory(Unit categoty) {
            return result.get(categoty);
        }

        public void putValue(Value value) {
            result.put(value.unit().getCategory(), value);
        }

        public void add(Set set) {
            for (Map.Entry<Unit, Value> entry : set.result.entrySet()) {
                Value value = entry.getValue();
                if (result.containsKey(entry.getKey())) {
                    Value currentValue = result.get(entry.getKey());
                    long raw = currentValue.get(currentValue.unit()) + value.get(currentValue.unit());
                    currentValue.set(currentValue.unit(), raw);
                } else {
                    result.put(entry.getKey(), value.clone());
                }
            }
        }

        private static final String FIELD_PLANNED = "planned";
        private static final String FIELD_RESULTS = "results";
    }

    public static class ExerciseLogItem {
        public String exercise;
        public List<Set> sets = new ArrayList<>();

        public ExerciseLogItem() {
        }

        public boolean isActiveValue() {
            for (Set set: sets) {
                if (set.isActiveValue()) {
                    return true;
                }
            }
            return false;
        }

        public ExerciseLogItem(ExerciseLogItem source) {
            exercise = source.exercise;
            for (Set set : source.sets) {
                Set newSet = new Set(set);
                sets.add(newSet);
            }
        }

        public ExerciseLogItem(JSONObject source) throws JSONException {
            exercise = source.getString(FIELD_EXERCISE);
            JSONArray repsArray = source.getJSONArray(FIELD_REPS);
            for (int j = 0; j < repsArray.length(); j++) {
                JSONObject repObject = repsArray.getJSONObject(j);
                Set set = new Set(repObject);
                sets.add(set);
            }
        }

        public void save(JSONObject target) throws JSONException {
            JSONArray repsArray = new JSONArray();
            target.put(FIELD_EXERCISE, exercise);
            for (Set set : sets) {
                JSONObject repObject = new JSONObject();
                set.save(repObject);
                repsArray.put(repObject);
            }
            target.put(FIELD_REPS, repsArray);
        }

        public ExerciseLogItem createBlank() {
            ExerciseLogItem newItem = new ExerciseLogItem();
            newItem.exercise = exercise;
            for (Set set : sets) {
                if (set.plannedReps > 0) {
                    Set newSet = set.createBlank();
                    newItem.sets.add(newSet);
                }
            }
            return newItem;
        }

        public Set getLastSet() {
            return getSet(sets.size() - 1);
        }

        public Set getSet(int index) {
            if (sets.size() <= index) {
                Log.e(getClass().getName(), "Attempt to access non-existing set");
                return null;
            }
            return sets.get(index);
        }

        public void applyActiveSets() {
            for (Set set : sets) {
                set.applyActiveValues(false);
            }
        }

        public boolean isResults() {
            for (Set set : sets) {
                if (set.isResults()) {
                    return true;
                }
            }
            return false;
        }

        public ExerciseLogItem getSummary() {
            ExerciseLogItem newItem = new ExerciseLogItem();
            newItem.exercise = exercise;
            Set newSet = new Set();
            newItem.sets.add(newSet);
            for (Set set: sets) {
                newSet.add(set);
            }
            return newItem;
        }

        private static final String FIELD_EXERCISE = "exercise";
        private static final String FIELD_REPS = "reps";
    }

    public static class  Entry {
        public String setComment = "";
        public String postComment = "";
        public List<ExerciseLogItem> log = new ArrayList<>();

        public Entry(JSONObject source) throws JSONException {
            setComment = readString(source, FIELD_PRE_COMMENT);
            postComment = readString(source, FIELD_POST_COMMENT);
            JSONArray logArray = source.getJSONArray(FIELD_LOG);
            for (int i = 0; i < logArray.length(); i++) {
                JSONObject logItemObject = logArray.getJSONObject(i);
                ExerciseLogItem logItem = new ExerciseLogItem(logItemObject);
                log.add(logItem);
            }
        }

        public boolean isSummaryEstimation() {
            if (log.isEmpty()) {
                return false;
            }
            for (ExerciseLogItem item: log) {
                for (Set set: item.sets) {
                    if (set.result.isEmpty() || (set.result.size() > 2)) {
                        return false;
                    }
                    Unit distanceUnit = set.getUnitForParameter(Unit.distance);
                    if (distanceUnit == null) {
                        return false;
                    }
                    if (set.result.size() == 2) {
                        Unit timeUnit = set.getUnitForParameter(Unit.time);
                        if (timeUnit == null) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        public Entry getSummary() {
            Entry entry = new Entry();
            for (ExerciseLogItem item: log) {
                ExerciseLogItem newItem = item.getSummary();
                entry.log.add(newItem);
            }
            return entry;
        }

        public void applyActiveItems() {
            for (ExerciseLogItem logItem: log) {
                logItem.applyActiveSets();
            }
        }

        public Entry(Entry entry) {
            setComment = entry.setComment;
            postComment = entry.postComment;
            for (ExerciseLogItem logItem: entry.log) {
                ExerciseLogItem newLogItem = new ExerciseLogItem(logItem);
                log.add(newLogItem);
            }
        }

        public Entry() {
        }

        public Entry createBlank() {
            Entry newEntry = new Entry();
            for (ExerciseLogItem logItem: log) {
                ExerciseLogItem newLogItem = logItem.createBlank();
                newEntry.log.add(newLogItem);
            }
            return newEntry;
        }

        public boolean isResults() {
            for (ExerciseLogItem logItem: log) {
                if (logItem.isResults()) {
                    return true;
                }
            }
            return false;
        }

        public void save(JSONObject target) throws JSONException {
            write(target, FIELD_PRE_COMMENT, setComment);
            write(target, FIELD_POST_COMMENT, postComment);
            JSONArray logArray = new JSONArray();
            for (ExerciseLogItem logItem : log) {
                JSONObject logItemObject = new JSONObject();
                logItem.save(logItemObject);
                logArray.put(logItemObject);
            }
            target.put(FIELD_LOG, logArray);
        }

        public boolean containsExercisesOf(Entry entry) {
            List<String> exercises = new ArrayList<>();
            for (ExerciseLogItem item: log) {
                exercises.add(item.exercise);
            }
            for (ExerciseLogItem entryItem: entry.log) {
                if (!exercises.contains(entryItem.exercise)) {
                    return false;
                }
            }
            return true;
        }

        private String readString(JSONObject src, String id) throws JSONException {
            if (src.has(id)) {
                return src.getString(id);
            }
            return null;
        }

        private void write(JSONObject target, String id, String value) throws JSONException {
            if (value != null) {
                target.put(id, value);
            }
        }

        public Entry(String exerciseId) {
            log = new ArrayList<>();
            ExerciseLogItem logItem = new ExerciseLogItem();
            logItem.exercise = exerciseId;
            log.add(logItem);
        }

        public Entry(List<String> exercises, int[] plan, String comment) {
            setComment = comment;
            log = new ArrayList<>();
            for (String exerciseId: exercises) {
                ExerciseLogItem logItem = new ExerciseLogItem();
                logItem.exercise = exerciseId;
                if (plan != null) {
                    logItem.sets = new ArrayList<>();
                    for (int i = 0; i < plan.length; i++) {
                        Set set = new Set();
                        set.plannedReps = plan[i];
                        logItem.sets.add(set);
                    }
                }
                log.add(logItem);
            }
        }

        public int getSetsCount() {
            return log.get(0).sets.size();
        }

        public void removeSet(int index) {
            for (ExerciseLogItem item: log) {
                item.sets.remove(index);
            }
        }

        public boolean isSet(int index) {
            return getSetsCount() > index;
        }

        public boolean isSetEmpty(int index) {
            for (Workout.ExerciseLogItem item: log) {
                if ((index < item.sets.size()) && (item.sets.get(index).isResults())) {
                    return false;
                }
            }
            return true;
        }

        public Set getExerciseSet(String exerciseId, int index) {
            if (!isSet(index)) {
                return null;
            }
            for (Workout.ExerciseLogItem item: log) {
                if (item.exercise.equals(exerciseId)) {
                    return item.sets.get(index);
                }
            }
            return null;
        }

        public Progress compareTo(Entry previousEntry, int position, Config config) {
            if (previousEntry == null) {
                int minReps = config.getMinReps();
                if (isMinRepsOk(position, minReps)) {
                    return Progress.initial();
                } else {
                    return Progress.failedGrowth();
                }
            }
            int index = 0;
            int size = Math.max(log.size(), previousEntry.log.size());
            for (int i = 0; i < size; i++) {
                Workout.Set current = null;
                Workout.Set previous = null;
                if (i < log.size()) {
                    current = getExerciseSet(log.get(i).exercise, position);
                }
                if (i < previousEntry.log.size()) {
                    previous = previousEntry.getExerciseSet(previousEntry.log.get(i).exercise, position);
                }
                index += getIndexChangeForSets(current, previous, config);
            }
            return Progress.positiveSummaryIndex(index);
        }

        private boolean isMinRepsOk(int position, int minReps) {
            for (Workout.ExerciseLogItem item: log) {
                Workout.Set set = getExerciseSet(item.exercise, position);
                if (!set.isMinRepsOk(minReps)) {
                    return false;
                }
            }
            return true;
        }

        public Progress compareTo(Entry previousEntry, Config config) {
            int index = 0;
            int size = Math.max(log.size(), previousEntry.log.size());
            for (int iItem = 0; iItem < size; iItem++) {
                ExerciseLogItem currentItem = null;
                ExerciseLogItem previousItem = null;
                int currentItems = 0;
                int previousItems = 0;
                if (iItem < log.size()) {
                    currentItem = log.get(iItem);
                    currentItems = currentItem.sets.size();
                }
                if (iItem < previousEntry.log.size()) {
                    previousItem = previousEntry.log.get(iItem);
                    previousItems = previousItem.sets.size();
                }
                int setsSize = Math.max(currentItems, previousItems);
                for (int i = 0; i < setsSize; i++) {
                    Set currentSet = null;
                    Set previousSet = null;
                    if ((currentItem != null) && (i < currentItems)) {
                       currentSet = currentItem.sets.get(i);
                    }
                    if ((previousItem != null) && (i < previousItems)) {
                        previousSet = previousEntry.getExerciseSet(previousItem.exercise, i);
                    }
                    index += getIndexChangeForSets(currentSet, previousSet, config);
                }
            }
            return Progress.positiveSummaryIndex(index);
        }

        private static int getIndexChangeForSets(Set current, Set previous, Config config) {
            int minReps = config.getMinReps();
            if (current == null) {
                return  previous.isMinRepsOk(minReps) ? -1 : 0;
            }
            if (previous == null) {
               return current.isMinRepsOk(minReps) ? 1 : 0;
            }
            if (!current.isMinRepsOk(minReps)) {
                return  previous.isMinRepsOk(minReps) ? -1 : 0;
            }
            Progress progress = current.compareTo(previous, config);
            if (progress.getEstimate() == Progress.Estimate.GOOD) {
                return 1;
            } else if (progress.getEstimate() == Progress.Estimate.BAD) {
                return -1;
            }
            return 0;
        }

        private static final String FIELD_PRE_COMMENT = "pre_comment";
        private static final String FIELD_POST_COMMENT = "post_comment";
        private static final String FIELD_LOG = "log";
    }

    public boolean isActive() {
        return mFinishedDate == null;
    }


    public interface OnLoadedListener {
        void onLoaded(Workout workout);
    }

    private DateTime mStartedDate;
    private DateTime mFinishedDate;
    private List<Entry> mPlan = new ArrayList<>();
    private Source mSource;
    private String mComment;

    private static final String FIELD_STARTED_DATE = "started";
    private static final String FIELD_FINISHED_DATE = "finished";
    private static final String FIELD_PLAN = "plan";
    private static final String FIELD_COMMENT = "comment";

    public static final String ACTIVE_FILE_NAME="active";
    public static final String FINISHED_FILE_NAME="finished";
}
