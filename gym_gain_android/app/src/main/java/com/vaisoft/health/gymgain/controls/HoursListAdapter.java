package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.text.format.DateFormat;

import com.vaisoft.health.gymgain.R;

public class HoursListAdapter extends SpinnerSimpleAdapter {

    public HoursListAdapter(Context context) {
        super(context);
        mIs24Format = DateFormat.is24HourFormat(context);
        mContext = context;
    }

    @Override
    public int getCount() {
        return 24;
    }

    @Override
    public String getItem(int position) {
        if (mIs24Format) {
            return position + " : 00";
        } else {
            return getAmPmFormat(position);
        }
    }

    private String getAmPmFormat(int position) {
        if (position == 0) {
            return mContext.getString(R.string.midnight);
        } else if (position == 12) {
            return mContext.getString(R.string.noon);
        } else {
            if (position < 12) {
                return position + " : 00 AM";
            } else {
                return (position - 12) + " : 00 PM";
            }
        }
    }

    private boolean mIs24Format;
    private Context mContext;
}
