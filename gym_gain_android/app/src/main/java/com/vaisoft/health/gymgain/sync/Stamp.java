package com.vaisoft.health.gymgain.sync;

import android.content.Context;
import android.util.Log;

import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;


public class Stamp {

    public Stamp() {

    }

    Stamp(JSONObject data) throws JSONException {
        long timeStamp = data.getLong(FIELD_TIME);
        if (timeStamp > 0) {
            mTimeStamp = new DateTime(timeStamp);
        }
    }

    public static Stamp load(JSONLoader loader) {
        JSONObject info = loader.loadObject(FILE_NAME, Utils.FileTarget.FILE_TARGET_PRIVATE);
        try {
            if (info != null) {
                return new Stamp(info);
            } else {
                return new Stamp();
            }
        } catch (JSONException e) {
            Log.e(Stamp.class.getName(), e.getMessage());
            return null;
        }
    }

    public void save(JSONObject target) throws JSONException {
        long timeStamp = 0;
        if (mTimeStamp != null) {
            timeStamp = mTimeStamp.getMillis();
        }
        target.put(FIELD_TIME, timeStamp);
    }

    public void save(Context context) throws JSONException {
        JSONObject target = new JSONObject();
        save(target);
        Utils.saveJSON(context, FILE_NAME, target);
    }

    public void onSynchronized() {
        mTimeStamp = Utils.now();
    }

    public DateTime getLastSyncDate() {
        return mTimeStamp;
    }

    private DateTime mTimeStamp;

    private static final String FIELD_TIME = "s";
    private static final String FILE_NAME = "cloud";
}
