package com.vaisoft.health.gymgain.workout;

public interface ActiveExerciseChangeListener {
    void onActiveExerciseChanged(int exercise);
}
