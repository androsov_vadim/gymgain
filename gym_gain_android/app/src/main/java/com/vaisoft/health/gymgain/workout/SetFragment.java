package com.vaisoft.health.gymgain.workout;

import android.os.Bundle;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.ExerciseResultEditor;
import com.vaisoft.health.gymgain.data.Workout;

public class SetFragment extends WorkoutFragment {

    @Override
    protected int getRootResource() {
        return R.layout.activity_workout_edit_item;
    }

    @Override
    protected void init() {
        Bundle args = getArguments();
        int exercise = args.getInt(WorkoutContentFragment.EXERCISE);
        initEditor(exercise);
    }

    private void initEditor(int exercise) {
        mEditor = (ExerciseResultEditor) getView().findViewById(R.id.editor);
        mExerciseId = mParent.getExercise(exercise);
        mEditor.init(mExerciseId, getCoach(), isWorkoutActive());
        mEditor.setOnValueChangedListener(mParent);
        invalidate();
    }

    @Override
    public void invalidate() {
        apply(mParent.getCurrentRep());
    }

    public void apply(int rep) {
        Workout.Set current = getEntry().getExerciseSet(mExerciseId, rep);
        Workout.Set previous = mParent.getPreviousSet(mExerciseId, rep);
        mEditor.setValue(current, previous, getCoach());
    }

    public void setParent(WorkoutContentFragment parent) {
        mParent = parent;
    }

    private Workout.Entry getEntry() {
        return mParent.getEntry();
    }

    //-------------------------------------------------------------------------------------

    private String mExerciseId;
    private ExerciseResultEditor mEditor;
    private WorkoutContentFragment mParent;
}
