package com.vaisoft.health.gymgain.coach;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.vaisoft.health.gymgain.R;

import java.util.ArrayList;
import java.util.List;

public class CoachTalk {

    public CoachTalk(Context context)
    {
        mContext = context;
    }

    public void bind(CoachConnect connect)
    {
        if (!isBound()) {
            mConnect = connect;
            mCoach = Coach.getInstance(mContext);
            notifyConnectedOrWait();
        } else {
            Log.w(getClass().getName(), "Coach service rebind attempt");
        }
    }

    public void unbind()
    {
        if (isBound()) {
            mConnect = null;
        } else {
            Log.w(getClass().getName(), "Not bound coach service unbound attempt");
        }
    }

    private boolean isBound()
    {
        return mCoach != null;
    }

    private void notifyConnectedOrWait() {
        if (mCoach.isReady()) {
            mConnect.onCoachConnected();
        } else {
            addToWaitList();
        }
        mConnect = null; //can delete because object is notified or added to wait list
    }

    private void fireConnectMessage() {
        if (sWaitList != null) {
            for (CoachConnect listener: sWaitList) {
                listener.onCoachConnected();
            }
            finishWait();
        }
    }

    public Coach getCoach() {
        if ((mCoach != null) && (!mCoach.isReady())) {
            return null;
        }
        return mCoach;
    }

    private void addToWaitList() {
        if (sWaitList == null) {
            startWait();
        }
        sWaitList.add(mConnect);
    }

    private void startWait() {
        sWaitList = new ArrayList<>();

        sProgressDialog = new ProgressDialog(mContext);
        sProgressDialog.setTitle(R.string.wait);
        sProgressDialog.setMessage(mContext.getResources().getString(R.string.wait_message));
        sProgressDialog.setCanceledOnTouchOutside(false);
        sProgressDialog.show();

        sCoachReadyChecker = new Handler();
        sCoachReadyChecker.post(new CoachReadyCheck());
    }

    private void finishWait() {
        sWaitList = null;
        sCoachReadyChecker = null;
        sProgressDialog.cancel();
        sProgressDialog = null;
    }

    public void updateCloudApplyIncoming(CoachConnect connect) {
        mConnect = connect;
        mCoach.updateCloudApplyIncoming(mContext);
        notifyConnectedOrWait();
    }

    private class CoachReadyCheck implements Runnable {
        @Override
        public void run() {
            if (mCoach.isReady()) {
                fireConnectMessage();
            } else {
                sCoachReadyChecker.postDelayed(this, 1000);
            }
        }
    }

    private Context mContext;
    private CoachConnect mConnect;

    private static List<CoachConnect> sWaitList = null;
    private static Handler sCoachReadyChecker = null;
    private static ProgressDialog sProgressDialog = null;

    private Coach mCoach;
}
