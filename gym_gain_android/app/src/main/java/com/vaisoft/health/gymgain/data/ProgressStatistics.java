package com.vaisoft.health.gymgain.data;

import com.vaisoft.health.gymgain.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ProgressStatistics {

    public int getTotal() {
        return mGoods.size() - mBads.size();
    }

    public void addGood(String id) {
        mGoods.add(id);
    }

    public void addSoso(String id) {
        mSosos.add(id);
    }

    public void addBad(String id) {
        mBads.add(id);
    }

    public ProgressCounter getCounter() {
        return new ProgressCounter(mGoods.size(), mSosos.size(), mBads.size());
    }

    public String getGoods(Localizer localizer) {
        return  Utils.convertListToString(mGoods, localizer);
    }

    public String getBads(Localizer localizer) {
        return  Utils.convertListToString(mBads, localizer);
    }

    public boolean isMultipleResults() {
        return (mGoods.size() + mBads.size() + mSosos.size()) > 1;
    }

    private List<String> mGoods = new ArrayList<>();
    private List<String> mBads = new ArrayList<>();
    private List<String> mSosos = new ArrayList<>();
}

