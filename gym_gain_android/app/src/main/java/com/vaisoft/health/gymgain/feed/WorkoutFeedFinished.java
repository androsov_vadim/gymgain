package com.vaisoft.health.gymgain.feed;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.controls.FeedItem;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.ProgressCounter;
import com.vaisoft.health.gymgain.data.ProgressStatistics;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.workout.WorkoutActivityView;
import com.vaisoft.health.gymgain.main.FeedFragment;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;

public class WorkoutFeedFinished extends Feed {

    public WorkoutFeedFinished(Context context, Workout workout, Coach coach) {
        super(context);
        mWorkout = workout;
        mCoach = coach;
    }

    @Override
    public void bind(ContextViewHolder viewHolder, Coach coach) {
        ViewHolder holder = (ViewHolder) viewHolder;
        FeedItem item = holder.getItem();
        item.reset();
        item.setCaption(coach.getLocalizedName(getContext(), mWorkout));
        if (mWorkout.isResults()) {
            initWorkoutWithResults(item, coach);
        } else {
            initBurnedOutWorkout(item, coach);
        }

    }

    private void initBurnedOutWorkout(FeedItem item, Coach coach) {
        item.setProgress(ProgressCounter.BAD);
        item.setItemBackgroundColor(R.color.feed_burned_out_workout_background);
        item.setIcon(R.drawable.ic_weekend_white_36dp, R.color.feed_icon);
        item.setNegativeText(getContext().getString(R.string.feed_wasted));
        String date = getWorkoutDate();
        item.setNeutralText(date, null);
    }

    private void initWorkoutWithResults(FeedItem item, Coach coach) {
        String elapsed = getElapsedTime(getContext());
        String date = getWorkoutDate();
        item.setNeutralText(date, elapsed);
        item.setItemBackgroundColor(R.color.feed_finished_workout_background);
        Workout previousWorkout = coach.getPreviousWorkout(mWorkout);
        if (previousWorkout == null) {
            item.showThumb(Progress.Estimate.GOOD);
            item.setProgress(ProgressCounter.GOOD);
        } else {
            Progress progress = mWorkout.compareTo(previousWorkout, coach.getConfig());
            item.showThumb(progress.getEstimate());
            ProgressStatistics statistics = mWorkout.getProgressStatistics(previousWorkout, coach.getConfig());
            if (statistics.isMultipleResults()) {
                item.applyDescription(statistics, coach.getLocalizer());
            }
            item.setProgress(statistics.getCounter());
        }
    }

    private String getElapsedTime(Context context) {
        StringBuilder text = new StringBuilder();
        text.append(context.getString(R.string.feed_elapsed));
        text.append(": ");
        text.append(Utils.getElapsedFormat(mWorkout.getStartedDate(), mWorkout.getFinishedDate(), false));
        return text.toString();
    }

    private String getWorkoutDate() {
        DateTime date = mWorkout.getFinishedDate();
        if (date == null) {
            date = mWorkout.getStartedDate();
        }
        return Utils.formatDate(date);
    }

    @Override
    public void clicked() {
        Context context = getContext();
        Intent intent = new Intent(context, WorkoutActivityView.class);
        intent.putExtra(WorkoutActivityView.WORKOUT_FINISHED, mCoach.getFinishedWorkoutIndex(mWorkout));
        intent.putExtra(WorkoutActivityView.WORKOUT_KEY, mWorkout.getSource().getKey());
        context.startActivity(intent);
    }

    @Override
    public void deleted(Coach coach) {
        mCoach.deleteFinishedWorkout(getContext(), mWorkout);
    }

    @Override
    public int getViewType() {
        return VIEW_TYPE;
    }

    @Override
    protected long getEffectiveTime() {
        return mWorkout.getEffectiveDate().getMillis();
    }

    //-----------------------------------------------------------------------------

    public static class ViewHolder extends Feed.ViewHolder {

        public ViewHolder(View view, FeedFragment.FeedAdapter adapter) {
            super(view, adapter);
        }
    }

    private Workout mWorkout;
    private Coach mCoach;

    public static final int VIEW_TYPE = R.layout.feed_item_workout_finished;
}
