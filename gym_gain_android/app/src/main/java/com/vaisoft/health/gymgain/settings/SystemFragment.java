package com.vaisoft.health.gymgain.settings;

import android.app.Activity;
import androidx.annotation.IdRes;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.CoachService;
import com.vaisoft.health.gymgain.controls.HoursListAdapter;
import com.vaisoft.health.gymgain.data.Config;

public class SystemFragment extends SettingsTabFragment {

    @Override
    protected void init() {
        initCheckout();
        initCloudEnabler();
        initLockScreenConfig();
    }

    private void initLockScreenConfig() {
        CheckBox lockSleepTimer = (CheckBox) findViewById(R.id.lock_timer);
        lockSleepTimer.setChecked(getConfig().isLockScreenTimer());
        lockSleepTimer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getConfig().setLockScreenTimer(isChecked);
            }
        });

        CheckBox lockSleepWorkout = (CheckBox) findViewById(R.id.lock_workout);
        lockSleepWorkout.setChecked(getConfig().isLockScreenWorkout());
        lockSleepWorkout.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getConfig().setLockScreenWorkout(isChecked);
            }
        });
    }

    private View findViewById(@IdRes int id) {
        return getView().findViewById(id);
    }

    private Config getConfig() {
        return getCoach().getConfig();
    }

    private void initCloudEnabler() {
        mCloudEnabler = (CheckBox) findViewById(R.id.cloud_enable);
        updateCloudEnabler();
        mCloudEnabler.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getConfig().setCloudEnabled(isChecked);
                openCloudTab();
            }
        });
    }

    private void openCloudTab() {
        Activity activity = getActivity();
        if (activity != null) {
            ((SettingsActivity)activity).openCloudTab();
        }
    }

    private void updateCloudEnabler() {
        mCloudEnabler.setChecked(getConfig().isCloudEnabled());
    }

    private void initCheckout() {
        Spinner checkoutTime = (Spinner) findViewById(R.id.checkout_hours);
        checkoutTime.setAdapter(new HoursListAdapter(getContext()));
        checkoutTime.setSelection(Config.getCheckpointHours(getContext()));
        checkoutTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CoachService.updateCheckout(getContext(), position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onTabSelected() {
        super.onTabSelected();
        if (mCloudEnabler != null) {
            updateCloudEnabler();
        }
    }

    @Override
    protected int getRootResource() {
        return R.layout.activity_settings_system;
    }

    private CheckBox mCloudEnabler;
    public static final int TAB = 1;
}
