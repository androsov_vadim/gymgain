package com.vaisoft.health.gymgain.feed;

import android.content.Context;
import android.view.View;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.controls.FeedItem;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.data.ProgressCounter;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.main.FeedFragment;

import org.joda.time.DateTime;

public class RecordFeed extends Feed {

    public RecordFeed(Context context, Record record) {
        super(context);
        mRecord = record;
    }

    @Override
    public void bind(ContextViewHolder viewHolder, Coach coach) {
        ViewHolder holder = (ViewHolder) viewHolder;
        FeedItem item = holder.getItem();
        item.reset();
        item.setProgress(ProgressCounter.SOSO);
        item.setCaption(getContext().getString(R.string.feed_hot_record));
        String recordDescription = mRecord.getDescription(getContext(), coach);
        item.setNeutralText(recordDescription, null);
        item.setItemBackgroundColor(R.color.feed_hot_record_background);
        item.setIcon(R.drawable.ic_whatshot_white_36dp, R.color.feed_icon);

        mCoach = coach;
    }

    @Override
    public void clicked() {
        mRecord.startNewWorkout(getContext(), mCoach);
    }

    @Override
    public void deleted(Coach coach) {
        coach.deleteRecord(getContext(), mRecord);
    }

    @Override
    public int getViewType() {
        return VIEW_TYPE;
    }

    @Override
    protected long getEffectiveTime() {
        return WHEN_I_100_OLD;
    }

    //--------------------------------------------------------------------

    public static class ViewHolder extends Feed.ViewHolder {

        public ViewHolder(View view, FeedFragment.FeedAdapter adapter) {
            super(view, adapter);
        }
    }

    private Coach mCoach;
    private Record mRecord;

    public static final int VIEW_TYPE = R.layout.feed_item_record;

    private static long WHEN_I_100_OLD = new DateTime(2082, 7, 26, 0, 0).getMillis();
}
