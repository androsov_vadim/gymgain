package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.vaisoft.health.gymgain.R;

public class TabHighlight extends FrameLayout {
    public TabHighlight(@NonNull Context context) {
        super(context);
        init();
    }

    public TabHighlight(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TabHighlight(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View.inflate(getContext(), R.layout.tab_highlight, this);
        mHighlight = findViewById(R.id.highlight);
    }

    public void setHighlight(boolean isOn) {
        int visibility = isOn ? VISIBLE : GONE;
        if (mHighlight.getVisibility() == visibility) {
            return;
        }
        mHighlight.setVisibility(visibility);
    }

    private View mHighlight;
}
