
package com.vaisoft.health.gymgain.edit;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.ButtonPanel;
import com.vaisoft.health.gymgain.controls.ExerciseParameter;
import com.vaisoft.health.gymgain.controls.InfoSearcher;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.dialogs.MultySelectDialog;
import com.vaisoft.health.gymgain.dialogs.MusclesDialog;
import com.vaisoft.health.gymgain.dialogs.TargetEditDialog;
import com.vaisoft.health.gymgain.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class ExerciseDetailActivity extends DetailsEditorActivity
    implements MultySelectDialog.Listener, TargetEditDialog.Listener
{
    @Override
    public void onCoachConnected() {
        setContentView(R.layout.activity_exercise_detail);
        initTarget();
        initInterface();
    }

    private void initTarget() {
        mExercise = getCoach().getEditTarget(Exercise.class);
        if (mExercise == null) {
            Intent intent = getIntent();
            if (intent.hasExtra(ITEM)) {
                try {
                    String exerciseId = intent.getStringExtra(ITEM);
                    mExercise = getCoach().getExercise(exerciseId).clone();
                    setInitialId(mExercise.getId());
                } catch (CloneNotSupportedException e) {
                    Log.e(getClass().getName(), e.getMessage());
                }
            } else {
                mExercise = new Exercise();
                mExercise.setId(getCoach().getDefaultExerciseId(getResources().getString(R.string.new_exercise_prefix)));
            }
            setEditTarget(mExercise);
        }
    }

    private void initInterface() {
        initOption(R.id.ex_weight, Unit.weight);
        initOption(R.id.ex_reps, Unit.reps);
        initOption(R.id.ex_distance, Unit.distance);
        initOption(R.id.ex_time, Unit.time);

        initAffectedMuscles();
        initAffectedBodyParts();

        initCommonInterface(mExercise.getId(), R.string.edit_target_exercise);
        initMusclesEditor();
        initInformation();
    }

    private void initInformation() {
        InfoSearcher informer = (InfoSearcher) findViewById(R.id.ex_info);
        informer.setListener(new InfoSearcher.Listener() {
            @Override
            public void searchInfo() {
                getCoach().searchInfo(mExercise.getId(), ExerciseDetailActivity.this);
            }
        });
    }

    private void deleteExercise() {
        if (isIdDefined()) {
            String id = getInitialId();
            if (id != null) {
                getCoach().deleteExercise(this, id);
            }
        }
        goBack();
    }

    private void initMusclesEditor() {
        final Button addMuscleButton = (Button) findViewById(R.id.ex_add_muscle);
        if (isEditable()) {
            addMuscleButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    final ArrayList<String> muscleIds = getCoach().getFilteredMuscles(mExercise);
                    bundle.putStringArrayList(MultySelectDialog.DATA,
                            getCoach().getMusclePartPairsLocalizedStrings(muscleIds));
                    MultySelectDialog dialog = new MusclesDialog();
                    dialog.setArguments(bundle);
                    dialog.show(getSupportFragmentManager(), "MusclesDialog");
                }
            });
        } else {
            addMuscleButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onChosen(List<Integer> data) {
        ArrayList<String> muscleIds = getCoach().getFilteredMuscles(mExercise);
        List<String> toAdd = new ArrayList<>();
        for (Integer index : data) {
            toAdd.add(muscleIds.get(index.intValue()));
        }
        addMuscles(toAdd);
    }

    private void addMuscles(List<String> toAdd)
    {
        addAffectedMuscles(toAdd, true);
        List<String> beforeParts = getCoach().getAffectedByMusclesParts(mExercise.getMuscles());
        mExercise.getMuscles().addAll(toAdd);
        List<String> afterParts = getCoach().getAffectedByMusclesParts(mExercise.getMuscles());
        afterParts.removeAll(beforeParts);
        if (!afterParts.isEmpty()) {
            addAffectedBodyParts(afterParts);
        }
        setChanged(true);
    }

    private void initOption(int id, final Unit parameter) {
        ExerciseParameter option = (ExerciseParameter) findViewById(id);
        boolean defined = mExercise.isParameter(parameter);
        Value parameterValue = getParameterValue(parameter);
        option.init(defined, parameterValue, parameter);
        option.setEditable(isEditable());
        mParameters.put(parameter, option);
        option.setListener(new ExerciseParameter.Listener() {
            @Override
            public void onEnabled(boolean enabled) {
                changeParameterEnabled(parameter, enabled);
            }

            @Override
            public void onTarget(boolean grow) {
                changeParameterTarget(parameter, grow);
            }
        });
    }

    public String localize(String id) {
        return getCoach().getLocalized(id);
    }

    public void showTargetEditDialog(Unit parameter) {
        Value parameterValue = getParameterValue(parameter);
        TargetEditDialog dialog = new TargetEditDialog();
        Bundle args = new Bundle();
        parameterValue.save(args);
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "TargetEditDialog");
    }

    private Value getParameterValue(Unit parameter) {
        Value value = mExercise.getParameter(parameter);
        if (value.isZero()) {
            Unit unit = getCoach().getConfig().getUnits(parameter);
            value = new Value(unit, 0, value.shouldGrow(), Value.Status.Undefined);
        } else {
            value = value.clone();
        }
        return value;
    }

    private void changeParameterEnabled(Unit parameter, boolean enabled) {
        if (enabled) {
            mExercise.addParameter(parameter);
        } else {
            mExercise.removeParameter(parameter);
        }
        setChanged(true);
    }

    private void changeParameterTarget(Unit parameter, boolean grow) {
        mExercise.setParameter(parameter, grow);
        setChanged(true);
    }

    private void initAffectedBodyParts() {
        addAffectedBodyParts(getCoach().getAffectedByMusclesParts(mExercise.getMuscles()));
    }

    private void addAffectedBodyParts(List<String> parts) {
        List<ButtonPanel> buttons = createButtonPanels(R.id.ex_parts_list, parts, false, true);
        if (mPartsButtons == null) {
            mPartsButtons = buttons;
        } else {
            mPartsButtons.addAll(buttons);
        }
    }

    private void initAffectedMuscles() {
        addAffectedMuscles(mExercise.getMuscles(), isEditable());
    }

    private void addAffectedMuscles(List<String> muscles, boolean editable) {
        List<ButtonPanel> buttons = createButtonPanels(R.id.ex_muscles_list, muscles, editable, false);
        if (mMusclesButtons == null) {
            mMusclesButtons = buttons;
        } else {
            mMusclesButtons.addAll(buttons);
        }
    }

    @Override
    protected void deleteObjectConfirmed() {
        deleteExercise();
    }

    @Override
    protected Record createRecord() {
        String id = getInitialId();
        if (id == null) {
            return getCoach().createRecord(mExercise);
        }
        return getCoach().createExerciseRecord(id);
    }

    @Override
    protected Record getRecord() {
        String id = getInitialId();
        if (id == null) {
            return null;
        }
        return getCoach().getRecordForExercise(id);
    }

    @Override
    protected Workout createWorkout() {
        Record record = new Record(mExercise);
        return record.createWorkout(getCoach());
    }

    @Override
    protected boolean isUseless() {
        return mExercise.isUseless();
    }

    @Override
    protected void onIdChanged(String id) {
        mExercise.setId(id);
    }

    @Override
    protected String getId() {
        return mExercise.getId();
    }

    @Override
    protected boolean isEditable() {
        return true;
    }

    @Override
    protected boolean canAutoSave() {
        return false;
    }

    @Override
    public void onSelectionChange(String id, boolean selected) {
        if (selected) {
            for (ButtonPanel button: mPartsButtons) {
                if ((!button.getTag().equals(id)) && button.isChecked()) {
                    button.setChecked(false);
                }
            }
            List<String> selectedMuscles = getCoach().getMusclesForPart(id);
            for (ButtonPanel button: mMusclesButtons) {
                button.setChecked(selectedMuscles.contains(button.getTag()));
            }
        } else {
            for (ButtonPanel button: mMusclesButtons) {
                button.setChecked(false);
            }
        }
    }

    @Override
    public void onDeleteItem(String id) {
         for (Iterator<ButtonPanel> iButton = mMusclesButtons.iterator(); iButton.hasNext();) {
            ButtonPanel button = iButton.next();
            if (button.getTag().equals(id)) {
                removeButtonPanel(button);
                mExercise.getMuscles().remove(id);
                iButton.remove();
                break;
            }
        }
        List<String> partsLeft = getCoach().getAffectedParts(mExercise);
        if (partsLeft.size() < mPartsButtons.size()) {
            for (Iterator<ButtonPanel> iButton = mPartsButtons.iterator(); iButton.hasNext();) {
                ButtonPanel button = iButton.next();
                if (!partsLeft.contains(button.getTag())) {
                    removeButtonPanel(button);
                    mPartsButtons.remove(button);
                    break;
                }
            }
        }
        setChanged(true);
    }

    @Override
    public boolean saveChanges() {
        if (checkAndNotifyWrongId()) {
            return false;
        }
        String initialId = getInitialId();
        if ((initialId == null) || (!initialId.equals(mExercise.getId()))) {
            if (!getCoach().isExerciseIdFree(mExercise.getId())) {
                Utils.showErrorDialog(getSupportFragmentManager(), R.string.error_id_exists);
                return false;
            }
        }
        getCoach().saveExercise(this, mExercise, initialId);
        return true;
    }

    @Override
    protected boolean isDeletable() {
        String id = getInitialId();
        if (id == null) {
            return false;
        }
        return !getCoach().isExerciseInUse(id);
    }

    @Override
    public void onTargetUpdated(Value value) {
        Unit parameter = value.unit().getCategory();
        boolean changed = mExercise.setParameter(parameter, value);
        if (changed) {
            updateParameter(parameter);
            setChanged(true);
        }
    }

    @Override
    public void onTargetDeleted(Unit parameter) {
        boolean changed = mExercise.resetParameter(parameter);
        if (changed) {
            updateParameter(parameter);
            setChanged(true);
        }
    }

    private void updateParameter(Unit parameter) {
        ExerciseParameter option = mParameters.get(parameter);
        Value value = getParameterValue(parameter);
        option.update(value);
    }

    private Exercise mExercise;
    private List<ButtonPanel> mPartsButtons;
    private List<ButtonPanel> mMusclesButtons;
    private Map<Unit, ExerciseParameter> mParameters = new HashMap<>();

    public static final String ITEM = "exercice";
}
