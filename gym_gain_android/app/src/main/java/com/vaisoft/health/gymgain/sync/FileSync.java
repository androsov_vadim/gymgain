package com.vaisoft.health.gymgain.sync;

import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;


public abstract class FileSync {

    public interface Listener {
        void fail();
    }

    public FileSync(GoogleApiClient apiClient, String fileName, Listener listener) {
        mDrive = apiClient;
        mFileName = fileName;
        mListener = listener;
    }

    protected String getFileName() {
        return mFileName;
    }

    protected GoogleApiClient getDrive() {
        return mDrive;
    }

    protected void onFail(Status status) {
        Log.e(getClass().getName(), "Cloud sync problem: " + status.getStatusMessage());
        mListener.fail();
    }

    private GoogleApiClient mDrive;
    private String mFileName;
    private Listener mListener;
}
