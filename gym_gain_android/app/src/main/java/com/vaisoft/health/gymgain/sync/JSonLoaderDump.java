package com.vaisoft.health.gymgain.sync;

import android.content.Context;
import android.util.Log;

import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class JSonLoaderDump extends JSonLoaderFile {

    public JSonLoaderDump(Context context, Map<String, String> dump) {
        super(context);
        mDump = dump;
    }

    @Override
    public JSONObject loadObject(String id, Utils.FileTarget target) {
        if (target == Utils.FileTarget.FILE_TARGET_ASSETS) {
            return super.loadObject(id, target);
        }
        if (!mDump.containsKey(id)) {
            return null;
        }
        try {
            return new JSONObject(mDump.get(id));
        } catch (JSONException e) {
            Log.e(getClass().getName(), "Error parsing '" + id + "' dump: " + e.getMessage());
            return null;
        }
    }

    @Override
    public JSONArray loadArray(String id, Utils.FileTarget target) {
        if (target == Utils.FileTarget.FILE_TARGET_ASSETS) {
            return super.loadArray(id, target);
        }
        if (!mDump.containsKey(id)) {
            return null;
        }
        try {
            return new JSONArray(mDump.get(id));
        } catch (JSONException e) {
            Log.e(getClass().getName(), "Error parsing '" + id + "' dump: " + e.getMessage());
            return null;
        }
    }

    private Map<String, String> mDump;
}
