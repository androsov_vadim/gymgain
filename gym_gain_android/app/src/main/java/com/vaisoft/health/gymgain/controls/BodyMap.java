package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.content.res.TypedArray;
import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.vaisoft.health.gymgain.R;

import java.util.List;

public class BodyMap extends FrameLayout {
    public BodyMap(Context context) {
        super(context);
        init(context, null);
    }

    public BodyMap(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public BodyMap(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        View.inflate(context, R.layout.body_map, this);
        if (attrs == null) {
            return;
        }
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.BodyMap, 0, 0);
        try {
            boolean arms = a.getBoolean(R.styleable.BodyMap_bmArms, false);
            boolean legs = a.getBoolean(R.styleable.BodyMap_bmLegs, false);
            boolean top_front = a.getBoolean(R.styleable.BodyMap_bmTopFront, false);
            boolean top_back = a.getBoolean(R.styleable.BodyMap_bmTopBack, false);
            boolean bottom_front = a.getBoolean(R.styleable.BodyMap_bmBottomFront, false);
            boolean bottom_back = a.getBoolean(R.styleable.BodyMap_bmBottomBack, false);
            apply(arms, legs, top_front, top_back, bottom_front, bottom_back);
        } catch (Exception e) {
            Log.e(getClass().getName(), "There was an error loading attributes.");
        }
        a.recycle();
    }

    public void reset() {
        apply(false, false, false, false, false, false);
    }

    public void apply(List<String> parts)
    {
        boolean arms = parts.contains("arms");
        boolean legs = parts.contains("legs");
        boolean top_front = parts.contains("chest");
        boolean top_back = parts.contains("back");
        boolean bottom_front = parts.contains("belly");
        boolean bottom_back = parts.contains("lower_back");
        apply(arms, legs, top_front, top_back, bottom_front, bottom_back);
    }

    private void apply(boolean arms, boolean legs, boolean top_front, boolean top_back, boolean bottom_front, boolean bottom_back) {
        setPartVisible(R.id.body_arms, arms);
        setPartVisible(R.id.body_legs, legs);
        setPartVisible(R.id.body_top_full, top_front && top_back);
        setPartVisible(R.id.body_top_front, top_front && (!top_back));
        setPartVisible(R.id.body_top_back, (!top_front) && top_back);
        setPartVisible(R.id.body_bottom_full, bottom_front && bottom_back);
        setPartVisible(R.id.body_bottom_front, bottom_front && (!bottom_back));
        setPartVisible(R.id.body_bottom_back, (!bottom_front) && bottom_back);
    }

    private void setPartVisible(int id, boolean visible) {
        findViewById(id).setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    public void setBackgroundColor(@ColorRes int color) {
        int theColor = ContextCompat.getColor(getContext(), color);
        ImageView background = (ImageView) findViewById(R.id.body_background);
        background.setColorFilter(theColor);
    }
}