package com.vaisoft.health.gymgain.data;

public class ProgressCounter {

    public  ProgressCounter(int good, int soso, int bad) {
        mGoodCount = good;
        mSosoCount = soso;
        mBadCount = bad;
    }

    public float getBadSosoLevel() {
        return mBadCount / getSummaryValue();
    }

    public float getSosoGoodLevel() {
        return (mBadCount + mSosoCount) / getSummaryValue();
    }

    private float getSummaryValue() {
        return (float)(mGoodCount + mBadCount + mSosoCount);
    }

    public static ProgressCounter GOOD = new ProgressCounter(1, 0, 0);
    public static ProgressCounter SOSO = new ProgressCounter(0, 1, 0);
    public static ProgressCounter BAD = new ProgressCounter(0, 0, 1);
    public static ProgressCounter UNDEFINED = new ProgressCounter(1, 1, 1);

    private int mGoodCount;
    private int mBadCount;
    private int mSosoCount;
}

