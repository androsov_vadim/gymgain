package com.vaisoft.health.gymgain.controls;


import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.vaisoft.health.gymgain.R;

public class InfoSearcher extends LinearLayout {
    public InfoSearcher(Context context) {
        super(context);
        init();
    }

    public InfoSearcher(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public InfoSearcher(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = View.inflate(getContext(), R.layout.info_searcher, this);
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.searchInfo();
                }
            }
        });
    }

    public void setListener(Listener listener) {
        mListener = listener;
    }

    public interface Listener {
        void searchInfo();
    }

    private Listener mListener;
}
