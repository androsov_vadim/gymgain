package com.vaisoft.health.gymgain.workout;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.controls.ProgressChart;
import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.IdGenerator;
import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.controls.TextTimeTicker;
import com.vaisoft.health.gymgain.data.Training;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.dialogs.TextInputDialog;
import com.vaisoft.health.gymgain.dialogs.TimeEditDialog;
import com.vaisoft.health.gymgain.edit.EditorActivity;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;

import java.util.List;

public abstract class WorkoutActivity extends EditorActivity
    implements TextInputDialog.Listener, TimeEditDialog.Listener
{
    protected abstract int getLayoutResource();
    protected abstract void invalidateNewMode();
    protected abstract void initWorkoutInterface();
    protected abstract Workout initActiveWorkout();
    protected abstract Workout initFinishedWorkout();
    protected abstract void onPostCommentUpdated(int set, String text);

    @Override
    public void onCoachConnected() {
        initData();
        setContentView(getLayoutResource());
        String localizedName = getCoach().getLocalizedName(this, mWorkout);
        initCommonInterface(localizedName);
        initTimer();
        initChart();
        initInterface();
        initWorkoutInterface();
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent.hasExtra(WORKOUT_FINISHED)) {
            mWorkoutIndex = intent.getIntExtra(WORKOUT_FINISHED, -1);
            mWorkoutKey = intent.getStringExtra(WORKOUT_KEY);
            initFinishedData();
        } else if (intent.hasExtra(WORKOUT_ACTIVE)) {
            mWorkoutIndex = intent.getIntExtra(WORKOUT_ACTIVE, -1);
            initActiveData();
            initWorkoutKey();
        } else {
            initNewData();
            initWorkoutKey();
        }
    }

    public void writeWorkoutTo(Bundle bundle) {
        switch (mStatus) {
            case ACTIVE:
                bundle.putInt(WORKOUT_ACTIVE, mWorkoutIndex);
                break;
            case FINISHED:
                bundle.putInt(WORKOUT_FINISHED, mWorkoutIndex);
                bundle.putString(WORKOUT_KEY, mWorkoutKey);
                break;
            default:
                Log.e(getClass().getName(), "Attempt to write bundle for not active or finished workout: " + mStatus);
        }
    }

    public static Workout readWorkoutFrom(Bundle bundle, Coach coach) {
        if (bundle.containsKey(WORKOUT_FINISHED)) {
            int index = bundle.getInt(WORKOUT_FINISHED);
            String key = bundle.getString(WORKOUT_KEY);
            return coach.getFinishedWorkout(key, index);
        } else if (bundle.containsKey(WORKOUT_ACTIVE)) {
            int index = bundle.getInt(WORKOUT_ACTIVE);
            return coach.getActiveWorkout(index);
        }
        Log.e(WorkoutActivity.class.getName(), "Attempt to read bundle with not active or finished workout:" + bundle);
        return null;
    }

    private void initWorkoutKey() {
        mWorkoutKey = mWorkout.getSource().getKey();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(EDIT_POST_COMMENT, mPostCommentEditSet);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mPostCommentEditSet = savedInstanceState.getInt(EDIT_POST_COMMENT);
    }

    protected void initInterface() {
        switch (mStatus) {
            case NEW:
                initNewInterface();
                break;
            case ACTIVE:
                initActiveInterface();
                break;
            case FINISHED:
                initFinishedInterface();
                break;
        }
    }

    @Override
    protected boolean canAutoSave() {
        return mStatus == Status.ACTIVE;
    }

    private void initChart() {
        mChart = findViewById(R.id.workouts_chart);
        mChart.setAdapter(new ChartAdapter());
    }

    private void initFinishedInterface() {
        TextView comment = findViewById(R.id.workoutComment);
        setAlwaysVisibleComment(mWorkout.getComment(), comment);
        comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCommentDialog(mWorkout.getComment(), R.string.edit_comment, COMMENT_WORKOUT_EDIT_CODE);
            }
        });

        initControlPanelFinished();
    }

     protected Workout getEditTarget() {
        return getCoach().getEditTarget(Workout.class);
    }

    protected boolean isNew() {
        return mStatus == Status.NEW;
    }

    public boolean isWorkoutActive() {
        return mStatus == Status.ACTIVE;
    }

    private void initActiveData() {
        if (getCoach().getConfig().isLockScreenWorkout()) {
            enableSleepLock();
        }
        mStatus = Status.ACTIVE;
        initWorkout();
    }

    private void initFinishedData() {
        mStatus = Status.FINISHED;
        initWorkout();
    }

    private void initNewData() {
        mStatus = Status.NEW;
        initWorkout();
    }

    private void initActiveInterface() {
        initPreviousWorkoutComment();
        initControlPanelRunning();
    }

    protected Workout getActiveWorkout() {
        return getCoach().getActiveWorkout(mWorkoutIndex);
    }

    protected Workout getFinishedWorkout() {
        return getCoach().getFinishedWorkout(mWorkoutKey, mWorkoutIndex);
    }

    private void initWorkout() {
        mWorkout = getEditTarget();
        switch (mStatus) {
            case NEW:
                if (mWorkout == null) {
                    mWorkout = getCoach().getPreparedWorkout();
                }
                mPreviousWorkout = getCoach().getLastFinishedWorkout(mWorkout.getSource().getKey());
                break;
            case ACTIVE:
                if (mWorkout == null) {
                    mWorkout = initActiveWorkout();
                }
                mPreviousWorkout = getCoach().getLastFinishedWorkout(mWorkout.getSource().getKey());
                break;
            case FINISHED:
                if (mWorkout == null) {
                    mWorkout = initFinishedWorkout();
                }
                mPreviousWorkout = getCoach().getPreviousWorkout(mWorkout);
                break;
        }
        setEditTarget(mWorkout);
    }

    private void initNewInterface() {
        initPreviousWorkoutComment();
        invalidateNewMode();

        View oneMore = findViewById(R.id.gym_one_more);
        oneMore.setVisibility(View.GONE);
        mTime.reset();
    }

    private void initPreviousWorkoutComment() {
        TextView comment = findViewById(R.id.workoutComment);
        if (mPreviousWorkout != null) {
            setHideableComment(mPreviousWorkout.getComment(), comment);
        } else {
            setHideableComment(null, comment);
        }
    }

    private void initTimer() {
        TextView textView = findViewById(R.id.time);
        mTime = new TextTimeTicker(textView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mTime != null) {
            mTime.stop();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mStatus == Status.ACTIVE) {
            start();
        }
    }

    private void start() {
        mTime.setInitialDate(mWorkout.getStartedDate());
        mTime.start();
    }

    private void updateStaticDate() {
        mTime.setInterval(mWorkout.getStartedDate(), mWorkout.getFinishedDate());
    }

    protected void initControlPanelRunning() {
        View gymCancel = findViewById(R.id.gym_cancel);
        View gymFinish = findViewById(R.id.gym_finish);

        gymCancel.setVisibility(View.VISIBLE);
        gymFinish.setVisibility(View.VISIBLE);
        start();

        initFinishListener(gymFinish);
        initCancelListener(gymCancel);
    }

    protected void initControlPanelFinished() {
        updateStaticDate();

        if (!mWorkout.isBurnedOut()) {
            mTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTimeEditDialog();
                }
            });
        }

        final View oneMore = findViewById(R.id.gym_one_more);
        if (isOneMoreTimePossible()) {
            oneMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirm(R.string.one_more_time_confirm, R.string.confirm_start_caption, ONE_MORE_TRY_CODE);
                }
            });
            oneMore.setVisibility(View.VISIBLE);
        } else {
            oneMore.setVisibility(View.GONE);
        }
    }

    private boolean isOneMoreTimePossible() {
        if (!mWorkout.isBurnedOut()) {
            return true;
        }
        return getCoach().isWorkoutSource(mWorkout.getSource().getKey());
    }

    private void showTimeEditDialog() {
        TimeEditDialog dialog = new TimeEditDialog();
        Bundle args = new Bundle();
        args.putLong(TimeEditDialog.START_TIME, mWorkout.getStartedDate().getMillis());
        args.putLong(TimeEditDialog.FINISH_TIME, mWorkout.getFinishedDate().getMillis());
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "TimeEditDialog");
    }

    @Override
    public void onTimeEdited(DateTime date) {
        mWorkout.setFinishedDate(date);
        updateStaticDate();
        setChanged(true);
    }

    private void initFinishListener(View gymFinish) {
        gymFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCommentDialog(null, R.string.confirm_finish_workout, COMMENT_WORKOUT_FINISH_CODE);
            }
        });
    }

    @Override
    public boolean onConfirmed(int code) {
        if (super.onConfirmed(code)) {
            return true;
        }
        if (code == ONE_MORE_TRY_CODE) {
            oneMoreTry();
        } else if (code == SAVE_UPDATED_CODE) {
            onSaveChanges();
        } else if (code == DISCARD_CODE) {
            onDiscardChanges();
        } else if (code == CANCEL_ACTIVE_CODE) {
            getCoach().deleteActiveWorkout(this, mWorkoutIndex);
            setChanged(false);
            goHome();
        } else {
            return false;
        }
        return true;
    }

    private void oneMoreTry() {
        Workout newWorkout = getWorkoutForOneMoreTry();
        getCoach().prepareWorkout(newWorkout);
        setEditTarget(null);
        initNewData();
        if (getClass().equals(WorkoutActivityView.class)) {
            initNewInterface();
            mChart.invalidate();
        } else {
            Intent intent = new Intent(getContext(), WorkoutActivityView.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
    }

    private Workout getWorkoutForOneMoreTry() {
        if (mWorkout.isBurnedOut()) {
            String key = mWorkout.getSource().getKey();
            Exercise sourceExercise = getCoach().getExerciseByKey(key);
            if (sourceExercise != null) {
                return new Workout(sourceExercise);
            } else {
                Training sourceTraining = getCoach().getTrainingByKey(key);
                if (sourceTraining != null) {
                    return new Workout(sourceTraining);
                }
            }
        } else {
            return mWorkout.createBlank();
        }
        return null;
    }

    private void initCancelListener(View gymCancel) {
        gymCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int code = mStatus == Status.ACTIVE ? CANCEL_ACTIVE_CODE : DISCARD_CODE;
                confirm(R.string.confirm_cancel_workout, R.string.confirm_cancel_workout_caption, code);
            }
        });
    }

    @Override
    public void onDiscardChanges() {
        setChanged(false);
        goHome();
    }

    @Override
    protected void goBack() {
        resetData();
        super.goBack();
    }

    @Override
    protected void goHome() {
        resetData();
        super.goHome();
    }

    private void resetData() {
        if (mStatus == Status.FINISHED) {
            getCoach().resetEditTarget();
        }
        resetPreparedForNewWorkout();
    }

    protected void startWorkout() {
        mWorkoutIndex = getCoach().start(this, mWorkout);
        resetPreparedForNewWorkout();
        initActiveData();
     }

    protected void resetPreparedForNewWorkout() {
        if (mStatus == Status.NEW) {
            getCoach().resetPreparedWorkout();
        }
    }

    @Override
    protected boolean isDeletable() {
        return true;
    }

    @Override
    protected void deleteObjectConfirmed() {
        switch (mStatus) {
            case NEW:
                break;
            case ACTIVE:
                getCoach().deleteActiveWorkout(this, mWorkoutIndex);
                break;
            case FINISHED:
                getCoach().deleteFinishedWorkout(this, mWorkout);
                break;
        }
        setChanged(false);
        goHome();
    }

    @Override
    protected int getEditTargetId() {
        return R.string.workout;
    }

    @Override
    protected boolean saveChanges() {
        switch (mStatus) {
            case NEW:
                break;
            case ACTIVE:
                getCoach().onActiveWorkoutUpdated(this, mWorkoutIndex, mWorkout);
                break;
            case FINISHED:
                getCoach().onFinishedWorkoutUpdated(this, mWorkoutIndex, mWorkout);
                break;
        }
        return true;
    }

    public Context getContext() {
        return this;
    }

    protected static void setHideableComment(String comment, TextView textView, View parent) {
        boolean isComment = Utils.isStringDefined(comment);
        if (parent == null) {
            parent = (View) textView.getParent();
        }
        parent.setVisibility(isComment ? View.VISIBLE : View.GONE);
        if (isComment) {
            textView.setText(comment);
        }
    }

    public static void setHideableComment(String comment, TextView textView) {
        setHideableComment(comment, textView, null);
    }

    public static void setAlwaysVisibleComment(String text, TextView textView) {
        if (!Utils.isStringDefined(text)) {
            textView.setText(R.string.no_comment);
        } else {
            textView.setText(text);
        }
    }

    private void showCommentDialog(String text, int title, int code) {
        TextInputDialog dialog = new TextInputDialog();
        Bundle args = new Bundle();
        args.putInt(TextInputDialog.TITLE, title);
        args.putInt(TextInputDialog.CODE, code);
        if (text != null) {
            args.putString(TextInputDialog.VALUE, text);
        }
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "TextInputDialog");
    }

    public void showPostCommentDialog(String value, int set) {
        mPostCommentEditSet = set;
        showCommentDialog(value, R.string.edit_comment, COMMENT_POST_ENTRY_CODE);
    }

    @Override
    public void onTextInput(int code, String text) {
        if (code == COMMENT_WORKOUT_FINISH_CODE) {
            mWorkout.setComment(text);
            onFinishAndSave();
        } else if (code == COMMENT_WORKOUT_EDIT_CODE) {
            final TextView textView = findViewById(R.id.workoutComment);
            mWorkout.setComment(text);
            setAlwaysVisibleComment(text, textView);
            setChanged(true);
        } else if (code == COMMENT_POST_ENTRY_CODE) {
            if (mPostCommentEditSet != -1) {
                onPostCommentUpdated(mPostCommentEditSet, text);
                mPostCommentEditSet = -1;
                setChanged(true);
            }
        }
    }

    private void onFinishAndSave() {
        Progress progress = mWorkout.compareTo(mPreviousWorkout, getCoach().getConfig());
        if (progress.getEstimate() == Progress.Estimate.GOOD) {
            showGoodOne();
        }
        getCoach().finish(this, mWorkoutIndex, mWorkout);
        setChanged(false);
        goHome();
    }

    private void showGoodOne() {
        Toast toast = Toast.makeText(getApplicationContext(),
                R.string.good_one,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public Workout getWorkout() {
        return mWorkout;
    }

    protected String getWorkoutKey() {
        return mWorkoutKey;
    }

    public Workout getPreviousWorkout() {
        return mPreviousWorkout;
    }

    protected void enableSleepLock() {
        ++mSleepLockCounter;
        if (mSleepLockCounter == 1) {
            Log.d(getClass().getName(), "Sleep lock enabled");
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    protected void disableSleepLock() {
        if (mSleepLockCounter > 0) {
            --mSleepLockCounter;
            if (mSleepLockCounter == 0) {
                Log.d(getClass().getName(), "Sleep lock disabled");
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            }
        }
    }

//--------------------------------------------------------------------------------------------------

    private class ChartAdapter implements ProgressChart.Adapter {

        public ChartAdapter() {
            mExistingWorkouts = getCoach().getFinishedExistingWorkouts(mWorkoutKey);
        }

        @Override
        public int getCount() {
            return mExistingWorkouts.size();
        }

        @Override
        public int getMarkerIndex() {
            if (mStatus == Status.FINISHED) {
                return getActualIndex(mWorkoutIndex);
            }
            return -1;
        }

        private int getActualIndex(int index) {
            return getCount() - index - 1;
        }

        @Override
        public Progress getProgress(int index) {
            int actualIndex = getActualIndex(index);
            Workout current = mExistingWorkouts.get(actualIndex);
            Workout previous = null;
            if (actualIndex >= 0) {
                previous = Knowledge.getPreviousWorkout(mExistingWorkouts, actualIndex);
            }
            return current.compareTo(previous, getCoach().getConfig());
        }

        private List<Workout> mExistingWorkouts;
    }

    protected void putStartParameters(Intent target) {
        if (mStatus == Status.ACTIVE) {
            target.putExtra(WORKOUT_ACTIVE, mWorkoutIndex);
        } else if (mStatus == Status.FINISHED) {
            target.putExtra(WORKOUT_FINISHED, mWorkoutIndex);
            target.putExtra(WORKOUT_KEY, mWorkoutKey);
        }
        Log.w(getClass().getName(), "Can't put activity start parameters");
    }

    private enum Status {
        NEW,
        ACTIVE,
        FINISHED
    }

    private Workout mWorkout;
    private Workout mPreviousWorkout;
    private int mWorkoutIndex;
    private String mWorkoutKey;
    private TextTimeTicker mTime;
    private Status mStatus;
    private int mPostCommentEditSet = -1;
    private ProgressChart mChart;
    private int mSleepLockCounter;

    public static final String WORKOUT_KEY = "id";
    public static final String WORKOUT_ACTIVE = "active";
    public static final String WORKOUT_FINISHED = "finish";
    public static final String EDIT_POST_COMMENT = "edit_pc";

    private static final int ONE_MORE_TRY_CODE = IdGenerator.generateCode();
    private static final int SAVE_UPDATED_CODE = IdGenerator.generateCode();
    private static final int DISCARD_CODE = IdGenerator.generateCode();
    private static final int CANCEL_ACTIVE_CODE = IdGenerator.generateCode();

    private static final int COMMENT_WORKOUT_FINISH_CODE = IdGenerator.generateCode();
    private static final int COMMENT_WORKOUT_EDIT_CODE = IdGenerator.generateCode();
    private static final int COMMENT_POST_ENTRY_CODE = IdGenerator.generateCode();
}
