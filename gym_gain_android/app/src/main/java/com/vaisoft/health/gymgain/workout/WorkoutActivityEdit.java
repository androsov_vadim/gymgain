package com.vaisoft.health.gymgain.workout;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import android.util.Log;
import android.view.View;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.CoachFragment;
import com.vaisoft.health.gymgain.controls.RotateFriendlyPagerAdapter;
import com.vaisoft.health.gymgain.controls.TabHighlight;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.utils.Utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkoutActivityEdit extends WorkoutActivity
    implements ActiveExerciseChangeListener
{
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_workout_edit;
    }

    @Override
    protected void invalidateNewMode() {
    }

    @Override
    protected void initWorkoutInterface() {
        setHomeButtonAsUp();
        initTabs();
        Intent intent = getIntent();
        mSelectedSet = intent.getIntExtra(WorkoutContentFragment.SET, -1);
        mSelectedExercise = intent.getIntExtra(WorkoutContentFragment.EXERCISE, -1);

        TabLayout.Tab tab = mTabLayout.getTabAt(mSelectedSet); //set is always selected - it's the only way to open editor
        tab.select();
        if (mSelectedSet == 0) {
            onEntrySelected(0);
        }
        if (getWorkout().getItemsCount() == 1) {
            mTabLayout.setVisibility(View.GONE);
        }
    }

    private void initTabs() {
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        mAdapter = new Adapter(getSupportFragmentManager());
        viewPager.setAdapter(mAdapter);
        mTabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = mTabLayout.getTabAt(i);
            tab.setCustomView(R.layout.set_tab);
            updateTabHighlight(i);
        }
        mTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                onEntrySelected(tab.getPosition());
            }
        });
    }

    private TabHighlight getTab(int index) {
        TabLayout.Tab tab = mTabLayout.getTabAt(index);
        return (TabHighlight) tab.getCustomView();
    }

    private void updateTabHighlight(int index) {
        TabHighlight tab = getTab(index);
        boolean isResults = getWorkout().getEntry(index).isResults();
        tab.setHighlight(!isResults);
    }

    public void setActiveTabHighlight(boolean highlight) {
        TabHighlight tab = getTab(mCurrentSet);
        tab.setHighlight(highlight);
    }

    @Override
    protected Workout initActiveWorkout() {
        Workout workout = getActiveWorkout().clone();
        return workout;
    }

    @Override
    protected Workout initFinishedWorkout() {
        Workout workout = getFinishedWorkout().clone();
        return workout;
    }

    @Override
    protected void onPostCommentUpdated(int set, String text) {
        WorkoutContentFragment fragment = mAdapter.getFragment(set);
        fragment.setPostComment(text);
    }

    @Override
    protected boolean isEditable() {
        return true;
    }

    @Override
    protected void goBack() {
        getCoach().resetPreparedWorkout();
        super.goBack();
    }

    @Override
    protected void goHome() {
        getCoach().resetPreparedWorkout();
        super.goHome();
    }

    public void onEntrySelected(int position) {
        mCurrentSet = position;
        WorkoutContentFragment fragment = mAdapter.getFragment(mCurrentSet);
        if (fragment != null) {
            updateSleepLock(fragment.getCurrentExercise());
        } else {
            updateSleepLock(0);
        }
    }

    public void updateSleepLock(int exercise) {
        Workout.Entry entry = getWorkout().getEntry(mCurrentSet);
        if ((entry != null) && (exercise < entry.log.size())) {
            Workout.ExerciseLogItem item = entry.log.get(exercise);
            if (item.isActiveValue()) {
                onActiveValueLockEnabled();
            } else {
                onActiveValueLockDisabled();
            }
        } else {
            Log.e(getClass().getName(), "Set = " + mCurrentSet + " exercise " + exercise + " access fail");
        }
    }

    @Override
    public void onActiveExerciseChanged(int exercise) {
        updateSleepLock(exercise);
    }

    private boolean isActiveValueLockEnabled() {
        return getCoach().getConfig().isLockScreenTimer();
    }

    public void onActiveValueLockEnabled() {
        if ((!mSleepLockUsed) && isActiveValueLockEnabled()) {
            enableSleepLock();
            mSleepLockUsed = true;
        }
    }

    public void onActiveValueLockDisabled() {
        if (mSleepLockUsed && isActiveValueLockEnabled()) {
            disableSleepLock();
            mSleepLockUsed = false;
        }
    }

    //----------------------------------------------------------------

    private class Adapter extends RotateFriendlyPagerAdapter {

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CoachFragment getItem(int position) {
            WorkoutContentFragment fragment = new WorkoutContentFragment();
            fragment.setExerciseChangeListener(WorkoutActivityEdit.this);
            Bundle args = new Bundle();
            args.putInt(WorkoutContentFragment.SET, position);
            if (position == mSelectedSet) {
                args.putInt(WorkoutContentFragment.EXERCISE, mSelectedExercise);
            }
            fragment.setArguments(args);
            mFragments.put(position, fragment);
            return fragment;
        }

        public WorkoutContentFragment getFragment(int set) {
            return mFragments.get(set);
        }

        @Override
        public int getCount() {
            return getWorkout().getItemsCount();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            List<String> exercises = getWorkout().getLogContent(position);
            String title = Utils.convertListToString(exercises, getCoach().getLocalizer());
            return title;
        }

        private Map<Integer, WorkoutContentFragment> mFragments = new HashMap<>();
    }

    private int mSelectedSet;
    private int mSelectedExercise;
    private Adapter mAdapter;
    private int mCurrentSet;
    private boolean mSleepLockUsed;
    TabLayout mTabLayout;
}
