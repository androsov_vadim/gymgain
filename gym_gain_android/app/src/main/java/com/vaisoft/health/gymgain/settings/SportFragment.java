package com.vaisoft.health.gymgain.settings;

import androidx.annotation.IdRes;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.controls.NumbersListAdapter;
import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.data.Unit;

public class SportFragment extends SettingsTabFragment {

    @Override
    protected void init() {
        initUnits();
        initCaptions();
        initReps();
        initEstimation();
    }

    private void initEstimation() {
        final RadioGroup estimation = (RadioGroup) findViewById(R.id.evaluation);
        final Config cfg = getConfig();
        switch (cfg.getEstimation()) {
            case STRICT:
                estimation.check(R.id.evaluation_strict);
                break;
            case NORMAL:
                estimation.check(R.id.evaluation_normal);
                break;
                default:
                    Log.e(getClass().getName(), "Unknown estimation apply attempt: " + cfg.getEstimation());
        }
        estimation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.evaluation_strict:
                        cfg.setEstimation(Config.Estimation.STRICT);
                        break;
                    case R.id.evaluation_normal:
                        cfg.setEstimation(Config.Estimation.NORMAL);
                        break;
                }
            }
        });
    }

    private View findViewById(@IdRes int id) {
        return getView().findViewById(id);
    }

    private Config getConfig() {
        return getCoach().getConfig();
    }

    private void initUnits()
    {
        final RadioGroup weight = (RadioGroup) findViewById(R.id.weight_units);
        final RadioGroup distance = (RadioGroup) findViewById(R.id.distance_units);
        final RadioGroup body = (RadioGroup) findViewById(R.id.body_units);

        final Config cfg = getConfig();
        switch (cfg.getWeightUnits())         {
            case kilogram:
                weight.check(R.id.weight_units_kg);
                break;
            case pound:
                weight.check(R.id.weight_units_pound);
                break;
        }
        switch (cfg.getDistanceUnits()) {
            case meter:
                distance.check(R.id.distance_units_meter);
                break;
            case foot:
                distance.check(R.id.distance_units_foot);
                break;
        }
        switch (cfg.getBodyUnits()) {
            case centimeter:
                body.check(R.id.body_units_centimeter);
                break;
            case inch:
                body.check(R.id.body_units_inch);
                break;
        }

        weight.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.weight_units_kg:
                        cfg.setWeightUnits(Unit.kilogram);
                        break;
                    case R.id.weight_units_pound:
                        cfg.setWeightUnits(Unit.pound);
                        break;
                }
            }
        });

        distance.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.distance_units_meter:
                        cfg.setDistanceUnits(Unit.meter);
                        break;
                    case R.id.distance_units_foot:
                        cfg.setDistanceUnits(Unit.foot);
                        break;
                }
            }
        });

        body.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.body_units_centimeter:
                        cfg.setBodyUnits(Unit.centimeter);
                        break;
                    case R.id.body_units_inch:
                        cfg.setBodyUnits(Unit.inch);
                        break;
                }
            }
        });

    }

    private void initReps() {
        final Config cfg = getConfig();

        Spinner minReps = (Spinner) findViewById(R.id.min_reps_count);
        minReps.setAdapter(new NumbersListAdapter(getContext(), 100, true));
        minReps.setSelection(cfg.getMinReps());
        minReps.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cfg.setMinReps(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Spinner maxReps = (Spinner) findViewById(R.id.max_reps_count);
        maxReps.setAdapter(new NumbersListAdapter(getContext(), 100, true));
        maxReps.setSelection(cfg.getMaxReps());
        maxReps.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cfg.setMaxReps(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCaptions() {
        RadioButton kg = (RadioButton) findViewById(R.id.weight_units_kg);
        RadioButton pound = (RadioButton) findViewById(R.id.weight_units_pound);
        RadioButton meter = (RadioButton) findViewById(R.id.distance_units_meter);
        RadioButton foot = (RadioButton) findViewById(R.id.distance_units_foot);
        RadioButton cm = (RadioButton) findViewById(R.id.body_units_centimeter);
        RadioButton inch = (RadioButton) findViewById(R.id.body_units_inch);

        Coach coach = getCoach();
        kg.setText(coach.getLocalized(Unit.kilogram));
        pound.setText(coach.getLocalized(Unit.pound));
        meter.setText(coach.getLocalized(Unit.meter));
        foot.setText(coach.getLocalized(Unit.foot));
        cm.setText(coach.getLocalized(Unit.centimeter));
        inch.setText(coach.getLocalized(Unit.inch));
    }

    @Override
    protected int getRootResource() {
        return R.layout.activity_settings_sport;
    }

    public static final int TAB = 0;
}
