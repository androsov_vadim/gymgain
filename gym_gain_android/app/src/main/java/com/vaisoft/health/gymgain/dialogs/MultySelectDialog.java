package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import com.vaisoft.health.gymgain.R;

import java.util.ArrayList;
import java.util.List;

public abstract class MultySelectDialog extends DialogFragment
{
    abstract protected int configDialogCreate(AlertDialog.Builder builder);

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        mSelectedItems = new ArrayList<>();
        Bundle bundle = getArguments();
        List<String> list = bundle.getStringArrayList(DATA);
        List<Integer> chosen = bundle.getIntegerArrayList(DATA_SELECTED);
        boolean[] chosenMask = null;
        if (chosen != null) {
            chosenMask = getSelectedMask(chosen, list.size());
            mSelectedItems.addAll(chosen);
        }
        CharSequence[] cs = list.toArray(new CharSequence[list.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMultiChoiceItems(cs, chosenMask, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                if (isChecked) {
                    mSelectedItems.add(which);
                } else if (mSelectedItems.contains(which)) {
                    mSelectedItems.remove(Integer.valueOf(which));
                }
            }
        });
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                listener.onChosen(mSelectedItems);
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        int title = configDialogCreate(builder);
        builder.setTitle(title);
        return builder.create();
    }

    boolean[] getSelectedMask(List<Integer> choosen, int size) {
        boolean[] result = new boolean[size];
        for (Integer i: choosen) {
            result[i] = true;
        }
        return result;
    }

    public interface Listener {
        void onChosen(List<Integer> data);
    }

    private List<Integer> mSelectedItems;

    public static final String DATA = "items";
    public static final String DATA_SELECTED = "selected";
}
