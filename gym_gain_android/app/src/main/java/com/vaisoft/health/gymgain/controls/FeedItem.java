package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Localizer;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.ProgressCounter;
import com.vaisoft.health.gymgain.data.ProgressStatistics;

import static com.vaisoft.health.gymgain.R.id.positive;

public class FeedItem extends LinearLayout {
    public FeedItem(Context context) {
        super(context);
        init(context);
    }

    public FeedItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public FeedItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        inflate(context, R.layout.feed_item, this);
        mCaption = (TextView) findViewById(R.id.caption);
        mNeutral = (TextView) findViewById(R.id.neutral);
        mNeutralRight = (TextView) findViewById(R.id.neutral_right);
        mPositive = (TextView) findViewById(positive);
        mNegative = (TextView) findViewById(R.id.negative);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mBackground = (ImageView) findViewById(R.id.background);
        mIcon = (ImageView) findViewById(R.id.icon);
    }

    public void reset() {
        mPositive.setVisibility(GONE);
        mNegative.setVisibility(GONE);
        mNeutral.setVisibility(GONE);
        mNeutralRight.setVisibility(GONE);
    }

    public void setProgress(ProgressCounter data) {
        mProgress.setData(data);
    }

    public void setCaption(String text) {
        mCaption.setText(text);
    }

    public void applyDescription(ProgressStatistics statistics, Localizer localizer) {
        String positives = statistics.getGoods(localizer);
        if (!positives.isEmpty()) {
            setBetter(positives);
        }
        String negatives = statistics.getBads(localizer);
        if (!negatives.isEmpty()) {
            setWorse(negatives);
        }
    }

    public void setBetter(String text) {
        StringBuilder result = new StringBuilder();
        result.append(getContext().getString(R.string.feed_better));
        result.append(": ");
        result.append(text);
        setPositiveText(result.toString());
    }

    public void setWorse(String text) {
        StringBuilder result = new StringBuilder();
        result.append(getContext().getString(R.string.feed_worse));
        result.append(": ");
        result.append(text);
        setNegativeText(result.toString());
    }

    public void setNeutralText(String text, String rightText) {
        if (text != null) {
            mNeutral.setVisibility(VISIBLE);
            mNeutral.setText(text);
        }
        if (rightText != null) {
            mNeutralRight.setVisibility(VISIBLE);
            mNeutralRight.setText(rightText);
        }
    }

    public void setPositiveText(String text) {
        mPositive.setVisibility(VISIBLE);
        mPositive.setText(text);
    }

    public void setNegativeText(String text) {
        mNegative.setVisibility(VISIBLE);
        mNegative.setText(text);
    }

    public TextView getNeutralField() {
        mNeutral.setVisibility(VISIBLE);
        return mNeutral;
    }

    public void setItemBackgroundColor(@ColorRes int color) {
        int colorId = ContextCompat.getColor(getContext(), color);
        mBackground.setColorFilter(colorId);
    }

    public void setIcon(@DrawableRes int icon) {
        Drawable pic = ContextCompat.getDrawable(getContext(), icon);
        mIcon.setImageDrawable(pic);
    }

    public void setIcon(@DrawableRes int icon, @ColorRes int color) {
        setIcon(icon);
        int colorId = ContextCompat.getColor(getContext(), color);
        mIcon.setColorFilter(colorId);
    }

    public void showThumb(Progress.Estimate estimate) {
        switch (estimate) {
            case GOOD:
                setIcon(R.drawable.ic_thumb_up_white_36dp, R.color.feed_icon_positive);
                break;
            case SOSO:
                setIcon(R.drawable.ic_thumbs_up_down_white_36dp, R.color.feed_icon_neutral);
                break;
            case BAD:
                setIcon(R.drawable.ic_thumb_down_white_36dp, R.color.feed_icon_negative);
                break;
        }
    }

    private TextView mCaption;
    private TextView mNeutral;
    private TextView mNeutralRight;
    private TextView mPositive;
    private TextView mNegative;
    private ProgressBar mProgress;
    private ImageView mBackground;
    private ImageView mIcon;
}
