package com.vaisoft.health.gymgain.controls;

import androidx.recyclerview.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.View;

import com.vaisoft.health.gymgain.R;

public abstract class ContextViewHolder extends RecyclerView.ViewHolder
        implements  View.OnClickListener,
                    View.OnCreateContextMenuListener,
                    View.OnLongClickListener {

    protected abstract void clicked();
    protected abstract boolean isDeletable();

    public View view;

    public static final int MENU_EDIT = R.string.edit;
    public static final int MENU_DELETE = R.string.delete;

    public ContextViewHolder(View itemView, ContextAdapter<ContextViewHolder> adapter) {
        super(itemView);
        view = itemView;
        view.setOnClickListener(this);
        view.setOnCreateContextMenuListener(this);
        view.setOnLongClickListener(this);
        mAdapter = adapter;
    }

    protected boolean isEditable()
    {
        return true;
    }

    @Override
    public void onClick(View v) {
        clicked();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (isEditable()) {
            menu.add(Menu.NONE, MENU_EDIT, 0, R.string.edit);
        }
        if (isDeletable()) {
            menu.add(Menu.NONE, MENU_DELETE, 0, R.string.delete);
        }
    }

    @Override
    public boolean onLongClick(View v) {
        int item = getAdapterPosition();
        if (item != -1) {
            mAdapter.setContextItem(item);
        }
        return false;
    }

    public void start() {

    }

    public void stop() {

    }

    private ContextAdapter<ContextViewHolder> mAdapter;
}
