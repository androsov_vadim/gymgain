package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.View;

public class ViewPagerWrapContent extends ViewPager {
    public ViewPagerWrapContent(Context context) {
        super(context);
    }

    public ViewPagerWrapContent(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public Fragment getCurrentFragment() {
        return ((RotateFriendlyPagerAdapter) getAdapter()).getFragment(getCurrentItem());
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if(null != getAdapter()) {
            int height = 0;
            View child = getCurrentFragment().getView();
            if (child != null) {
                child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
                height = child.getMeasuredHeight();
                if (height < getMinimumHeight()) {
                    height = getMinimumHeight();
                }
            }

            heightMeasureSpec = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
