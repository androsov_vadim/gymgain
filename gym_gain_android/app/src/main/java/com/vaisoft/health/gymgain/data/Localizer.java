package com.vaisoft.health.gymgain.data;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Localizer {

    public Localizer(JSONObject data) {
        Iterator<String> keys = data.keys();
        mTranslations = new HashMap<>();
        while (keys.hasNext()) {
            String key = keys.next();
            try {
                String value = data.getString(key);
                mTranslations.put(key, value);
            } catch (JSONException e) {
                Log.e(getClass().getName(), e.getMessage());
                return;
            }

        }
        mOk = true;
    }

    public String translate(String id)
    {
        String result = mTranslations.get(id);
        if (result == null) {
            return id;
        }
        return result;
    }

    public List<String> translate(List<String> ids) {
        List<String> result = new ArrayList<>(ids.size());
        for (String id: ids) {
            String translated = translate(id);
            result.add(translated);
        }
        return result;
    }

    public boolean isOk()
    {
        return mOk;
    }

    private boolean mOk = false;
    private Map<String, String> mTranslations;
}
