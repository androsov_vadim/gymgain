package com.vaisoft.health.gymgain.main;

import com.vaisoft.health.gymgain.controls.CoachListFragment;

public abstract class MainActivityFragment extends CoachListFragment {

    protected MainActivity getMainActivity() {
        return (MainActivity) getActivity();
    }
}
