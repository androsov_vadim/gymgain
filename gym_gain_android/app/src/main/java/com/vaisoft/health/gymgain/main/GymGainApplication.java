package com.vaisoft.health.gymgain.main;

import android.app.Application;
import android.content.Context;

import com.google.android.gms.ads.MobileAds;
import com.vaisoft.health.gymgain.R;

public class GymGainApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        String id = getString(R.string.ads_app_id);
        Context context = getApplicationContext();
        MobileAds.initialize(context, id);
    }
}
