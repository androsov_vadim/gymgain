package com.vaisoft.health.gymgain.controls;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.appcompat.app.AppCompatActivity;

import com.vaisoft.health.gymgain.R;

public abstract class GymGainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getResources().getBoolean(R.bool.portrait_only)){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        loadThemePreferences();
        setTheme(THEME);
    }

    /*
    @Override
    protected void onResume() {
        super.onResume();
        fullScreencall();
    }

    public void fullScreencall() {
        //try fitssystemwindow=false
        //https://developer.android.com/training/system-ui/navigation.html
        View decorView = getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN |
                View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }
    */

    private void loadThemePreferences() {
        SharedPreferences preferences = getAppPreferences();
        int theme = preferences.getInt(THEME_ID, getCurrentTheme());
        applyTheme(theme);
    }

    private SharedPreferences getAppPreferences() {
        return getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE);
    }

    protected boolean applyTheme(@StyleRes int theme) {
        if (theme == THEME) {
            return false;
        }
        SharedPreferences preferences = getAppPreferences();
        preferences.edit().putInt(THEME_ID, theme).apply();
        THEME = theme;
        return true;
    }

    private @StyleRes int getCurrentTheme() {
        return THEME;
    }

    private static @StyleRes int THEME = R.style.Theme_AppDesign;

    private static final String THEME_ID = "theme";
    private static final String APP_PREFERENCES = "app_prefs";
}
