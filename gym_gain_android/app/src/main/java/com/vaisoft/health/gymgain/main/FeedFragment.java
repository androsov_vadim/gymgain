package com.vaisoft.health.gymgain.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.feed.Feed;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.dialogs.AddActionDialog;
import com.vaisoft.health.gymgain.edit.BodyLogActivity;

import java.util.ArrayList;
import java.util.List;

public class FeedFragment extends MainActivityFragment {

    @Override
    public void onContextMenu(int id) {
        switch (id) {
            case ContextViewHolder.MENU_EDIT:
                mAdapter.getContextFeed().clicked();
                break;
            case ContextViewHolder.MENU_DELETE:
                mAdapter.deleteContextFeed();
                break;
        }
    }

    @Override
    protected Adapter createAdapter() {
        mAdapter = new FeedRecyclerViewAdapter();
        return mAdapter;
    }

    public void showAddActionDialog() {
        AddActionDialog dialog = new AddActionDialog();

        List<Record> records = getCoach().getRecords();
        ArrayList<String> recordNames = new ArrayList<>();
        for (Record record : records) {
            recordNames.add(record.getName(getCoach()));
        }

        Bundle args = new Bundle();
        args.putStringArrayList(AddActionDialog.DATA, recordNames);
        dialog.setArguments(args);
        dialog.setTargetFragment(this, 0);
        dialog.show(getActivity().getSupportFragmentManager(), "AddActionDialog");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (data.hasExtra(AddActionDialog.BODY)) {
                addNewBodyLogItem();
            } else if (data.hasExtra(AddActionDialog.RECORD)) {
                int recordIndex = data.getIntExtra(AddActionDialog.RECORD, -1);
                startRecord(recordIndex);
            } else if (data.hasExtra(AddActionDialog.LIST_TRAININGS)) {
                activateParentTab(MainActivity.TAB_TRAINING);
            } else if (data.hasExtra(AddActionDialog.LIST_EXERCISES)) {
                activateParentTab(MainActivity.TAB_EXERCISE);
            } else if (data.hasExtra(AddActionDialog.BOOSTER)) {
                showAddBoosterDialog();
            }
        }
    }

    private void showAddBoosterDialog() {
        getMainActivity().showAddBoosterDialog();
    }

    private void activateParentTab(int tab) {
        getMainActivity().activateTab(tab);
    }

    private void addNewBodyLogItem() {
        startActivity(new Intent(getContext(), BodyLogActivity.class));
    }

    private void startRecord(int index) {
        List<Record> records = getCoach().getRecords();
        int i = 0;
        Record source = null;
        for (Record record : records) {
            if (i == index) {
                source = record;
                break;
            }
            ++i;
        }
        source.startNewWorkout(getContext(), getCoach());
    }

    public abstract class FeedAdapter extends Adapter<ContextViewHolder> {
        public abstract Feed getItem(int position);
    }

    public class FeedRecyclerViewAdapter extends FeedAdapter
    {
        @Override
        public void updateData(boolean force) {
            Context context = getContext();
            getCoach().updateFeedFromGui(context);
            if (force || getCoach().shouldUpdateRecordsList()) {
                mData = getCoach().getFilteredFeed(context, getFilter());
                notifyDataSetChanged();
            }
        }

        @Override
        public ContextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(viewType, parent, false);
            return Feed.createViewHolder(viewType, view, this);
        }

        @Override
        public Feed getItem(int position) {
            return mData.get(position);
        }

        @Override
        public int getItemViewType(int position) {
            return getItem(position).getViewType();
        }

        @Override
        public void onBindViewHolder(ContextViewHolder holder, int position) {
            Feed feed = getItem(position);
            feed.bind(holder, getCoach());
        }

        @Override
        public void onViewAttachedToWindow(ContextViewHolder holder) {
            super.onViewAttachedToWindow(holder);
            holder.start();
        }

        @Override
        public void onViewDetachedFromWindow(ContextViewHolder holder) {
            super.onViewDetachedFromWindow(holder);
            holder.stop();
        }

        @Override
        public int getItemCount() {
            if (mData != null) {
                return mData.size();
            } else {
                return 0;
            }
        }

        public Feed getContextFeed() {
            return getItem(getContextItem());
        }

        public void deleteContextFeed() {
            getContextFeed().deleted(getCoach());
            mData.remove(getContextItem());
            notifyDataSetChanged();
            updateData(true);
        }

        private List<Feed> mData;
     }

    private FeedRecyclerViewAdapter mAdapter;
}
