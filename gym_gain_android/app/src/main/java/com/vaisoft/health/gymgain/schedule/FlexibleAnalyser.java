package com.vaisoft.health.gymgain.schedule;

import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.joda.time.Days;

public class FlexibleAnalyser extends AbstractScheduleAnalyser
{
    public FlexibleAnalyser(DateTime date, DateTime lastUpdatedDate, Schedule.Flexible flexible, DateTime lastAnalysed) {
        super(date, lastUpdatedDate, lastAnalysed);
        mSchedule = flexible;
        init();
    }

    @Override
    protected DateTime getCurrentPeriodStart(DateTime checkDate) {
        return getFlexiblePeriodStart(checkDate);
    }

    @Override
    protected DateTime analyseWorkoutsFrom() {
        int period = mSchedule.getDaysEstimate();
        return getCurrentPeriodStart().minusDays(period);
    }

    @Override
    protected int getPlannedCount(DateTime start, DateTime finish) {
        int days = Days.daysBetween(start, finish).getDays() + 1;
        int estimate = mSchedule.getDaysEstimate();
        int result = days / estimate * mSchedule.times;
        result += days % estimate * mSchedule.times / estimate; // mSchedule.times / estimate is 1 / step
        return result;
    }

    @Override
    protected boolean isPlannedOn(DateTime date) {
        int period = mSchedule.getDaysEstimate();
        int workouts = mSchedule.times;
        if (workouts >= period) {
            return true;
        }
        int step = period / workouts;
        DateTime start;
        if (date.isBefore(getHistoryStart())) {
            return false;
        }
        if (date.isBefore(getCurrentPeriodStart())) {
            start = getHistoryStart();
        } else {
            start = getCurrentPeriodStart();
        }
        int daysPassed = Days.daysBetween(start, date).getDays() + 1;
        int stepsPassed = daysPassed / step;
        DateTime targetDate;
        if (stepsPassed == workouts) {
            targetDate = start.plusDays(period - 1);
        } else {
            targetDate = start.plusDays(stepsPassed * step - 1);
        }
        return Utils.isEqualDay(date, targetDate);
    }

    @Override
    protected boolean isLegacyActual() {
        return isAboutMonth();
    }

    @Override
    protected boolean isSingleDayPlan() {
        return (mSchedule.period == Schedule.PeriodType.DAY) && (mSchedule.periodsCount == 1);
    }

    private boolean isAboutMonth() {
        return mSchedule.getDaysEstimate() <= 30;
    }

    private DateTime getFlexiblePeriodStart(DateTime date) {
        //начало текущего периода
        int periodsBack = mSchedule.periodsCount - 1;
        switch (mSchedule.period) {
            case DAY:
                return date.minusDays(periodsBack);
            case WEEK:
                DateTime weeklyPeriodStart = getWeeklyPeriodStart(date);
                return weeklyPeriodStart.minusWeeks(periodsBack);
            case MONTH:
                DateTime monthStart = date.withDayOfMonth(1);
                return monthStart.minusMonths(periodsBack);
            case YEAR:
                DateTime yearStart = date.withDayOfYear(1);
                return yearStart.minusYears(periodsBack);
        }
        return null;
    }

    private Schedule.Flexible mSchedule;
}
