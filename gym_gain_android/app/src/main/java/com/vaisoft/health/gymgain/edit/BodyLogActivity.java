package com.vaisoft.health.gymgain.edit;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.controls.ContextAdapter;
import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.controls.ProgressChart;
import com.vaisoft.health.gymgain.data.BodyMeasure;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.dialogs.AddBodyLogDialog;
import com.vaisoft.health.gymgain.dialogs.BodyLogDialog;
import com.vaisoft.health.gymgain.dialogs.EditBodyLogEntryDialog;
import com.vaisoft.health.gymgain.dialogs.ModifyBodyLogDialog;
import com.vaisoft.health.gymgain.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class BodyLogActivity extends EditorActivity
    implements EditBodyLogEntryDialog.Listener
{
    @Override
    public void onCoachConnected() {
        setContentView(R.layout.activity_body_log);

        Intent intent = getIntent();
        mIndex = intent.getIntExtra(ITEM, -1);

        if (mIndex >= 0) {
            mEntry = getCoach().getBodyMeasurer().getLogEntry(mIndex);
        }

        mIsNew = !dataExists();
        if (mIsNew) {
            initCommonInterface(R.string.body_log_new);
        } else {
            initCommonInterface(R.string.body_log_edit);
        }

        RecyclerView rw = findViewById(R.id.log_items);
        Utils.initOrientationAwareList(rw);
        mLogData = new BodyLogAdapter(this);
        rw.setAdapter(mLogData);

        initButtons();

        if (!mLogData.loadEditTarget()) {
            initLogData();
        }

        mChart = findViewById(R.id.body_log_chart);
        mChart.setAdapter(new ChartAdapter());
    }

    private void initLogData() {
        if (dataExists()) {
            mLogData.setItems(mEntry);
        } else {
            BodyMeasure.LogEntry lastEntry = getCoach().getBodyMeasurer().getLastEntry();
            if (lastEntry != null) {
                mLogData.setItems(lastEntry);
            }
        }
    }

    private boolean dataExists() {
        return mEntry != null;
    }

    private void initButtons() {
        FloatingActionButton addMetric = findViewById(R.id.add_metric);
        addMetric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddDialog();
            }
        });
        mSaveAllButton = findViewById(R.id.save_all);
        mSaveAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mIsNew || isChanged()) {
                    onSaveChanges();
                } else {
                    goBack();
                }
            }
        });
        mSaveAllButton.setVisibility(View.INVISIBLE);
    }

    private void showAddDialog() {
        AddBodyLogDialog dialog = new AddBodyLogDialog();
        Bundle args = new Bundle();
        args.putStringArrayList(BodyLogDialog.USED_ITEMS, mLogData.getUsedIds());
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "AddBodyLogDialog");
    }

    private void showModifyDialog(BodyMeasure.LogItem item) {
        ModifyBodyLogDialog dialog = new ModifyBodyLogDialog();
        Bundle args = new Bundle();
        args.putStringArrayList(BodyLogDialog.USED_ITEMS, mLogData.getUsedIds());
        args.putString(ModifyBodyLogDialog.METRIC_NAME, item.object);
        args.putString(ModifyBodyLogDialog.UNIT, item.unitGeneral.toString());
        args.putString(ModifyBodyLogDialog.GROW, Value.getTargetString(item.value.shouldGrow()));
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), "ModifyBodyLogDialog");
    }

    private void showEditDialog(final int itemIndex) {
        EditBodyLogEntryDialog dialog = new EditBodyLogEntryDialog();
        BodyMeasure.LogItem item = mLogData.getItems().get(itemIndex);
        Bundle arguments = new Bundle();
        Unit unit = getUnit(item);
        arguments.putString(EditBodyLogEntryDialog.ARG_CAPTION, getCoach().getLocalized(item.object));
        arguments.putString(EditBodyLogEntryDialog.ARG_UNIT, unit.toString());
        arguments.putLong(EditBodyLogEntryDialog.ARG_VALUE, item.value.get(unit));
        arguments.putInt(EditBodyLogEntryDialog.ARG_SELECTED, itemIndex);
        dialog.setArguments(arguments);
        dialog.show(getSupportFragmentManager(), "EditBodyLogEntryDialog");
    }

    private Unit getUnit(BodyMeasure.LogItem item) {
        Unit category = item.value.unit().getCategory();
        return getCoach().getConfig().getUnits(category);
    }

    @Override
    public void onEntryValueChanged(int index, long value) {
        BodyMeasure.LogItem item = mLogData.getItems().get(index);
        Unit unit = getUnit(item);
        item.value.set(unit, value);
        onDataChanged();
    }

    @Override
    public void onEntryDeleted(int index) {
        removeLogItem(index);
    }

    private void removeLogItem(int index) {
        mLogData.getItems().remove(index);
        onDataChanged();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case ContextViewHolder.MENU_EDIT:
                showEditDialog(mLogData.getContextItem());
                break;
            case ContextViewHolder.MENU_DELETE:
                removeLogItem(mLogData.getContextItem());
                break;
        }
        return super.onContextItemSelected(item);
    }

    public void addPattern(BodyMeasure.Pattern pattern) {
        BodyMeasure.LogItem item = getCoach().getBodyMeasurer().createLogItem(pattern, getCoach().getConfig());
        mLogData.addItem(item);
        onDataChanged();
    }

    public void modifyPattern(String initialName, BodyMeasure.Pattern pattern) {
        BodyMeasure.LogItem item = mLogData.getItemForPart(initialName);
        boolean changes = false;
        if (!initialName.equals(pattern.part)) {
            mLogData.renameItem(initialName, pattern.part);
            scheduleRename(initialName, pattern.part);
            changes = true;
        }
        if (item.value.shouldGrow() != pattern.targetInc) {
            item.value.setShouldGrow(pattern.targetInc);
            changes = true;
        }
        if (changes) {
            onDataChanged();
        }
    }

    private void scheduleRename(String source, String target) {
        for (RenameOperation operation: mPendingRename) {
            if (operation.target.equals(source)) {
                operation.target = target;
                return;
            }
        }
        mPendingRename.add(new RenameOperation(source, target));
    }

    private void applyRename() {
        for (RenameOperation operation: mPendingRename) {
            operation.apply();
        }
        mPendingRename.clear();
    }

    private void onDataChanged() {
        mLogData.notifyDataSetChanged();
        mChart.notifyDataChanged();
        setChanged(true);
        mSaveAllButton.show();
    }

    @Override
    protected boolean isEditable() {
        return true;
    }

    @Override
    protected boolean canAutoSave() {
        return false;
    }

    @Override
    protected boolean isDeletable() {
        return true;
    }

    @Override
    protected void deleteObjectConfirmed() {
        if (dataExists()) {
            getCoach().deleteBodyLog(this, mEntry);
            setChanged(false);
        }
        goBack();
    }

    @Override
    protected int getEditTargetId() {
        return R.string.measurements;
    }

    @Override
    protected boolean saveChanges() {
        List<BodyMeasure.LogItem> items = mLogData.getItems();
        if (items.isEmpty()) {
            if (dataExists()) {
                getCoach().deleteBodyLog(this, mEntry);
            }
            return true;
        }
        applyRename();
        if (!dataExists()) {
            BodyMeasure.LogEntry entry = new BodyMeasure.LogEntry();
            entry.date = Utils.now();
            entry.items = items;
            getCoach().addBodyLog(this, entry);
        } else {
            getCoach().updateBodyLog(this, items, mEntry);
        }
        return true;
    }

    private BodyMeasure.LogItem getPreviousItem(BodyMeasure.LogItem item) {
        return getCoach().getBodyMeasurer().getPreviousTo(mEntry, item);
    }

    //------------------------------------------------------------------------------

    private class ChartAdapter implements ProgressChart.Adapter {

        public ChartAdapter() {
            mMeasurer = getCoach().getBodyMeasurer();
        }

        @Override
        public int getCount() {
            return mMeasurer.getValidLogSize();
        }

        @Override
        public int getMarkerIndex() {
            return mIndex;
        }

        @Override
        public Progress getProgress(int index) {
            if (index == 0) {
                return Progress.initial();
            }
            int change = mMeasurer.getTotalProgressSafe(index);
            if (change == 0) {
                return Progress.stable();
            } else {
                return Progress.changed(change > 0, true);
            }
        }

        private BodyMeasure mMeasurer;
    }

    private class BodyLogAdapter
            extends ContextAdapter<BodyLogAdapter.ViewHolder> {

        public class ViewHolder extends ContextViewHolder {

            public ViewHolder(View view, ContextAdapter adapter) {
                super(view, adapter);
                mName = view.findViewById(R.id.log_id);
                mValue = view.findViewById(R.id.log_value);
                mUnit = view.findViewById(R.id.log_unit);
                mProgress = view.findViewById(R.id.log_progress);
                mIcon = view.findViewById(R.id.log_editable);
                mIconBackground = view.findViewById(R.id.log_icon_background);
                initEditor(view);
            }

            private void initEditor(View view) {
                View editor = view.findViewById(R.id.log_edit);
                editor.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showModifyDialog(mItem);
                    }
                });
            }

            public void bind(BodyMeasure.LogItem item, Unit itemUnit) {
                mItem = item;
                mName.setText(getCoach().getLocalized(item.object));
                mUnit.setText(getCoach().getLocalized(itemUnit));
                mValue.setText(itemUnit.toString(item.value.get(itemUnit)));

                Progress progress = Progress.initial();
                BodyMeasure.LogItem previousItem = getPreviousItem(item);
                long diff = 0;
                if (previousItem != null) {
                    progress = item.getProgress(previousItem);
                    diff = Math.abs(item.value.get(itemUnit) - previousItem.value.get(itemUnit));
                }
                applyProgressChange(progress.getChange(), itemUnit.toString(diff));
                applyProgressEstimate(progress.getEstimate());
            }

            private void applyProgressEstimate(Progress.Estimate estimate) {
                int color = 0;
                switch (estimate) {
                    case GOOD:
                        color = getColor(R.color.chart_good);
                        break;
                    case BAD:
                        color = getColor(R.color.chart_bad);
                        break;
                    case SOSO:
                        color = getColor(R.color.chart_soso);
                        break;
                }

                mIconBackground.setColorFilter(color);
                mProgress.setTextColor(color);
            }

            private void applyProgressChange(Progress.Change change, String diff) {
                StringBuilder progressNumber = null;
                Drawable pic = null;
                switch (change) {
                    case MORE:
                        mProgress.setVisibility(View.VISIBLE);
                        progressNumber = new StringBuilder("(");
                        progressNumber.append('+');
                        pic = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_trending_up_white_36dp);
                        break;
                    case LESS:
                        mProgress.setVisibility(View.VISIBLE);
                        progressNumber = new StringBuilder("(");
                        progressNumber.append('-');
                        pic = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_trending_down_white_36dp);
                        break;
                    case STABLE:
                        mProgress.setVisibility(View.GONE);
                        pic = ContextCompat.getDrawable(view.getContext(), R.drawable.ic_trending_flat_white_36dp);
                        break;
                }
                if (progressNumber != null) {
                    progressNumber.append(diff);
                    progressNumber.append(")");
                    mProgress.setText(progressNumber.toString());
                }
                mIcon.setImageDrawable(pic);
            }

            private int getColor(int res) {
                return ContextCompat.getColor(view.getContext(), res);
            }

            @Override
            protected void clicked() {
                int item = getAdapterPosition();
                if (item != -1) {
                    showEditDialog(item);
                }
            }

            @Override
            protected boolean isDeletable() {
                return true;
            }

            private final TextView mName;
            private final TextView mUnit;
            private final TextView mValue;
            private final TextView mProgress;
            private final ImageView mIcon;
            private final ImageView mIconBackground;
            private BodyMeasure.LogItem mItem;
        }

        public BodyLogAdapter(Context context) {
            mInflater = LayoutInflater.from(context);
            mLogItems = new ArrayList<>();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.body_log_item, parent, false);
            return new ViewHolder(view, this);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            BodyMeasure.LogItem item = mLogItems.get(position);
            Unit unit = getUnit(item);
            holder.bind(item, unit);
      }

        public void setItems(BodyMeasure.LogEntry logEntry) {
            mLogItems.clear();
            for (BodyMeasure.LogItem item: logEntry.items) {
                mLogItems.add(item.clone());
            }
            setEditTarget(mLogItems);
            notifyDataSetChanged();
        }

        public boolean loadEditTarget() {
            List<BodyMeasure.LogItem> target = getCoach().getEditTarget(List.class);
            if (target != null) {
                mLogItems = target;
                return true;
            }
            return false;
        }

        public void addItem(BodyMeasure.LogItem item) {
            mLogItems.add(item);
        }

        public List<BodyMeasure.LogItem> getItems() {
            return mLogItems;
        }

        @Override
        public int getItemCount() {
            return mLogItems.size();
        }

        public ArrayList<String> getUsedIds() {
            ArrayList<String> result = new ArrayList<>();
            for (BodyMeasure.LogItem item: mLogItems) {
                result.add(item.object);
            }
            return result;
        }

        public void renameItem(String source, String target) {
            for (BodyMeasure.LogItem item: mLogItems) {
                item.tryRename(source, target);
            }
        }

        public BodyMeasure.LogItem getItemForPart(String part) {
            for (BodyMeasure.LogItem item: mLogItems) {
                if (item.object.equals(part)) {
                    return item;
                }
            }
            return null;
        }

        private LayoutInflater mInflater;
        private List<BodyMeasure.LogItem> mLogItems;
    }

    private class RenameOperation {
        public String source;
        public String target;

        public RenameOperation(String source, String target) {
            this.source = source;
            this.target = target;
        }

        private boolean hasSense() {
            return !source.equals(target);
        }

        public void apply() {
            if (hasSense()) {
                Coach coach = getCoach();
                coach.renameBodyLogItem(BodyLogActivity.this, source, target);
            }
        }
    }

    private BodyMeasure.LogEntry mEntry;
    private BodyLogAdapter mLogData;
    private ProgressChart mChart;
    private int mIndex;
    private boolean mIsNew;
    private List<RenameOperation> mPendingRename = new ArrayList<>();
    private FloatingActionButton mSaveAllButton;

    public static final String ITEM = "item";
}
