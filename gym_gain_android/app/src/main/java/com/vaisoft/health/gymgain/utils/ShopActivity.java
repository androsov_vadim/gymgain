package com.vaisoft.health.gymgain.utils;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.IdRes;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.billing.Billing;
import com.vaisoft.health.gymgain.edit.CoachActivity;
import com.vaisoft.health.gymgain.main.MainActivity;

import org.joda.time.DateTime;

public class ShopActivity extends CoachActivity {
    @Override
    public void onCoachConnected() {
        setContentView(R.layout.activity_shop);
        initCommonInterface(R.string.menu_shop);

        if (isRewardAvailable()) {
            initReward();
        }
        initBasic();
        initPro();
        initPremium();

        mBilling = new Billing(this, true, new Billing.ReadyListener() {
            @Override
            public void onBillingReady() {
                billingLoaded();
            }
        });
        mBilling.bind();
    }

    private void billingLoaded() {
        if (mBilling.isOk()) {
            onBillingEnabled();
        } else {
            onBillingDisabled();
        }
    }

    private void onBillingDisabled() {
        activateReward(true);
    }

    private void onBillingEnabled() {
        if (mBilling.isBasicPurchased()) {
            setPositiveBackgroungColor(R.id.purchase_basic_back);
        }
        if (mBilling.isProPurchased()) {
            setPositiveBackgroungColor(R.id.purchase_pro_back);
        }
        if (mBilling.isPremiumPurchased()) {
            setPositiveBackgroungColor(R.id.purchase_premium_back);
        }
        boolean isBasicDone = (mBilling.isBasicOrBetter() || mBilling.isProOrBetter() || mBilling.isPremiumOrBetter());
        activateReward(!isBasicDone);
        activateBasic(!isBasicDone);
        activatePro(!(mBilling.isProOrBetter() || mBilling.isPremiumOrBetter()));
        activatePremium(!mBilling.isPremiumOrBetter());
    }

    private void activateBasic(boolean active) {
        mBuyBasic.setEnabled(active);
        if (active) {
            String price = mBilling.getPriceBasic();
            mPriceBasic.setVisibility(View.VISIBLE);
            mPriceBasic.setText(price);
        } else {
            mPriceBasic.setVisibility(View.INVISIBLE);
        }
    }

    private void activatePremium(boolean active) {
        mBuyPremium.setEnabled(active);
        if (active) {
            String price = mBilling.getPricePremium();
            mPricePremium.setVisibility(View.VISIBLE);
            mPricePremium.setText(price);
        } else {
            mPricePremium.setVisibility(View.INVISIBLE);
        }
        String price = active ? mBilling.getPricePremium() : "";
        mPricePremium.setText(price);
    }

    private void activatePro(boolean active) {
        mBuyPro.setEnabled(active);
        if (active) {
            String price = mBilling.getPricePro();
            mPricePro.setVisibility(View.VISIBLE);
            mPricePro.setText(price);
        } else {
            mPricePro.setVisibility(View.INVISIBLE);
        }
    }

    private void initPremium() {
        mPricePremium = (TextView) findViewById(R.id.purchase_premium_cost);
        mBuyPremium = (Button) findViewById(R.id.buy_premium);
        mBuyPremium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPurchaseUpdate = true;
                mBilling.purchasePremium();
            }
        });
    }

    private void initPro() {
        mPricePro = (TextView) findViewById(R.id.purchase_pro_cost);
        mBuyPro = (Button) findViewById(R.id.buy_pro);
        mBuyPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPurchaseUpdate = true;
                mBilling.purchasePro();
            }
        });
    }

    private void initBasic() {
        mPriceBasic = (TextView) findViewById(R.id.purchase_basic_cost);
        mBuyBasic = (Button) findViewById(R.id.buy_basic);
        mBuyBasic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPurchaseUpdate = true;
                mBilling.purchaseBasic();
            }
        });
    }

    private void initReward() {
        mWatchTrailer = (Button) findViewById(R.id.watch_trailer);
        mWaitWatchTrailer = findViewById(R.id.rewarded_ads_wait);
        mWatchedText = (TextView) findViewById(R.id.rewarded_ads_ok);

        mWatchTrailer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAds.isLoaded()) {
                    mAds.show();
                }
            }
        });
    }

    private void deactivateReward() {
        mWaitWatchTrailer.setVisibility(View.INVISIBLE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((mBilling != null) && mBilling.isOk() && mBilling.canHandle(requestCode)) {
            if (resultCode == Activity.RESULT_OK) {
                mBilling.handleBuyResult(requestCode, data);
            }
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void activateReward(boolean active) {
        if (active && isRewardAvailable()) {
            showAdsFreeInfo();
            if (mAds == null) {
                prepareReward();
            }
            mWatchTrailer.setEnabled(active && mAds.isLoaded());
        } else {
            deactivateReward();
            mWatchedText.setVisibility(View.INVISIBLE);
            mWatchTrailer.setEnabled(false);
        }
    }

    private void prepareReward() {
        mAds = MobileAds.getRewardedVideoAdInstance(this);
        mAds.setRewardedVideoAdListener(new AdsListener() {
            @Override
            public void onRewardedVideoAdLoaded() {
                mWatchTrailer.setEnabled(true);
                mWaitWatchTrailer.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onRewarded(RewardItem rewardItem) {
                onRewardReceived();
                mWatchTrailer.setEnabled(false);
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int i) {
                View error = findViewById(R.id.rewarded_ads_fail);
                error.setVisibility(View.VISIBLE);
                mWaitWatchTrailer.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onRewardedVideoCompleted() {

            }
        });

        loadRewardedVideoAd();
    }

    private boolean isRewardAvailable() {
        return true;
    }

    private void onRewardReceived() {
        getCoach().getConfig().adsFreeGained(this);
        showAdsFreeInfo();
    }

    private void setPositiveBackgroungColor(@IdRes int id) {
        View view = findViewById(id);
        int color = ContextCompat.getColor(this, R.color.positive_color);
        view.setBackgroundColor(color);
    }

    private void showAdsFreeInfo() {
        DateTime adsFreeEnd = getCoach().getConfig().getAdsFreePeriodEnd();
        if (adsFreeEnd != null) {
            setPositiveBackgroungColor(R.id.rewarded_ads_back);
            mWatchedText.setVisibility(View.VISIBLE);
            String message = getString(R.string.shop_ads_free_up_to);
            message += " ";
            message += Utils.formatDateTime(adsFreeEnd);
            mWatchedText.setText(message);
        }
    }

    private void loadRewardedVideoAd() {
        String unitId = getString(R.string.ads_rewarded_unit_id);
        AdRequest request = new AdRequest.Builder().build();
        mAds.loadAd(unitId, request);
    }

    @Override
    protected void onDestroy() {
        if (mAds != null) {
            mAds.destroy(this);
        }
        if (mBilling != null) {
            mBilling.unbind();
        }
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        if (mAds != null) {
            mAds.pause(this);
        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        if (mAds != null) {
            mAds.resume(this);
        }
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        Intent intent = NavUtils.getParentActivityIntent(this);
        if (mPurchaseUpdate) {
            intent.putExtra(MainActivity.UPDATE_PURCHASES, true);
        }
        NavUtils.navigateUpTo(this, intent);
    }

    //-----------------------------------------------------------------------

    private abstract class AdsListener implements RewardedVideoAdListener {

        @Override
        public void onRewardedVideoAdOpened() {

        }

        @Override
        public void onRewardedVideoStarted() {

        }

        @Override
        public void onRewardedVideoAdClosed() {

        }

        @Override
        public void onRewardedVideoAdLeftApplication() {

        }
    }

    private RewardedVideoAd mAds;
    private Billing mBilling;
    private boolean mPurchaseUpdate;

    private Button mWatchTrailer;
    private Button mBuyBasic;
    private Button mBuyPro;
    private Button mBuyPremium;

    private View mWaitWatchTrailer;

    private TextView mPriceBasic;
    private TextView mPricePro;
    private TextView mPricePremium;
    private TextView mWatchedText;
}
