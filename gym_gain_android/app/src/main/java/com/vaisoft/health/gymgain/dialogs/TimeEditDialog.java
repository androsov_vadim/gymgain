package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.View;
import android.widget.NumberPicker;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;

public class TimeEditDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.edit_elapsed_time);
        builder.setIcon(R.drawable.ic_schedule_black_24dp);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                long time = Utils.getTime(
                        mHours.getValue(),
                        mMinutes.getValue(),
                        mSeconds.getValue());
                Listener listener = (Listener) getActivity();
                listener.onTimeEdited(new DateTime(mStartTime + time));
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.setView(createInterface());

        initArguments();

        return builder.create();
    }

    private void initArguments() {
        Bundle args = getArguments();
        mStartTime = args.getLong(START_TIME, 0);
        long finishTime = args.getLong(FINISH_TIME, 0);
        if ((mStartTime > 0) && (finishTime > 0)) {
            long diffSec = (finishTime - mStartTime) / 1000;
            int h = (int) Utils.getHours(diffSec);
            if (h > MAX_HOURS) {
                mHours.setMaxValue(h);
            }
            mHours.setValue(h);
            mMinutes.setValue((int) Utils.getMins(diffSec));
            mSeconds.setValue((int) Utils.getSecs(diffSec));
        }
    }

    private View createInterface() {
        mView = View.inflate(getActivity(), R.layout.edit_time, null);

        mHours = (NumberPicker) mView.findViewById(R.id.pick_hours);
        mHours.setMinValue(0);
        mHours.setMaxValue(MAX_HOURS);

        mMinutes = (NumberPicker) mView.findViewById(R.id.pick_minutes);
        mMinutes.setMinValue(0);
        mMinutes.setMaxValue(60);

        mSeconds = (NumberPicker) mView.findViewById(R.id.pick_seconds);
        mSeconds.setMinValue(0);
        mSeconds.setMaxValue(60);

        return mView;
    }

    //------------------------------------------------------------------------------

    public interface Listener {
        void onTimeEdited(DateTime date);
    }

    private View mView;

    private NumberPicker mHours;
    private NumberPicker mMinutes;
    private NumberPicker mSeconds;
    private long mStartTime;

    public static final String START_TIME = "start";
    public static final String FINISH_TIME = "finish";

    private static final int MAX_HOURS = 99;
}
