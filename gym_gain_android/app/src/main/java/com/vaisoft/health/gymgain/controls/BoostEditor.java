package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Handler;
import androidx.annotation.ColorRes;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ProgressBar;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;

public abstract class BoostEditor extends LinearLayout {

    protected abstract long[] getSteps();

    protected abstract String getStepAsText(long step);

    protected abstract String getValueAsText(long value);

    protected abstract long getTextAsValue(String text);

    protected abstract void setUnit(Unit unit);

    protected abstract int getValueInputType();


    public BoostEditor(Context context) {
        super(context);
        init();
    }

    public BoostEditor(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoostEditor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected void setValue(long value) {
        mValue = value;
        updateValue();
    }

    protected void startValueProgress() {
        setValue(0);
        applyTargetValue();
    }

    private void setBoostValue(long value, boolean fireChangeEvent) {
        if (value < 0) {
            value = 0;
        }
        updateBoost();
        setValue(value);
        if (fireChangeEvent) {
            applyValue();
        }
    }

    private void applyValue() {
        mStatus = Value.Status.Applied;
        notifyChangeListeners();
    }

    private void updateValue() {
        long effectiveValue = getEffectiveValue();
        String text = getValueAsText(effectiveValue);
        mValueText.setText(text);
    }

    protected void notifyChangeListeners() {
        if (mListener != null) {
            mNotificationInProgress = true;
            mListener.onValueChanged(this, mValue, mStatus);
            mNotificationInProgress = false;
        }
    }

    private void init() {
        mAutoSelectTabOnClick = true;
        mStep = 1;
        mPreviousValue = -1;
        View.inflate(getContext(), R.layout.boost_edit, this);

        initLabel();
        initProgress();
        initEditors();
    }

    private void initEditors() {
        mPlusButton = (ImageView) findViewById(R.id.plus);

        mPlusButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                increment(true);
            }
        });

        mPlusButton.setOnLongClickListener(
                new OnLongClickListener() {
                    public boolean onLongClick(View arg0) {
                        if (!mAutoIncrement) {
                            mAutoIncrement = true;
                            schedule(new RptUpdater());
                            return true;
                        }
                        return false;
                    }
                }
        );

        mPlusButton.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && mAutoIncrement) {
                    mAutoIncrement = false;
                    notifyChangeListeners();
                    return true;
                }
                return false;
            }
        });

        mMinusButton = (ImageView) findViewById(R.id.minus);

        mMinusButton.setOnLongClickListener(
                new OnLongClickListener() {
                    public boolean onLongClick(View arg0) {
                        if (!mAutoDecrement) {
                            mAutoDecrement = true;
                            schedule(new RptUpdater());
                        }
                        return false;//should be false to let finish cleck event
                    }
                }
        );

        mMinusButton.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if ((event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                        && mAutoDecrement) {
                    mAutoDecrement = false;
                    notifyChangeListeners();
                }
                return false;//should be false to let finish cleck event
            }
        });

        mMinusButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                decrement(true);
                resetBoost();
            }
        });
    }

    private void initLabel() {
        mBoostEdit = findViewById(R.id.boost_edit);
        mValueText = (TextView) findViewById(R.id.value);
        mEditText = (EditTextBackNotify) findViewById(R.id.value_edit);
        mEditText.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    return applyDirectInput();
                } else if (keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyEvent.getKeyCode()) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            return applyDirectInput();
                    }
                }
                return false;
            }
        });
        mEditText.setOnBackPressListener(new EditTextBackNotify.BackPressListener() {
            @Override
            public void onBackPressed() {
                finishDirectEdit();
            }
        });
        mEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if ((mEditText == view) && (!b)) {
                    applyDirectInput();
                }
            }
        });

        mValueText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                startDirectEdit();
            }
        });

        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                onDirectEditChanged();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initProgress() {
        mTargetValue = (TextView) findViewById(R.id.target_value);
        mTargetPercent = (TextView) findViewById(R.id.target_percent);
        mTargetProgress = (ProgressBar) findViewById(R.id.target_progress);
    }

    private void onDirectEditChanged() {
        if (!isDirectInputOn()) {
            return;
        }
        String text = mEditText.getText().toString();
        long parsedValue = getTextAsValue(text);
        if ((parsedValue >= 0) && (parsedValue != mValue)) {
            setValue(parsedValue);
            applyValue();
        }
    }

    private void finishDirectEdit() {
        mEditText.setVisibility(GONE);
        mBoostEdit.setVisibility(VISIBLE);
        setSoftKeyboardVisible(false);
    }

    private void startDirectEdit() {
        if (!isDirectEditEnabled()) {
            return;
        }
        String text = mValueText.getText().toString();
        mEditText.setInputType(getValueInputType());
        mEditText.setText(text);
        mBoostEdit.setVisibility(GONE);

        mEditText.setVisibility(VISIBLE);
        mEditText.setSelection(0, mEditText.getText().length());
        mEditText.requestFocus();
        setSoftKeyboardVisible(true);
    }

    protected void setSoftKeyboardVisible(boolean visible) {
        EditText editText = getEditTextField();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (visible) {
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        } else {
            imm.hideSoftInputFromWindow(editText.getApplicationWindowToken(), 0);
        }
    }

    protected EditText getEditTextField() {
        return mEditText;
    }

    protected boolean isDirectEditEnabled() {
        return true;
    }

    protected void schedule(Runnable task) {
        mRepeatUpdateHandler.post(task);
    }

    protected void schedule(Runnable task, long delay) {
        mRepeatUpdateHandler.postDelayed(task, delay);
    }

    protected void setEditable(boolean editable) {
        mAutoSelectTabOnClick = editable;
        setEditControlsVisible(editable);
        boolean progressVisible = (!editable) && (mPreviousValue != 0);
        setProgressControlsVisible(progressVisible);
    }

    private void setProgressControlsVisible(boolean visible) {
        int visibility = visible ? VISIBLE : GONE;
        if (mTargetProgress.getVisibility() != visibility) {
            mTargetProgress.setVisibility(visibility);
            mTargetValue.setVisibility(visibility);
            mTargetPercent.setVisibility(visibility);
        }
        if (visible) {
            mTargetValue.setText(getTargetValueAsText(mPreviousValue));
        }
    }

    private void setProgress(int progress) {
        if (progress == 0) {
            setProgressColor(R.color.chart_bad);
        } else if (progress == 100) {
            setProgressColor(R.color.chart_good);
        }
        mTargetPercent.setText("" + progress + "%");
        mTargetProgress.setProgress(progress);
    }

    private void setProgressColor(@ColorRes int colorId) {
        int color = ContextCompat.getColor(getContext(), colorId);
        mTargetProgress.getProgressDrawable().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    private void applyTargetValue() {
        int percent = 0;
        float effectiveValue = getEffectiveValue();
        if (mPreviousValue != 0) {
            percent = (int)((effectiveValue / mPreviousValue) * 100);
        }
        if (percent < 0) {
            percent = 0;
        }
        if (percent > 100) {
            percent = 100;
        }
        setProgress(percent);
    }

    protected String getTargetValueAsText(long value) {
        return getValueAsText(value);
    }

    private void setEditControlsVisible(boolean visible) {
        int visibility = visible ? VISIBLE : INVISIBLE;
        mPlusButton.setVisibility(visibility);
        mMinusButton.setVisibility(visibility);
    }

    public void setPreviousValue(long value) {
        mPreviousValue = value;
    }

    public void setValue(Unit unit, long value, Value.Status status) {
        applyValue(unit, value, status);
        applyTargetValue();
        if (!mNotificationInProgress) {
            autoSelectTab();
        }
    }

    private void applyValue(Unit unit, long value, Value.Status status) {
        if (mNotificationInProgress) {
            return;
        }
        applyDirectInput();
        mStatus = status;
        setUnit(unit);
        setBoostValue(value, false);
        resetBoost();
        update();
    }

    private long getStep(int index) {
        return getSteps()[index];
    }

    private void generateControls() {
        long steps[] = getSteps();
        if ((steps != null) && (steps.length > 0)) {
            initTabs(steps.length);
            mMinimalStep = Long.MAX_VALUE;
            for (int i = 0; i < steps.length; i++) {
                initTab(i);
                if (mMinimalStep > steps[i]) {
                    mMinimalStep = steps[i];
                }
            }
            autoSelectTab();
        }
    }

    private void autoSelectTab() {
        if (mTabs.length > 0) {
            if ((mTabs.length > 1) && (mValue == 0)) {
                selectTab(1);
            } else {
                selectTab(0);
            }
        }
    }

    protected void update() {
        long steps[] = getSteps();
        if ((mTabs != null) && (mTabs.length == steps.length)) {
            for (int i = 0; i < steps.length; i++) {
                initTab(i);
            }
        } else {
            generateControls();
        }
        updateValue();
    }

    protected void initTabs(int length) {
        ViewGroup container = (LinearLayout) findViewById(R.id.boost_step);
        TextView[] result = new TextView[length];
        for (int i = 0; i < length; i++) {
            TextView reusedTab = null;
            if ((mTabs != null) && (mTabs.length > i)) {
                reusedTab = mTabs[i];
            }
            if (reusedTab != null) {
                result[i] = reusedTab;
            } else {
                TextView tab = (TextView) LayoutInflater.from(getContext()).inflate(
                        R.layout.boost_edit_textview, container, false);
                result[i] = tab;
                container.addView(tab);
            }
            int tabBackground = R.drawable.boost_tab_middle;
            if (i == 0) {
                tabBackground = R.drawable.boost_tab_left;
            } else if (i == length - 1) {
                tabBackground = R.drawable.boost_tab_right;
            }
            result[i].setBackgroundResource(tabBackground);
        }
        if (mTabs != null) {
            for (int i = length; i < mTabs.length; i++) {
                container.removeView(mTabs[i]);
            }
        }
        container.setVisibility(VISIBLE);
        mTabs = result;
    }

    private void initTab(final int index) {
        TextView booster = mTabs[index];
        long step = getStep(index);
        booster.setText(getStepAsText(step));
        booster.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onTabClick(index);
            }
        });
    }

    private void onTabClick(int index) {
        if (mAutoSelectTabOnClick) {
            setTabSelected(index);
        }
        long step = getStep(index);
        onTabClicked(step);
    }

    private void selectTab(int index) {
        setTabSelected(index);
        mStep = getStep(index);
    }

    private void setTabSelected(int index) {
        for (int b = 0; b < mTabs.length; b++) {
            TextView booster = mTabs[b];
            booster.setSelected(b == index);
        }
    }

    protected void onTabClicked(long step) {
        mStep = step;
    }

    public void decrement(boolean fireChangeEvent) {
        long step = mStep;
        if (mStep == mMinimalStep) {
            long partialStep = mValue % step;
            if (partialStep != 0) {
                step = partialStep;
            }
        }
        setBoostValue(mValue - step, fireChangeEvent);
    }

    public void increment(boolean fireChangeEvent) {
        long step = mStep;
        if (mStep == mMinimalStep) {
            long partialStep = mValue % step;
            if (partialStep != 0) {
                step = step - partialStep;
            }
        }
        setBoostValue(mValue + step, fireChangeEvent);
    }

    private boolean applyDirectInput() {
        if (isDirectInputOn()) {
            finishDirectEdit();
            return true;
        }
        return false;
    }

    private boolean isDirectInputOn() {
        return mEditText.getVisibility() == VISIBLE;
    }

    public long accessValue() {
        applyDirectInput();
        return mValue;
    }

    protected long getEffectiveValue() {
        return mValue;
    }

    private void updateBoost() {
        mUpdatesCounter++;
        long time = mUpdatesCounter * mDelay;
        if (time >= BOOST_PERIOD) {
            mDelay -= BOOST_STEP;
            if (mDelay < REP_MIN_DELAY) {
                mDelay = REP_MIN_DELAY;
            }
        }
    }

    private void resetBoost() {
        mDelay = REP_START_DELAY;
        mUpdatesCounter = 0;
    }

    public void setOnValueChangedListener(OnValueChangedListener listener) {
        mListener = listener;
    }

    protected void setStatus(Value.Status status) {
        if (mStatus != status) {
            mStatus = status;
            generateControls();
        }
    }

    protected boolean isStatus(Value.Status status) {
        return mStatus == status;
    }

    protected Value.Status getStatus() {
        return mStatus;
    }

    //-------------------------------------------------------------------------------------

    private class RptUpdater implements Runnable {
        public void run() {
            if(mAutoIncrement){
                increment(false);
                schedule(this, mDelay);
            } else if(mAutoDecrement){
                decrement(false);
                schedule(this, mDelay);
            } else {
                resetBoost();
            }
        }
    }

    public interface OnValueChangedListener {
        void onValueChanged(BoostEditor source, long value, Value.Status status);
    }

    private Handler mRepeatUpdateHandler = new Handler();
    private boolean mAutoIncrement = false;
    private boolean mAutoDecrement = false;
    private long mValue;
    private long mPreviousValue;
    private EditTextBackNotify mEditText;
    private long mStep;
    private long mDelay;
    private int mUpdatesCounter;
    private OnValueChangedListener mListener;
    private long mMinimalStep;
    private Value.Status mStatus;
    private boolean mAutoSelectTabOnClick;
    private boolean mNotificationInProgress;

    private View mBoostEdit;
    private ImageView mPlusButton;
    private ImageView mMinusButton;
    private TextView[] mTabs;
    private TextView mValueText;
    private TextView mTargetPercent;
    private TextView mTargetValue;
    private ProgressBar mTargetProgress;

    private static final long REP_START_DELAY = 200;
    private static final long REP_MIN_DELAY = 50;
    private static final long BOOST_PERIOD = 2000;
    private static final long BOOST_STEP = 20;
}
