package com.vaisoft.health.gymgain.workout;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.utils.Utils;

import java.util.List;
import java.util.Map;

public class SetResultsAdapter extends BaseAdapter {

    public SetResultsAdapter(Workout.Entry entry, Workout.Entry previousEntry, Coach coach, Context context) {
        mInflater = LayoutInflater.from(context);
        mCoach = coach;
        init(entry, previousEntry);
    }

    public void init(Workout.Entry entry, Workout.Entry previousEntry) {
        mEntry = entry;
        mPreviousEntry = previousEntry;
    }

    @Override
    public int getCount() {
        return getItemsCount();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    private int getItemsCount() {
        return mEntry.getSetsCount();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.activity_workout_set_item, parent, false);
        } else {
            view = convertView;
        }
        view.setTag(position);

        TextView text = getTextView(view);
        View marker = view.findViewById(R.id.marker);

        applyData(marker, text, position);
        return view;
    }

    private TextView getTextView(View view) {
        return (TextView) view.findViewById(android.R.id.text1);
    }

    private void appendResults(StringBuilder result, Workout.Set set, List<Unit> exerciseParameters) {
        boolean slash = false;
        for (Unit parameter: exerciseParameters) {
            if (slash) {
                result.append('/');
            } else {
                slash = true;
            }
            Unit unit = mCoach.getConfig().getUnits(parameter);
            Value value = set.getValueForCategory(parameter);
            if (value != null) {
                result.append(unit.toString(value.get(unit)));
            } else {
                result.append('-');
            }
        }
    }

    private String describe(Workout.Entry currentEntry, Workout.Entry previousEntry, int position) {
        StringBuilder result = new StringBuilder();
        boolean newLine = false;
        for (Workout.ExerciseLogItem item: currentEntry.log) {
            if (newLine) {
                result.append("<br>");
            } else {
                newLine = true;
            }
            if (currentEntry.log.size() == 1) {
                result.append(position + 1);
            } else {
                result.append(mCoach.getLocalized(item.exercise));
            }
            Workout.Set set = item.sets.get(position);
            if (set.plannedReps > 0) {
                result.append('(');
                result.append(set.plannedReps);
                result.append(')');
            }
            result.append(": ");

            StringBuilder resultValues = new StringBuilder();
            Exercise exercise = mCoach.getExercise(item.exercise);
            List<Unit> exerciseParameters = exercise.getParameterUnits();
            boolean setComplete = set.isComplete(exerciseParameters);
            appendResults(resultValues, set, exerciseParameters);
            Workout.Set previousSet = null;
            if (setComplete && (previousEntry != null)) {
                previousSet = previousEntry.getExerciseSet(item.exercise, position);
            }
            Progress progress = null;
            boolean colorize = false;
            if (setComplete) {
                progress = set.compareTo(previousSet, mCoach.getConfig());
                colorize = progress.getEstimate() != Progress.Estimate.SOSO;
            }
            if (colorize) {
                result.append("<font color = '");
                result.append(Utils.getEstimateColorHtml(progress.getEstimate(), mInflater.getContext()));
                result.append("'>");
            }
            result.append(resultValues);
            if (colorize) {
                result.append("</font>");
            }
        }
        return result.toString();
    }

    public boolean canBeActive() {
        for (Workout.ExerciseLogItem item: mEntry.log) {
            Exercise exercise = mCoach.getExercise(item.exercise);
            List<Unit> exerciseParameters = exercise.getParameterUnits();
            for (Unit parameter: exerciseParameters) {
                if (Value.canBeActive(parameter)) {
                    return true;
                }
            }
        }
        return false;
    }

    protected boolean isData(int position) {
        return true;
    }

    protected void applyData(View marker, TextView text, int position) {
        Progress progress = mEntry.compareTo(mPreviousEntry, position, mCoach.getConfig());
        int color = Utils.getEstimateColor(progress.getEstimate(), mInflater.getContext());
        updateText(text, position);
        marker.setVisibility(View.VISIBLE);
        marker.setBackgroundColor(color);
    }

    private void updateText(TextView text, int position) {
        String html = describe(mEntry, mPreviousEntry, position);
        text.setText(Utils.html(html));
    }

    protected static int getPosition(View view) {
        return (Integer) view.getTag();
    }

    public boolean tick(View view) {
        int position = getPosition(view);
        if (!isData(position)) {
            return false;
        }
        boolean isActive = false;
        for (Workout.ExerciseLogItem item: mEntry.log) {
            Workout.Set set = item.sets.get(position);
            for (Map.Entry<Unit, Value> entry : set.result.entrySet()) {
                if (entry.getValue().isActive()) {
                    isActive = true;
                    break;
                }
            }
            if (isActive) {
                break;
            }
        }
        if (!isActive) {
            return false;
        }
        TextView text = getTextView(view);
        updateText(text, position);
        return true;
    }

    private LayoutInflater mInflater;
    private Workout.Entry mEntry;
    private Workout.Entry mPreviousEntry;
    private Coach mCoach;
}
