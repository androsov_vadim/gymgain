package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.WeekDaysMap;
import com.vaisoft.health.gymgain.schedule.Schedule;

import java.util.ArrayList;
import java.util.List;

public class RecordScheduleView extends LinearLayout {

    public RecordScheduleView(Context context) {
        super(context);
        init(context);
    }

    public RecordScheduleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RecordScheduleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


   protected void init(Context context) {
        inflate(context, R.layout.record_schedule, this);
        initOptions();
        initInterface();
        initWeekDays();
    }

    private void initWeekDays() {
        mWeekDays = new CheckBox[8];
        mWeekDays[0] = (CheckBox) findViewById(R.id.schedule_wd_1);
        mWeekDays[1] = (CheckBox) findViewById(R.id.schedule_wd_2);
        mWeekDays[2] = (CheckBox) findViewById(R.id.schedule_wd_3);
        mWeekDays[3] = (CheckBox) findViewById(R.id.schedule_wd_4);
        mWeekDays[4] = (CheckBox) findViewById(R.id.schedule_wd_5);
        mWeekDays[5] = (CheckBox) findViewById(R.id.schedule_wd_6);
        mWeekDays[6] = (CheckBox) findViewById(R.id.schedule_wd_7);
        mWeekDays[7] = (CheckBox) findViewById(R.id.schedule_wd_8);

        mWdCheckChangeListener = new OnWDCheckChange();
        mAllCheckChangeListener = new OnAllCheckChange();


        mWeekDays[0].setText(R.string.record_schedule_weekdays_all);
        mWeekDays[0].setOnCheckedChangeListener(mAllCheckChangeListener);

        String language = getContext().getString(R.string.locale);
        mWeekDaysMap = new WeekDaysMap(language);
        String[] dayNames = mWeekDaysMap.getShortDaysNames();
        for (int i = 1; i <= 7; i++) {
            mWeekDays[i].setText(dayNames[mWeekDaysMap.getWeekdayByIndex(i)]);
            mWeekDays[i].setOnCheckedChangeListener(mWdCheckChangeListener);
        }
    }

    private void initInterface() {
        mTimesPerDescription = (TextView) findViewById(R.id.count_times_per);
        updateTimesPer(1);

        mTimesCount = (Spinner) findViewById(R.id.count_times);
        mTimesCount.setAdapter(new NumbersListAdapter(getContext(), 100, false));
        mTimesCount.setOnItemSelectedListener(new SelectionListener() {
            @Override
            protected void onSelected(int position) {
                setTimesPer(position + 1);
            }
        });

        mPeriodsCount = (Spinner) findViewById(R.id.count_periods);
        mPeriodsCount.setAdapter(new NumbersListAdapter(getContext(), 100, false));
        mPeriodsCount.setOnItemSelectedListener(new SelectionListener() {
            @Override
            protected void onSelected(int position) {
                updatePeriodsCount();
            }
        });

        mPeriodTypes = (Spinner) findViewById(R.id.count_period_types);
        mPeriodTypes.setAdapter(new PeriodsAdapter());
    }

    private void updatePeriodsCount() {
        ((PeriodsAdapter) mPeriodTypes.getAdapter()).notifyDataSetChanged();
        applyScheduleType(Schedule.Type.FLEXIBLE);
        mIsChanged = true;
    }

    private void setTimesPer(int times) {
        updateTimesPer(times);
        applyScheduleType(Schedule.Type.FLEXIBLE);
        mIsChanged = true;
    }

    private void updateTimesPer(int count) {
        mTimesPerDescription.setText(getResources().getQuantityString(R.plurals.times_per, count));
    }

    private void initOptions() {
        mOptions = new ArrayList<>();

        RadioButton schedule0 = (RadioButton) findViewById(R.id.schedule0);
        View schedule0Tab = findViewById(R.id.schedule0_tab);
        schedule0.setTag(schedule0Tab);
        mOptions.add(schedule0);

        RadioButton schedule1 = (RadioButton) findViewById(R.id.schedule1);
        View schedule1Tab = findViewById(R.id.schedule1_tab);
        schedule1.setTag(schedule1Tab);
        mOptions.add(schedule1);

        RadioButton schedule2 = (RadioButton) findViewById(R.id.schedule2);
        View schedule2Tab = findViewById(R.id.schedule2_tab);
        schedule2.setTag(schedule2Tab);
        mOptions.add(schedule2);

        RadioButton schedule3 = (RadioButton) findViewById(R.id.schedule3);
        View schedule3Tab = findViewById(R.id.schedule3_tab);
        schedule3.setTag(schedule3Tab);
        mOptions.add(schedule3);

        setOptionChecked(schedule0, true);

        CompoundButton.OnCheckedChangeListener optionChangeHandler = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                RadioButton b = (RadioButton) buttonView;
                for (RadioButton rb : mOptions) {
                    rb.setOnCheckedChangeListener(null);
                    setOptionChecked(rb, rb == b);
                    rb.setOnCheckedChangeListener(this);
                }
            }
        };

        for (RadioButton rb : mOptions) {
            rb.setOnCheckedChangeListener(optionChangeHandler);
        }
    }

    private void setOptionChecked(RadioButton option, boolean checked) {
        option.setChecked(checked);
        View tab = (View) option.getTag();
        tab.setSelected(checked);
        mIsChanged = true;
    }

    public void setSchedule(Schedule schedule) {
        if (schedule != null) {
            mSchedule = schedule;
        } else {
            mSchedule = new Schedule();
            mIsChanged = true;
        }

        applyScheduleType (mSchedule.getType());

        for (int i = 1; i <= 7; i++) {
            mWeekDays[i].setOnCheckedChangeListener(null);
            mWeekDays[i].setChecked(mSchedule.isWeekday(mWeekDaysMap.getWeekdayByIndex(i)));
            mWeekDays[i].setOnCheckedChangeListener(mWdCheckChangeListener);
        }

        applyFlexibleSchedule();
    }

    private void applyScheduleType(Schedule.Type type) {
        switch (type) {
            case NOT_SCHEDULED:
                mOptions.get(0).setChecked(true);
                break;
            case MANUAL:
                mOptions.get(1).setChecked(true);
                break;
            case FLEXIBLE:
                mOptions.get(2).setChecked(true);
                break;
            case WEEKLY:
                mOptions.get(3).setChecked(true);
                break;
        }
    }

    private void applyFlexibleSchedule() {
        Schedule.Flexible plan = mSchedule.getFlexible();
        mTimesCount.setSelection(plan.times - 1);
        mPeriodsCount.setSelection(plan.periodsCount - 1);
        switch (plan.period) {
            case DAY:
                mPeriodTypes.setSelection(0);
                break;
            case WEEK:
                mPeriodTypes.setSelection(1);
                break;
            case MONTH:
                mPeriodTypes.setSelection(2);
                break;
            case YEAR:
                mPeriodTypes.setSelection(3);
                break;
        }
    }

    public Schedule save() {
        if (!mIsChanged) {
            return null;
        }

        if (mOptions.get(0).isChecked()) {
            mSchedule.setType(Schedule.Type.NOT_SCHEDULED);
            return mSchedule;
        } else if (mOptions.get(1).isChecked()) {
            mSchedule.setType(Schedule.Type.MANUAL);
        } else if (mOptions.get(2).isChecked()) {
            mSchedule.setType(Schedule.Type.FLEXIBLE);
        } else if (mOptions.get(3).isChecked()) {
            mSchedule.setType(Schedule.Type.WEEKLY);
        }
        for (int i = 1; i <= 7; i++) {
            mSchedule.setWeekday(mWeekDaysMap.getWeekdayByIndex(i), mWeekDays[i].isChecked());
        }

        Schedule.Flexible plan = mSchedule.getFlexible();
        plan.times = mTimesCount.getSelectedItemPosition() + 1;
        plan.periodsCount = mPeriodsCount.getSelectedItemPosition() + 1;
        switch (mPeriodTypes.getSelectedItemPosition()) {
            case 0:
                plan.period = Schedule.PeriodType.DAY;
                break;
            case 1:
                plan.period = Schedule.PeriodType.WEEK;
                break;
            case 2:
                plan.period = Schedule.PeriodType.MONTH;
                break;
            case 3:
                plan.period = Schedule.PeriodType.YEAR;
                break;
        }

        mSchedule.onUpdated();
        mIsChanged = false;
        return mSchedule;
    }

//---------------------------------------------------------------------------------

    private abstract class SelectionListener implements AdapterView.OnItemSelectedListener
    {
        protected abstract void onSelected(int position);

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (parent.getTag() != null) {
                onSelected(position);
            } else {
                parent.setTag(true);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }

    private class OnWDCheckChange implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            applyScheduleType(Schedule.Type.WEEKLY);
            boolean allCheck = true;
            boolean allUnCheck = true;
            for (int i = 1; i <= 7; i++) {
                if (mWeekDays[i].isChecked()) {
                    allUnCheck = false;
                } else {
                    allCheck = false;
                }
            }
            if (allCheck) {
                setChecked(mWeekDays[0], true);
                mWeekDays[0].animate().alpha(1);
            } else if (allUnCheck) {
                setChecked(mWeekDays[0], false);
                mWeekDays[0].animate().alpha(1);
            } else if (mWeekDays[0].isChecked()) {
                setChecked(mWeekDays[0], true);
                mWeekDays[0].animate().alpha(UNDEFINED_STATE_ALPHA);
            }
            mIsChanged = true;
        }

        private void setChecked(CheckBox allBox, boolean checked) {
            allBox.setOnCheckedChangeListener(null);
            allBox.setChecked(checked);
            allBox.setOnCheckedChangeListener(mAllCheckChangeListener);
        }

        private static final float UNDEFINED_STATE_ALPHA = 0.5f;
    }

    private class OnAllCheckChange implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            applyScheduleType(Schedule.Type.WEEKLY);
            for (int i = 1; i <= 7; i++) {
                setChecked(mWeekDays[i], isChecked);
            }
            mWeekDays[0].animate().alpha(1);
            mIsChanged = true;
        }

        private void setChecked(CheckBox wdBox, boolean checked) {
            wdBox.setOnCheckedChangeListener(null);
            wdBox.setChecked(checked);
            wdBox.setOnCheckedChangeListener(mWdCheckChangeListener);
        }
    }

    private int getPeriodsCount() {
        return  mPeriodsCount.getSelectedItemPosition() + 1;
    }


    private class PeriodsAdapter extends SpinnerSimpleAdapter {

        public PeriodsAdapter() {
            super(getContext());
            mItems = new ArrayList<>();
            mItems.add(R.plurals.days);
            mItems.add(R.plurals.weeks);
            mItems.add(R.plurals.months);
            mItems.add(R.plurals.years);
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public String getItem(int position) {
            return getResources().getQuantityString(mItems.get(position), getPeriodsCount());
        }

        private List<Integer> mItems;
    }

    WeekDaysMap mWeekDaysMap;
    private Schedule mSchedule;
    private boolean mIsChanged;
    private List<RadioButton> mOptions;
    private TextView mTimesPerDescription;
    private CheckBox[] mWeekDays;
    private OnWDCheckChange mWdCheckChangeListener;
    private OnAllCheckChange mAllCheckChangeListener;
    private Spinner mTimesCount;
    private Spinner mPeriodsCount;
    private Spinner mPeriodTypes;
}
