package com.vaisoft.health.gymgain.controls;

import androidx.recyclerview.widget.RecyclerView;

public abstract class ContextAdapter<T extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<T> {

    public  void setContextItem(int item) {
        mContextMenuItem = item;
    }

    public int getContextItem() {
        return mContextMenuItem;
    }

    private int mContextMenuItem;
}