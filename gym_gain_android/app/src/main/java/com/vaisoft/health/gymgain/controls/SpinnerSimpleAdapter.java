package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;

public abstract class SpinnerSimpleAdapter extends BaseAdapter {

    public SpinnerSimpleAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public abstract String getItem(int position);

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = mInflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        } else {
            view = convertView;
        }
        setData(view, position);
        return view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = mInflater.inflate(R.layout.spinner_item, parent, false);
        } else {
            view = convertView;
        }
        setData(view, position);
        return view;
    }

    private void setData(View view, int position) {
        TextView text = (TextView) view.findViewById(android.R.id.text1);
        text.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
        text.setText(getItem(position));
    }

    private final LayoutInflater mInflater;
}
