package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.NumbersListAdapter;

import java.util.Arrays;

public class SetConfigDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle args = getArguments();
        mData = args.getIntArray(DATA);
        mComment = args.getString(COMMENT);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.precise_plan);
        builder.setIcon(R.drawable.ic_fitness_center_black_24dp);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                listener.onSetDataChanged(mData, mComment);
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.setView(createInterface());

        return builder.create();
    }

    private View createInterface() {
        View view = View.inflate(getActivity(), R.layout.set_config, null);

        initCommentEditor(view);
        initSetsList(view);
        initSetsCounter(view);

        Bundle args = getArguments();
        boolean supportsReps = args.getBoolean(REPS);
        if (!supportsReps) {
            View repsEditor = view.findViewById(R.id.reps_editor);
            repsEditor.setVisibility(View.GONE);
        }

        return view;
    }

    private void initSetsList(View view) {
        ListView plan = (ListView) view.findViewById(R.id.repeats_plan);
        mPlanAdapter = new SetsPrecisePlanAdapter();
        plan.setAdapter(mPlanAdapter);
    }

    private void initSetsCounter(View view) {
        Spinner setsCounter = (Spinner) view.findViewById(R.id.sets_count);
        setsCounter.setAdapter(new NumbersListAdapter(getContext(), 20, true));
        if (mData != null) {
            setsCounter.setSelection(mData.length);
        }
        setsCounter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    mData = null;
                } else {
                    if (mData == null) {
                        mData = new int[position];
                    } else {
                        mData = Arrays.copyOf(mData, position);
                    }
                }
                mPlanAdapter.notifyDataSetChanged();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCommentEditor(View view) {
        EditText comment = (EditText) view.findViewById(R.id.text_comment);
        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mComment = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        if (mComment != null) {
            comment.setText(mComment);
        }
    }

    //-----------------------------------------------------------------------------------------

    private class SetsPrecisePlanAdapter extends BaseAdapter {

        public SetsPrecisePlanAdapter() {
            mInflater = LayoutInflater.from(getContext());
        }

        @Override
        public int getCount() {
            if (mData == null) {
                return 0;
            } else {
                return mData.length;
            }
        }

        @Override
        public Object getItem(int position) {
            return "" + (position + 1);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final View view;
            if (convertView == null) {
                view = mInflater.inflate(R.layout.set_config_item, parent, false);
            } else {
                view = convertView;
            }

            TextView textNumber = (TextView) view.findViewById(R.id.text_number);
            textNumber.setText((String) getItem(position));

            final TextView repeatsText = (TextView) view.findViewById(R.id.text_repeats_count);

            Spinner count = (Spinner) view.findViewById(R.id.repeats_count);
            count.setAdapter(new NumbersListAdapter(getContext(), 100, true));
            if (mData != null) {
                count.setSelection(mData[position]);
            }
            count.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int itemPosition, long id) {
                    repeatsText.setText(getResources().getQuantityString(R.plurals.repeats_count, itemPosition));
                    mData[position] = itemPosition;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            return view;
        }

        private final LayoutInflater mInflater;
    }

    public interface Listener {
        void onSetDataChanged(int[] data, String comment);
    }

    private SetsPrecisePlanAdapter mPlanAdapter;
    private int[] mData;
    private String mComment;

    public static final String DATA = "data";
    public static final String REPS = "reps";
    public static final String COMMENT = "comment";
}
