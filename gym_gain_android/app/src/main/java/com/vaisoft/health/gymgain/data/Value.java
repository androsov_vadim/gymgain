package com.vaisoft.health.gymgain.data;

import android.os.Bundle;
import android.util.Log;

import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

public class Value implements Cloneable {

    public Value(JSONObject source) throws JSONException {
        mUnit = Unit.valueOf(source.getString(UNIT));
        mStatus = Status.valueOf(source.getString(STATUS));
        mValue = source.getLong(VALUE);
        mTargetInc = getTargetValue(source.getString(TARGET));
    }

    public Value(Bundle source) {
        mUnit = Unit.valueOf(source.getString(UNIT));
        mStatus = Status.valueOf(source.getString(STATUS));
        mValue = source.getLong(VALUE);
        mTargetInc = getTargetValue(source.getString(TARGET));
    }

    public Value(Unit unit, long value, boolean targetInc, Status status) {
        mUnit = unit;
        mStatus = status;
        set(value);
        mTargetInc = targetInc;
    }

    public Value(Unit unit, boolean targetInc) {
        this(unit, 0, targetInc, Status.Undefined);
    }

    public void save(JSONObject target) throws JSONException {
        target.put(UNIT, mUnit.toString());
        target.put(VALUE, mValue);
        target.put(TARGET, getTargetString(mTargetInc));
        target.put(STATUS, mStatus.toString());
    }

    public void save(Bundle target) {
        target.putString(UNIT, mUnit.toString());
        target.putLong(VALUE, mValue);
        target.putString(TARGET, getTargetString(mTargetInc));
        target.putString(STATUS, mStatus.toString());
    }

    public void set(Unit unit, long value) {
        mUnit = unit;
        set(value);
    }

    private void set(long value) {
        if (isTimeTicking()) {
            setRelativeTime(value);
        } else {
            mValue = value;
        }
    }

    private long get() {
        if (isTimeTicking()) {
            return getAbsoluteTime();
        }
        return mValue;
    }

    private boolean isTimeTicking() {
        return isActive() && (mUnit == Unit.time);
    }

    public boolean isZero() {
        return get() == 0;
    }

    public void setZero() {
        set(0);
    }

    public long get(Unit unit) {
        if (mUnit == unit) {
            return get();
        }
        return convert(unit, mValue);
    }

    private void setRelativeTime(long absoluteTime) {
        mValue = Utils.now().getMillis() - absoluteTime;
    }

    private long getAbsoluteTime() {
        return Utils.now().getMillis() - mValue;
    }

    public Unit unit() {
        return mUnit;
    }

    @Override
    public Value clone() {
        return new Value(mUnit, get(), mTargetInc, mStatus);
    }

    private long convert(Unit unit, long value) {
        if (unit.getCategory() != mUnit.getCategory()) {
            Log.e(getClass().getName(), "Can't convert " + unit.toString() + " to " + mUnit.toString());
            return -1;
        }
        switch (unit) {
            case centimeter://inch
                return mult(value, 2.54);
            case inch://centimeter
                return mult(value, 0.393701);
            case meter://foot
                return mult(value, 0.304800164592);
            case foot://meter
                return mult(value, 3.280841666667);
            case kilogram://pound
                return mult(value, 0.453592);
            case pound://kilogram
                return mult(value, 2.20462);
            default:
                Log.e(getClass().getName(), "Can't convert " + unit.toString() + " to " + mUnit.toString());
        }
        return -1;
    }

    private long mult(long value, double k) {
        return (long) (value * k);
    }

    public static boolean getTargetValue(String target) {
        return target.equals(TARGET_INC);
    }

    public static String getTargetString(boolean target) {
        return target ? TARGET_INC : TARGET_DEC;
    }

    public static int getTargetIndex(boolean target) {
        return target ? 0 : 1;
    }

    public static boolean getTargetForIndex(int index) {
        return index == 0;
    }

    public boolean shouldGrow() {
        return mTargetInc;
    }

    public void setShouldGrow(boolean shouldGrow) {
        mTargetInc = shouldGrow;
    }

    @Override
    public String toString() {
        return mUnit.toString(mValue);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Value)) {
            return false;
        }
        Value value = (Value) o;
        return (value.mUnit == mUnit) && (value.mValue == mValue) && (value.mTargetInc == mTargetInc);
    }

    public Status getStatus() {
        return mStatus;
    }

    public boolean isActive() {
        return mStatus == Status.Progress;
    }

    public boolean isApplied() {
        return mStatus == Status.Applied;
    }

    public void apply() {
        setStatus(Status.Applied);
    }

    public void setStatus(Status status) {
        if (isTimeTicking()) {
            if (status != Status.Progress) {
                mValue = getAbsoluteTime();
            }
        } else {
            if (status == Status.Progress) {
                setRelativeTime(mValue);
            }
        }
        mStatus = status;
    }

    public static boolean canBeActive(Unit unit) {
        return unit == Unit.time;
    }

    //---------------------------------------------------------------------------------------

    public enum Status {
        Progress,
        Pause,
        Applied,
        Undefined
    }

    private Unit mUnit;
    private long mValue;
    private boolean mTargetInc;
    private Status mStatus;

    private static final String UNIT="unit";
    private static final String VALUE="value";
    private static final String TARGET="target";
    private static final String STATUS="status";

    private static final String TARGET_INC = "+";
    private static final String TARGET_DEC = "-";
}
