package com.vaisoft.health.gymgain.feed;

import android.content.Context;
import android.view.View;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.billing.Billing;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.controls.FeedItem;
import com.vaisoft.health.gymgain.data.ProgressCounter;
import com.vaisoft.health.gymgain.main.FeedFragment;
import com.vaisoft.health.gymgain.main.MainActivity;
import com.vaisoft.health.gymgain.utils.Utils;

import java.util.List;

public class HelperFeed extends Feed {

    private HelperFeed(Context context, Type type) {
        super(context);
        mType = type;
    }

    @Override
    public void bind(ContextViewHolder viewHolder, Coach coach) {
        ViewHolder holder = (ViewHolder) viewHolder;
        FeedItem item = holder.getItem();
        item.reset();
        Context context = getContext();
        switch (mType) {
            case WATCH_TUTORIAL:
                initWatchTutorial(item, context);
                break;
            case QUICK_START:
                initQuickStart(item, context);
                break;
            case SHOPPING:
                initShopping(item, context);
                break;
        }
    }

    private void initQuickStart(FeedItem item, Context context) {
        item.setIcon(R.drawable.ic_add_white_24dp, android.R.color.white);
        item.setItemBackgroundColor(R.color.accent);
        item.setCaption(context.getString(R.string.helper_quick_start_caption));
        item.setNeutralText(context.getString(R.string.helper_quick_start_description), null);
    }

    private void initWatchTutorial(FeedItem item, Context context) {
        item.setIcon(R.drawable.ic_ondemand_video_white_36dp, R.color.positive_color);
        item.setCaption(context.getString(R.string.helper_tutorial_caption));
        item.setNeutralText(context.getString(R.string.helper_tutorial_description), null);
    }

    private void initShopping(FeedItem item, Context context) {
        item.setIcon(R.drawable.ic_shopping_cart_white_36dp, android.R.color.holo_blue_dark);
        item.setCaption(context.getString(R.string.helper_shop_caption));
        item.setNeutralText(context.getString(R.string.helper_shop_description), null);
    }

    @Override
    public void clicked() {
        Context context = getContext();
        switch (mType) {
            case WATCH_TUTORIAL:
                watchTutorial(context);
                break;
            case QUICK_START:
                quickStart(context);
                break;
            case SHOPPING:
                shopping(context);
                break;
        }
    }

    private void shopping(Context context) {
        MainActivity activity = (MainActivity) Utils.getActivity(context);
        Billing.startShop(activity);
    }

    private void quickStart(Context context) {
        MainActivity activity = (MainActivity) Utils.getActivity(context);
        activity.showAddEntityDialog();
    }

    private void watchTutorial(Context context) {
        Utils.openTutorial(context);
    }

    @Override
    public void deleted(Coach coach) {

    }

    @Override
    public int getViewType() {
        return VIEW_TYPE;
    }

    @Override
    protected long getEffectiveTime() {
        return 0;
    }

    public static void loadTo(Context context, List<Feed> result) {
        result.add(new HelperFeed(context, Type.QUICK_START));
        result.add(new HelperFeed(context, Type.WATCH_TUTORIAL));
        result.add(new HelperFeed(context, Type.SHOPPING));
    }

    //-----------------------------------------------------------------------------

    public static class ViewHolder extends Feed.ViewHolder {

        public ViewHolder(View view, FeedFragment.FeedAdapter adapter) {
            super(view, adapter);
        }

        @Override
        protected boolean isDeletable() {
            return false;
        }

        @Override
        protected boolean isEditable() {
            return false;
        }

    }

    private enum Type {
        WATCH_TUTORIAL,
        QUICK_START,
        SHOPPING
    }

    private Type mType;

    public static final int VIEW_TYPE = R.layout.feed_item_helper;
}
