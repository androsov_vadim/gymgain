package com.vaisoft.health.gymgain.edit;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.GymGainActivity;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.coach.CoachConnect;
import com.vaisoft.health.gymgain.coach.CoachTalk;
import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.dialogs.ConfirmDialog;

public abstract class CoachActivity extends GymGainActivity
    implements CoachConnect {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCoachTalk = new CoachTalk(this);
        mCoachTalk.bind(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCoachTalk.unbind();
    }

    protected void initCommonInterface(String id) {
        initToolbar(id);
    }

    protected void initCommonInterface(int id) {
        initToolbar(id);
    }

    private void initToolbar(int id) {
        initToolbar();
        getSupportActionBar().setTitle(id);
    }

    private void initToolbar(String id) {
        initToolbar();
        getSupportActionBar().setTitle(getCoach().getName(id));
    }

    private void initToolbar() {
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    protected boolean isCoach() {
        return (mCoachTalk != null) && (mCoachTalk.getCoach() != null);
    }

    public Coach getCoach() {
        return mCoachTalk.getCoach();
    }

    protected void returnFromActivity() {
         if (isCoach()) {
            getCoach().resetEditTarget();
         }
        Config.onCoachJobDone();
    }

    protected void setEditTarget(Object target) {
        getCoach().setEditTarget(target);
    }

    protected void updateCloudApplyIncoming() {
        if (isCoach()) {
            mCoachTalk.updateCloudApplyIncoming(this);
        }
    }

    protected void confirm(int question, int caption, int code) {
        if (getSupportFragmentManager().findFragmentByTag(ConfirmDialog.TAG) != null) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putInt(ConfirmDialog.CAPTION, caption);
        bundle.putInt(ConfirmDialog.MESSAGE, question);
        bundle.putInt(ConfirmDialog.CODE, code);
        ConfirmDialog dialog = new ConfirmDialog();
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), ConfirmDialog.TAG);
    }

    private CoachTalk mCoachTalk;
}
