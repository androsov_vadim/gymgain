package com.vaisoft.health.gymgain.main;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.BodyMap;
import com.vaisoft.health.gymgain.controls.CoachListFragment;
import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.edit.ExerciseDetailActivity;

import java.util.List;

public class ExcerciseListFragment extends MainActivityFragment {

    @Override
    public void onContextMenu(int id) {
        switch (id) {
            case ContextViewHolder.MENU_EDIT:
                mAdapter.edit(mAdapter.getContextItem());
                break;
            case ContextViewHolder.MENU_DELETE:
                mAdapter.deleteContextExercise();
                break;
        }
    }

    @Override
    protected Adapter createAdapter() {
        mAdapter = new ExercisesRecyclerViewAdapter();
        return mAdapter;
    }

    public class ExercisesRecyclerViewAdapter
            extends CoachListFragment.Adapter<ExercisesRecyclerViewAdapter.ViewHolder>
    {
        @Override
        public void updateData(boolean force) {
            if (force || getCoach().shouldUpdateExerciseList()) {
                mData = getCoach().getFilteredExercises(getFilter());
                notifyDataSetChanged();
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.excercise_item, parent, false);
            return new ViewHolder(view, this);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) { //http://stackoverflow.com/questions/34942840/lint-error-do-not-treat-position-as-fixed-only-use-immediately
            Exercise exercise = mData.get(position);

            Coach coach = getCoach();
            holder.textViewName.setText(coach.getName(exercise));
            holder.textViewAffected.setText(coach.getAffectedString(exercise));

            List<String> parts = coach.getAffectedParts(exercise);
            holder.bodyMap.apply(parts);
            holder.bodyMap.setBackgroundColor(R.color.exercise_background);
        }

        @Override
        public int getItemCount() {
            if (mData != null) {
                return mData.size();
            } else {
                return 0;
            }
        }

        public void edit(int item) {
            Context context = getContext();
            Intent intent = new Intent(context, ExerciseDetailActivity.class);
            intent.putExtra(ExerciseDetailActivity.ITEM, getExerciseId(item));
            context.startActivity(intent);
        }

        public void deleteContextExercise() {
            String excerciseId = getExerciseId(getContextItem());
            getCoach().deleteExercise(getContext(), excerciseId);
            updateData(true);
        }

        private String getExerciseId(int item) {
            return mData.get(item).getId();
        }

        private boolean isExerciseInUse(int item) {
            String exerciseId = getExerciseId(item);
            return getCoach().isExerciseInUse(exerciseId);
        }

        public class ViewHolder extends ContextViewHolder
        {
            public final TextView textViewName;
            public final TextView textViewAffected;
            public final BodyMap bodyMap;

            @Override
            protected void clicked() {
                int item = getAdapterPosition();
                if (item != -1) {
                    edit(item);
                }
            }

            @Override
            protected boolean isDeletable() {
                int item = getAdapterPosition();
                if (item == -1) {
                    return false;
                }
                return !isExerciseInUse(item);
            }

            public ViewHolder(View view, Adapter adapter) {
                super(view, adapter);
                textViewName = view.findViewById(R.id.name);
                textViewAffected = view.findViewById(R.id.affected);
                bodyMap = view.findViewById(R.id.body_map);
            }
        }

        private List<Exercise> mData;
    }

    private ExercisesRecyclerViewAdapter mAdapter;
}
