package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.coach.CoachConnect;
import com.vaisoft.health.gymgain.coach.CoachTalk;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.workout.SetResultsAdapter;
import com.vaisoft.health.gymgain.workout.WorkoutActivity;
import com.vaisoft.health.gymgain.workout.WorkoutFragment;

public class ViewPreviousWorkoutDialog extends DialogFragment {

    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setPositiveButton(R.string.ok, null);
        builder.setTitle(R.string.previous_workout_results);

        Bundle args = getArguments();
        int setId = args.getInt(WorkoutFragment.SET);
        mAdapter = new Adapter(setId);
        builder.setAdapter(mAdapter, null);

        mConnector = new CoachTalk(getContext());
        mConnector.bind(new CoachConnect() {
            @Override
            public void onCoachConnected() {
                onCoachReady();
            }
        });

        return builder.create();
    }

    private void onCoachReady() {
        Coach coach = mConnector.getCoach();
        Workout workout = WorkoutActivity.readWorkoutFrom(getArguments(), coach);
        if (workout != null) {
            if (workout.isActive()) {
                workout = coach.getLastFinishedWorkout(workout.getSource().getKey());
            } else {
                workout = coach.getPreviousWorkout(workout);
            }
            mAdapter.init(coach, workout);
            mAdapter.notifyDataSetChanged();
        } else {
            Log.e(getClass().getName(), "Workout not found: " + getArguments());
        }
    }

    @Override
    public void onDestroy() {
        mConnector.unbind();
        super.onDestroy();
    }

    private class Adapter extends BaseAdapter
    {
        public Adapter(int setId) {
            mSetId = setId;
        }

        public void init(Coach coach, Workout workout) {
            Workout.Entry entry = workout.getEntry(mSetId);
            for (int i = 0; i < 3; ++i) { //go back up to 3 workouts
                if ((entry != null) && entry.isResults()) {
                    break;
                }
                workout = coach.getPreviousWorkout(workout);
                entry = workout.getEntry(mSetId);
            }
            Workout previousWorkout = coach.getPreviousWorkout(workout);;
            Workout.Entry previousEntry = null;
            if (previousWorkout != null) {
                previousEntry = previousWorkout.getSameEntry(entry);
            }
            mResultsAdapter = new SetResultsAdapter(entry, previousEntry, coach, getContext());
        }

        @Override
        public int getCount() {
            if (mResultsAdapter != null) {
                return mResultsAdapter.getCount();
            }
            return 0;
        }

        @Override
        public Object getItem(int i) {
            return mResultsAdapter.getItem(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            return mResultsAdapter.getView(i, view, viewGroup);
        }

        private int mSetId;
        private SetResultsAdapter mResultsAdapter;
    }

    private Adapter mAdapter;
    private CoachTalk mConnector;
}
