package com.vaisoft.health.gymgain.data;

import android.content.Context;
import android.util.Log;

import com.vaisoft.health.gymgain.sync.JSONLoader;
import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Body mMuscles map.
 */
public class Body
{
    private Body(JSONArray data)
    {
        if (data == null) {
            return;
        }
        try {
            for (int i=0; i < data.length(); i++)
            {
                JSONObject theObject = data.getJSONObject(i);
                Part part = new Part(theObject);
                mParts.add(part);
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    public static Body load(JSONLoader loader) {
        JSONArray bodyArray = loader.loadArray(FILE_NAME, Utils.FileTarget.FILE_TARGET_DYNAMIC);
        return new Body(bodyArray);
    }

    public void save(Context context) {
        JSONArray data = new JSONArray();
        try {
            for (Part part: mParts) {
                JSONObject partObject = part.save();
                data.put(partObject);
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
        Utils.saveJSON(context, FILE_NAME, data);
    }

    public List<String> getPartsForMuscles(List<String> muscles)
    {
        Set<String> uniqueList = new HashSet<>();
        for (String theMuscle: muscles) {
            uniqueList.add(getPartForMuscle(theMuscle));
        }
        return new ArrayList<>(uniqueList);
    }

    public String getPartForMuscle(String muscle) {
        for (Part thePart: mParts) {
            if (thePart.getMuscles().contains(muscle)) {
                return thePart.getId();
            }
        }
        Log.e(getClass().getName(), "Unknown body part for muscle " + muscle);
        return null;
    }

    public boolean isValidMuscle(String muscle) {
        for (Part thePart: mParts) {
            if (thePart.getMuscles().contains(muscle)) {
                return true;
            }
        }
        Log.e(getClass().getName(), muscle);
        return false;
    }

    public Part getPart(String id) {
        for (Part thePart: mParts) {
            if (thePart.getId().equals(id)) {
                return  thePart;
            }
        }
        return null;
    }

    public List<String> getAllParts() {
        List<String> result = new ArrayList<>();
        for (Part thePart: mParts) {
            result.add(thePart.getId());
        }
        return result;
    }

    public ArrayList<String> getFilteredMuscles(List<String> exclude) {
        ArrayList<String> result = new ArrayList<>();
        for (Part thePart: mParts) {
            result.addAll(thePart.getMuscles());
        }
        if (exclude != null) {
            result.removeAll(exclude);
        }
        return result;
    }

    public static void loadMuscles(Collection<String> muscles, JSONArray data) {
        try {
            for (int i=0; i < data.length(); i++) {
                String muscle = data.getString(i);
                muscles.add(muscle);
            }
        } catch (JSONException e) {
            Log.e(Body.class.getName(), e.getMessage());
        }
    }

    private static JSONArray saveMuscles(List<String> muscles) {
        JSONArray target = new JSONArray();
        for (String muscle: muscles) {
            target.put(muscle);
        }
        return target;
    }

    public static void addSourceFilesToList(List<String> files) {
        files.add(FILE_NAME);
    }

    private boolean isPart(String id) {
        for (Part part: mParts) {
            if (part.getId().equals(id)) {
                return true;
            }
        }
        return false;
    }

    public boolean merge(Body body) {
        boolean changed = false;
        for (Part part: body.mParts) {
            if (!isPart(part.getId())) {
                mParts.add(part);
                changed = true;
            }
        }
        return changed;
    }

    public class Part
    {
        public Part(JSONObject data) {
            try {
                mId = data.getString(FIELD_PART);
                mMuscles = new ArrayList<>();
                JSONArray muscles = data.getJSONArray(FIELD_MUSCLES);
                Body.loadMuscles(mMuscles, muscles);
           } catch (JSONException e) {
                Log.e(getClass().getName(), e.getMessage());
           }
         }

        public JSONObject save() throws JSONException {
            JSONObject target = new JSONObject();
            target.put(FIELD_PART, mId);
            JSONArray muscles = Body.saveMuscles(mMuscles);
            target.put(FIELD_MUSCLES, muscles);
            return target;
        }

        public String getId() {
            return mId;
        }

        public List<String> getMuscles() {
            return mMuscles;
        }

        private String mId;
        private List<String> mMuscles;
    }

    private List<Part> mParts = new ArrayList<>();

    private static final String FILE_NAME = "body";

    private static final String FIELD_PART = "part";
    private static final String FIELD_MUSCLES = "muscles";
}
