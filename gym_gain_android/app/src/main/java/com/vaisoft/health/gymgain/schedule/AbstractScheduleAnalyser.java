package com.vaisoft.health.gymgain.schedule;

import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractScheduleAnalyser implements ScheduleAnalyser {

    protected abstract DateTime getCurrentPeriodStart(DateTime checkDate);
    protected abstract DateTime analyseWorkoutsFrom();
    protected abstract int getPlannedCount(DateTime start, DateTime finish);
    protected abstract boolean isPlannedOn(DateTime date);
    protected abstract boolean isLegacyActual();
    protected abstract boolean isSingleDayPlan();

    public AbstractScheduleAnalyser(DateTime date, DateTime lastUpdatedDate, DateTime lastAnalysed) {
        mInitDate = new DateTime(lastUpdatedDate);
        mLastAnalysed = lastAnalysed;
        mCheckDate = date;
    }

    protected void init() {
        DateTime rawStart = getCurrentPeriodStart(mCheckDate);
        mCurrentPeriodStart = clampInitDate(rawStart);
    }

    private void prepareAnalysis(List<DateTime> workouts) {
        mCurrentPeriodWorkouts.clear();
        if (!workouts.isEmpty()) {
            mEdgeWorkout = workouts.get(0);
        } else {
            mEdgeWorkout = null;
        }
        for (DateTime workout : workouts) {
            if (workout.isBefore(mCurrentPeriodStart)) {
                mPreviousPeriodWorkouts.add(workout);
            } else {
                mCurrentPeriodWorkouts.add(workout);
            }
        }
    }

    public WorkoutHealth analyseHealth(List<DateTime> workouts) {
        prepareAnalysis(workouts);
        return analyseHealth();
    }

    public List<DateTime> analyseBurnout(List<DateTime> workouts) {
        prepareAnalysis(workouts);
        analyseBurnout();
        return mBurnedOut;
    }

    protected int getPreviousPeriodWorkoutsCount() {
        return mPreviousPeriodWorkouts.size();
    }

    protected List<DateTime> getCurrentPeriodWorkouts() {
        return mCurrentPeriodWorkouts;
    }

    protected int getCurrentPeriodWorkoutsCountSoFar(DateTime upTo) {
        int result = 0;
        for (DateTime workout: mCurrentPeriodWorkouts) {
            if (Utils.isBeforeOrEqualDay(workout, upTo)) {
                ++result;
            } else {
                break;
            }
        }
        return result;
    }

    protected boolean isCurrentPeriodWorkout(DateTime date) {
        for (DateTime periodDate: mCurrentPeriodWorkouts) {
            if (Utils.isEqualDay(periodDate, date)) {
                return true;
            }
        }
        return false;
    }

    protected List<DateTime> getPreviousPeriodWorkouts() {
        return mPreviousPeriodWorkouts;
    }

    protected DateTime getCheckDate() {
        return mCheckDate;
    }

    protected DateTime getLastAnalysedDate() {
        return mLastAnalysed;
    }

    protected void addBurnout(DateTime workout) {
        mBurnedOut.add(workout);
    }

    protected void resetBurnouts() {
        mBurnedOut.clear();
    }

    public DateTime getFirstDateToAnalyse() {
        DateTime toAnalyse = new DateTime(getCurrentPeriodStart());
        DateTime lastAnalysed = getLastAnalysedDate();
        if (Utils.isBeforeOrEqualDay(toAnalyse, lastAnalysed)) {
            toAnalyse = lastAnalysed.plusDays(1);
        }
        if ((mEdgeWorkout != null) && Utils.isBeforeOrEqualDay(toAnalyse, mEdgeWorkout)) {
            toAnalyse = mEdgeWorkout.plusDays(1);
        }
        return toAnalyse;
    }

    protected DateTime clampInitDate(DateTime date) {
        if (date.isBefore(mInitDate)) {
            return mInitDate;
        }
        return date;
    }

    protected DateTime getCurrentPeriodStart() {
        return mCurrentPeriodStart;
    }

    @Override
    public DateTime getHistoryStart() {
        if (mHistoryStart == null) {
            DateTime start = analyseWorkoutsFrom();
            mHistoryStart = clampInitDate(start);
        }
        return mHistoryStart;
    }

    protected static DateTime getWeeklyPeriodStart(DateTime date) {
        return date.withDayOfWeek(1);
    }

    private boolean isCurrentPeriodOpenFail(DateTime date) {
        List<DateTime> currentWorkouts = getCurrentPeriodWorkouts();
        DateTime currentPeriodStart = getCurrentPeriodStart();
        return isOpenFail(currentWorkouts, currentPeriodStart, date);
    }

    private boolean isPreviousPeriodOpenFail() {
        List<DateTime> previousWorkouts = getPreviousPeriodWorkouts();
        DateTime historyStart = getHistoryStart();
        DateTime currentPeriodStart = getCurrentPeriodStart();
        return isOpenFail(previousWorkouts, historyStart, currentPeriodStart);
    }

    private void analyseBurnout() {
        DateTime analysing = getFirstDateToAnalyse();
        DateTime current = getCheckDate();

        resetBurnouts();

        DateTime dayBefore = analysing.minusDays(1);
        int balance = getInitialState(dayBefore);

        while (Utils.isBeforeOrEqualDay(analysing, current)) {
            if (isPlannedOn(analysing)) {
                balance++;
                if (balance > 1) {
                    addBurnout(analysing);
                    balance = 1;
                }
            }
            analysing = analysing.plusDays(1);
        }
    }

    private WorkoutHealth analyseHealth() {
        DateTime date = getCheckDate();

        int workoutsCountSoFar = getCurrentPeriodWorkoutsCountSoFar(date);
        int plannedCurrent = getCurrentPeriodPlannedCount(date);
        if (plannedCurrent <= workoutsCountSoFar) {
            return WorkoutHealth.Healthy;
        }

        if (isCurrentPeriodWorkout(date)) {
            return WorkoutHealth.Healthy;
        }

        if (isPlannedOn(date)) {
            return WorkoutHealth.Notify;
        }

        if (isCurrentPeriodOpenFail(date)) {
            return WorkoutHealth.Critical;
        }

        int plannedPrevious = getPreviousPeriodPlannedCount();
        int workoutsPrevious = getPreviousPeriodWorkoutsCount();
        if (plannedPrevious < workoutsPrevious) {
            if (isPreviousPeriodOpenFail()) {
                return WorkoutHealth.Critical;
            }
        }

        return WorkoutHealth.Healthy;
    }

    private int getPreviousPeriodPlannedCount() {
        DateTime historyStart = getHistoryStart();
        DateTime currentPeriodStart = getCurrentPeriodStart();
        return getPlannedCount(historyStart, currentPeriodStart);
    }

    private int getCurrentPeriodPlannedCount(DateTime date) {
        DateTime start = getCurrentPeriodStart();
        return getPlannedCount(start, date);
    }

    private int getLegacyState() {
        if (!isLegacyActual()) {
            return 0;
        }
        int planned = getPreviousPeriodPlannedCount();
        int workouts = getPreviousPeriodWorkoutsCount();
        int status = planned - workouts;
        if (status < 0) {
            return -1; //было больше занятий, чем по плану - даем бонус
        } else if (status > 0) { //занятий меньше, чем надо, ищем дыру
            if (isSingleDayPlan() || isPreviousPeriodOpenFail()) {
                return 1; //Дыра в конце есть - получаем штрафное очко
            }
        }
        return 0;
    }

    private int getInitialState(DateTime current) {
        int legacyState = getLegacyState();
        if (current.isBefore(getCurrentPeriodStart())) {
            return legacyState;
        }
        int workoutsCountSoFar = getCurrentPeriodWorkoutsCountSoFar(current);
        int plannedSoFar = getCurrentPeriodPlannedCount(current);
        if (workoutsCountSoFar == plannedSoFar) {
            return 0;
        } else if (workoutsCountSoFar > plannedSoFar)  {
            return -1;
        } else if (isCurrentPeriodOpenFail(current)) {
            if (legacyState < 0) {
                return 0;
            } else {
                return 1; //Если прошлая запланипованная тренировка пропущена, начинаем со штрафным баллом
            }
        }
        //С этим периодом непонятно - переходим к анализу предыдущего периода
        return legacyState;
    }

    private boolean isOpenFail(List<DateTime> workouts, DateTime start, DateTime finish) {
        DateTime current = new DateTime(finish);
        while (current.isAfter(start)) {
            current = current.minusDays(1);
            if (isPlannedOn(current)) {
                return !workouts.contains(current);
            }
        }
        return false;
    }

    private DateTime mInitDate;
    private DateTime mCurrentPeriodStart;
    private DateTime mLastAnalysed;
    private DateTime mCheckDate;
    private DateTime mHistoryStart;
    private DateTime mEdgeWorkout;

    private List<DateTime> mCurrentPeriodWorkouts = new ArrayList<>();
    private List<DateTime> mPreviousPeriodWorkouts = new ArrayList<>();
    private List<DateTime> mBurnedOut = new ArrayList<>();
}
