package com.vaisoft.health.gymgain.controls;


import androidx.recyclerview.widget.RecyclerView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.utils.Utils;

public abstract class CoachListFragment
        extends CoachFragment {

    public abstract void onContextMenu(int id);

    public abstract class Adapter<T extends RecyclerView.ViewHolder>
            extends ContextAdapter<T> {


        public abstract void updateData(boolean force);


        public void setCoach(Coach coach) {
            mCoach = coach;
            if (mCoach != null) {
                updateData(true);
            }
        }

        public void updateIfNecessary() {
            if (mCoach != null) {
                updateData(false);
            }
        }

        public Coach getCoach() {
            return mCoach;
        }

        public void setFilter(String filter) {
            if ((mFilter == null) || (!mFilter.equals(filter))) {
                mFilter = filter;
                if (mCoach != null) {
                    updateData(true);
                }
            }
        }

        public void resetFilter() {
            setFilter(null);
        }

        protected String getFilter() {
            return mFilter;
        }

        private String mFilter;
        private Coach mCoach;
    }

    protected abstract Adapter createAdapter();

    @Override
    protected void init() {
        mList = (RecyclerView) getView();
        Utils.initOrientationAwareList(mList);
        mAdapter = createAdapter();
        mAdapter.setCoach(getCoach());
        mList.setAdapter(mAdapter);
    }

    @Override
    protected int getRootResource() {
        return R.layout.fragment_data_list;
    }

    @Override
    public void onResume() {
        super.onResume();
        update();
    }

    public void update() {
        if (mAdapter != null) {
            mAdapter.updateIfNecessary();
        }
    }

    public void applyFilter(String filter) {
        if (mAdapter != null) {
            mAdapter.setFilter(filter);
        }
    }

    public void resetFilter() {
        if (mAdapter != null) {
            mAdapter.resetFilter();
        }
    }

    @Override
    public void onDestroy() {
        if (mList != null) {
            mList.clearOnScrollListeners();
        }
        super.onDestroy();
    }

    private Adapter mAdapter;
    private RecyclerView mList;
}
