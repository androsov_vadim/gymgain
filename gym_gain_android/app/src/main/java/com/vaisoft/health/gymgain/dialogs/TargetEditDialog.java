package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.View;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.BoostEditor;
import com.vaisoft.health.gymgain.controls.BoostNumberEditor;
import com.vaisoft.health.gymgain.controls.BoostTimeEditor;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.edit.ExerciseDetailActivity;

public class TargetEditDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.edit_exercise_target);
        builder.setIcon(R.drawable.ic_my_location_black_24dp);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                mValue.set(mValue.unit(), mEditor.accessValue());
                listener.onTargetUpdated(mValue);
            }
        });
        builder.setNeutralButton(R.string.delete_exercise_target, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                listener.onTargetDeleted(getParameter());
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        Bundle args = getArguments();
        mValue = new Value(args);
        if (savedInstanceState != null) {
            mCaption = savedInstanceState.getString(CAPTION);
        }
        builder.setView(createInterface());
        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CAPTION, mCaption);
    }

    private View createInterface() {
        View view = View.inflate(getActivity(), R.layout.exercise_parameter_editor, null);
        if (mValue.unit() == Unit.time) {
            mEditor = (BoostTimeEditor) view.findViewById(R.id.time_editor);
        } else {
            mEditor = (BoostNumberEditor) view.findViewById(R.id.number_editor);
        }
        mEditor.setVisibility(View.VISIBLE);
        long value = mValue.get(mValue.unit());
        mEditor.setValue(mValue.unit(), value, mValue.getStatus());
        TextView caption = (TextView) view.findViewById(R.id.parameter_name);
        if (mCaption == null) {
            ExerciseDetailActivity activity = (ExerciseDetailActivity) getActivity();
            mCaption = activity.localize(getParameter().toString());
        }
        caption.setText(mCaption);
        return view;
    }

    private Unit getParameter() {
        return mValue.unit().getCategory();
    }

    //------------------------------------------------------------------------------

    public interface Listener {
        void onTargetUpdated(Value value);
        void onTargetDeleted(Unit parameter);
    }

    private BoostEditor mEditor;
    private Value mValue;
    private String mCaption;

    private static final String CAPTION = "caption";
}
