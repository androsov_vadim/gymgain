package com.vaisoft.health.gymgain.sync;

import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public interface JSONLoader {
    JSONObject loadObject(String id, Utils.FileTarget target);
    JSONArray loadArray(String id, Utils.FileTarget target);
}
