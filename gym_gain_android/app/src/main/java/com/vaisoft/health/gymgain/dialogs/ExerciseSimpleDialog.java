package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.vaisoft.health.gymgain.R;

public class ExerciseSimpleDialog extends MultySelectDialog {
    @Override
    protected int configDialogCreate(AlertDialog.Builder builder) {
        builder.setNeutralButton(R.string.delete_set, null);
        return R.string.select_exercises;
    }

    @Override
    public void onStart() {
        super.onStart();
        AlertDialog dialog = (AlertDialog)getDialog();
        if(dialog != null) {
            Bundle bundle = getArguments();
            boolean setSelected = bundle.getBoolean(SET_SELECTED);
            Button neutralButton = dialog.getButton(Dialog.BUTTON_NEUTRAL);
            if (setSelected) {
                neutralButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Listener listener = (Listener) getActivity();
                        listener.onSetDeleteConfirmed();
                        dismiss();
                    }
                });
            } else {
                neutralButton.setEnabled(false);
            }
        }
    }

    public interface Listener {
        void onSetDeleteConfirmed();
    }

    public static final String SET_SELECTED = "set";
}
