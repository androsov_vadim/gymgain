package com.vaisoft.health.gymgain.data;

import android.util.Log;

import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Exercise extends Lifecycle implements Cloneable
{
    @Override
    public Exercise clone() throws CloneNotSupportedException {
        Exercise newExercise = (Exercise) super.clone();
        newExercise.mMuscles = new ArrayList<>(mMuscles);
        newExercise.mParameters = new HashMap<>();
        for (Map.Entry<Unit, ParameterConfig> entry : mParameters.entrySet()) {
            newExercise.mParameters.put(entry.getKey(), entry.getValue().clone());
        }
        return newExercise;
    }

    public Exercise() {
        mParameters = new HashMap<>();
        mMuscles = new ArrayList<>();
    }

    public Exercise(JSONObject data) {
        try {
            mId = data.getString(FIELD_ID);
            loadLifecycle(data);
            if (!isDeleted()) {
                initLifecycleUniqueId();
                mParameters = new HashMap<>();
                for (Unit parameter : Unit.parametersOrder) {
                    readParameter(data, parameter);
                }
                mMuscles = new ArrayList<>();
                Body.loadMuscles(mMuscles, data.getJSONArray(FIELD_MUSCLES));
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    private void initLifecycleUniqueId() {
        if (getLifecycleUniqueId() == 0) {
            ++UNIQUE_ID_GENERATOR;
            setLifecycleUniqueId(UNIQUE_ID_GENERATOR);
        }
    }

    private void readParameter(JSONObject data, Unit metric) throws JSONException {
        String id = metric.toString();
        if (!data.has(id)) {
            return;
        }
        mParameters.put(metric, new ParameterConfig(metric, data, id));
    }

    private void writeParameter(JSONObject data, Unit parameter) throws JSONException {
        if (isParameter(parameter)) {
            data.put(parameter.toString(), mParameters.get(parameter).write());
        }
    }

    public JSONObject writeToJson() {
        JSONObject object = new JSONObject();
        try {
            object.put(FIELD_ID, mId);

            saveLifecycle(object);

            if (!isDeleted()) {
                for (Unit parameter : Unit.parametersOrder) {
                    writeParameter(object, parameter);
                }

                JSONArray muscles = new JSONArray();
                for (String theMuscle : mMuscles) {
                    muscles.put(theMuscle);
                }
                object.put(FIELD_MUSCLES, muscles);
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }

        return  object;
    }

    public String getId() {
        return mId;
    }

    public List<Unit> getParameterUnits() {
        List<Unit> result = new ArrayList<>();
        result.addAll(mParameters.keySet());
        return result;
    }

    public boolean isParameter(Unit parameter) {
        return mParameters.containsKey(parameter);
    }

    public Value getParameter(Unit parameter) {
        if (mParameters.containsKey(parameter)) {
            return mParameters.get(parameter).target;
        } else {
            return getParameterDefaultValue(parameter).target;
        }
    }

    public boolean resetParameter(Unit parameter) {
        Value value = getParameter(parameter);
        if (value.isZero()) {
            return false;
        }
        value.setZero();
        return true;
    }

    public void setParameter(Unit parameter, boolean grow) {
        mParameters.put(parameter, new ParameterConfig(parameter, grow));
    }

    public boolean setParameter(Unit parameter, Value value) {
        if (getParameter(parameter).equals(value)) {
            return false;
        }
        mParameters.put(parameter, new ParameterConfig(value));
        return true;
    }

    public List<String> getMuscles() {
        return mMuscles;
    }

    public void setId(String id) {
        mId = id;
    }

    public boolean validate(Body body) {
        boolean result = true;
        if (isDeleted() && (mMuscles == null)) {
            return true;
        }
        for (String theMuscle: mMuscles) {
            if (!body.isValidMuscle(theMuscle)) {
                result = false;
            }
        }
        return result;
    }


    public String getAffectedFullString(Body body, Localizer localizer) {
        if (mMuscles.isEmpty()) {
            return "";
        }
        List<String> parts = body.getPartsForMuscles(mMuscles);
        String partsString = Utils.convertListToString(parts, localizer);

        StringBuilder result = new StringBuilder(partsString);

        result.append(": ");

        String musclesString = Utils.convertListToString(mMuscles, localizer);
        result.append(musclesString);

        return result.toString();
    }

    public void addParameter(Unit parameter) {
        mParameters.put(parameter, getParameterDefaultValue(parameter));
    }

    private ParameterConfig getParameterDefaultValue(Unit parameter) {
        boolean shouldGrow = false;
        switch (parameter) {
            case weight:
                shouldGrow = true;
                break;
            case reps:
                shouldGrow = true;
                break;
            case time:
                shouldGrow = false;
                break;
            case distance:
                shouldGrow = true;
                break;
        }
        return new ParameterConfig(parameter, shouldGrow);
    }

    public void removeParameter(Unit parameter) {
        mParameters.remove(parameter);
    }

    public boolean isUseless() {
        return mParameters.isEmpty();
    }

    public List<Value> getTargetedParameters() {
        List<Value> result = new ArrayList<>();
        for (Map.Entry<Unit, ParameterConfig> entry : mParameters.entrySet()) {
            Value value = entry.getValue().target;
            if (!value.isZero()) {
                result.add(value);
            }
        }
        return result;
    }

    private class ParameterConfig implements Cloneable {
        public ParameterConfig(Unit parameter, boolean shouldGrow) {
            target = new Value(parameter, shouldGrow);
        }

        public ParameterConfig(Value value) {
            target = value;
        }

        public ParameterConfig(Unit parameter, JSONObject source, String fieldId) throws JSONException {
            JSONObject object = null;
            try {
                object = source.getJSONObject(fieldId);
            } catch (JSONException ex) {
                //ok: this field is not an object
            }
            if (object != null) {
                target = new Value(object);
            } else {
                target = new Value(parameter, Value.getTargetValue(source.getString(fieldId)));
            }
        }

        public JSONObject write() throws JSONException {
            JSONObject targetObject = new JSONObject();
            target.save(targetObject);
            return targetObject;
        }

        @Override
        public ParameterConfig clone() throws CloneNotSupportedException {
            ParameterConfig result = (ParameterConfig) super.clone();
            result.target = target.clone();
            return result;
        }

        Value target;
    }

    private String mId;
    private Map<Unit, ParameterConfig> mParameters;
    private List<String> mMuscles;

    private static final String FIELD_ID = "id";
    private static final String FIELD_MUSCLES = "muscles";

    private static long UNIQUE_ID_GENERATOR = 0;
}
