package com.vaisoft.health.gymgain.dialogs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.coach.CoachConnect;
import com.vaisoft.health.gymgain.coach.CoachTalk;
import com.vaisoft.health.gymgain.data.Exercise;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExerciseDialog extends DialogFragment implements AdapterView.OnItemClickListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        mSelectedExercises = new ArrayList<>();

        Bundle bundle = getArguments();
        mExcludeExercises = bundle.getStringArrayList(DATA);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_exercise);
        builder.setIcon(R.drawable.ic_directions_run_black_24dp);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                listener.onExercisesSelected(mSelectedExercises);
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.setNeutralButton(R.string.back, null);
        builder.setView(createInterface());

        mConnector = new CoachTalk(getContext());
        mConnector.bind(new CoachConnect() {
            @Override
            public void onCoachConnected() {
                onCoachReady();
            }
        });

        return builder.create();
    }

    private void onCoachReady() {
        setListViewClickable(mPages[SLIDE_FILTER_TYPE], true);
    }

    private void autoApplyFilter() {
        String filterValue = mResultFilter.getText().toString();
        if (!filterValue.isEmpty()) {
            applyFilter(filterValue);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateState();
    }

    @Override
    public void onDestroy() {
        mConnector.unbind();
        super.onDestroy();
    }

    private View createInterface() {
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        View view = View.inflate(getActivity(), R.layout.exercise_selector, null);

        mPages = new View[SLIDES_COUNT];

        mPages[SLIDE_FILTER_TYPE] = view.findViewById(R.id.ex_choose_filter);
        mPages[SLIDE_FILTER_PARAMETERS] = view.findViewById(R.id.ex_use_filter);
        mPages[SLIDE_FILTER_RESULT] = view.findViewById(R.id.ex_result_page);
        mResultList = (ListView) view.findViewById(R.id.ex_result);

        mResultFilter = (EditText) view.findViewById(R.id.ex_result_filter);
        mResultFilter.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                applyFilter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ImageButton clearFilter = (ImageButton) view.findViewById(R.id.ex_result_filter_clear);
        clearFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetFilter();
            }
        });

        initFilterTypesSlide();
        initFilterApplySlide();
        initResultSlide();

        mCurrentSlide = 0;
        mBackwardSlide = -1;

        return view;
    }

    private void applyFilter(String filter) {
        getFilterResultAdapter().applyFilter(filter);
    }

    private void resetFilter() {
        mResultFilter.setText("");
        getFilterResultAdapter().resetFilter();
    }

    private void initFilterTypesSlide() {
        ListView slide = (ListView) mPages[SLIDE_FILTER_TYPE];
        slide.setAdapter(new FilterTypeAdapter());
    }

    private void initFilterApplySlide() {
        ListView slide = (ListView) mPages[SLIDE_FILTER_PARAMETERS];
        slide.setVisibility(View.GONE);
        slide.setAdapter(new FilterParametersAdapter());
    }

    private void initResultSlide() {
        mPages[SLIDE_FILTER_RESULT].setVisibility(View.GONE);
        mResultList.setAdapter(new FilterResultAdapter());
        mResultList.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
    }

    @Override
    public void onStart() {
        super.onStart();
        AlertDialog dialog = (AlertDialog)getDialog();
        if(dialog != null) {
            mOkButton = dialog.getButton(Dialog.BUTTON_POSITIVE);
            mOkButton.setEnabled(false);
            Button neutralButton = dialog.getButton(Dialog.BUTTON_NEUTRAL);
            neutralButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mCurrentSlide == SLIDE_FILTER_TYPE) {
                        dismiss();
                    } else {
                        backward();
                    }
                }
            });
        }
    }

    private void resetResult() {
        mSelectedExercises.clear();
        mResultList.clearChoices();
        resetFilter(); //call mResultList.requestLayout() or adapter.notifyDataSetChanged() when moving it above clearChoices
    }

    private void backward() {
        if (mBackwardSlide >= 0) {
            crossFade(false);
            if (mCurrentSlide == SLIDE_FILTER_RESULT) {
                resetResult();
            }
            setCurrentSlide(mBackwardSlide);
            mBackwardSlide--;
        }
    }

    private void setCurrentSlide(int slide) {
        mCurrentSlide = slide;
        updateState();
        if (mCurrentSlide == SLIDE_FILTER_RESULT) {
            autoApplyFilter();
        }
    }

    private void updateState() {
        mOkButton.setEnabled(mCurrentSlide == SLIDE_FILTER_RESULT);
    }

    private void gotoSlide(int slide) {
        if ((slide != mCurrentSlide) && (slide >= 0) && (slide < SLIDES_COUNT)) {
            mBackwardSlide = mCurrentSlide;
            setCurrentSlide(slide);
            crossFade(true);
        } else {
            Log.e(getClass().getName(), "Attempt to activate wrong slide");
        }
    }

    private void crossFade(boolean forward) {
        final View toHide = mPages[forward ? mBackwardSlide : mCurrentSlide];
        final View toShow = mPages[forward ? mCurrentSlide : mBackwardSlide];
        float dx = toHide.getMeasuredWidth();
        if (!forward) {
            dx = -dx;
        }
        setListViewClickable(toHide, false);
        toHide.animate().x(-dx).
                setDuration(mShortAnimationDuration).setListener(null);

        toShow.setVisibility(View.VISIBLE);
        setListViewClickable(toShow, false);
        toShow.setX(dx);
        toShow.animate().x(0).
                setDuration(mShortAnimationDuration).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                setListViewClickable(toShow, true);
                toHide.setVisibility(View.GONE);
            }
        });
    }

    private void setListViewClickable(View lw, boolean clickable) {
        ListView list = null;
        if (lw == mPages[SLIDE_FILTER_RESULT]) {
            list = mResultList;
        } else {
            list = (ListView) lw;
        }
        list.setOnItemClickListener(clickable ? this : null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (mCurrentSlide) {
            case SLIDE_FILTER_TYPE:
                onFilterTypeSelected(getFilterType(position));
                break;
            case SLIDE_FILTER_PARAMETERS:
                onFilterParametersSelected(getFilterTypeAdapter().getName(position));
                break;
            case SLIDE_FILTER_RESULT:
                onFilterResultClick(position);
                break;
        }
    }

    private void onFilterResultClick(int position) {
        boolean checked = mResultList.isItemChecked(position);
        String id = getFilterResultAdapter().getId(position);
        if (checked) {
            mSelectedExercises.add(id);
        } else {
            mSelectedExercises.remove(id);
        }
    }

    private void onFilterTypeSelected(FilterType type) {
        switch (type) {
            case BODY_PART:
            case MUSCLE:
                if (getFilterTypeAdapter().setFilterType(type)) {
                    gotoSlide(SLIDE_FILTER_PARAMETERS);
                } else {
                    showNothingFoundMessage();
                }
                break;
            case ALL:
            case USED:
                if (getFilterResultAdapter().setParameter(type)) {
                    gotoSlide(SLIDE_FILTER_RESULT);
                } else {
                    showNothingFoundMessage();
                }
                break;
        }
    }

    private void onFilterParametersSelected(String parameters) {
        if (getFilterResultAdapter().setParameter(parameters)) {
            gotoSlide(SLIDE_FILTER_RESULT);
        } else {
            showNothingFoundMessage();
        }
    }

    private void showNothingFoundMessage() {
        Toast toast = Toast.makeText(getContext(), R.string.ex_result_empty, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    private FilterParametersAdapter getFilterTypeAdapter() {
        ListView lw = (ListView) mPages[SLIDE_FILTER_PARAMETERS];
        return (FilterParametersAdapter) lw.getAdapter();
    }

    private FilterResultAdapter getFilterResultAdapter() {
         return (FilterResultAdapter) mResultList.getAdapter();
    }

    static private FilterType getFilterType(int position)
    {
        switch (position) {
            case 0:
                return FilterType.ALL;
            case 1:
                return FilterType.USED;
            case 2:
                return FilterType.BODY_PART;
            case 3:
                return FilterType.MUSCLE;
            default:
                Log.e(ExerciseDialog.class.getName(), "Unknown filter type");
                return FilterType.UNDEFINED;
        }
    }

    private Coach getCoach() {
        return mConnector.getCoach();
    }

    private void applySelectedExercises(List<Exercise> data) {
        if (!mSelectedExercises.isEmpty()) {
            mResultList.clearChoices();
            for (String exerciseId: mSelectedExercises) {
                int index = indexOfExercise(data, exerciseId);
                if (index != -1) {
                    mResultList.setItemChecked(index, true);
                }
            }
        }
    }

    private int indexOfExercise(List<Exercise> exercises, String id) {
        int index = 0;
        for (Exercise exercise: exercises) {
            if (exercise.getId().equals(id)) {
                return index;
            }
            ++index;
        }
        return -1;
    }

    //---------------------------------------------------------------

    private class FilterResultAdapter extends PlainTextAdapter {

        public FilterResultAdapter() {
            super(true);
        }

        public boolean setParameter(FilterType filterType) {
            switch (filterType) {
                case ALL:
                    return setData(getCoach().getFilteredExercises(null));
                case USED:
                    return setData(getCoach().getUsedExercises());
                default:
                    Log.e(getClass().getName(), "Wrong filter without parameters");
            }
            return false;
        }

        public boolean setParameter(String parameter) {
            setData(getCoach().getFilteredExercises(parameter));
            return applyChanges();
        }

        private boolean setData(List<Exercise> data) {
            mData = data;
            for (Iterator<Exercise> iExercise = mData.iterator(); iExercise.hasNext();) {
                Exercise exercise = iExercise.next();
                if (mExcludeExercises.contains(exercise.getId())) {
                    iExercise.remove();
                }
            }
            return resetFilter();
        }

        public boolean resetFilter() {
            return setFilteredData(mData);
        }

        private boolean setFilteredData(List<Exercise> mData) {
            mFilteredData = mData;
            boolean isData = applyChanges();
            if (isData) {
                applySelectedExercises(mFilteredData);
            }
            return isData;
        }

        public void applyFilter(String filter) {
            Coach coach = getCoach();
            if (coach != null) {
                List<Exercise> newData = coach.getQuickFilteredExercises(mData, filter);
                setFilteredData(newData);
            }
        }

        @Override
        public int getCount() {
            if (mFilteredData != null) {
                return mFilteredData.size();
            } else {
                return 0;
            }
        }

        @Override
        public Object getItem(int position) {
            return getCoach().getLocalized(getId(position));
        }

        public String getId(int position) {
            return mFilteredData.get(position).getId();
        }

        private List<Exercise> mData;
        private List<Exercise> mFilteredData;
    }

    private class FilterParametersAdapter extends PlainTextAdapter {

        public FilterParametersAdapter() {
            super(false);
        }

        public boolean setFilterType(FilterType filterType) {
            switch (filterType) {
                case MUSCLE:
                    mData = getCoach().getAllMuscles();
                    break;
                case BODY_PART:
                    mData = getCoach().getAllParts();
                    break;
                default:
                    Log.e(getClass().getName(), "Error: filter has no parameters");
            }
            return applyChanges();
        }

        public String getName(int position) {
            String id = mData.get(position);
            return getCoach().getLocalized(id);
        }

        @Override
        public int getCount() {
            if (mData != null) {
                return mData.size();
            } else {
                return 0;
            }
        }

        @Override
        public Object getItem(int position) {
            return getName(position);
        }

        private List<String> mData;
    }

    private class FilterTypeAdapter extends PlainTextAdapter {

        public FilterTypeAdapter() {
            super(false);
            mFilters = new ArrayList<>();
            Context context = getContext();
            mFilters.add(context.getResources().getString(R.string.ex_filter_all));
            mFilters.add(context.getResources().getString(R.string.ex_filter_used));
            mFilters.add(context.getResources().getString(R.string.ex_filter_body_part));
            mFilters.add(context.getResources().getString(R.string.ex_filter_muscle));
        }

        @Override
        public int getCount() {
            return mFilters.size();
        }

        @Override
        public Object getItem(int position) {
            return mFilters.get(position);
        }

        private List<String> mFilters;
    }

    private abstract class PlainTextAdapter extends BaseAdapter {

        public PlainTextAdapter(boolean multipleChoise) {
            mInflater = LayoutInflater.from(getContext());
            mMultipleChoise = multipleChoise;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        protected boolean applyChanges() {
            notifyDataSetChanged();
            return getCount() > 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                int layout = mMultipleChoise ?
                        android.R.layout.simple_list_item_multiple_choice :
                        android.R.layout.simple_list_item_1;
                view = mInflater.inflate(layout, parent, false);
            } else {
                view = convertView;
            }
            TextView text = (TextView) view.findViewById(android.R.id.text1);
            text.setText((String) getItem(position));
            return view;
        }

        private final boolean mMultipleChoise;
        private final LayoutInflater mInflater;
    }

    public interface Listener {
        void onExercisesSelected(List<String> exercises);
    }

    private View[] mPages;
    private int mCurrentSlide;
    private int mBackwardSlide;
    private int mShortAnimationDuration;
    private CoachTalk mConnector;
    private ListView mResultList;
    private EditText mResultFilter;
    private Button mOkButton;
    private List<String> mSelectedExercises;
    private List<String> mExcludeExercises;

    private enum FilterType {
        ALL,
        USED,
        BODY_PART,
        MUSCLE,
        UNDEFINED
    }

    private static final int SLIDE_FILTER_TYPE = 0;
    private static final int SLIDE_FILTER_PARAMETERS = 1;
    private static final int SLIDE_FILTER_RESULT = 2;
    private static final int SLIDES_COUNT = 3;

    public static final String DATA = "filterOut";
}
