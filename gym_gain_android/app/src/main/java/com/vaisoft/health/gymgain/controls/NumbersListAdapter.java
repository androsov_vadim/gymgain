package com.vaisoft.health.gymgain.controls;

import android.content.Context;

public class NumbersListAdapter extends SpinnerSimpleAdapter {

    public NumbersListAdapter(Context context, int maxValue, boolean zeroAsUndefined) {
        super(context);
        mMaxValue = maxValue;
        mZeroAsUndefined = zeroAsUndefined;
    }

    @Override
    public int getCount() {
        return mMaxValue + (mZeroAsUndefined ? 1 : 0);
    }

    @Override
    public String getItem(int position) {
        if (mZeroAsUndefined) {
            if (position == 0) {
                return "?";
            } else {
                return "" + position;
            }
        } else {
            return "" + (position + 1);
        }
    }

    private int mMaxValue;
    private boolean mZeroAsUndefined;
}
