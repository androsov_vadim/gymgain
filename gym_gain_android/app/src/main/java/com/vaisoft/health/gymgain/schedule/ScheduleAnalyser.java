package com.vaisoft.health.gymgain.schedule;

import org.joda.time.DateTime;

import java.util.List;

public interface ScheduleAnalyser {
    DateTime getHistoryStart();
    WorkoutHealth analyseHealth(List<DateTime> workouts);
    List<DateTime> analyseBurnout(List<DateTime> workouts);
}
