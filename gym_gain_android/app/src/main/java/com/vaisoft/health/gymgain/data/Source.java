package com.vaisoft.health.gymgain.data;

import org.json.JSONException;
import org.json.JSONObject;

public class Source implements Cloneable {

    public static Source createTraining(String id) {
        return new Source(id, Type.Training);
    }

    public static Source createExercise(String id) {
        return new Source(id, Type.Exercise);
    }

    public Source(JSONObject source) throws JSONException {
        mId = source.getString(ID);
        mType = Type.valueOf(source.getString(TYPE));
    }

    private Source(String id, Type type) {
        mId = id;
        mType = type;
    }

    public void save(JSONObject target) throws JSONException {
        target.put(ID, mId);
        target.put(TYPE, mType.toString());
    }

    public boolean isTraining() {
        return mType == Type.Training;
    }

    public boolean isExercise() {
        return mType == Type.Exercise;
    }

    public String getId() {
        return mId;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Source) {
            Source source = (Source) obj;
            return mId.equals(source.mId) && (mType == source.mType);
        } else {
            return false;
        }
    }

    public boolean isForTraining(String id) {
        if (isTraining()) {
            return id.equals(mId);
        } else {
            return false;
        }
    }

    public boolean isForExercise(String id) {
        if (isExercise()) {
            return id.equals(mId);
        } else {
            return false;
        }
    }

    @Override
    protected Source clone() {
        return new Source(mId, mType);
    }

    public String getKey() {
        return getKey(mType, mId);
    }

    public static String getKey(Type type, String id) {
        switch (type) {
            case Exercise:
                return EXERCISE_PREFIX + id;
            case Training:
                return TRAINING_PREFIX + id;
        }
        return null;
    }

    public static String getId(String key) {
        if (key.startsWith(EXERCISE_PREFIX)) {
            return key.substring(EXERCISE_PREFIX.length());
        }
        if (key.startsWith(TRAINING_PREFIX)) {
            return key.substring(TRAINING_PREFIX.length());
        }
        return null;
    }

    public static String getKey(Training training) {
        return TRAINING_PREFIX + training.getId();
    }

    public static String getKey(Exercise exercise) {
        return EXERCISE_PREFIX + exercise.getId();
    }

    public enum Type {
        Exercise,
        Training
    }

    private Type mType;
    private String mId;

    private static final String TYPE = "type";
    private static final String ID = "id";

    private static final String EXERCISE_PREFIX = "e";
    private static final String TRAINING_PREFIX = "t";
}
