package com.vaisoft.health.gymgain.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import androidx.annotation.IdRes;
import androidx.annotation.StringRes;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.sync.Cloud;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;

import java.util.List;

public class CloudFragment extends SettingsTabFragment implements Cloud.SyncFinishListener {
    @Override
    protected void init() {
        mTabs = new View[3];
        mTabs[TAB_INIT] = findViewById(R.id.tab_init);
        mTabs[TAB_WAIT] = findViewById(R.id.tab_wait);
        mTabs[TAB_INFO] = findViewById(R.id.tab_info);

        mSyncChoice = (RadioGroup) findViewById(R.id.cloud_sync_options);

        initGSMSyncConfig();
        initButtons();
    }

    @Override
    public void onResume() {
        super.onResume();
        final Handler ticker = new Handler();
        ticker.post(new Runnable() {
            @Override
            public void run() {
                if (isReady()) {
                    quickStart();
                } else {
                    ticker.postDelayed(this, 500);
                }
            }
        });
    }

    private boolean isReady() {
        return isInitialized() && (getActivity() != null) && (getCurrentTabIndex() == TAB) && (getCoach() != null);
    }

    @Override
    public void onTabSelected() {
        super.onTabSelected();
        if (isInitialized()) {
            quickStart();
        }
    }

    private void initButtons() {
        Button starter = (Button) findViewById(R.id.init_cloud);
        starter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCloud();
            }
        });

        Button canceler = (Button) findViewById(R.id.cancel_connect_cloud);
        canceler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelCloud();
            }
        });

        FloatingActionButton refresher = (FloatingActionButton) findViewById(R.id.cloud_update);
        refresher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshCloud();
            }
        });
    }

    private void refreshCloud() {
        showWaitingConnectionTab();
        mCloud.updateInfo();
    }

    public void setActivityLocked(boolean locked) {
        ((SettingsActivity)getActivity()).setLocked(locked);
    }

    private void initGSMSyncConfig() {
        CheckBox enableAutoGSMSync = (CheckBox) findViewById(R.id.auto_sync_gsm);
        enableAutoGSMSync.setChecked(getConfig().isAutoGSMSyncEnabled());
        enableAutoGSMSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getConfig().setAutoGSMSyncEnabled(isChecked);
            }
        });
    }

    private Config getConfig() {
        return getCoach().getConfig();
    }

    private void cancelCloud() {
        disconnectCloud();
        showInitConnectionTab();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ((mCloud != null) && mCloud.isResolveCode(requestCode)) {
            boolean ok = resultCode == Activity.RESULT_OK;
            if (ok) {
                mCloud.reconnectAfterRecovery();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean isInitialized() {
        return mTabs != null;
    }

    @Override
    public void onStop() {
        if (mCloud != null) {
            if (mCloud.isInProgress()) {
                mCloud.setListener(null);
                sSavedCloud = mCloud;
                setActivityLocked(false);
            } else {
                mCloud.close();
            }
            mCloud = null;
        }
        mStarted = false;
        super.onStop();
    }

    private void disconnectCloud() {
        if (mCloud != null) {
            mCloud.disconnect();
        }
    }

    private void quickStart() {
        if (mStarted) {
            return;
        }
        mStarted = true;
        if (!applySavedCloud()) {
            if (getConfig().canAutoConnectToCloud(getContext())) {
                requestConnection();
            } else {
                showInitConnectionTab();
            }
        }
    }

    private boolean applySavedCloud() {
        if (sSavedCloud == null) {
            return false;
        }
        mCloud = sSavedCloud;
        sSavedCloud = null;
        if (mCloud.isInProgress()) {
            mCloud.setListener(this);
            showWaitingConnectionTab();
        } else if (mCloud.isFinishedSuccessfully()) {
            mCloud.setListener(this);
            showConnectedTab();
        } else {
            mCloud = null;
            return false;
        }
        return true;
    }

    private void startCloud() {
        SettingsActivity activity = (SettingsActivity) getActivity();
        if (!activity.checkProOrBetter()) {
            return;
        }
        if (Utils.isNetworkConnection(getContext())) {
            requestConnection();
        } else {
            Utils.showErrorDialog(getChildFragmentManager(), R.string.error_no_internet);
        }
    }

    private void requestConnection() {
        if (mCloud == null) {
            mCloud = new Cloud(getActivity());
            mCloud.setListener(this);
        }
        mCloud.requestDriveConnection();
        showWaitingConnectionTab();
    }

    private void showInitConnectionTab() {
        setActivityLocked(false);
        showTab(TAB_INIT);
    }

    private void showConnectedTab() {
        setActivityLocked(false);
        initInfoTab();
        showTab(TAB_INFO);
    }

    private void showWaitingConnectionTab() {
        setActivityLocked(true);
        showTab(TAB_WAIT);
    }

    private void showTab(int index) {
        for (int i = 0; i < mTabs.length; ++i) {
            int visibility = (i == index) ? View.VISIBLE : View.GONE;
            mTabs[i].setVisibility(visibility);
        }
    }

    private void initInfoTab() {
        DateTime local = getCoach().getCloudInfo().getLastSyncDate();
        DateTime remote = mCloud.getInfo().getLastSyncDate();

        View syncTab = findViewById(R.id.tab_info_sync);
        View okTab = findViewById(R.id.tab_info_ok);
        if (getConfig().nothingToSync(local, remote)) {
            syncTab.setVisibility(View.GONE);
            okTab.setVisibility(View.VISIBLE);
        } else {
            syncTab.setVisibility(View.VISIBLE);
            okTab.setVisibility(View.GONE);
            initInfoTabSync(local, remote);
        }

        Button syncButton = (Button) findViewById(R.id.sync_cloud);
        syncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sync();
            }
        });
    }

    private void initInfoTabSync(DateTime local, DateTime remote) {
        initLabel(R.id.cloud_label_local, R.string.cloud_label_local, local);
        initLabel(R.id.cloud_label_cloud, R.string.cloud_label_cloud, remote);
        updateInfoButtons(local, remote);
    }

    private void updateInfoButtons(DateTime local, DateTime remote) {
        View info = mTabs[TAB_INFO];
        RadioButton bMerge = (RadioButton) info.findViewById(R.id.cloud_sync_merge);
        RadioButton bLocalToCloud = (RadioButton) info.findViewById(R.id.cloud_sync_local);
        RadioButton bCloudToLocal = (RadioButton) info.findViewById(R.id.cloud_sync_cloud);

        if (remote == null) {
            bMerge.setEnabled(false);
            bCloudToLocal.setEnabled(false);
            bLocalToCloud.setChecked(true);
        } else {
            bMerge.setEnabled(true);
            bCloudToLocal.setEnabled(true);
            if ((local != null) && local.equals(remote)) {
                bLocalToCloud.setChecked(true);
            } else {
                bMerge.setChecked(true);
            }
        }
    }

    private List<String> startSync() {
        showWaitingConnectionTab();
        return getCoach().getFilesToSync();
    }

    private void sync() {
        int option = mSyncChoice.getCheckedRadioButtonId();
        if (option == -1) {
            return;
        }
        List<String> files = startSync();
        switch (option) {
            case R.id.cloud_sync_cloud:
                mCloud.cloudToLocal(getCoach());
                break;
            case R.id.cloud_sync_local:
                mCloud.localToCloud(files, getCoach());
                break;
            case R.id.cloud_sync_merge:
                mCloud.merge(getCoach());
                break;
        }
    }

    private void initLabel(@IdRes int labelId, @StringRes int prefixId, DateTime date) {
        StringBuilder text = new StringBuilder();
        text.append("<b>");
        text.append(getString(prefixId));
        text.append("</b>");
        text.append(": <font color='");

        if (date != null) {
            String color = Utils.getHtmlColor(R.color.positive_color, getContext());
            text.append(color);
            text.append("'>");
            text.append(Utils.formatDateTime(date));
        } else {
            String color = Utils.getHtmlColor(R.color.accent, getContext());
            text.append(color);
            text.append("'>");
            text.append(getString(R.string.cloud_sync_never));
        }

        text.append("</font>");
        TextView textView = (TextView) findViewById(labelId);
        textView.setText(Utils.html(text.toString()));
    }

    private View findViewById(@IdRes int id) {
        return getView().findViewById(id);
    }

    @Override
    protected int getRootResource() {
        return R.layout.activity_settings_cloud;
    }

    @Override
    public void onOperationCompleted(boolean dataTransferred) {
        if (mCloud.isFinishedSuccessfully()) {
            if (dataTransferred) {
                getConfig().onCloudSync();
            }
            onSuccess();
        } else {
            onFail();
        }
    }

    private boolean isFragmentExist() {
        return getView() != null;
    }

    private void onSuccess() {
        autoEnableCloudConfig();
        if (isFragmentExist()) {
            showConnectedTab();
        }
    }

    private void autoEnableCloudConfig() {
        Config config = getConfig();
        config.setCloudEnabled(true);
        config.save(getContext());
    }

    private void onFail() {
        disconnectCloud();
        if (isFragmentExist()) {
            showInitConnectionTab();
        }
    }

    private Cloud mCloud;
    private View[] mTabs;
    private boolean mStarted;

    private RadioGroup mSyncChoice;

    private static Cloud sSavedCloud = null;
    public static final int TAB = 2;

    private static final int TAB_INIT = 0;
    private static final int TAB_WAIT = 1;
    private static final int TAB_INFO = 2;
}
