package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import androidx.core.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class NestedScrollViewFocusFixed extends NestedScrollView {
    public NestedScrollViewFocusFixed(Context context) {
        super(context);
    }

    public NestedScrollViewFocusFixed(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NestedScrollViewFocusFixed(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            computeScroll();
        }
        return super.onInterceptTouchEvent(ev);
    }
}
