package com.vaisoft.health.gymgain.workout;

import android.os.Handler;
import android.view.View;
import android.widget.ListView;

public class WorkoutTicker {

    public WorkoutTicker(ListView list, SetResultsAdapter adapter) {
        mResultsList = list;
        mResultsAdapter = adapter;
        mTickHandler = new Handler();
    }

    public void setActive(boolean active) {
        if (mActive != active) {
            mActive = active;
            if (active) {
                mTickHandler.post(new Ticker());
            }
        }
    }

    private class Ticker implements Runnable {
        public void run() {
            if (!mActive) {
                return;
            }
            int first = mResultsList.getFirstVisiblePosition();
            int last = mResultsList.getLastVisiblePosition();
            for (int i = first; i <= last; ++i) {
                View view = mResultsList.getChildAt(i);
                mResultsAdapter.tick(view);
            }
            mTickHandler.postDelayed(this, TICK_INTERVAL);
        }
    }

    private ListView mResultsList;
    private SetResultsAdapter mResultsAdapter;
    private Handler mTickHandler;
    private boolean mActive;

    public static long TICK_INTERVAL = 100;
}
