package com.vaisoft.health.gymgain.feed;

import android.content.Context;
import android.view.View;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.controls.FeedItem;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.main.FeedFragment;

public abstract class Feed implements Comparable<Feed> {
    public abstract void bind(ContextViewHolder viewHolder, Coach coach);
    public abstract void clicked();
    public abstract void deleted(Coach coach);
    public abstract int getViewType();
    protected abstract long getEffectiveTime();

    public Feed(Context context) {
        mContext = context;
    }

    protected Context getContext() {
        return mContext;
    }

    public static ViewHolder createViewHolder(int viewType, View view, FeedFragment.FeedAdapter adapter) {
        switch (viewType) {
            case BodyLogFeed.VIEW_TYPE:
                return new BodyLogFeed.ViewHolder(view, adapter);
            case RecordFeed.VIEW_TYPE:
                return new RecordFeed.ViewHolder(view, adapter);
            case WorkoutFeedActive.VIEW_TYPE:
                return new WorkoutFeedActive.ViewHolder(view, adapter);
            case WorkoutFeedFinished.VIEW_TYPE:
                return new WorkoutFeedFinished.ViewHolder(view, adapter);
            case HelperFeed.VIEW_TYPE:
                return new HelperFeed.ViewHolder(view, adapter);
        }
        return null;
    }

    @Override
    public int compareTo(Feed feed) {
        long diff = feed.getEffectiveTime() - getEffectiveTime();
        if (diff > 0) {
            return 1;
        }
        if (diff < 0) {
            return -1;
        }
        return 0;
    }

    public static class ViewHolder extends ContextViewHolder {

        public ViewHolder(View view, FeedFragment.FeedAdapter adapter) {
            super(view, adapter);
            mAdapter = adapter;
            mFeedItem = view.findViewById(R.id.data);
        }

        @Override
        protected void clicked() {
            int item = getAdapterPosition();
            if (item != -1) {
                Feed feed = mAdapter.getItem(item);
                feed.clicked();
            }
        }

        @Override
        protected boolean isDeletable() {
            return true;
        }

        @Override
        protected boolean isEditable() {
            return true;
        }

        public FeedItem getItem() {
            return mFeedItem;
        }

        public String getString(int id) {
            return view.getContext().getString(id);
        }

        private FeedFragment.FeedAdapter mAdapter;
        private FeedItem mFeedItem;
    }

    private Context mContext;
}
