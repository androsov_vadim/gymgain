package com.vaisoft.health.gymgain.coach;

import android.app.ActivityManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.BodyMeasure;
import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.data.Lifecycle;
import com.vaisoft.health.gymgain.data.Localizer;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.data.Source;
import com.vaisoft.health.gymgain.data.Training;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.feed.Feed;
import com.vaisoft.health.gymgain.feed.HelperFeed;
import com.vaisoft.health.gymgain.feed.RecordFeed;
import com.vaisoft.health.gymgain.feed.WorkoutFeedActive;
import com.vaisoft.health.gymgain.feed.WorkoutFeedFinished;
import com.vaisoft.health.gymgain.main.MainActivity;
import com.vaisoft.health.gymgain.schedule.Schedule;
import com.vaisoft.health.gymgain.schedule.ScheduleAnalyser;
import com.vaisoft.health.gymgain.schedule.WorkoutHealth;
import com.vaisoft.health.gymgain.sync.BackgroundCloudUpdater;
import com.vaisoft.health.gymgain.sync.Stamp;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Coach {

    private Coach() {
        mKnowledge = new Knowledge();
    }

    private void onCreate(Context context) {
        initTasksList();
        scheduleInitTask(context);
        scheduleUpdateTask(context);
        executeTasks(context);
    }

    public static Coach getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new Coach();
            sInstance.onCreate(context);
        }
        return sInstance;
    }

    public static void runBackgroundUpdate(Context context) {
        if (sInstance == null) {
            sInstance = new Coach();
        }
        sInstance.onUpdate(context);
    }

    private void onUpdate(Context context) {
        if (isTasksActive()) {
            return;
        }
        initTasksList();
        scheduleInitTask(context);
        scheduleUpdateTask(context);
        scheduleCloudSyncTask(context);
        executeTasks(context);
    }

    public void updateCloudApplyIncoming(Context context){
        if (isTasksActive()) {
            return;
        }
        initTasksList();
        scheduleCloudSyncTask(context);
        executeTasks(context);
    }

    public void updateCloud(final Context context, final BackgroundCloudUpdater.Listener listener) {
        if (isTasksActive()) {
            return;
        }
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                listener.onIncomingData();
            }
        };

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                if (isTimeToBackgroundCloudUpdate(context)) {
                    BackgroundCloudUpdater updater = new BackgroundCloudUpdater(context, Coach.this, false);
                    updater.update();
                    if (updater.hasPendingIncomingData()) {
                        handler.sendEmptyMessage(0);
                    }
                }
            }
        });
        thread.start();
    }

    private boolean isTimeToBackgroundCloudUpdate(Context context) {
        Config config = getConfig();
        if (config == null){
            return false;
        }
        return config.canBackgroundConnectToCloud(context);
    }

    private static synchronized void initTasksList() {
        sTasks = new ArrayList<>();
    }

    private static synchronized void resetTasksList() {
        sTasks.clear();
        sTasks = null;
    }

    private static synchronized boolean isTasksActive() {
        return (sTasks != null);
    }

    private void scheduleInitTask(final Context context) {
        if (mInitialized) {
            return;
        }
        sTasks.add(new Runnable() {
            @Override
            public void run() {
                init(context);
                onInitialized();
            }
        });
    }

    /*
    Если захочется делать апдейт чаще, не забыть проверить, а корректно
    ли работает алгоритм сгорания тренировок при частоте обновления чаще,
    чем раз в сутки. Обязательно сделать тест на устойчивость!
     */
    private void scheduleUpdateTask(final Context context) {
        sTasks.add(new Runnable() {
            @Override
            public void run() {
                update(context);
            }
        });
    }

    private void scheduleCloudSyncTask(final Context context) {
        sTasks.add(new Runnable() {
            @Override
            public void run() {
              if (isTimeToBackgroundCloudUpdate(context)) {
                    BackgroundCloudUpdater updater = new BackgroundCloudUpdater(context,Coach.this, true);
                    updater.update();
                }
            }
        });
    }

    private void executeTasks(Context context) {
        if (sTasks.isEmpty()) {
            resetTasksList();
            return;
        }
        if (isMainThread()) {
            executeAllTasksInOtherThread(context);
        } else {
            executeAllTasksInSameThread(context);
        }
    }

    private void executeAllTasksInSameThread(Context context) {
        executeAllTAsks();
        onTasksFinished(context);
    }

    private void executeAllTasksInOtherThread(Context context) {
        final Handler handler = new TasksFinishHandler(context);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                executeAllTAsks();
                handler.sendEmptyMessage(0);
            }
        });
        thread.start();
    }

    private void executeAllTAsks() {
        for (Runnable task: sTasks) {
            task.run();
        }
        resetTasksList();
    }

    private boolean isMainThread() {
        return  Looper.myLooper() == Looper.getMainLooper();
    }

    private void onTasksFinished(Context context) {
        if (mSaveRecordsList) {
            saveRecords(context);
            mSaveRecordsList = false;
        }
    }

    private void update(Context context) {
        if (isAppInBackground(context)) {
            autoFinishActiveWorkouts(context);
        }
        updateWorkouts(context);
    }

    private void updateWorkouts(Context context) {
        List<Record> recordsWithBurnouts = null;
        List<Record> criticalRecords = new ArrayList<>();
        int burnoutsCount = 0;
        List<Record> records = getRecords();
        for (Record record : records) {
            WorkoutHealth health = getHealth(record, Utils.now());
            if (health.showWarning()) {
                criticalRecords.add(record);
            }
            List<DateTime> recordBurnouts = getBurnouts(record);
            if ((recordBurnouts != null) && (!recordBurnouts.isEmpty())) {
                for (DateTime burned: recordBurnouts) {
                    burnOut(context, record, burned);
                }
                if (recordsWithBurnouts == null) {
                    recordsWithBurnouts = new ArrayList<>();
                }
                burnoutsCount += recordBurnouts.size();
                recordsWithBurnouts.add(record);
            }
        }
        if (burnoutsCount > 0) {
            notifyBurnouts(context, recordsWithBurnouts, burnoutsCount);
            onWorkoutsFinished();
            mKnowledge.onFinishedWorkoutsChanged(context);
        }
        if (isAppInBackground(context) && (!criticalRecords.isEmpty())) {
            notifyCritical(context, criticalRecords);
        }
    }

    private void burnOut(Context context, Record record, DateTime date) {
        Workout burnedWorkout = Workout.burnOut(record.getSource(),
                date, context.getString(R.string.burnout));
        addWorkout(burnedWorkout);
    }

    private void notifyCritical(Context context, List<Record> criticalRecords) {
        int criticalsCount = criticalRecords.size();
        StringBuilder title = new StringBuilder();
        title.append(context.getResources().getQuantityString(R.plurals.critical_notify_title, criticalsCount));
        if (criticalsCount > 1) {
            title.append(" (");
            title.append(criticalsCount);
            title.append(')');
        }

        StringBuilder text = new StringBuilder();
        boolean coma = false;
        for (Record record: criticalRecords) {
            if (coma) {
                text.append(", ");
            } else {
                coma = true;
            }
            text.append(record.getName(this));
        }

        showNotification(context, title.toString(), text.toString(),
                R.drawable.ic_whatshot_white_24dp, R.color.notify_hot);
    }

    private void notifyBurnouts(Context context, List<Record> recordsWithBurnouts, int burnoutsCount) {
        StringBuilder title = new StringBuilder();
        title.append(context.getResources().getQuantityString(R.plurals.burnout_notify_title, burnoutsCount));
        if (burnoutsCount > 1) {
            title.append(" (");
            title.append(burnoutsCount);
            title.append(')');
        }

        StringBuilder text = new StringBuilder();
        boolean coma = false;
        for (Record record: recordsWithBurnouts) {
            if (coma) {
                text.append(", ");
            } else {
                coma = true;
            }
            text.append(record.getName(this));
        }

        showNotification(context, title.toString(), text.toString(),
                R.drawable.ic_whatshot_white_24dp, R.color.notify_fail);
    }

    private void addWorkout(Workout workout) {
        List<Workout> workouts = getFinishedWorkoutsByKey(workout.getSource().getKey());
        workouts.add(0, workout);
    }

    private List<DateTime> getBurnouts(Record record) {
        DateTime analyseDate = Utils.now().minusDays(1);
        if (!record.shouldAnalyseOnBurnouts(analyseDate)) {
            return null;
        }
        if (isWorkoutsFor(record, analyseDate)) {
            onRecordAnalysed(record, analyseDate);
            return null;
        }
        ScheduleAnalyser analyser = record.createScheduleAnalyser(analyseDate);
        if (analyser == null) {
            onRecordAnalysed(record, analyseDate);
            return null;
        }
        List<DateTime> workouts = getWorkoutsDeadlineDates(record, analyser.getHistoryStart(), analyseDate, false);
        List<DateTime> result = analyser.analyseBurnout(workouts);
        onRecordAnalysed(record, analyseDate);
        return result;
    }

    private void onRecordAnalysed(Record record, DateTime analyseDate) {
        record.onBurnoutsAnalysed(analyseDate);
        mSaveRecordsList = true;
    }

    private boolean isWorkoutsFor(Record record, DateTime analyseDate) {
        List<Workout> finishedWorkouts = getFinishedWorkoutsByKey(record.getSource().getKey());
        if ((finishedWorkouts != null) && (!finishedWorkouts.isEmpty())) {
            Workout workout = finishedWorkouts.get(0);
            if (isWorkoutsFor(workout, record, analyseDate)) {
                return true;
            }
        }
        for (Workout workout: mKnowledge.getActiveWorkouts()) {
            if (isWorkoutsFor(workout, record, analyseDate)) {
                return true;
            }
        }
        return false;
    }

    private boolean isWorkoutsFor(Workout workout, Record record, DateTime analyseDate) {
        if (!workout.getSource().equals(record.getSource())) {
            return false;
        }
        DateTime workoutDate = workout.getStartedDate();
        return Utils.isAfterOrEqualDay(workoutDate, analyseDate);
    }

    public void updateFeedFromGui(Context context) {
        autoFinishActiveWorkouts(context);
    }

    private boolean isAppInBackground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (appProcesses == null) {
            return true;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND &&
                    appProcess.processName.equals(packageName)) {
                return false;
            }
        }
        return true;
    }

    public void autoFinishActiveWorkouts(Context context) {
        int finished = mKnowledge.autoFinishActiveWorkouts(context);
        if (finished > 0) {
            notifyAutofinished(context, finished);
        }
    }

    private void notifyAutofinished(Context context, int count) {
        StringBuilder title = new StringBuilder();
        title.append(context.getString(R.string.autofinish_1));
        title.append(' ');
        title.append(count);
        title.append(' ');
        title.append(context.getResources().getQuantityString(R.plurals.autofinish_2, count));
        String text = context.getString(R.string.autofinish_3);
        showNotification(context, title.toString(), text,
                R.drawable.ic_done_all_white_24dp, R.color.notify_good);
    }

    private void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            NotificationChannel channel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    context.getString(R.string.notify_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(context.getString(R.string.notify_channel_description));
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void showNotification(Context context, String title, String text, int icon, int color) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_ID)
                        .setSmallIcon(icon)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setColor(ContextCompat.getColor(context, color));
        Intent resultIntent = new Intent(context, MainActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(resultPendingIntent);
        builder.setAutoCancel(true);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (mNotificationManager != null) {
            mNotificationManager.notify(0, builder.build());
        }
    }

    public synchronized boolean isReady() {
        return mInitialized && (sTasks == null);
    }

    private synchronized void onInitialized() {
        mInitialized = true;
    }

    private void init(Context context) {
        initLocalization(context);
        initChannels(context);
        mKnowledge.init(context);

        mUpdateExerciseList = false;
        mUpdateTrainingList = false;
        mUpdateRecordsList = false;
    }

    private void initLocalization(Context context) {
        try {
            JSONObject localizationData = new JSONObject(context.getString(R.string.data));
            mLocalizer = new Localizer(localizationData);
            if (!mLocalizer.isOk()) {
                mLocalizer = null;
            }
            Locale locale = new Locale(context.getString(R.string.locale));
            mCollator = Collator.getInstance(locale);
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    public boolean shouldUpdateExerciseList() {
        boolean result = mUpdateExerciseList;
        mUpdateExerciseList = false;
        return  result;
    }

    public boolean shouldUpdateTrainingList() {
        boolean result = mUpdateTrainingList;
        mUpdateTrainingList = false;
        return  result;
    }

    public Exercise getExercise(String id) {
        return mKnowledge.getExercise(id);
    }

    public Exercise getExerciseByKey(String key) {
        return mKnowledge.getExerciseByKey(key);
    }

    public Training getTraining(String id) {
        return mKnowledge.getTraining(id);
    }

    public Training getTrainingByKey(String key) {
        return mKnowledge.getTrainingByKey(key);
    }

    public boolean isWorkoutSource(String key) {
        return mKnowledge.isWorkoutSource(key);
    }

    public String getName(Exercise exercise) {
        String id = exercise.getId();
        return getName(id);
    }

    public String getName(Training training) {
        String id = training.getId();
        return getName(id);
    }

    public String getName(String id) {
        return mLocalizer.translate(id);
    }

    public String getAffectedString(Exercise exercise) {
        return exercise.getAffectedFullString(mKnowledge.getBody(), mLocalizer);
    }

    public String getAffectedString(List<String> parts) {
        return Utils.convertListToString(parts, mLocalizer);
    }

    public List<String> getAffectedByMusclesParts(List<String> muscles) {
        return mKnowledge.getAffectedByMusclesParts(muscles);
    }

    public List<String> getAffectedParts(Exercise exercise) {
        return getAffectedByMusclesParts(exercise.getMuscles());
    }

    public List<String> getAllParts() {
        return mKnowledge.getAllParts();
    }

    public List<String> getAffectedByExercisesParts(List<String> exercises) {
        return mKnowledge.getAffectedByExercisesParts(exercises);
    }

    public List<String> getAllMuscles() {
        return mKnowledge.getAllMuscles();
    }

    public List<String> getMusclesForPart(String bodyPart) {
        return mKnowledge.getMusclesForPart(bodyPart);
    }

    public List<String> getMusclesForExercises(List<String> exercises) {
        return mKnowledge.getMusclesForExercises(exercises);
    }

    public void saveExercise(Context context, Exercise exercise, String previousId) {
        mKnowledge.saveExercise(context, exercise, previousId);
        mUpdateExerciseList = true;
    }

    public void deleteExercise(Context context, String exerciseId) {
        mKnowledge.deleteExercise(context, exerciseId);
        mUpdateExerciseList = true;
        mUpdateRecordsList = true;
    }

    public void saveTraining(Context context, Training training, String previousId) {
        mKnowledge.saveTraining(context, training, previousId);
        invalidateAllLists(); //some exercises can be locked by Training object updated
    }

    public void deleteTraining(Context context, String trainingId) {
        mKnowledge.deleteTraining(context, trainingId);
        invalidateAllLists();//some exercises can be unlocked after Training object deleted
    }

    public void invalidateAllLists() {
        mUpdateTrainingList = true;
        mUpdateExerciseList = true;
        mUpdateRecordsList = true;
    }

    public List<Exercise> getFilteredExercises(String filter) {
        List<Exercise> exercises = mKnowledge.getExercises();
        exercises = getFilteredExercises(exercises, filter);
        Collections.sort(exercises, new Comparator<Exercise>() {
            @Override
            public int compare(Exercise o1, Exercise o2) {
                String name1 = getName(o1);
                String name2 = getName(o2);
                return mCollator.compare(name1, name2);
            }
        });
        return exercises;
    }

    public List<Exercise> getQuickFilteredExercises(List<Exercise> exercises, String filter) {
        if ((filter == null) || filter.isEmpty()) {
            return exercises;
        }

        String filterLowerCase = filter.toLowerCase();
        ArrayList<Exercise> filteredExercises = new ArrayList<>();
        for (Exercise ex: exercises) {
            if (checkFilter(getName(ex), filterLowerCase)) {
                filteredExercises.add(ex);
            }
        }

        return filteredExercises;
    }

    public List<Exercise> getFilteredExercises(List<Exercise> exercises, String filter)
    {
        ArrayList<Exercise> filteredExercises = new ArrayList<>();
        if ((filter == null) || filter.isEmpty()) {
            filteredExercises.addAll(exercises);
            return filteredExercises;
        }

        String filterLowerCase = filter.toLowerCase();
        for (Exercise ex: exercises) {
            if (checkFilter(ex, filterLowerCase)) {
                filteredExercises.add(ex);
            }
        }

        return filteredExercises;
    }

    public String getDefaultTrainingId(String prefix) {
        for (int i = 1; i < MAX_INDEX; i++) {
            String id = getPrefixed(prefix, i);
            if (!isTraining(id)) {
                return id;
            }
        }
        Log.e(getClass().getName(), "Can not generate training default name");
        return prefix;
    }

    public String getDefaultExerciseId(String prefix) {
        for (int i = 1; i < MAX_INDEX; i++) {
            String id = getPrefixed(prefix, i);
            if (!isExercise(id)) {
                return id;
            }
        }
        Log.e(getClass().getName(), "Can not generate exercise default name");
        return prefix;
    }

    private String getPrefixed(String prefix, int i) {
        return prefix + " " + i;
    }

    public List<Training> getFilteredTrainings(String filter) {
        List<Training> trainings = mKnowledge.getTrainings();
        List<Training> filteredTrainings = new ArrayList<>();
        if ((filter == null) || filter.isEmpty()) {
            filteredTrainings.addAll(trainings);
        } else {
            String filterLowerCase = filter.toLowerCase();
            for (Training tr : trainings) {
                if (checkFilter(tr, filterLowerCase)) {
                    filteredTrainings.add(tr);
                }
            }
        }

        Collections.sort(filteredTrainings, new Comparator<Training>() {
            @Override
            public int compare(Training o1, Training o2) {
                String name1 = getName(o1);
                String name2 = getName(o2);
                return mCollator.compare(name1, name2);
            }
        });

        return filteredTrainings;
    }

    private boolean isTraining(String id) {
        List<Training> trainings = mKnowledge.getTrainings();
        for (Training tr: trainings) {
            if (tr.getId().equals(id) || getName(tr.getId()).equals(id)) {
                return true;
            }
        }
        return false;
    }

    private boolean isExercise(String id) {
        List<Exercise> exercises = mKnowledge.getExercises();
        for (Exercise exercise: exercises) {
            if (exercise.getId().equals(id) || getName(exercise.getId()).equals(id)) {
                return  true;
            }
        }
        return false;
    }

    private boolean checkFilter(List<String> data, String filterLowerCase) {
        if ((filterLowerCase == null) || filterLowerCase.isEmpty()) {
            return true;
        }
        for (String item: data) {
            if (checkFilter(item, filterLowerCase)) {
                return true;
            }
        }
        return false;
    }

    private boolean checkFilter(String data, String filterLowerCase) {
        if ((filterLowerCase == null) || filterLowerCase.isEmpty()) {
            return true;
        }
        String dataLowerCase = data.toLowerCase();
        return dataLowerCase.contains(filterLowerCase);
    }

    private boolean checkFilter(Exercise data, String filterLowerCase) {
        if ((filterLowerCase == null) || filterLowerCase.isEmpty()) {
            return true;
        }
        if (checkFilter(getName(data), filterLowerCase)) {
            return true;
        }
        List<String> muscles = mLocalizer.translate(data.getMuscles());
        if (checkFilter(muscles, filterLowerCase)) {
            return true;
        }
        List<String> parts = mLocalizer.translate((getAffectedParts(data)));
        return checkFilter(parts, filterLowerCase);
    }

    private boolean checkFilter(Training data, String filterLowerCase) {
        if ((filterLowerCase == null) || filterLowerCase.isEmpty()) {
            return true;
        }
        if (checkFilter(mLocalizer.translate(data.getId()), filterLowerCase)) {
            return true;
        }
        List<String> muscles = mKnowledge.getAffectedByExercisesMuscles(data.getExercises());
        List<String> musclesLocalized = mLocalizer.translate(muscles);
        if (checkFilter(musclesLocalized, filterLowerCase)) {
            return true;
        }
        List<String> parts = getAffectedByMusclesParts(muscles);
        List<String> partsLocalized = mLocalizer.translate(parts);
        return checkFilter(partsLocalized, filterLowerCase);
    }

    private boolean checkFilter(Workout data, String filterLowerCase) {
        if ((filterLowerCase == null) || filterLowerCase.isEmpty()) {
            return true;
        }
        Training training = mKnowledge.getTrainingByKey(data.getSource().getKey());
        if (training != null) {
            return checkFilter(training, filterLowerCase);
        }
        Exercise exercise = mKnowledge.getExerciseByKey(data.getSource().getKey());
        if (exercise != null) {
            return checkFilter(exercise, filterLowerCase);
        }
        return false;
    }

    public String getLocalizedNameByKey(String key) {
        String id = Source.getId(key);
        return getLocalized(id);
    }

    public String getLocalizedName(Context context, Workout workout) {
        String name = getLocalizedNameByKey(workout.getSource().getKey());
        if (name != null) {
            return name;
        }
        Workout.Entry firstEntry = workout.getEntry(0);
        if ((firstEntry == null) || firstEntry.log.isEmpty()) {
            return context.getString(R.string.undefined);
        }
        return getLocalized(firstEntry.log.get(0).exercise) + " ...";
    }

    private boolean checkFilter(Context context, int pattern, String filterLowerCase) {
        if ((filterLowerCase == null) || filterLowerCase.isEmpty()) {
            return true;
        }
        String patternText = context.getString(pattern);
        return checkFilter(patternText, filterLowerCase);
    }

    public List<String> getExercisesForBodyPart(Training training, String id) {
        return mKnowledge.getExercisesForBodyPart(training, id);
    }

    public void saveRecords(Context context) {
        mKnowledge.saveRecords(context);
        mKnowledge.setDataModified(context, true);
        mUpdateRecordsList = true;
    }

    public void addBodyLog(Context context, BodyMeasure.LogEntry entry) {
        mKnowledge.addBodyLog(context, entry);
        mUpdateRecordsList = true;
    }

    public void renameBodyLogItem(Context context, String source, String target) {
        mKnowledge.renameBodyLogItem(context, source, target);
        mUpdateRecordsList = true;
    }

    public void updateBodyLog(Context context, List<BodyMeasure.LogItem> items, BodyMeasure.LogEntry entry) {
        mKnowledge.updateBodyLog(context, items, entry);
        mUpdateRecordsList = true;
    }

    public void deleteBodyLog(Context context, BodyMeasure.LogEntry entry) {
        mKnowledge.deleteBodyLog(context, entry);
        mUpdateRecordsList = true;
    }

    public boolean shouldUpdateRecordsList() {
        boolean result = mUpdateRecordsList;
        mUpdateRecordsList = false;
        return  result;
    }

    public WorkoutHealth getHealth(Record record, DateTime date) {
        ScheduleAnalyser analyser = record.createScheduleAnalyser(date);

        DateTime now = Utils.now();
        DateTime start = new DateTime(now.getYear(), now.getMonthOfYear(), now.getDayOfMonth(), 0, 0);
        DateTime finish = start.plusDays(1);
        List<DateTime> todayWorkouts = getWorkoutsDeadlineDates(record, start, finish, true);
        if (todayWorkouts.isEmpty()) {
            if (analyser == null) {
                return WorkoutHealth.Notify;
            }
        } else {
            return WorkoutHealth.Healthy;
        }

        List<DateTime> workouts = getWorkoutsDeadlineDates(record,
                analyser.getHistoryStart(), date, true);
        return analyser.analyseHealth(workouts);
    }

    private List<DateTime> getWorkoutsDeadlineDates(Record record, DateTime start, DateTime finish, boolean mustHaveResults) {
        List<DateTime> workouts = new ArrayList<>();
        List<Workout> finishedWorkouts = getFinishedWorkoutsByKey(record.getSource().getKey());
        List<DateTime> targetFinished = addDatesToAnalyse(record, finishedWorkouts, start, finish, mustHaveResults);
        List<Workout> activeWorkouts = mKnowledge.getActiveWorkouts();
        List<DateTime> targetActive = addDatesToAnalyse(record, activeWorkouts, start, finish, false);
        workouts.addAll(targetFinished);
        workouts.addAll(targetActive);
        return workouts;
    }

    private List<DateTime> addDatesToAnalyse(Record record, List<Workout> source,
                                             DateTime start, DateTime finish, boolean mustHaveResults) {
        List<DateTime> target = new ArrayList<>();
        for (Workout workout : source) {
            if (!workout.getSource().equals(record.getSource())) {
                continue;
            }
            if (mustHaveResults && (!workout.isResults())) {
                continue;
            }
            DateTime analyseDate = workout.getEffectiveDate();
            if (analyseDate.isAfter(finish)) {
                continue;
            }
            if (analyseDate.isBefore(start)) {
                break;
            }
            target.add(analyseDate);
        }
        return target;
    }

    public List<Feed> getFilteredFeed(Context context, String filter) {
        List<Feed> result = new ArrayList<>();
        String filterLowerCase = null;
        if (filter != null) {
            filterLowerCase = filter.toLowerCase();
        }

        loadScheduleFeed(context, filterLowerCase, result);
        loadBodyLogFeed(context, filterLowerCase, result);
        loadActiveWorkoutsFeed(context, filterLowerCase, result);
        loadFinishedWorkoutsFeed(context, filterLowerCase, result);

        if (result.isEmpty()) {
            HelperFeed.loadTo(context, result);
        }

        Collections.sort(result);
        return result;
    }

    private void loadActiveWorkoutsFeed(Context context, String filterLowerCase, List<Feed> result) {
        List<Workout> activeWorkouts = mKnowledge.getActiveWorkouts();
        for (Workout workout: activeWorkouts) {
            if (checkFilter(workout, filterLowerCase)) {
                result.add(new WorkoutFeedActive(context, workout, this));
            }
        }
    }

    private void loadFinishedWorkoutsFeed(Context context, String filterLowerCase, List<Feed> result) {
        Map<String, List<Workout>> finishedWorkouts = mKnowledge.getFinishedWorkouts();
        for (Map.Entry<String, List<Workout>> entry : finishedWorkouts.entrySet()) {
            for (Workout workout: entry.getValue()) {
                if (workout.isDeleted()) {
                    continue;
                }
                if (checkFilter(workout, filterLowerCase)) {
                    result.add(new WorkoutFeedFinished(context, workout, this));
                }
            }
        }
    }

    private void loadBodyLogFeed(Context context, String filterLowerCase, List<Feed> result) {
        if (checkFilter(context, R.string.filter_feed_body, filterLowerCase)) {
            result.addAll(mKnowledge.getBodyMeasurer().getFeed(context));
        }
    }

    private void loadScheduleFeed(Context context, String filterLowerCase, List<Feed> result) {
        if (checkFilter(context, R.string.filter_feed_record, filterLowerCase)) {
            DateTime today = Utils.now();
            List<Record> records = getRecords();
            for (Record record : records) {
                WorkoutHealth health = getHealth(record, today);
                if (health.showFeed()) {
                    RecordFeed feed = new RecordFeed(context, record);
                    result.add(feed);
                }
            }
        }
    }

    public List<Exercise> getUsedExercises() {
        List<Exercise> exercises = mKnowledge.getExercises();
        ArrayList<Exercise> filteredExercises = new ArrayList<>();
        for (Exercise ex: exercises) {
            if (isExerciseInUse(ex.getId())) {
                filteredExercises.add(ex);
            }
        }

        return filteredExercises;
    }

    public boolean isExerciseInUse(String exerciseId) {
        return mKnowledge.isExerciseInUse(exerciseId);
    }

    public Record getRecordByKey(String key) {
        return mKnowledge.getRecordByKey(key);
    }

    public List<Record>  getRecords() {
        return mKnowledge.getRecords();
    }

    public Record createRecord(Exercise exercise) {
        return mKnowledge.createRecord(exercise);
    }

    public Record createRecord(Training training) {
        return mKnowledge.createRecord(training);
    }

    public Record createExerciseRecord(String id) {
        return mKnowledge.createExerciseRecord(id);
    }

    public Record createTrainingRecord(String id) {
        return mKnowledge.createTrainingRecord(id);
    }

    public Record getRecordForExercise(String id) {
        return mKnowledge.getRecordForExercise(id);
    }

    public Record getRecordForTraining(String id) {
        return mKnowledge.getRecordForTraining(id);
    }

    public void deleteRecord(Context context, Record record) {
        updateListsOnRecordChange(record);
        mKnowledge.deleteRecord(context, record);
    }

    private void updateListsOnRecordChange(Record record) {
        if (record.isExercise()) {
            mUpdateExerciseList = true;
        }
        if (record.isTraining()) {
            mUpdateTrainingList = true;
        }
    }

    public int start(Context context, Workout workout) {
        mKnowledge.start(context, workout);
        mUpdateRecordsList = true;
        mUpdateExerciseList = true; //some exercises can be locked by Workout object updated
        return 0;
    }

    public void finish(Context context, int workoutIndex, Workout workout) {
        mKnowledge.finish(context, workoutIndex, workout);
        onWorkoutsFinished();
    }

    private void onWorkoutsFinished() {
        mUpdateRecordsList = true;
    }

    public void onActiveWorkoutUpdated(Context context, int index, Workout workout) {
        mKnowledge.onActiveWorkoutUpdated(context, index, workout);
        mUpdateRecordsList = true;
        mUpdateExerciseList = true; //some exercises can be locked by Workout object updated
    }

    public void onFinishedWorkoutUpdated(Context context, int index, Workout workout) {
        mKnowledge.onFinishedWorkoutUpdated(context, index, workout);
        mUpdateRecordsList = true;
        mUpdateExerciseList = true; //some exercises can be locked by Workout object updated
    }

    public void deleteActiveWorkout(Context context, int sourceIndex) {
        Workout workout = mKnowledge.getActiveWorkout(sourceIndex);
        deleteActiveWorkout(context, workout);
    }

    public void deleteActiveWorkout(Context context, Workout workout) {
        mKnowledge.deleteActiveWorkout(context, workout);
        mUpdateRecordsList = true;
        mUpdateExerciseList = true; //some exercises can be unlocked by Workout object deleted
    }

    public void deleteFinishedWorkout(Context context, Workout workout) {
        mKnowledge.deleteFinishedWorkout(context, workout);
        mUpdateRecordsList = true;
        mUpdateExerciseList = true; //some exercises can be unlocked by Workout object deleted
    }

    public Workout getActiveWorkout(int workoutId) {
        return mKnowledge.getActiveWorkout(workoutId);
    }

    public Workout getFinishedWorkout(String key, int index) {
        List<Workout> target = getFinishedWorkoutsByKey(key);
        return Lifecycle.get(target, index);
    }

    public List<Workout> getFinishedWorkoutsByKey(String key) {
        return mKnowledge.getFinishedWorkoutsByKey(key);
    }

    public Workout getLastFinishedWorkout(String key) {
        List<Workout> workouts = getFinishedWorkoutsByKey(key);
        return Knowledge.getPreviousWorkout(workouts, -1);
    }

    public Workout getPreviousWorkout(Workout workout) {
        List<Workout> target = getFinishedWorkoutsByKey(workout.getSource().getKey());
        int index = workout.indexIn(target);
        if (index == -1) {
            return null;
        }
        return Knowledge.getPreviousWorkout(target, index);
    }

    public int getActiveWorkoutId(Workout workout) {
        return mKnowledge.getActiveWorkoutId(workout);
    }

    public int getFinishedWorkoutIndex(Workout workout) {
        List<Workout> target = getFinishedWorkoutsByKey(workout.getSource().getKey());
        return Lifecycle.indexOf(target, workout);
    }

    public List<Workout> getFinishedExistingWorkouts(String key) {
        List<Workout> workouts = getFinishedWorkoutsByKey(key);
        List<Workout> result = new ArrayList<>();
        for (Workout workout: workouts) {
            if (!workout.isDeleted()) {
                result.add(workout);
            }
        }
        return result;
    }

    public ArrayList<String> getFilteredMuscles(Exercise excludeFrom) {
        return mKnowledge.getFilteredMuscles(excludeFrom);
    }

    public ArrayList<String> getMusclePartPairsLocalizedStrings(List<String> muscles) {
        ArrayList<String> result = new ArrayList<>();
        for (String muscle: muscles) {
            String item = mLocalizer.translate(muscle) + " (" +
                    mLocalizer.translate(mKnowledge.getBodyPartForMuscle(muscle)) +
                    ")";
            result.add(item);
        }
        return  result;
    }

    public ArrayList<String> getLocalized(List<String> ids) {
        ArrayList<String> result = new ArrayList<>();
        for (String id: ids) {
            result.add(mLocalizer.translate(id));
        }
        return  result;
    }

    public void prepareWorkout(Workout workout) {
        mPreparedWorkout = workout;
    }

    public void resetPreparedWorkout() {
        mPreparedWorkout = null;
    }

    public Workout getPreparedWorkout() {
        return mPreparedWorkout;
    }

    public String getLocalized(String id) {
        return mLocalizer.translate(id);
    }

    public String getLocalized(Unit unit) {
        return mLocalizer.translate(unit.toString());
    }

    public boolean isExerciseIdFree(String id) {
        List<Exercise> exercises = mKnowledge.getExercises();
        for (Exercise exercise: exercises) {
            if (exercise.getId().equals(id)) {
                return  false;
            }
            if (getName(exercise).equals(id)) {
                return  false;
            }
        }
        return true;
    }

    public boolean isTrainingIdFree(String id) {
        List<Training> trainings = mKnowledge.getTrainings();
        for (Training training: trainings) {
            if (training.getId().equals(id) || getLocalized(training.getId()).equals(id)) {
                return  false;
            }
        }
        return true;
    }

    public void saveConfig(Context context) {
        Config config = getConfig();
        if (config.hasCloudSensitiveChanges()) {
            config.setDataModifiedAfterCloudSync(true);
        }
        mKnowledge.saveConfig(context);
    }

    public Config getConfig() {
        return mKnowledge.getConfig();
    }

    public Localizer getLocalizer() {
        return mLocalizer;
    }

    public BodyMeasure getBodyMeasurer() {
        return mKnowledge.getBodyMeasurer();
    }

    public void setEditTarget(Object object) {
        mEditTarget = object;
    }

    public Object getEditTarget() {
        return mEditTarget;
    }

    public  <T> T getEditTarget(Class<T> targetClass) {
        Object target = getEditTarget();
        if ((target != null) && (targetClass.isInstance(target))) {
            return (T)target;
        }
        return null;
    }

    public void resetEditTarget() {
        setEditTarget(null);
    }

    public void searchInfo(String id, FragmentActivity activity) {
        Intent search = new Intent(Intent.ACTION_WEB_SEARCH);
        String localizedId = getLocalized(id);
        String infoHelper = activity.getString(R.string.info_search_helper);
        search.putExtra(SearchManager.QUERY, localizedId + " " + infoHelper);
        try {
            activity.startActivity(search);
        } catch (Exception ex) {
            Utils.showErrorDialog(activity.getSupportFragmentManager(), R.string.error_no_google);
        }
    }

    public Stamp getCloudInfo() {
        return mKnowledge.getCloudInfo();
    }

    public List<String> getFilesToSync() {
        return mKnowledge.getFilesToSync();
    }

    public boolean apply(Context context, Knowledge knowledge, Stamp stamp, boolean merge) {
        boolean changed = false;
        knowledge.enableCloud();
        if (merge) {
            changed = mKnowledge.merge(knowledge);
        } else {
            mKnowledge = knowledge;
            changed = true;
        }
        mKnowledge.save(context);
        updateStamp(context, stamp);
        invalidateAllLists();
        return changed;
    }

    public void updateStamp(Context context, Stamp stamp) {
        mKnowledge.onCloudSynchronized(context, stamp);
    }

    public void enableCloudAndSave(Context context) {
        mKnowledge.enableCloudAndSave(context);
    }

    public void setRecordSchedule(Context context, @NonNull Record record, @NonNull Schedule schedule) {
        record.setSchedule(schedule);
        saveRecords(context);
    }

    private class TasksFinishHandler extends Handler {
        public TasksFinishHandler(Context context) {
            mContext = context;
        }

        @Override
        public void handleMessage(Message msg) {
            onTasksFinished(mContext);
        }

        private final Context mContext;
    };
//-----------------------------------------------------------------------------------------------------

    private Knowledge mKnowledge;

    private Workout mPreparedWorkout;
    private boolean mInitialized;
    private Object mEditTarget;
    private boolean mUpdateExerciseList;
    private boolean mUpdateTrainingList;
    private boolean mUpdateRecordsList;
    private boolean mSaveRecordsList;
    private Localizer mLocalizer;
    private Collator mCollator;

    private static List<Runnable> sTasks;

    private static String NOTIFICATION_CHANNEL_ID = "default";

    private static final int MAX_INDEX = 1000;
    private static Coach sInstance;
}
