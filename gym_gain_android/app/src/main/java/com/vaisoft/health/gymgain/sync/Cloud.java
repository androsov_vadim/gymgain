package com.vaisoft.health.gymgain.sync;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.data.Knowledge;
import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Cloud implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public Cloud(Activity activity) {
        d("Foreground instance created");
        mActivity = activity;
        init(activity);
    }

    public Cloud(Context context) {
        d("Background instance created");
        mActivity = null;
        init(context);
    }

    private void init(Context context) {
        mContext = context;
        mStatus = SyncStatus.Undefined;
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_APPFOLDER)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    private boolean isForeground() {
        return mActivity != null;
    }

    public void setListener(SyncFinishListener listener) {
        mListener = listener;
    }

    public void requestDriveConnection() {
        d("Connection request");
        mDataExchangeStarted = false;
        mGoogleApiClient.connect();
    }

    public void updateInfo() {
        mStatus = SyncStatus.Progress;
        d("Updating...");
        Drive.DriveApi.requestSync(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                if (!status.isSuccess()) {
                    Log.e(getClass().getName(), "Cloud sync error: " + status.getStatusMessage());
                    Cloud.this.onResult(false);
                    return;
                }
                cloudReady();
            }
        });
    }

    public void reconnectAfterRecovery() {
        d("Reconnecting after recovery");
        mInitialization = true;
        mGoogleApiClient.connect();
    }

    public void disconnect() {
        d("Disconnect");
        mGoogleApiClient.disconnect();
    }

    public void close() {
        d("Close connection");
        mListener = null;
        mGoogleApiClient.unregisterConnectionCallbacks(this);
        mGoogleApiClient.disconnect();
        mGoogleApiClient = null;
    }

    public Stamp getInfo() {
        if (mInfo != null) {
            return mInfo;
        }
        Stamp info = new Stamp();
        return info;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (mInitialization) {
            mInitialization = false;
            updateInfo();
        } else {
            cloudReady();
        }
    }

    private void cloudReady() {
        d("Reading stamp...");
        mStatus = SyncStatus.Progress;
        new FileSyncRead(mGoogleApiClient, STAMP_FILE, new FileSyncRead.Listener() {
            @Override
            public void read(InputStream is) {
                d("Stamp read: data available");
                boolean ok = readRemoteStamp(is);
                onResult(ok);
            }

            @Override
            public void noFile() {
                d("Stamp read: file does not exist");
                onResult(true);
            }

            @Override
            public void fail() {
                d("Stamp read: fail");
                onResult(false);
            }
        });
    }

    private boolean readRemoteStamp(InputStream is) {
        try {
            String json = Utils.readString(is);
            JSONObject data = new JSONObject(json);
            mInfo = new Stamp(data);
        } catch (Exception e) {
            Log.e(getClass().getName(), "Cloud drive stamp read error: " + e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (isForeground() && connectionResult.hasResolution()) {
            try {
                d("Recovering...");
                connectionResult.startResolutionForResult(mActivity, REQUEST_CODE_RESOLVE_ERR);
            } catch (IntentSender.SendIntentException e) {
                d(e.getMessage());
                onResult(false);
            }
        } else {
            d("Connection failed");
            onResult(false);
        }
    }

    private void onResult(boolean success) {
        mStatus = success ? SyncStatus.Success : SyncStatus.Fail;
        if (mListener != null) {
            boolean dataTransferred = success && mDataExchangeStarted;
            mListener.onOperationCompleted(dataTransferred);
        }
    }

    public boolean isResolveCode(int code) {
        return code == REQUEST_CODE_RESOLVE_ERR;
    }

    public void merge(final Coach coach) {
        d("Merging data...");
        mDataExchangeStarted = true;
        mStatus = SyncStatus.Progress;
        new FileSyncRead(mGoogleApiClient, DATA_FILE, new FilePullCloud() {
            @Override
            public void read(InputStream is) {
                readDataMerge(coach, is);
            }
        });
    }

    public void cloudToLocal(final Coach coach) {
        d("Receiving data...");
        mDataExchangeStarted = true;
        mStatus = SyncStatus.Progress;
        new FileSyncRead(mGoogleApiClient, DATA_FILE, new FilePullCloud() {
            @Override
            public void read(InputStream is) {
                readDataReplace(coach, is);
            }
        });
    }

    public void localToCloud(List<String> files, Coach coach) {
        mDataExchangeStarted = true;
        pushData(files, coach);
    }

    private void readDataMerge(final Coach coach, InputStream is) {
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == READ_RESULT_CHANGED) {
                    d("Merge OK. Pushing back");
                    List<String> files = coach.getFilesToSync();
                    localToCloud(files, coach);
                } else if (msg.what == READ_RESULT_SAME) {
                    d("Merge OK. No changes");
                    onResult(true);
                } else {
                    onResult(false);
                }
            }
        };

        readDataAsync(coach, is, true,  handler);
    }

    private void readDataReplace(Coach coach, InputStream is) {
        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                boolean result = msg.what != READ_RESULT_FAIL;
                onResult(result);
            }
        };

        readDataAsync(coach, is, false,  handler);
    }

    private void readDataAsync(final Coach coach, final InputStream is, final boolean merge, final Handler handler) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                int result = readData(coach, is, merge);
                handler.sendEmptyMessage(result);
            }
        });

        thread.start();
    }

    private Knowledge parseData(Map<String, String> content) {
        Knowledge knowledge = new Knowledge();
        JSONLoader loader = new JSonLoaderDump(mContext, content);
        knowledge.init(loader);
        return knowledge;
    }

    private int readData(Coach coach, InputStream is, boolean merge) {
        Map<String, String> content = readData(is);
        if (content != null) {
            int result = READ_RESULT_SAME;
            d("Parsing data...");
            Knowledge knowledge = parseData(content);
            d("Applying data...");
            boolean changed = coach.apply(mContext, knowledge, mInfo, merge);
            if (changed) {
                result = READ_RESULT_CHANGED;
            }
            d("Done!");
            return result;
        } else {
            return READ_RESULT_FAIL;
        }
    }

    private Map<String, String> readData(InputStream is) {
        ZipEntry entry;
        ZipInputStream zis = new ZipInputStream(is);
        Map<String, String> content = new HashMap<>();
        try {
            LineNumberReader scanner = new LineNumberReader(new InputStreamReader(zis));
            while((entry = zis.getNextEntry()) != null) {
                String name = entry.getName();
                d("Receiving file " + name);
                String json = new String();
                String line;
                while ((line = scanner.readLine()) != null) {
                    json += line;
                }
                content.put(name, json);
            }
        } catch (IOException e) {
            Log.e(getClass().getName(), "Cloud data read error: " + e.getMessage());
            return null;
        }
        return content;
    }

    private void pushData(final List<String> files, final Coach coach) {
        d("Sending data...");
        mStatus = SyncStatus.Progress;
        new FileSyncWrite(mGoogleApiClient, DATA_FILE, "application/zip", new FileSyncWrite.Listener() {
            @Override
            public boolean write(OutputStream os) {
                d("Data write: streaming...");
                return writeData(os, files);
            }

            @Override
            public void done() {
                d("Data write: done");
                coach.enableCloudAndSave(mContext);
                commitStamp(coach);
            }

            @Override
            public void fail() {
                d("Data write: fail");
                onResult(false);
            }
        });
    }

    private boolean writeData(OutputStream os, List<String> files) {
        try {
            ZipOutputStream zos = null;
            Writer writer = null;
            for (String file: files) {
                if (Utils.isFileExist(mContext, file)) {
                    d("Streaming " + file);
                    String json = Utils.getJSON(mContext, file, Utils.FileTarget.FILE_TARGET_DYNAMIC);
                    ZipEntry entry = new ZipEntry(file);
                    if (zos == null) {
                        zos = new ZipOutputStream(os);
                        writer = new PrintWriter(zos);
                    }
                    zos.putNextEntry(entry);
                    writer.write(json);
                    writer.flush();
                }
            }
            if (zos != null) {
                writer.close();
                zos.close();
            } else {
                d("No data to commit");
            }
        } catch (Exception e) {
            Log.e(getClass().getName(), "Data preparation error: " + e.getMessage());
            return false;
        }
        return true;
    }

    private void commitStamp(final Coach coach) {
        d("Writing stamp...");
        new FileSyncWrite(mGoogleApiClient, STAMP_FILE, "application/json", new FileSyncWrite.Listener() {
            @Override
            public boolean write(OutputStream os) {
                d("Stamp write: streaming...");
                return writeStamp(os, coach);
            }

            @Override
            public void done() {
                d("Stamp write: done");
                onResult(true);
            }

            @Override
            public void fail() {
                d("Stamp write: fail");
                onResult(false);
            }
        });
    }

    private boolean writeStamp(OutputStream os, Coach coach) {
        JSONObject data = new JSONObject();
        mInfo = new Stamp();
        mInfo.onSynchronized();
        try {
            mInfo.save(data);
            Utils.writeString(os, data);
            coach.updateStamp(mContext, mInfo);
        } catch (Exception e) {
            Log.e(getClass().getName(), "Cloud drive stamp read error: " + e.getMessage());
            onResult(false);
            return false;
        }
        return true;
    }

    private void d(String message) {
        Log.d(getClass().getName(), message);
    }

    public boolean isFinishedSuccessfully() {
        return mStatus == SyncStatus.Success;
    }

    public boolean isInProgress() {
        return mStatus == SyncStatus.Progress;
    }

    //----------------------------------------------------------------------------------

    private abstract class FilePullCloud implements FileSyncRead.Listener {

        @Override
        public void noFile() {
            d("Cloud pull: file missing");
            onResult(false);
        }

        @Override
        public void fail() {
            d("Cloud pull: fail");
            onResult(false);
        }
    }

    private enum SyncStatus {
        Undefined,
        Progress,
        Success,
        Fail
    }

    public interface SyncFinishListener {
        void onOperationCompleted(boolean dataTransferred);
    }

    private GoogleApiClient mGoogleApiClient;
    private Stamp mInfo;
    private Activity mActivity;
    private Context mContext;
    private boolean mInitialization;
    private SyncFinishListener mListener;
    private SyncStatus mStatus;
    private boolean mDataExchangeStarted;

    private static final int REQUEST_CODE_RESOLVE_ERR = 9000;
    private static final String STAMP_FILE = "stamp.json";
    private static final String DATA_FILE = "data.dat";

    private static final int READ_RESULT_FAIL = 0;
    private static final int READ_RESULT_CHANGED = 1;
    private static final int READ_RESULT_SAME = 2;
}
