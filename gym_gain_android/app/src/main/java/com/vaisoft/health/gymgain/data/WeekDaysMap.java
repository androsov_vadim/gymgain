package com.vaisoft.health.gymgain.data;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

public class WeekDaysMap {

    public WeekDaysMap(String language) {
        mLocale = new Locale(language);
        prepareWeekDaysOrder();
    }

    public String[] getShortDaysNames() {
        DateFormatSymbols symbols = new DateFormatSymbols(mLocale);
        return symbols.getShortWeekdays();
    }

    private void prepareWeekDaysOrder() {
        int weekDay = Calendar.getInstance(mLocale).getFirstDayOfWeek();
        for (int i = 0; i < 7; i++) {
            mDayMap[i] = weekDay;
            if (weekDay == 7) {
                weekDay = 1;
            } else {
                weekDay++;
            }
        }
    }

    public int getWeekdayByIndex(int index) {
        return mDayMap[index - 1];
    }

    private int[] mDayMap = new int[7];
    private Locale mLocale;
}
