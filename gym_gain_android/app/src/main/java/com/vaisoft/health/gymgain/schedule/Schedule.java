package com.vaisoft.health.gymgain.schedule;

import android.content.res.Resources;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.WeekDaysMap;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Schedule implements Cloneable {

    public Schedule() {
        mType = Type.NOT_SCHEDULED;
    }

    public Schedule(JSONObject source) throws JSONException {
        mType = Type.valueOf(source.getString(FIELD_TYPE));
        JSONArray wd = source.getJSONArray(FIELD_WD);
        for (int i = 0; i < wd.length(); i++) {
            mWeekDays.add(wd.getInt(i));
        }
        mFlexible.times = source.getInt(FIELD_FLEXIBLE_TIMES);
        mFlexible.period = PeriodType.valueOf(source.getString(FIELD_FLEXIBLE_PERIOD));
        mFlexible.periodsCount = source.getInt(FIELD_FLEXIBLE_PERIODS);
        if (source.has(FIELD_UPDATED)) {
            mLastUpdatedDate = new DateTime(source.getLong(FIELD_UPDATED));
        }
        if (source.has(FIELD_ANALYSED_BURNOUTS)) {
            mLastBurnoutsAnalysedDate = new DateTime(source.getLong(FIELD_ANALYSED_BURNOUTS));
        }
    }

    public JSONObject save() throws JSONException {
        JSONObject object = new JSONObject();
        object.put(FIELD_TYPE, mType.toString());
        JSONArray wd = new JSONArray();
        for (Integer weekDay: mWeekDays) {
            wd.put(weekDay.intValue());
        }
        object.put(FIELD_WD, wd);
        object.put(FIELD_FLEXIBLE_TIMES, mFlexible.times);
        object.put(FIELD_FLEXIBLE_PERIODS, mFlexible.periodsCount);
        object.put(FIELD_FLEXIBLE_PERIOD, mFlexible.period.toString());
        if (mLastUpdatedDate != null) {
            object.put(FIELD_UPDATED, mLastUpdatedDate.getMillis());
        }
        if (mLastBurnoutsAnalysedDate != null) {
            object.put(FIELD_ANALYSED_BURNOUTS, mLastBurnoutsAnalysedDate.getMillis());
        }
        return object;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public Type getType() {
        return mType;
    }

    public void setType(Type type) {
        mType = type;
    }

    public boolean isWeekday(int weekDay) {
        int jodaWeekDay = Utils.mapCalendarWeekDayToJoda(weekDay);
        return mWeekDays.contains(jodaWeekDay);
    }

    public void setWeekday(int weekDay, boolean value) {
        int jodaWeekDay = Utils.mapCalendarWeekDayToJoda(weekDay);
        if (value) {
            mWeekDays.add(jodaWeekDay);
        } else {
            mWeekDays.remove(jodaWeekDay);
        }
    }

    public Flexible getFlexible() {
        return mFlexible;
    }

    public void onUpdated() {
        setLastUpdatedDate(Utils.now());
    }

    public void setLastUpdatedDate(DateTime date) {
        mLastUpdatedDate = new DateTime(date);
    }

    public void onBurnoutsAnalysed(DateTime lastAnalysed) {
        mLastBurnoutsAnalysedDate = lastAnalysed;
    }

    public boolean shouldAnalyseOnBurnouts(DateTime date) {
        if (mLastBurnoutsAnalysedDate == null) {
            return true;
        }
        return !Utils.isBeforeOrEqualDay(date, mLastBurnoutsAnalysedDate);
    }

    public ScheduleAnalyser createAnalyser(DateTime date) {
        if ((mType == Type.NOT_SCHEDULED) || (mType ==Type.MANUAL)) {
            return null;
        }
        DateTime dateTime = date.withTimeAtStartOfDay();
        if (mLastBurnoutsAnalysedDate == null) {
            mLastBurnoutsAnalysedDate = mLastUpdatedDate;
        }
        switch (mType) {
            case FLEXIBLE:
                return new FlexibleAnalyser(dateTime, mLastUpdatedDate, mFlexible, mLastBurnoutsAnalysedDate);
            case WEEKLY:
                return new WeeklyAnalyser(dateTime, mLastUpdatedDate, mWeekDays, mLastBurnoutsAnalysedDate);
        }
        return null;
    }

    public String getDescription(Resources resources) {
        switch (mType) {
            case NOT_SCHEDULED:
                return resources.getString(R.string.record_schedule_no);
            case MANUAL:
                return resources.getString(R.string.record_schedule_flexible);
            case FLEXIBLE:
                return  getFlexibleDescription(resources);
            case WEEKLY:
                return getWeeklyDescription(resources);
        }
        return null;
    }

    private String getWeeklyDescription(Resources resources) {
        StringBuilder result = new StringBuilder();
        result.append(resources.getString(R.string.feed_weekly));
        result.append(' ');

        String language = resources.getString(R.string.locale);
        WeekDaysMap daysMap = new WeekDaysMap(language);
        String dayNames[] = daysMap.getShortDaysNames();
        List<String> days = new ArrayList<>();
        for (int i = 1; i <=7; i++) {
            if (isWeekday(i)) {
                days.add(dayNames[i]);
            }
        }
        result.append(Utils.convertListToString(days, null));

        return result.toString();
    }

    private String getFlexibleDescription(Resources resources) {
        StringBuilder result = new StringBuilder();
        result.append(mFlexible.times);
        result.append(' ');
        result.append(resources.getQuantityString(R.plurals.times_per, mFlexible.times));
        result.append(' ');
        result.append(mFlexible.periodsCount);
        result.append(' ');
        switch (mFlexible.period) {
            case DAY:
                result.append(resources.getQuantityString(R.plurals.days, mFlexible.periodsCount));
                break;
            case WEEK:
                result.append(resources.getQuantityString(R.plurals.weeks, mFlexible.periodsCount));
                break;
            case MONTH:
                result.append(resources.getQuantityString(R.plurals.months, mFlexible.periodsCount));
                break;
            case YEAR:
                result.append(resources.getQuantityString(R.plurals.years, mFlexible.periodsCount));
                break;
        }
        return result.toString();
    }

    public static Schedule createFlexible(Schedule.PeriodType period, int periodsCount, int times) {
        Schedule schedule = new Schedule();
        schedule.setType(Type.FLEXIBLE);
        Schedule.Flexible flexible = schedule.getFlexible();
        flexible.period = period;
        flexible.periodsCount = periodsCount;
        flexible.times = times;
        schedule.onUpdated();
        return schedule;
    }

    public static Schedule createWeekly(int days[]) {
        Schedule schedule = new Schedule();
        schedule.setType(Type.WEEKLY);
        for (int i = 0; i < days.length; ++i) {
            schedule.setWeekday(days[i], true);
        }
        schedule.onUpdated();
        return schedule;
    }

//--------------------------------------------------------------------------------

    public enum Type {
        NOT_SCHEDULED,
        MANUAL,
        FLEXIBLE,
        WEEKLY
    }

    public static class Flexible {
        public int times = 1;
        public int periodsCount = 1;
        public PeriodType period = PeriodType.WEEK;

        public int getDaysEstimate() {
            switch (period) {
                case DAY:
                    return periodsCount;
                case WEEK:
                    return periodsCount * 7;
                case MONTH:
                    return periodsCount * 30;
                case YEAR:
                    return periodsCount * 365;
            }
            return 0;
        }
    }

    public enum PeriodType {
        DAY,
        WEEK,
        MONTH,
        YEAR
    }

//---------------------------------------------------------------------------------\

    private Type mType;
    private Set<Integer> mWeekDays = new HashSet<>();
    private Flexible mFlexible = new Flexible();
    private DateTime mLastUpdatedDate;
    private DateTime mLastBurnoutsAnalysedDate;

    private static final String FIELD_TYPE="type";
    private static final String FIELD_WD="wd";
    private static final String FIELD_FLEXIBLE_TIMES="times";
    private static final String FIELD_FLEXIBLE_PERIODS="periods";
    private static final String FIELD_FLEXIBLE_PERIOD="period";
    private static final String FIELD_UPDATED="update";
    private static final String FIELD_ANALYSED_BURNOUTS="burn_analysed" +
            "e";
}
