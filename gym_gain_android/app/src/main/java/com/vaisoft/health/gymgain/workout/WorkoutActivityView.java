package com.vaisoft.health.gymgain.workout;

import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.IdGenerator;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.data.Source;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.dialogs.ScheduleDialog;
import com.vaisoft.health.gymgain.schedule.Schedule;
import com.vaisoft.health.gymgain.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class WorkoutActivityView extends WorkoutActivity
    implements ScheduleDialog.Listener
{
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_workout_view;
    }

    @Override
    protected void invalidateNewMode() {
        final FloatingActionButton gymStart = findViewById(R.id.gym_start);
        if (isUseless()) {
            gymStart.hide();
            return;
        }
        gymStart.show();

        gymStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startWorkout();
                openEditorActivity();
            }
        });

        if (mWorkoutAdapter != null) {
            mWorkoutAdapter.notifyDataSetChanged();
        }
    }

    private boolean isUseless() {
        return getWorkout().getItemsCount() == 0;
    }

    @Override
    protected void initWorkoutInterface() {
        RecyclerView rv = findViewById(R.id.todo);
        Utils.initOrientationAwareList(rv);
        if (mWorkoutAdapter == null) {
            mWorkoutAdapter = new WorkoutAdapter();
        }
        rv.setAdapter(mWorkoutAdapter);
        FloatingActionButton schedulerButton = findViewById(R.id.schedule);
        schedulerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onShowScheduleClick();
            }
        });

    }

    private void onShowScheduleClick() {
        Record record = getRecord();
        showRecordDialog(record);
    }

    @Override
    protected Workout initActiveWorkout() {
        return getActiveWorkout(); //no need to clone - it's just a viewer
    }

    @Override
    protected Workout initFinishedWorkout() {
        return getFinishedWorkout(); //no need to clone - it's just a viewer
    }

    @Override
    protected void onPostCommentUpdated(int set, String text) {

    }

    private void openEditor(int set, int exercise) {
        mSelectedSet = set;
        mSelectedExercise = exercise;
        if (isNew()) {
            confirm(R.string.confirm_start_workout, R.string.confirm_start_caption, START_WORKOUT_CODE);
        } else {
            openEditorActivity();
        }
    }

    @Override
    protected boolean isEditable() {
        return !isNew();
    }

    @Override
    public boolean onConfirmed(int code) {
        if (code == START_WORKOUT_CODE) {
            startWorkout();
            openEditorActivity();
            return true;
        }
        return super.onConfirmed(code);
    }

    private void openEditorActivity() {
        Intent intent = new Intent(this, WorkoutActivityEdit.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        putStartParameters(intent);
        intent.putExtra(WorkoutContentFragment.SET, mSelectedSet);
        intent.putExtra(WorkoutContentFragment.EXERCISE, mSelectedExercise);
        setEditTarget(null);
        finish();
        startActivity(intent);
    }

    protected void initControlPanelRunning() {
        hideStartButton();
        super.initControlPanelRunning();
    }

    protected void initControlPanelFinished() {
        hideStartButton();
        super.initControlPanelFinished();
    }

    private void hideStartButton() {
        FloatingActionButton gymStart = findViewById(R.id.gym_start);
        gymStart.hide();
    }

    @Override
    protected Record createRecord() {
        Source source = getWorkout().getSource();
        String id = source.getId();
        boolean isExercise = source.isExercise();
        if (isExercise) {
            return getCoach().createExerciseRecord(id);
        } else {
            return getCoach().createTrainingRecord(id);
        }
    }

    @Override
    protected Record getRecord() {
        String workoutKey = getWorkoutKey();
        return getCoach().getRecordByKey(workoutKey);
    }

    @Override
    public void onScheduleUpdated(@NonNull Schedule schedule) {
        Record record = getRecord();
        if (record ==  null) {
            record = createRecord();
        }
        getCoach().setRecordSchedule(getContext(), record, schedule);
    }

    @Override
    public void onScheduleDeleted() {
        deleteSchedule();
    }

    //--------------------------------------------------------------------------------------------------

    private class WorkoutViewHolder extends RecyclerView.ViewHolder
            implements  View.OnClickListener{

        public WorkoutViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mName = itemView.findViewById(R.id.name);
            mPrevComment = itemView.findViewById(R.id.prevComment);
            mPostComment = itemView.findViewById(R.id.postComment);
            mResultsList = itemView.findViewById(R.id.set_results);
            mPostCommentParent = itemView.findViewById(R.id.postComment_parent);
            mSetComment = itemView.findViewById(R.id.setComment);
        }

        public void apply(int position) {
            mPosition = position;
            String name = getStep(position);
            mName.setText(Utils.html(name));

            Workout.Entry entry = getWorkout().getEntry(position);
            Workout.Entry previousEntry = null;

            if (getPreviousWorkout() != null) {
                previousEntry = getPreviousWorkout().getSameEntry(entry);
                if (previousEntry != null) {
                    setHideableComment(previousEntry.postComment, mPrevComment);
                } else {
                    setHideableComment(null, mPrevComment);
                }
            } else {
                setHideableComment(null, mPrevComment);
            }

            initList(entry, previousEntry);

            setHideableComment(entry.setComment, mSetComment);
            setHideableComment(entry.postComment, mPostComment, mPostCommentParent);
        }

        private void initList(Workout.Entry entry, Workout.Entry previousEntry) {
            mResultsAdapter = new SetResultsAdapter(entry, previousEntry,
                    getCoach(), WorkoutActivityView.this);
            mResultsList.setAdapter(mResultsAdapter);
            mResultsList.setOnTouchListener(null);
            mResultsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    openEditor(mPosition, i);
                }
            });
            if (mResultsAdapter.canBeActive()) {
                mTicker = new WorkoutTicker(mResultsList, mResultsAdapter);
            }
        }

        public void setTickerActive(boolean active) {
            if (mTicker != null) {
                mTicker.setActive(active);
            }
        }

        private String getStep(int position) {
            List<String> exercises = getWorkout().getLogContent(position);
            boolean coma = false;
            StringBuilder result = new StringBuilder();
            List<String> variableUnits = new ArrayList<>();
            result.append("<b>");
            for (String exerciseId: exercises) {
                if (coma) {
                    result.append(", ");
                } else {
                    coma = true;
                }
                result.append(getCoach().getLocalized(exerciseId));
                Exercise exercise = getCoach().getExercise(exerciseId);
                List<Unit> parameters = exercise.getParameterUnits();
                for (Unit parameter: parameters) {
                    Unit actualUnit = getCoach().getConfig().getUnits(parameter);
                    if (actualUnit != parameter) {
                        String unitId = actualUnit.toString();
                        if (!variableUnits.contains(unitId)) {
                            variableUnits.add(unitId);
                        }
                    }
                }
                List<Value> targeted = exercise.getTargetedParameters();
                if (!targeted.isEmpty()) {
                    result.append('(');
                    result.append(Utils.convertValuesToString(targeted, getCoach().getLocalizer()));
                    result.append(')');
                }
            }
            result.append("</b>");
            if (!variableUnits.isEmpty()) {
                Utils.appendHint(result, Utils.convertListToString(variableUnits, getCoach().getLocalizer()));
            }
            return result.toString();
        }

        @Override
        public void onClick(View view) {
            openEditor(mPosition, -1);
        }

        private WorkoutTicker mTicker;
        private TextView mName;
        private TextView mPrevComment;
        private TextView mPostComment;
        private ViewGroup mPostCommentParent;
        private ListView mResultsList;
        private SetResultsAdapter mResultsAdapter;
        private TextView mSetComment;
        private int mPosition;

    }

    private class WorkoutAdapter extends RecyclerView.Adapter<WorkoutViewHolder> {

        public WorkoutAdapter() {
            mInflater = LayoutInflater.from(getContext());
        }

        @Override
        public WorkoutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.activity_workout_set, parent, false);
            return new WorkoutViewHolder(view);
        }

        @Override
        public void onBindViewHolder(WorkoutViewHolder holder, int position) {
            holder.apply(position);
         }

        @Override
        public int getItemCount() {
            return getWorkout().getItemsCount();
        }

        @Override
        public void onViewAttachedToWindow(WorkoutViewHolder holder) {
            super.onViewAttachedToWindow(holder);
            holder.setTickerActive(true);
        }

        @Override
        public void onViewDetachedFromWindow(WorkoutViewHolder holder) {
            holder.setTickerActive(false);
            super.onViewDetachedFromWindow(holder);
        }

        private LayoutInflater mInflater;
    }

    private WorkoutAdapter mWorkoutAdapter;
    private int mSelectedSet;
    private int mSelectedExercise;

    private static final int START_WORKOUT_CODE = IdGenerator.generateCode();
}
