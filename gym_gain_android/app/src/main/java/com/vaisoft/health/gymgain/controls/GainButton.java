package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.graphics.PorterDuff;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;

import com.vaisoft.health.gymgain.R;

public class GainButton extends androidx.appcompat.widget.AppCompatButton {
    public GainButton(Context context) {
        super(context);
        applyColor();
    }

    public GainButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyColor();
    }

    public GainButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyColor();
    }

    private void applyColor() {
        getBackground().setColorFilter(
                ContextCompat.getColor(getContext(), R.color.button_background),
                PorterDuff.Mode.MULTIPLY);
    }
}
