package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import com.vaisoft.health.gymgain.R;

public class ShowErrorDialog extends DialogFragment
{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        Bundle bundle = getArguments();
        int message = bundle.getInt(DATA);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        
        builder.setTitle(R.string.error);
        builder.setMessage(message);
        builder.setIcon(R.drawable.ic_error_outline_black_24dp);
        builder.setPositiveButton(R.string.ok, null);

        return builder.create();
    }

    public static final String DATA = "message";
}
