package com.vaisoft.health.gymgain.data;

import android.content.Context;
import android.text.InputType;
import android.util.Log;

import com.vaisoft.health.gymgain.sync.JSONLoader;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Duration;
import org.joda.time.Minutes;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;

public class Config {

    public static final int DEFAULT_MIN_REPS = 6;

    private Config() {
        initDefaults();
    }

    private Config(JSONArray rules, JSONObject defaults) {
        mChanged = false;
        loadDefaultUnits(defaults);
        loadUnitsRules(rules);
    }

    public static Config load(JSONLoader loader) {
        JSONArray rules = loader.loadArray(RULES_FILE_NAME, Utils.FileTarget.FILE_TARGET_ASSETS);
        JSONObject defaults = loader.loadObject(FILE_NAME, Utils.FileTarget.FILE_TARGET_PRIVATE);
        return new Config(rules, defaults);
    }

    public static Config test(int minReps, Config.Estimation estimation) {
        Config config = new Config();
        config.setMinReps(minReps);
        config.setEstimation(estimation);
        return config;
    }

    public static void addSourceFilesToList(List<String> list) {
        list.add(FILE_NAME);
    }

    public static int getCheckpointHours(Context context) {
        if (sCheckpoint == -1) {
            JSONObject data = Utils.loadJSONObject(context, CHECKPOINT_ID, Utils.FileTarget.FILE_TARGET_PRIVATE);
            if (data != null) {
                try {
                    sCheckpoint = data.getInt(CHECKPOINT_ID);
                } catch (JSONException e) {
                    setDefaultCheckpoint();
                    Log.e(Config.class.getName(), "Load checkpoint error");
                }
            } else {
                setDefaultCheckpoint();
            }
        }
        return sCheckpoint;
    }

    public static boolean setCheckpointHours(Context context, int value) {
        if (sCheckpoint != value) {
            sCheckpoint = value;
            JSONObject data = new JSONObject();
            try {
                data.put(CHECKPOINT_ID, sCheckpoint);
                Utils.saveJSON(context, CHECKPOINT_ID, data);
            } catch (JSONException e) {
                Log.e(Config.class.getName(), "Save checkpoint error");
            }
            return true;
        }
        return false;
    }

    private static void setDefaultCheckpoint() {
        sCheckpoint = 19;
    }

    private void loadUnitsRules(JSONArray data) {
        if (data != null) {
            try {
                for (int i = 0; i < data.length(); i++) {
                    JSONObject ruleData = data.getJSONObject(i);

                    UnitRule rule = new UnitRule();
                    Unit unit = Unit.valueOf(ruleData.getString(RULE_UNIT_ID));

                    if (ruleData.has(RULE_UNIT_DECIMALS)) {
                        rule.setFormat(ruleData.getInt(RULE_UNIT_DECIMALS));
                    }

                    unit.setRule(rule);

                    if (ruleData.has(RULE_UNIT_STEP)) {
                        JSONArray steps = ruleData.getJSONArray(RULE_UNIT_STEP);
                        rule.step = new int[steps.length()];
                        for (int s = 0; s < steps.length(); s++) {
                            rule.step[s] = (int)unit.toInternalFormat(steps.getDouble(s));
                        }
                    }
                }
            } catch (JSONException e) {
                Log.e(getClass().getName(), e.getMessage());
            }
        }
    }

     private void loadDefaultUnits(JSONObject src) {
        if (src == null) {
            initDefaults();
        } else {
            try {
                loadData(src);
            } catch (JSONException e) {
                Log.e(getClass().getName(), e.getMessage());
                initDefaults();
            }
        }
    }

    private long loadStamp(JSONObject src, String id) throws JSONException {
        if (src.has(id)) {
            return src.getLong(id);
        } else {
            return 0;
        }
    }

    private void initDefaults() {
        setSiUnits();
        mMinReps = DEFAULT_MIN_REPS;
        mMaxReps = DEFAULT_MAX_REPS;
        mFirstRun = true;
        mAutoCloudSyncGSM = false;
        mDataModifiedAfterCloudSync = false;
        mLastCloudSync = 0;
        mCloudEnabled = false;
        mLockSleepTimer = true;
        mLockSleepWorkout = false;
        mLastRatedStamp = 0;
        mEstimation = Estimation.NORMAL;
    }

    public void save(Context context) {
        if (!mChanged) {
            return;
        }
        JSONObject data = new JSONObject();
        try {
            saveData(data);
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
        Utils.saveJSON(context, FILE_NAME, data);
        mChanged = false;
    }

    private void saveData(JSONObject data) throws JSONException {
        data.put(WEIGHT_UNITS_ID, mWeightUnits.toString());
        data.put(WEIGHT_UNITS_STAMP_ID, mWeightUnitsStamp);

        data.put(DISTANCE_UNITS_ID, mDistanceUnits.toString());
        data.put(DISTANCE_UNITS_STAMP_ID, mDistanceUnitsStamp);

        data.put(BODY_UNITS_ID, mBodyUnits.toString());
        data.put(BODY_UNITS_STAMP_ID, mBodyUnitsStamp);

        data.put(MIN_REPS_ID, mMinReps);
        data.put(MIN_REPS_STAMP_ID, mMinRepsStamp);

        data.put(MAX_REPS_ID, mMaxReps);
        data.put(MAX_REPS_STAMP_ID, mMaxRepsStamp);

        data.put(FIRST_RUN_ID, mFirstRun);
        data.put(CLOUD_GSM_ID, mAutoCloudSyncGSM);
        data.put(MODIFIED_ID, mDataModifiedAfterCloudSync);
        data.put(LAST_CLOUD_SYNC_ID, mLastCloudSync);
        data.put(CLOUD_ENABLED_ID, mCloudEnabled);

        data.put(LOCK_SLEEP_TIMER_ID, mLockSleepTimer);
        data.put(LOCK_SLEEP_WORKOUT_ID, mLockSleepWorkout);
        data.put(LOCK_SLEEP_STAMP, mLockSleepStamp);
        data.put(ADS_FREE_STAMP_ID, mAdsFreeStamp);

        data.put(RATE_ME_STAMP, mLastRatedStamp);
        data.put(ESTIMATION_ID, mEstimation);
    }

    private void loadData(JSONObject src) throws JSONException {
        mWeightUnits = Unit.valueOf(src.getString(WEIGHT_UNITS_ID));
        mWeightUnitsStamp = loadStamp(src, WEIGHT_UNITS_STAMP_ID);

        mDistanceUnits = Unit.valueOf(src.getString(DISTANCE_UNITS_ID));
        mDistanceUnitsStamp = loadStamp(src, DISTANCE_UNITS_STAMP_ID);

        mBodyUnits = Unit.valueOf(src.getString(BODY_UNITS_ID));
        mBodyUnitsStamp = loadStamp(src, BODY_UNITS_STAMP_ID);

        mMinReps = src.getInt(MIN_REPS_ID);
        mMinRepsStamp = loadStamp(src, MIN_REPS_STAMP_ID);

        mMaxReps = src.getInt(MAX_REPS_ID);
        mMaxRepsStamp = loadStamp(src, MAX_REPS_STAMP_ID);

        mFirstRun = src.getBoolean(FIRST_RUN_ID);
        mAutoCloudSyncGSM = src.getBoolean(CLOUD_GSM_ID);
        mDataModifiedAfterCloudSync = src.getBoolean(MODIFIED_ID);
        mLastCloudSync = src.getLong(LAST_CLOUD_SYNC_ID);
        mCloudEnabled = src.getBoolean(CLOUD_ENABLED_ID);

        mLockSleepTimer = src.getBoolean(LOCK_SLEEP_TIMER_ID);
        mLockSleepWorkout = src.getBoolean(LOCK_SLEEP_WORKOUT_ID);
        mLockSleepStamp = src.getLong(LOCK_SLEEP_STAMP);
        mAdsFreeStamp = src.getLong(ADS_FREE_STAMP_ID);

        mLastRatedStamp = src.getLong(RATE_ME_STAMP);
        mEstimation = Estimation.valueOf(src.getString(ESTIMATION_ID));
    }

    public Unit getUnits(Unit category){
        switch (category) {
            case size:
                return getBodyUnits();
            case weight:
                return getWeightUnits();
            case distance:
                return getDistanceUnits();
        }
        return category;
    }

    public Unit getWeightUnits() {
        return mWeightUnits;
    }

    public void setWeightUnits(Unit weightUnits) {
        if (mWeightUnits != weightUnits) {
            mWeightUnits = weightUnits;
            updateWeightStamp();
            mChanged = true;
            mIsCloudSensitiveChanges = true;
        }
    }

    public Unit getDistanceUnits() {
        return mDistanceUnits;
    }

    public void setDistanceUnits(Unit distanceUnits) {
        if (mDistanceUnits != distanceUnits) {
            mDistanceUnits = distanceUnits;
            updateDistanceStamp();
            mChanged = true;
            mIsCloudSensitiveChanges = true;
        }
    }

    public int getMaxReps() {
        return mMaxReps;
    }

    public int getMinReps() {
        return mMinReps;
    }

    public void setMinReps(int value) {
        if (mMinReps != value) {
            mMinReps = value;
            updateMinRepsStamp();
            mChanged = true;
            mIsCloudSensitiveChanges = true;
        }
    }

    public Estimation getEstimation() {
        return mEstimation;
    }

    public void setEstimation(Estimation estimation) {
        if (mEstimation != estimation) {
            mEstimation = estimation;
            mChanged = true;
            mIsCloudSensitiveChanges = true;
        }
    }

    public void setMaxReps(int value) {
        if (mMaxReps != value) {
            mMaxReps = value;
            updateMaxRepsStamp();
            mChanged = true;
            mIsCloudSensitiveChanges = true;
        }
    }

    public Unit getBodyUnits() {
        return mBodyUnits;
    }

    public void setBodyUnits(Unit bodyUnits) {
        if (mBodyUnits != bodyUnits) {
            mBodyUnits = bodyUnits;
            updateBodyUnitsStamp();
            mChanged = true;
            mIsCloudSensitiveChanges = true;
        }
    }

    public static long getCheckoutUtcMillis(Context context) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int currentHour = calendar.get(Calendar.HOUR_OF_DAY);
        int targetHour = getCheckpointHours(context);
        calendar.set(Calendar.HOUR_OF_DAY, targetHour);
        calendar.set(Calendar.MINUTE, 0);
        if (currentHour >= targetHour) {
            calendar.add(Calendar.DATE, 1);
        }
        return calendar.getTimeInMillis();
    }

    private void setSiUnits() {
        mWeightUnits = Unit.kilogram;
        mDistanceUnits = Unit.meter;
        mBodyUnits = Unit.centimeter;
    }

    public void useSiUnits(boolean initial) {
        setSiUnits();
        mChanged = true;
        if (initial) {
            onInitialAction();
        } else {
            updateWeightStamp();
            updateDistanceStamp();
            updateBodyUnitsStamp();
            mIsCloudSensitiveChanges = true;
        }
    }

    public void useUsUnits(boolean initial) {
        mWeightUnits = Unit.pound;
        mDistanceUnits = Unit.foot;
        mBodyUnits = Unit.inch;
        mChanged = true;
        if (initial) {
            onInitialAction();
        } else {
            updateWeightStamp();
            updateDistanceStamp();
            updateBodyUnitsStamp();
            mIsCloudSensitiveChanges = true;
        }
    }

    private void onInitialAction() {
        mFirstRun = false;
        adsFreeGained(null);
        rateMeLater(null);
    }

    public boolean isFirstRun() {
        return mFirstRun;
    }

    public boolean canAutoConnectToCloud(Context context) {
        boolean cloudEnabled = isCloudEnabled();
        boolean isWifi = Utils.isNetworkConnectionWifi(context);
        boolean isConnection = Utils.isNetworkConnection(context);
        boolean gsmIsOk = isAutoGSMSyncEnabled();
        return cloudEnabled && (isWifi || (isConnection && gsmIsOk));
    }

    public boolean canBackgroundConnectToCloud(Context context) {
        return canAutoConnectToCloud(context) && isTimeForBackgroundSync();
    }

    public void setAutoGSMSyncEnabled(boolean enabled) {
        if (mAutoCloudSyncGSM != enabled) {
            mAutoCloudSyncGSM = enabled;
            mChanged = true;
        }
    }

    public void setDataModifiedAfterCloudSync(boolean modified) {
        if (mDataModifiedAfterCloudSync != modified) {
            mDataModifiedAfterCloudSync = modified;
            mChanged = true;
        }
    }

    public void setCloudEnabled(boolean enabled) {
        if (mCloudEnabled != enabled) {
            mCloudEnabled = enabled;
            mChanged = true;
        }
    }

    public boolean isCloudEnabled() {
        return mCloudEnabled;
    }

    public boolean isDataModifiedAfterCloudSync() {
        return mDataModifiedAfterCloudSync;
    }

    public boolean isAutoGSMSyncEnabled() {
        return mAutoCloudSyncGSM;
    }

    public void setLockScreenTimer(boolean lock) {
        if (mLockSleepTimer != lock) {
            mLockSleepTimer = lock;
            mLockSleepStamp = getCurrentTimeStamp();
            mChanged = true;
        }
    }

    public boolean isLockScreenTimer() {
        return mLockSleepTimer;
    }

    public boolean isLockScreenWorkout() {
        return mLockSleepWorkout;
    }

    public void setLockScreenWorkout(boolean lock) {
        if (mLockSleepWorkout != lock) {
            mLockSleepWorkout = lock;
            mLockSleepStamp = getCurrentTimeStamp();
            mChanged = true;
        }
    }

    public boolean merge(Config config) {
        if (mWeightUnitsStamp < config.mWeightUnitsStamp) {
            setWeightUnits(config.mWeightUnits);
        }
        if (mDistanceUnitsStamp < config.mDistanceUnitsStamp) {
            setDistanceUnits(config.mDistanceUnits);
        }
        if (mBodyUnitsStamp < config.mBodyUnitsStamp) {
            setBodyUnits(config.mBodyUnits);
        }
        if (mMinRepsStamp < config.mMinRepsStamp) {
            setMinReps(config.mMinReps);
        }
        if (mMaxRepsStamp < config.mMaxRepsStamp) {
            setMaxReps(config.mMaxReps);
        }
        if (mLockSleepStamp < config.mLockSleepStamp) {
            setLockScreenTimer(config.mLockSleepTimer);
            setLockScreenWorkout(config.mLockSleepWorkout);
        }
        if (config.mLastRatedStamp == -1) {
            setLastRatedStamp(-1);
        }
        return mChanged;
    }

    private void updateWeightStamp() {
        mWeightUnitsStamp = getCurrentTimeStamp();
    }

    private void updateDistanceStamp() {
        mDistanceUnitsStamp = getCurrentTimeStamp();
    }

    private void updateBodyUnitsStamp() {
        mBodyUnitsStamp = getCurrentTimeStamp();
    }

    private void updateMinRepsStamp() {
        mMinRepsStamp = getCurrentTimeStamp();
    }

    private void updateMaxRepsStamp() {
        mMaxRepsStamp = getCurrentTimeStamp();
    }

    private long getCurrentTimeStamp() {
        return Utils.now().getMillis();
    }

    public void onCloudSync() {
        mLastCloudSync = getCurrentTimeStamp();
        mChanged = true;
    }

    private boolean isTimeForBackgroundSync() {
        Duration duration = new Duration(mLastCloudSync, getCurrentTimeStamp());
        long minutes = duration.getStandardMinutes();
        return minutes >= MINUTES_BETWEEN_BACKGROUND_CLOUD_UPDATE;
    }

    public boolean nothingToSync(DateTime local, DateTime remote) {
        if ((local == null) || (remote == null)) {
            return false;
        }
        boolean sameStamps = local.equals(remote);
        boolean noChanges = !isDataModifiedAfterCloudSync();
        return sameStamps && noChanges;
    }

    public boolean hasCloudSensitiveChanges() {
        boolean result = mIsCloudSensitiveChanges;
        mIsCloudSensitiveChanges = false;
        return result;
    }

    public static void onCoachJobDone() {
        ++sCoachJobsDone;
    }

    public boolean isTimeForAds() {
        if (getAdsFreePeriodEnd() != null) {
            return false;
        }
        if (sCoachJobsDone % 2 == 1) {
            if (mLastApprovedTimeForAds != sCoachJobsDone) {
                mLastApprovedTimeForAds = sCoachJobsDone;
                return true;
            }
        }
        return false;
    }

    public void adsFreeGained(Context context) {
        mAdsFreeStamp = Utils.now().getMillis();
        mChanged = true;
        if (context != null) {
            save(context);
        }
    }

    public DateTime getAdsFreePeriodEnd() {
        long diff = Utils.now().getMillis() - mAdsFreeStamp;
        if ((diff < 0) || (diff > ADS_FREE_PERIOD)) {
            return null;
        }
        return new DateTime(mAdsFreeStamp + ADS_FREE_PERIOD);
    }

    public void rateMeLater(Context context) {
        setLastRatedStamp(Utils.now().getMillis());
        if (context != null) {
            save(context);
        }
    }

    public void rateMeDisable(Context context) {
        if (setLastRatedStamp(-1)) {
            mIsCloudSensitiveChanges = true;
        }
        if (context != null) {
            save(context);
        }
    }

    private boolean setLastRatedStamp(long stamp) {
        if (mLastRatedStamp != stamp) {
            mLastRatedStamp = stamp;
            mChanged = true;
            return true;
        }
        return false;
    }

    public boolean isTimeToShowRateMe() {
        if (mLastRatedStamp == -1) {
            return false;
        }
        int period = Days.daysBetween(new DateTime(mLastRatedStamp), Utils.now()).getDays();
        return period > RATE_ME_LATER_DAYS;
    }

    //----------------------------------------------------------------------

    public class UnitRule {
        public int[] step;

        public String toText(long value) {
            if (mDecimals == 0) {
                return "" + value;
            }
            return mFormatter.format((double)value / mScaler);
        }

        public long parseText(String text) {
            double parsedValue = 0;
            try {
                parsedValue = Double.parseDouble(text);
            } catch (NumberFormatException ex) {
                return -1;
            }
            if (mDecimals > 0) {
                parsedValue *= mScaler;
            }
            return (long) parsedValue;
        }

        public long toInternalFormat(double source) {
            return  (long) (source * mScaler);
        }

        private long getDecimalScale() {
            long result = 1;
            for (int i = 0; i < mDecimals; i++) {
                result *= 10;
            }
            return result;
        }

        public void setFormat(int decimals) {
            mDecimals = decimals;
            mScaler = getDecimalScale();
            StringBuilder format = new StringBuilder("0");
            if (decimals > 0) {
                format.append('.');
                for (int i = 0; i < decimals; i++) {
                    format.append('0');
                }
            }
            mFormatter = new DecimalFormat(format.toString());
        }

        public int getValueInputType() {
            if (mDecimals == 0) {
                return InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL;
            } else {
                return InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL;
            }
        }

        public boolean isTextFormat() {
            return mFormatter != null;
        }

        private int mDecimals;
        private long mScaler;
        private DecimalFormat mFormatter;
    }

    public enum Estimation {
        STRICT,
        NORMAL
    }

    private Unit mWeightUnits;
    private long mWeightUnitsStamp;
    private Unit mDistanceUnits;
    private long mDistanceUnitsStamp;
    private Unit mBodyUnits;
    private long mBodyUnitsStamp;
    private int mMinReps;
    private long mMinRepsStamp;
    private int mMaxReps;
    private long mMaxRepsStamp;
    private boolean mLockSleepTimer;
    private boolean mLockSleepWorkout;
    private long mLockSleepStamp;
    private long mAdsFreeStamp;
    private Estimation mEstimation;

    private boolean mChanged;
    private boolean mFirstRun;
    private boolean mAutoCloudSyncGSM;
    private boolean mDataModifiedAfterCloudSync;
    private long mLastCloudSync;
    private boolean mCloudEnabled;
    private boolean mIsCloudSensitiveChanges;
    private int mLastApprovedTimeForAds;
    private long mLastRatedStamp;

    private static int sCheckpoint = -1;
    private static int sCoachJobsDone = 0;

    private static final String WEIGHT_UNITS_ID = "weight";
    private static final String WEIGHT_UNITS_STAMP_ID = "s_weight";
    private static final String DISTANCE_UNITS_ID = "distance";
    private static final String DISTANCE_UNITS_STAMP_ID = "s_distance";
    private static final String BODY_UNITS_ID = "body";
    private static final String BODY_UNITS_STAMP_ID = "s_body";
    private static final String MIN_REPS_ID = "min_reps";
    private static final String MIN_REPS_STAMP_ID = "s_min_reps";
    private static final String MAX_REPS_ID = "max_reps";
    private static final String MAX_REPS_STAMP_ID = "s_max_reps";
    private static final String LAST_CLOUD_SYNC_ID = "last_cloud_sync";
    private static final String CLOUD_ENABLED_ID = "cloud_on";
    private static final String LOCK_SLEEP_TIMER_ID = "nosleep_timer";
    private static final String LOCK_SLEEP_WORKOUT_ID = "nosleep_workout";
    private static final String LOCK_SLEEP_STAMP = "s_nosleep";
    private static final String ADS_FREE_STAMP_ID = "af";
    private static final String RATE_ME_STAMP = "s_rate";
    private static final String ESTIMATION_ID = "estimate";

    private static final String FIRST_RUN_ID = "first_run";
    private static final String CHECKPOINT_ID = "checkpoint";
    private static final String CLOUD_GSM_ID = "auto_gsm";
    private static final String MODIFIED_ID = "modified";

    private static final String RULE_UNIT_ID = "id";
    private static final String RULE_UNIT_DECIMALS = "decimals";
    private static final String RULE_UNIT_STEP = "step";

    private static final String FILE_NAME = "cfg";
    private static final String RULES_FILE_NAME = "units";

    private static final int RATE_ME_LATER_DAYS = 5;
    private static final int MINUTES_BETWEEN_BACKGROUND_CLOUD_UPDATE = 60;
    private static final int DEFAULT_MAX_REPS = 12;
    private static final long ADS_FREE_PERIOD = 60 * 60 * 24 * 1000;
}
