package com.vaisoft.health.gymgain.controls;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.edit.CoachActivity;

public abstract class CoachFragment extends Fragment {

    protected abstract void init();
    protected abstract int getRootResource();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mActivity = (CoachActivity) getActivity();
        if (getCoach() != null) {
            init();
        }
    }

    public void invalidate() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getRootResource(), container, false);
    }

    protected Coach getCoach() {
        return mActivity.getCoach();
    }

    private CoachActivity mActivity;
}
