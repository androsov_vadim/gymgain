package com.vaisoft.health.gymgain.utils;

import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.GymGainActivity;

public class AboutActivity extends GymGainActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.menu_about);

        final TextView emailField =(TextView) findViewById(R.id.about_email);
        emailField.setClickable(true);
        emailField.setMovementMethod(LinkMovementMethod.getInstance());
        String email = getResources().getString(R.string.about_email_address);
        String text = "<a href='mailto:" + email + "'> " + email + " </a>";
        emailField.setText(Utils.html(text));

        try {
            final TextView versionField =(TextView) findViewById(R.id.about_version);
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            versionField.setText(versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getName(), e.getMessage());
        }

        final Button rate = (Button) findViewById(R.id.about_rate);
        rate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Utils.openStore(this);
    }

    @Override
    protected void onDestroy() {
        final Button rate = (Button) findViewById(R.id.about_rate);
        rate.setOnClickListener(null);
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return onOptionsItemSelected(item);
    }
}
