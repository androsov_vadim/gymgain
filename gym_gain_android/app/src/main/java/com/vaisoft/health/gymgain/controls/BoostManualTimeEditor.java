package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.util.AttributeSet;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;

import static com.vaisoft.health.gymgain.data.Value.Status;

public class BoostManualTimeEditor extends BoostTimeEditor {

    public BoostManualTimeEditor(Context context) {
        super(context);
    }

    public BoostManualTimeEditor(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BoostManualTimeEditor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setValue(Unit unit, long value, Value.Status status) {
        super.setValue(unit, value, status);
        updateEditableState();
    }

    private void updateEditableState() {
        boolean editable = canEdit();
        setEditable(editable);
    }

    private boolean canEdit() {
        return isStatus(Value.Status.Applied);
    }

    @Override
    protected long getEffectiveValue() {
        switch (getStatus()) {
            case Undefined:
                return 0;
        }
        return super.getEffectiveValue();
    }

    @Override
    protected String getTargetValueAsText(long value) {
        return super.getValueAsText(value);
    }

    @Override
    protected String getStepAsText(long step) {
        switch (getStatus()) {
            case Progress:
                return getString(step == 0 ? R.string.timer_apply : R.string.timer_pause);
            case Pause:
                return getString(step == 0 ? R.string.timer_apply : R.string.timer_resume);
            case Undefined:
                return getString(step == 0 ? R.string.timer_manual : R.string.timer_start);
        }
        return super.getStepAsText(step);
    }

    @Override
    protected long[] getSteps() {
        if (isStatus(Value.Status.Applied)) {
            return super.getSteps();
        } else {
            return BUTTONS;
        }
    }

    private String getString(int resId) {
        return getContext().getString(resId);
    }

    @Override
    protected void onTabClicked(long step) {
        if (isStatus(Value.Status.Applied)) {
            super.onTabClicked(step);
        } else {
            onTabClick(step);
            update();
        }
    }

    private void onTabClick(long step) {
        switch (getStatus()) {
            case Progress:
                if (step == 0) {
                    applyChanges();
                } else {
                    pauseTimer();
                }
                break;
            case Pause:
                if (step == 0) {
                    applyChanges();
                } else {
                    resumeTimer();
                }
                break;
            case Undefined:
                if (step == 0) {
                    setManualStage();
                } else {
                    startTimer();
                }
                break;
        }
    }

    protected boolean isDirectEditEnabled() {
        return !(isStatus(Status.Progress) || isStatus(Status.Pause));
    }

    private void resumeTimer() {
        setStatus(Status.Progress);
        notifyChangeListeners();
    }

    private void pauseTimer() {
        setStatus(Status.Pause);
        notifyChangeListeners();
    }

    private void applyChanges() {
        setManualStage();
        notifyChangeListeners();
    }

    private void startTimer() {
        startValueProgress();
        setStatus(Status.Progress);
        notifyChangeListeners();
    }

    private void setManualStage() {
        setStatus(Status.Applied);
        setEditable(true);
    }

    private static final long[] BUTTONS = new long[] {0, 1};
}
