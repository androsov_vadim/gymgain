package com.vaisoft.health.gymgain.data;

import android.content.Context;
import androidx.core.util.Pair;
import android.util.Log;

import com.vaisoft.health.gymgain.sync.JSONLoader;
import com.vaisoft.health.gymgain.sync.Merger;
import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Sport
{
    private Sport(JSONArray exercisesArray, Body body) {
        parseExercises(exercisesArray, body);
    }

    public static Sport load(JSONLoader loader, Body body) {
        JSONArray exercisesArray = loader.loadArray(FILE_NAME, Utils.FileTarget.FILE_TARGET_DYNAMIC);
        return new Sport(exercisesArray, body);
    }

    public static void addSourceFilesToList(List<String> list) {
        list.add(FILE_NAME);
    }

    public List<Exercise> getExercises() {
        List<Exercise> result = new ArrayList<>();
        for (Exercise exercise: mExercises) {
            if (!exercise.isDeleted()) {
                result.add(exercise);
            }
        }
        return  result;
    }

    public void saveExercise(Context context, Exercise exercise, String previousId) {
        if (previousId == null) {
            addExercise(exercise);
            exercise.onCreated();
        } else {
            insertExercise(exercise, previousId);
            exercise.onUpdated();
        }
        saveAllExercises(context);
    }

    private void insertExercise(Exercise exercise, String previousId) {
        if (!previousId.equals(exercise.getId())) {
            removeExerciseFromMap(previousId);
        }
        for (int i = 0; i < mExercises.size(); i++) {
            if (mExercises.get(i).getId().equals(previousId)) {
                mExercises.set(i, exercise);
                addExerciseToMap(exercise);
                break;
            }
        }
    }

    private void addExercise(Exercise exercise) {
        if (isExerciseIdFree(exercise.getId())) {
            mExercises.add(exercise);
            addExerciseToMap(exercise);
        } else {
             insertExercise(exercise, exercise.getId());
        }
    }

    public String deleteExercise(Context context, String exerciseId) {
        String key = deleteExercise(exerciseId);
        saveAllExercises(context);
        return key;
    }

    private String deleteExercise(String exerciseId) {
        for (int i = 0; i < mExercises.size(); i++) {
            Exercise exercise = mExercises.get(i);
            if (exercise.getId().equals(exerciseId)) {
                if (!exercise.markAsDeleted()) {
                    mExercises.remove(i);
                }
                removeExerciseFromMap(exerciseId);
                return Source.getKey(exercise);
            }
        }
        return null;
    }

    public void saveAllExercises(Context context) {
        if (context != null) {
            JSONArray objects = new JSONArray();
            for (Exercise exercise: mExercises) {
                objects.put(exercise.writeToJson());
            }
            Utils.saveJSON(context, FILE_NAME, objects);
        }
    }

    public Exercise getExerciseRaw(long id) {
        for (Exercise exercise: mExercises) {
            if (exercise.isSameObject(id)) {
                return exercise;
            }
        }
        return  null;
    }

    public Exercise getExercise(String id) {
        for (Exercise exercise: mExercises) {
            if (exercise.isDeleted()) {
                continue;
            }
            if (exercise.getId().equals(id)) {
                return exercise;
            }
        }
        return  null;
    }

    public Exercise getExerciseByKey(String key) {
        return mExercisesMap.get(key);
    }

    private void parseExercises(JSONArray data, Body body) {
        if (data == null) {
            return;
        }
        try {
            for (int i=0; i < data.length(); i++)
            {
                JSONObject theObject = data.getJSONObject(i);
                Exercise newExercise = new Exercise(theObject);
                if (newExercise.validate(body)) {
                    addExercise(newExercise);
                }
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    public boolean isExercise(String key) {
        return  mExercisesMap.containsKey(key);
    }

    public boolean isExerciseIdFree(String key) {
        for (Exercise exercise: mExercises) { //including deleted
            if (exercise.getId().equals(key)) {
                return false;
            }
        }
        return true;
    }

    public List<Exercise> getDeletingExercises(Sport sport) {
        return Merger.getDeletingItems(mExercises, sport.mExercises);
    }

    public boolean merge(Sport mSport, List<Pair<Lifecycle, Lifecycle>> renamed) {
        Merger.Statistics statistics = new Merger.Statistics();
        mExercises = Merger.merge(mExercises, mSport.mExercises, statistics);
        if (statistics.isChanged()) {
            statistics.getRenamed(renamed);
            invalidateMap();
            return true;
        }
        return false;
    }

    private void invalidateMap() {
        mExercisesMap.clear();
        for (Exercise exercise: mExercises) {
            addExerciseToMap(exercise);
        }
    }

    private void removeExerciseFromMap(String id) {
        String key = Source.getKey(Source.Type.Exercise, id);
        mExercisesMap.remove(key);
    }

    private void addExerciseToMap(Exercise exercise) {
        if (!exercise.isDeleted()) {
            String key = Source.getKey(exercise);
            mExercisesMap.put(key, exercise);
        }
    }

    public boolean enableCloud() {
        boolean wasEnabled = false;
        for (Exercise exercise: mExercises) {
            if (exercise.enableCloud()) {
                wasEnabled = true;
            }
        }
        return wasEnabled;
    }

    public void undeleteExercise(Exercise deletedExercise) {
        boolean replaced = false;
        for (int i = 0; i < mExercises.size(); ++i) {
            Exercise exercise = mExercises.get(i);
            if (exercise.getId().equals(deletedExercise.getId())) {
                mExercises.set(i, deletedExercise);
                replaced = true;
                break;
            }
        }
        if (!replaced) {
            mExercises.add(deletedExercise);
        }
    }

    private List<Exercise> mExercises = new ArrayList<>();
    private Map<String, Exercise> mExercisesMap = new HashMap<>();

    private static final String FILE_NAME = "ex";
}
