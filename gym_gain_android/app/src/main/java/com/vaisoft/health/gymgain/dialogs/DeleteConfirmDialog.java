package com.vaisoft.health.gymgain.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.vaisoft.health.gymgain.R;

public class DeleteConfirmDialog extends DialogFragment
{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        
        builder.setTitle(R.string.editor_option_delete);

        Bundle bundle = getArguments();
        int messageData = bundle.getInt(MESSAGE);
        String message = getResources().getString(R.string.editor_delete_question_prefix) +
                " " + getResources().getString(messageData) + "?";
        builder.setMessage(message);

        builder.setIcon(R.drawable.ic_delete_black_24dp);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                if (listener != null) {
                    listener.onDeleteConfirmed();
                } else {
                    Fragment fragment = getTargetFragment();
                    fragment.onActivityResult(0, Activity.RESULT_OK, new Intent());
                }
            }
        });

        builder.setNegativeButton(R.string.cancel, null);

        return builder.create();
    }

    public interface Listener {
        void onDeleteConfirmed();
    }

    public static final String MESSAGE = "message";
}
