package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.LayoutRes;
import androidx.annotation.StringRes;
import androidx.fragment.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.coach.CoachConnect;
import com.vaisoft.health.gymgain.coach.CoachTalk;
import com.vaisoft.health.gymgain.controls.SpinnerSimpleAdapter;
import com.vaisoft.health.gymgain.data.BodyMeasure;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;

import java.util.ArrayList;

public abstract class BodyLogDialog extends DialogFragment implements CoachConnect {

    protected abstract void onOk(BodyMeasure.Pattern pattern);
    protected abstract @StringRes int getCaption();
    protected abstract @LayoutRes int getLayout();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getCaption());
        builder.setIcon(R.drawable.ic_accessibility_black_24dp);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BodyMeasure.Pattern pattern = new BodyMeasure.Pattern();
                pattern.part = getEnteredValue();
                pattern.unit = getUnitForIndex(mUnits.getSelectedItemPosition());
                pattern.targetInc = Value.getTargetForIndex(mTarget.getSelectedItemPosition());
                onOk(pattern);
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.setView(createInterface());

        return builder.create();
    }

    private String getEnteredValue() {
        String tag = (String) mName.getTag();
        String localizedTag = getCoach().getLocalized(tag);
        String text = mName.getText().toString();
        if (text.equals(localizedTag)) {
            return tag;
        } else {
            return text;
        }
    }

    @Override
    public void onDestroy() {
        mCoachTalk.unbind();
        super.onDestroy();
    }

    private View createInterface() {
        mView = View.inflate(getActivity(), getLayout(), null);

        mCoachTalk = new CoachTalk(getContext());
        mCoachTalk.bind(this);

        return mView;
    }

    protected View findViewById(@IdRes int id) {
        return mView.findViewById(id);
    }

    @Override
    public void onStart() {
        super.onStart();
        mOkButton = ((AlertDialog)getDialog()).getButton(Dialog.BUTTON_POSITIVE);
        mOkButton.setEnabled(false);

        mUnits = (Spinner) findViewById(R.id.metric_unit);
        mTarget = mView.findViewById(R.id.metric_target);
        mUsedIds = getArguments().getStringArrayList(USED_ITEMS);

        initialize();
    }

    @Override
    public void onCoachConnected() {
        initialize();
    }

    private void initialize() {
        if (mOkButton == null || getCoach() == null) {
            return;
        }
        mUnits.setAdapter(new UnitsAdapter(getContext()));
        initNameEditor();
        validate(mName.getText().toString());
    }

    private void initNameEditor() {
        mName = (EditText) mView.findViewById(R.id.metric_name);
        mName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = s.toString();
                validate(text);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    protected void validate(String name) {
        validate(!(name.isEmpty() || isNameInUse(name)));
    }

    protected void validate(boolean valid) {
        TextView errText = (TextView) mView.findViewById(R.id.metric_used_error);
        errText.setVisibility(valid ? View.GONE : View.VISIBLE);
        mOkButton.setEnabled(valid);
    }

    protected Coach getCoach() {
        return mCoachTalk.getCoach();
    }

    private static int getUnitIndex(Unit unit) {
        switch (unit) {
            case size: return UNITS_SIZE_INDEX;
            case weight: return UNITS_WEIGHT_INDEX;
        }
        return -1;
    }

    private static Unit getUnitForIndex(int index) {
        switch (index) {
            case UNITS_SIZE_INDEX: return Unit.size;
            case UNITS_WEIGHT_INDEX: return Unit.weight;
        }
        return Unit.undefined;
    }

    protected boolean isNameInUse(String name) {
        for (String id: mUsedIds) {
            if (id.equals(name)) {
                return true;
            }
            String localized = getCoach().getLocalized(id);
            if (localized.equals(name)) {
                return true;
            }
        }
        return false;
    }

    protected void setName(String name) {
        mName.setTag(name);
        mName.setText(getCoach().getLocalized(name));
    }

    protected void setUnit(Unit unit, boolean editable) {
        mUnits.setSelection(getUnitIndex(unit));
        if (!editable) {
            mUnits.setEnabled(false);
        }
    }

    protected  void setTargetInc(boolean targetInc) {
        mTarget.setSelection(Value.getTargetIndex(targetInc));
    }

    //------------------------------------------------------------------------------

    private class UnitsAdapter extends SpinnerSimpleAdapter {

        public UnitsAdapter(Context context) {
            super(context);
            mWeight = getCoach().getLocalized(Unit.weight);
            mSize = getCoach().getLocalized(Unit.size);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public String getItem(int position) {
            switch (position) {
                case UNITS_WEIGHT_INDEX:
                    return mWeight;
                case UNITS_SIZE_INDEX:
                    return mSize;
            }
            return "?";
        }

        private String mWeight;
        private String mSize;

    }

    private CoachTalk mCoachTalk;
    private View mView;
    private Spinner mUnits;
    private EditText mName;
    private ArrayList<String> mUsedIds;
    private Button mOkButton;
    private Spinner mTarget;

    private static final int UNITS_WEIGHT_INDEX = 0;
    private static final int UNITS_SIZE_INDEX = 1;

    public static final String USED_ITEMS = "used";
}
