package com.vaisoft.health.gymgain.coach;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;

import com.vaisoft.health.gymgain.data.Config;
import com.vaisoft.health.gymgain.utils.UpdateListener;

public class CoachService extends JobIntentService {

    public static final String INTENT_UPDATE="com.vaisoft.Update";

    public CoachService() {

    }

    public static void updateCheckout(Context context, int hours) {
        if (Config.setCheckpointHours(context, hours)) {
            subscribe(context,true);
        }
    }

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, CoachService.class, JOB_ID, work);
    }

    @Override
    public int onStartCommand(@Nullable Intent intent, int flags, int startId) {
        onHandleWork(intent);
        return START_NOT_STICKY;
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        switch (intent.getAction()) {
            case INTENT_UPDATE:
                onUpdate();
                break;
        }
    }

    private void onUpdate() {
        Coach.runBackgroundUpdate(this);
    }

    @Override
    public IBinder onBind(@NonNull Intent intent) {
        IBinder binder = super.onBind(intent);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return new Binder();
        } else {
            return binder;
        }
    }

    public static void scheduleUpdates(Context context) {
        subscribe(context, false);
    }

    private static void subscribe(Context context, boolean force) {
        Intent intent = new Intent(context, UpdateListener.class);
        intent.setAction(INTENT_UPDATE);
        PendingIntent alarmIntent;
        if (!force) {
            alarmIntent = PendingIntent.getBroadcast(context, 0, intent,
                    PendingIntent.FLAG_NO_CREATE);
            if (alarmIntent != null) {
                return;
            }
        }
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        if (alarmMgr != null) {
            alarmIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmMgr.setInexactRepeating(AlarmManager.RTC, Config.getCheckoutUtcMillis(context),
                    AlarmManager.INTERVAL_DAY, alarmIntent);
        }
    }

    static final int JOB_ID = 26071982;
}
