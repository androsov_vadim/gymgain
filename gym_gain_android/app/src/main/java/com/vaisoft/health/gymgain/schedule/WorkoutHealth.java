package com.vaisoft.health.gymgain.schedule;

public enum WorkoutHealth {
    Critical,
    Notify,
    Healthy;

    public boolean showFeed() {
        return (this == Critical) || (this == Notify);
    }

    public boolean showWarning() {
        return this == Critical;
    }
}

