package com.vaisoft.health.gymgain.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.coach.CoachService;

public class UpdateListener extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent updateIntent = new Intent(context, Coach.class);
        updateIntent.setAction(CoachService.INTENT_UPDATE);
        CoachService.enqueueWork(context, updateIntent);
    }
}
