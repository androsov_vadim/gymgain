package com.vaisoft.health.gymgain.data;

import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public abstract class Lifecycle {

    public Lifecycle() {
        mDeleted = false;
        mCloud = false;
        mLastUpdate = 0;
    }

    public boolean isDeleted() {
        return mDeleted;
    }

    public boolean enableCloud() {
        if (mCloud) {
            return false;
        }
        mCloud = true;
        return true;
    }

    protected void saveLifecycle(JSONObject target) throws JSONException {
        target.put(DELETED_FIELD, mDeleted);
        target.put(CLOUD_FIELD, mCloud);
        target.put(UPDATED_FIELD, mLastUpdate);
        target.put(CREATED_FIELD, mCreated);
    }

    protected void loadLifecycle(JSONObject source) throws JSONException {
        if (source.has(DELETED_FIELD)) {
            mDeleted = source.getBoolean(DELETED_FIELD);
        } else {
            mDeleted = false;
        }
        if (source.has(CLOUD_FIELD)) {
            mCloud = source.getBoolean(CLOUD_FIELD);
        } else {
            mCloud = false;
        }
        if (source.has(UPDATED_FIELD)) {
            mLastUpdate = source.getLong(UPDATED_FIELD);
        } else {
            mLastUpdate = 0;
        }
        if (source.has(CREATED_FIELD)) {
            mCreated = source.getLong(CREATED_FIELD);
        } else {
            mCreated = 0;
        }
    }

    protected boolean markAsDeleted() {
        if (isLocalOnly()) {
            return false;
        }
        mDeleted = true;
        onUpdated();
        return true;
    }

    private boolean isLocalOnly() {
        return !(mCloud || (mCreated == 0));//created=0 for predefined data
    }

    protected void onUpdated() {
        mLastUpdate = Utils.now().getMillis();
    }

    protected void onCreated() {
        mCreated = Utils.now().getMillis();
        onUpdated();
    }

    public boolean isSameObject(Lifecycle target) {
        return isSameObject(target.mCreated);
    }

    public boolean isSameObject(long targetCreated) {
        return mCreated == targetCreated;
    }

    public long getLifecycleUniqueId() {
        return mCreated;
    }

    public void setLifecycleUniqueId(long time) {
        mCreated = time;
    }

    public boolean isNewer(Lifecycle target) {
        return mLastUpdate > target.mLastUpdate;
    }

    public void testSetCreatedTime(long time) {
        mCreated = time;
        mLastUpdate = time;
    }

    public long testGetUpdatedTime() {
        return mLastUpdate;
    }

    public void testSetUpdatedTime(long value) {
        mLastUpdate = value;
    }

    public void testSetCloud() {
        mCloud = true;
    }

    public static <T extends Lifecycle> int indexOf(List<T> source, T target) {
        int index = 0;
        for (T item: source) {
            if (item.isDeleted()) {
                continue;
            }
            if (item == target) {
                return index;
            }
            ++index;
        }
        return -1;
    }

    public static <T extends Lifecycle> T get(List<T> source, int targetIndex) {
        int index = 0;
        for (T item: source) {
            if (item.isDeleted()) {
                continue;
            }
            if (index == targetIndex) {
                return item;
            }
            ++index;
        }
        return null;
    }

    public String getId() {
        return "";
    }

    public void setId(String id) {

    }

    private long mLastUpdate;
    private long mCreated;
    private boolean mDeleted;
    private boolean mCloud;

    private static final String CREATED_FIELD = "lc_create";
    private static final String DELETED_FIELD = "lc_delete";
    private static final String UPDATED_FIELD = "lc_update";
    private static final String CLOUD_FIELD = "lc_cloud";
}
