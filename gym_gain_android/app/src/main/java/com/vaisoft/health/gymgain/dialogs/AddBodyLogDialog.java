package com.vaisoft.health.gymgain.dialogs;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.SpinnerSimpleAdapter;
import com.vaisoft.health.gymgain.data.BodyMeasure;
import com.vaisoft.health.gymgain.edit.BodyLogActivity;

public class AddBodyLogDialog extends BodyLogDialog {

    @Override
    protected void onOk(BodyMeasure.Pattern pattern) {
        BodyLogActivity activity = (BodyLogActivity) getActivity();
        activity.addPattern(pattern);
    }

    @Override
    protected int getCaption() {
        return R.string.add_metric;
    }

    @Override
    protected int getLayout() {
        return R.layout.add_body_metric;
    }

    @Override
    public void onCoachConnected() {
        super.onCoachConnected();
        mBodyMeasurer = getCoach().getBodyMeasurer();

        final Spinner patterns = (Spinner) findViewById(R.id.metric_pattern);
        patterns.setAdapter(new PatternAdapter(getContext()));
        patterns.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    applyPattern(position - 1);
                    patterns.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void applyPattern(int patternIndex) {
        BodyMeasure.Pattern pattern = mBodyMeasurer.getPattern(patternIndex);
        setName(pattern.part);
        setUnit(pattern.unit, true);
        setTargetInc(pattern.targetInc);

        validate(!isBodyMeasurePatternInUse(patternIndex));
    }

    private boolean isBodyMeasurePatternInUse(int index) {
        String id = mBodyMeasurer.getPattern(index).part;
        return isNameInUse(id) || isNameInUse(getCoach().getLocalized(id));
    }

    //------------------------------------------------------------------------------

    private class PatternAdapter extends SpinnerSimpleAdapter {

        public PatternAdapter(Context context) {
            super(context);
            mSelectPatternPrompt = getResources().getString(R.string.metric_select_pattern);
        }

        @Override
        public int getCount() {
            return mBodyMeasurer.getPatternsCount() + 1;
        }

        @Override
        public String getItem(int position) {
            if (position == 0) {
                return mSelectPatternPrompt;
            }
            BodyMeasure.Pattern pattern = mBodyMeasurer.getPattern(position - 1);
            return getCoach().getLocalized(pattern.part);
        }

        private String mSelectPatternPrompt;
    }

    private BodyMeasure mBodyMeasurer;
}
