package com.vaisoft.health.gymgain.settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.billing.Billing;
import com.vaisoft.health.gymgain.controls.CoachFragment;
import com.vaisoft.health.gymgain.controls.RotateFriendlyPagerAdapter;
import com.vaisoft.health.gymgain.edit.CoachActivity;

public class SettingsActivity extends CoachActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(TAB, mTabLayout.getSelectedTabPosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mSavedTab = savedInstanceState.getInt(TAB);
    }

    public void setLocked(boolean locked) {
        mLocked = locked;
    }

    @Override
    public void onBackPressed() {
        if (mLocked) {
            return;
        }
        getCoach().saveConfig(this);
        super.onBackPressed();
    }

    @Override
    public void onCoachConnected() {
        setContentView(R.layout.activity_settings);

        initBilling();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.menu_settings);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        mPageAdapter = new Adapter(getSupportFragmentManager());
        viewPager.setAdapter(mPageAdapter);
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(viewPager);
        mTabLayout.getTabAt(SportFragment.TAB).setIcon(R.drawable.ic_fitness_center_white_24dp);
        mTabLayout.getTabAt(SystemFragment.TAB).setIcon(R.drawable.ic_settings_white_24dp);
        mTabLayout.getTabAt(CloudFragment.TAB).setIcon(R.drawable.ic_cloud_white_24dp);

        mTabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                int tabIndex = tab.getPosition();
                notifyTabSelected(tabIndex);
            }
        });

        initActiveTab();
    }

    private void initBilling() {
        mBilling = new Billing(this, false, new Billing.ReadyListener() {
            @Override
            public void onBillingReady() {
                applyBilling();
            }
        });
        mBilling.bind();
    }

    private void applyBilling() {

    }

    public boolean checkProOrBetter() {
        boolean allow = (mBilling != null) && mBilling.isOk() && mBilling.isProOrBetter();
        if (allow) {
            return true;
        }
        Billing.startShop(this);
        return false;
    }

    @Override
    protected void onDestroy() {
        mBilling.unbind();
        super.onDestroy();
    }

    private void notifyTabSelected(int tabIndex) {
        SettingsTabFragment fragment = (SettingsTabFragment) mPageAdapter.getFragment(tabIndex);
        mAciveTab = tabIndex;
        if (fragment != null) {
            fragment.onTabSelected();
        }
    }

    public int getActiveTab() {
        return mAciveTab;
    }

    private void initActiveTab() {
        int activeTab;
        if (mSavedTab != -1) {
            activeTab = mSavedTab;
            mSavedTab = -1;
        } else {
            activeTab = getIntent().getIntExtra(TAB, -1);
        }
        mTabLayout.getTabAt(activeTab).select();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = mPageAdapter.getFragment(mTabLayout.getSelectedTabPosition());
        fragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void openCloudTab() {
        mTabLayout.getTabAt(CloudFragment.TAB).select();
    }

    //--------------------------------------------------------------------------

    private class Adapter extends RotateFriendlyPagerAdapter {

        public Adapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        protected CoachFragment getItem(int position) {
            switch (position) {
                case SportFragment.TAB:
                    return new SportFragment();
                case SystemFragment.TAB:
                    return new SystemFragment();
                case CloudFragment.TAB:
                    return new CloudFragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case SportFragment.TAB:
                    return getString(R.string.tab_settings_sport);
                case SystemFragment.TAB:
                    return getString(R.string.tab_settings_system);
                case CloudFragment.TAB:
                    return getString(R.string.tab_settings_cloud);
            }
            return null;
        }
    }

    public static Intent getStartIntentCloud(Context context) {
        return getStartIntent(context, CloudFragment.TAB);
    }

    public static Intent getStartIntentConfig(Context context) {
        return getStartIntent(context, SportFragment.TAB);
    }

    private static Intent getStartIntent(Context context, int tab) {
        Intent intent = new Intent(context, SettingsActivity.class);
        intent.putExtra(SettingsActivity.TAB, tab);
        return intent;
    }


    private int mSavedTab = -1;
    private Adapter mPageAdapter;
    private TabLayout mTabLayout;
    private boolean mLocked;
    private int mAciveTab;
    private Billing mBilling;

    private static final String TAB = "tab";
}
