package com.vaisoft.health.gymgain.utils;

import android.os.Handler;

public class Ticker implements Runnable {

    public Ticker(Listener listener, long delay) {
        mListener = listener;
        mDelay = delay;
    }

    public void start() {
        if (mStarted) {
            return;
        }
        mStarted = true;
        run();
    }

    public void stop() {
        mStarted = false;
    }

    @Override
    public void run() {
        if (mStarted) {
            mListener.tick();
            mTick.postDelayed(this, mDelay);
        }
    }

    public interface Listener {
        void tick();
    }

    private boolean mStarted;
    private Listener mListener;
    private long mDelay;
    private Handler mTick = new Handler();
}
