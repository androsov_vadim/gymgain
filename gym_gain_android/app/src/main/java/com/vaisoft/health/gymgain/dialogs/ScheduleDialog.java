package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.RecordScheduleView;
import com.vaisoft.health.gymgain.coach.CoachConnect;
import com.vaisoft.health.gymgain.coach.CoachTalk;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.schedule.Schedule;

public class ScheduleDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.plan_training);
        builder.setIcon(R.drawable.ic_schedule_black_24dp);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Schedule schedule = mContent.save();
                Listener listener = (Listener) getActivity();
                if (schedule != null) {
                    if (schedule.getType() != Schedule.Type.NOT_SCHEDULED) {
                        listener.onScheduleUpdated(schedule);
                    } else {
                        listener.onScheduleDeleted();
                    }
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, null);

        mContent = new RecordScheduleView(getContext());
        builder.setView(mContent);

        mConnector = new CoachTalk(getContext());
        mConnector.bind(new CoachConnect() {
            @Override
            public void onCoachConnected() {
                Schedule schedule = null;
                Bundle bundle = getArguments();
                if (bundle != null) {
                    String recordKey = bundle.getString(RECORD);
                    if (recordKey != null) {
                        Record record = mConnector.getCoach().getRecordByKey(recordKey);
                        schedule = record.getSchedule();
                    }
                }
                mContent.setSchedule(schedule);
            }
        });

        return builder.create();
    }

    @Override
    public void onDestroy() {
        mConnector.unbind();
        super.onDestroy();
    }

    public interface Listener {
        void onScheduleUpdated(@NonNull Schedule schedule);
        void onScheduleDeleted();
    }

    private CoachTalk mConnector;
    private RecordScheduleView mContent;
    public static final String RECORD = "record";
}
