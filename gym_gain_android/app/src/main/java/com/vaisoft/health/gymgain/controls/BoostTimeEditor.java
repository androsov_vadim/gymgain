package com.vaisoft.health.gymgain.controls;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.utils.Utils;

public class BoostTimeEditor extends BoostEditor {

    public BoostTimeEditor(Context context) {
        super(context);
        init();
    }

    public BoostTimeEditor(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BoostTimeEditor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        Activity activity = getActivity();
        if (activity == null) {
            Log.e(getClass().getName(), "Can't access parent activity");
            return;
        }
        EditText editText = getEditTextField();
        if (editText == null) {
            Log.e(getClass().getName(), "Can't access text edit field");
            return;
        }
        if (activity.findViewById(R.id.keyboard_time) != null) {
            mTimeKeyboard = new TimeKeyboard(activity, R.id.keyboard_time, R.xml.keyboard_time);
            mTimeKeyboard.registerEditText(editText);
        }
    }

    private Activity getActivity() {
        Context context = getContext();
        return Utils.getActivity(context);
    }

    @Override
    protected long[] getSteps() {
        return STEPS;
    }

    @Override
    protected String getStepAsText(long step) {
        int textResource = 0;
        if (step == STEP_TENTH) {
            textResource = R.string.abbrev_tenth;
        } else if (step == STEP_SECOND) {
            textResource = R.string.abbrev_second;
        } else if (step == STEP_MINUTE) {
            textResource = R.string.abbrev_minute;
        }
        return  getResources().getString(textResource);
    }

    @Override
    protected String getValueAsText(long value) {
        return Unit.time.toString(value);
    }

    @Override
    protected long getTextAsValue(String text) {
        return Unit.time.parseString(text);
    }

    @Override
    protected void setUnit(Unit unit) {
        if (unit != Unit.time) {
            Log.e(getClass().getName(), "Attempt to edit " + unit.toString() + " with time editor");
        }
    }

    @Override
    protected void setSoftKeyboardVisible(boolean visible) {
        if (mTimeKeyboard == null) {
            super.setSoftKeyboardVisible(visible);
            return;
        }
        EditText editText = getEditTextField();
        if (visible) {
            mTimeKeyboard.showCustomKeyboard(editText);
        } else {
            mTimeKeyboard.hideCustomKeyboard();
        }
    }

    @Override
    protected int getValueInputType() {
        if (mTimeKeyboard != null) {
            return InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
        } else {
            return Unit.time.getValueInputType();
        }
    }

    private TimeKeyboard mTimeKeyboard;

    private static final long STEP_TENTH = 100;
    private static final long STEP_SECOND = 1000;
    private static final long STEP_MINUTE = 1000 * 60;
    private static long[] STEPS = {STEP_TENTH, STEP_SECOND, STEP_MINUTE};
}
