package com.vaisoft.health.gymgain.data;

import android.content.Context;
import android.util.Log;

import com.vaisoft.health.gymgain.feed.BodyLogFeed;
import com.vaisoft.health.gymgain.feed.Feed;
import com.vaisoft.health.gymgain.sync.JSONLoader;
import com.vaisoft.health.gymgain.sync.Merger;
import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BodyMeasure {

    private BodyMeasure(JSONArray source) throws JSONException {
        mPatterns = new ArrayList<>();
        mLog = new ArrayList<>();
        if (source == null) {
            return;
        }
        for (int i=0; i < source.length(); i++) {
            JSONObject theObject = source.getJSONObject(i);
            Pattern pattern = new Pattern();
            pattern.part = theObject.getString(LOG_ITEMS_OBJECT);
            pattern.unit = Unit.valueOf(theObject.getString(LOG_ITEMS_UNIT));
            pattern.targetInc = Value.getTargetValue(theObject.getString(LOG_ITEMS_TARGET));
            mPatterns.add(pattern);
        }
    }

    public static BodyMeasure load(JSONLoader loader) {
        JSONArray patterns = loader.loadArray(PATTERNS_FILE_NAME, Utils.FileTarget.FILE_TARGET_DYNAMIC);
        try {
            BodyMeasure bodyMeasure = new BodyMeasure(patterns);
            JSONArray log = loader.loadArray(LOG_FILE_NAME, Utils.FileTarget.FILE_TARGET_PRIVATE);
            if (log != null) {
                bodyMeasure.loadLog(log);
            }
            return bodyMeasure;
        } catch (JSONException e) {
            Log.e(BodyMeasure.class.getName(), e.getMessage());
            return null;
        }
    }

    public static void addSourceFilesToList(List<String> list) {
        list.add(PATTERNS_FILE_NAME);
        list.add(LOG_FILE_NAME);
    }

    public void loadLog(JSONArray source)  throws JSONException {
        for (int i=0; i < source.length(); i++) {
            JSONObject theObject = source.getJSONObject(i);
            LogEntry entry = new LogEntry(theObject);
            mLog.add(entry);
        }
    }

    public void saveLog(Context context) {
        JSONArray target = new JSONArray();
        try {
            for (LogEntry entry: mLog) {
                JSONObject entryObject = entry.save();
                target.put(entryObject);
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
        Utils.saveJSON(context, LOG_FILE_NAME, target);
    }

    public int getPatternsCount() {
        return mPatterns.size();
    }

    public Pattern getPattern(int index) {
        return mPatterns.get(index);
    }

    public LogEntry getLogEntry(int index) {
        return Lifecycle.get(mLog, index);
    }

    public LogEntry getLogEntryRaw(int index) {
        return mLog.get(index);
    }

    public int getRawLogSize() {
        return mLog.size();
    }

    public int getValidLogSize() {
        int result = 0;
        for (LogEntry entry: mLog) {
            if (!entry.isDeleted()) {
                ++result;
            }
        }
        return result;
    }

    public void log(List<LogItem> entryItems, LogEntry entry) {
        entry.items = entryItems;
        entry.onUpdated();
    }

    public void log(LogEntry entry) {
        entry.onCreated();
        mLog.add(entry);
    }

    public void unlog(LogEntry entry) {
        if (!entry.markAsDeleted()) {
            mLog.remove(entry);
        }
    }

    public LogItem createLogItem(Pattern pattern, Config config) {
        LogItem result = new LogItem();
        result.object = pattern.part;
        result.unitGeneral = pattern.unit;
        switch (result.unitGeneral) {
            case size:
                result.value = new Value(config.getBodyUnits(), pattern.targetInc);
                break;
            case weight:
                result.value = new Value(config.getWeightUnits(), pattern.targetInc);
                break;
            case distance:
                result.value = new Value(config.getDistanceUnits(), pattern.targetInc);
                break;
            default:
                result.value = null;
        }
        return result;
    }

    public LogItem getPreviousTo(LogEntry entry, LogItem item) {
        int entryIndex = getEntryIndex(entry);
        if (entryIndex == 0) {
            return null;
        }
        if (entryIndex == -1) {
            entryIndex = mLog.size();
        }
        for (int i = entryIndex - 1; i >= 0; i--) {
            LogEntry currentEntry = mLog.get(i);
            if (currentEntry.isDeleted()) {
                continue;
            }
            LogItem currentItem = currentEntry.getSameItem(item);
            if (currentItem != null) {
                return currentItem;
            }
        }
        return null;
    }

    public ProgressStatistics getProgressStatistics(LogEntry entry) {
        ProgressStatistics result = new ProgressStatistics();
        for (LogItem item: entry.items) {
            LogItem previous = getPreviousTo(entry, item);
            if (previous == null) {
                result.addGood(item.object);
            } else {
                Progress progress = item.getProgress(previous);
                switch (progress.getEstimate()) {
                    case GOOD:
                        result.addGood(item.object);
                        break;
                    case BAD:
                        result.addBad(item.object);
                        break;
                    case SOSO:
                        result.addSoso(item.object);
                        break;
                }
            }
        }
        return result;
    }

    public int getTotalProgress(LogEntry entry) {
        int result = 0;
        for (LogItem item: entry.items) {
            LogItem previous = getPreviousTo(entry, item);
            if (previous == null) {
                result++;
            } else {
                Progress progress = item.getProgress(previous);
                switch (progress.getEstimate()) {
                    case GOOD:
                        result++;
                        break;
                    case BAD:
                        result--;
                        break;
                }
            }
        }
        return result;
    }

    public int getTotalProgressSafe(int entryIndex) {
        int i = 0;
        for (LogEntry entry: mLog) {
            if (entry.isDeleted()) {
                continue;
            }
            if (i == entryIndex) {
                return getTotalProgress(entry);
            }
            ++i;
        }
        return 0;
    }

    public int getEntryIndex(LogEntry entry) {
        return Lifecycle.indexOf(mLog, entry);
    }

    public List<Feed> getFeed(Context context) {
        List<Feed> result = new ArrayList<>(mLog.size());
        for (LogEntry entry: mLog) {
            if (!entry.isDeleted()) {
                result.add(0, new BodyLogFeed(context, entry, this));
            }
        }
        return result;
    }

    public LogEntry getLastEntry() {
        for (int i = mLog.size() - 1; i >= 0; --i) {
            LogEntry entry = mLog.get(i);
            if (!entry.isDeleted()) {
                return entry;
            }
        }
        return null;
    }

    public boolean enableCloud() {
        boolean wasEnabled = false;
        for (LogEntry entry: mLog) {
            if (entry.enableCloud()) {
                wasEnabled = true;
            }
        }
        return wasEnabled;
    }

    public boolean merge(BodyMeasure bodyMeasure) {
        Merger.Statistics statistics = new Merger.Statistics();
        mLog = Merger.merge(mLog, bodyMeasure.mLog, statistics);
        return statistics.isChanged();
    }

    public void renameItem(String source, String target) {
        for (LogEntry entry: mLog) {
            if (entry.isDeleted()) {
                continue;
            }
            for (LogItem item: entry.items) {
                item.tryRename(source, target);
            }
        }
    }

    //------------------------------------------------------------------------------------

    public static class Pattern {
        public String part;
        public Unit unit;
        public boolean targetInc;
    }

    public static class LogItem implements Cloneable {
        public String object;
        public Unit unitGeneral;
        public Value value;

        public LogItem clone() {
            LogItem newItem = new LogItem();

            newItem.object = object;
            newItem.unitGeneral = unitGeneral;
            newItem.value = value.clone();

            return newItem;
        }

        public boolean isSame(LogItem item) {
            return object.equals(item.object) && (unitGeneral == item.unitGeneral) &&
                    (value.unit().getCategory() == item.value.unit().getCategory());
        }

        public Progress getProgress(LogItem previous) {
            Unit unit = value.unit();
            long previousValue = previous.value.get(unit);
            if (value.get(unit) == previousValue) {
                return Progress.stable();
            } else {
                return Progress.changed(value.get(unit) > previousValue, value.shouldGrow());
            }
        }

        public void tryRename(String source, String target) {
            if (object.equals(source)) {
                object = target;
            }
        }
    }

    public static class LogEntry extends Lifecycle {
        public DateTime date;
        public List<LogItem> items;

        public LogEntry() {

        }

        public LogEntry(JSONObject source) throws JSONException {
            loadLifecycle(source);
            if (!isDeleted()) {
                date = new DateTime(source.getLong(LOG_DATE));
                JSONArray sourceItems = source.getJSONArray(LOG_ITEMS);
                items = loadLogItems(sourceItems);
            }
        }

        private List<LogItem> loadLogItems(JSONArray source) throws JSONException {
            List<LogItem> items = new ArrayList<>();
            for (int i=0; i < source.length(); i++) {
                JSONObject theObject = source.getJSONObject(i);
                LogItem item = new LogItem();
                item.object = theObject.getString(LOG_ITEMS_OBJECT);
                item.unitGeneral = Unit.valueOf(theObject.getString(LOG_ITEMS_UNIT_GENERAL));
                item.value = new Value(theObject);
                items.add(item);
            }
            return items;
        }


        public LogItem getSameItem(LogItem item) {
            for (LogItem i: items) {
                if (item.isSame(i)) {
                    return i;
                }
            }
            return null;
        }

        public JSONObject save() throws JSONException {
            JSONObject entryObject = new JSONObject();
            saveLifecycle(entryObject);
            if (!isDeleted()) {
                entryObject.put(LOG_DATE, date.getMillis());
                JSONArray savedItems = saveLogItems(items);
                entryObject.put(LOG_ITEMS, savedItems);
            }
            return entryObject;
        }

        private JSONArray saveLogItems(List<LogItem> items) throws JSONException {
            JSONArray itemsObject = new JSONArray();
            for (LogItem item : items) {
                JSONObject itemObject = new JSONObject();
                itemObject.put(LOG_ITEMS_OBJECT, item.object);
                itemObject.put(LOG_ITEMS_UNIT_GENERAL, item.unitGeneral.toString());
                item.value.save(itemObject);
                itemsObject.put(itemObject);
            }
            return itemsObject;
        }
    }

    //--------------------------------------------------------------------------------------

    private List<Pattern> mPatterns;
    private List<LogEntry> mLog;

    private static final String LOG_DATE="date";
    private static final String LOG_ITEMS="items";

    private static final String LOG_ITEMS_OBJECT="object";
    private static final String LOG_ITEMS_UNIT_GENERAL="general";
    private static final String LOG_ITEMS_UNIT="unit";
    private static final String LOG_ITEMS_TARGET="target";

    private static final String PATTERNS_FILE_NAME="body_measure";
    private static final String LOG_FILE_NAME="body_log";
}
