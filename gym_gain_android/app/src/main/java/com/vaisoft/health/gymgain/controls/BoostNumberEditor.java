package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.util.AttributeSet;

import com.vaisoft.health.gymgain.data.Unit;

public class BoostNumberEditor extends BoostEditor {

    public BoostNumberEditor(Context context) {
        super(context);
    }

    public BoostNumberEditor(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BoostNumberEditor(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected long[] getSteps() {
        int[] intResult = mUnit.getStep();
        long[] longResult = new long[intResult.length];
        for (int i = 0; i < intResult.length; i++) {
            longResult[i] = intResult[i];
        }
        return longResult;
    }

    @Override
    protected String getStepAsText(long step) {
        return mUnit.toString(step);
    }

    @Override
    protected String getValueAsText(long value) {
        return mUnit.toString(value);
    }

    @Override
    protected long getTextAsValue(String text) {
        return mUnit.parseString(text);
    }

    @Override
    protected void setUnit(Unit unit) {
        mUnit = unit;
    }

    @Override
    protected int getValueInputType() {
        return mUnit.getValueInputType();
    }

    private Unit mUnit;
}
