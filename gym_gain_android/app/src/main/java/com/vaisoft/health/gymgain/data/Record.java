package com.vaisoft.health.gymgain.data;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.Nullable;
import android.util.Log;

import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.schedule.Schedule;
import com.vaisoft.health.gymgain.schedule.ScheduleAnalyser;
import com.vaisoft.health.gymgain.sync.JSONLoader;
import com.vaisoft.health.gymgain.utils.Utils;
import com.vaisoft.health.gymgain.workout.WorkoutActivityView;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class Record extends Lifecycle implements Cloneable {

    public Record(Training training) {
        this(Source.createTraining(training.getId()));
    }

    public Record(Exercise exercise) {
        this(Source.createExercise(exercise.getId()));
    }

    private Record(Source source) {
        mSource = source;
        onCreated();
    }

    public static Record createTraining(String id) {
        Source source = Source.createTraining(id);
        return new Record(source);
    }

    public static Record createExercise(String id) {
        Source source = Source.createExercise(id);
        return new Record(source);
    }

    public static void load(JSONLoader loader, OnLoadedListener listener) {
        JSONArray recs = loader.loadArray(FILE_NAME, Utils.FileTarget.FILE_TARGET_PRIVATE);
        if (recs == null) {
            return;
        }
        try {
            for (int i=0; i < recs.length(); i++) {
                JSONObject theObject = recs.getJSONObject(i);
                Record newRecord = new Record(theObject);
                listener.onLoaded(newRecord);
            }
        } catch (JSONException e) {
            Log.e(Record.class.getName(), e.getMessage());
        }
    }

    public static void addSourceFilesToList(List<String> list) {
        list.add(FILE_NAME);
    }

    public Record(JSONObject source) {
        try {
            mSource = new Source(source);
            loadLifecycle(source);
            if (!isDeleted()) {
                mSchedule = new Schedule(source.getJSONObject(FIELD_PLAN));
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    public void save(JSONArray container) {
        JSONObject target = new JSONObject();
        try {
            mSource.save(target);
            saveLifecycle(target);
            if (!isDeleted()) {
                saveData(target);
            }
            container.put(target);
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    private void saveData(JSONObject target) throws JSONException {
        JSONObject plan = mSchedule.save();
        target.put(FIELD_PLAN, plan);
    }

    public Source getSource() {
        return mSource;
    }

    public boolean isForTraining(String id) {
        return mSource.isForTraining(id);
    }

    public boolean isForExercise(String id) {
        return mSource.isForExercise(id);
    }

    public boolean invalidateExercise(String id, String previousId) {
        if (mSource.isForExercise(previousId)) {
            mSource = Source.createExercise(id);
            onUpdated();
            return true;
        }
        return false;
    }

    public boolean invalidateTraining(String id, String previousId) {
        if (mSource.isForTraining(previousId)) {
            mSource = Source.createTraining(id);
            onUpdated();
            return true;
        }
        return false;
    }

    public String getName(Coach coach) {
        return coach.getLocalized(mSource.getId());
    }

    public Workout createWorkout(Coach coach) {
        if (mSource.isExercise()) {
            Exercise exercise = coach.getExerciseByKey(mSource.getKey());
            return new Workout(exercise);
        } else if (mSource.isTraining()) {
            Training training = coach.getTrainingByKey(mSource.getKey());
            return new Workout(training);
        }
        return null;
    }

    @Nullable
    public Schedule getSchedule() {
        return mSchedule;
    }

    public void setSchedule(Schedule schedule) {
        mSchedule = schedule;
        onUpdated();
    }

    public String getDescription(Context context, Coach coach) {
        StringBuilder result = new StringBuilder();
        result.append(getName(coach));
        result.append(": ");
        result.append(mSchedule.getDescription(context.getResources()));
        return result.toString();
    }

    public void startNewWorkout(Context context, Coach coach) {
        coach.prepareWorkout(createWorkout(coach));
        context.startActivity(new Intent(context, WorkoutActivityView.class));
    }

    public boolean isExercise() {
        return mSource.isExercise();
    }

    public boolean isTraining() {
        return mSource.isTraining();
    }

    public ScheduleAnalyser createScheduleAnalyser(DateTime date) {
        return mSchedule.createAnalyser(date);
    }

    public void onBurnoutsAnalysed(DateTime lastAnalysed) {
        mSchedule.onBurnoutsAnalysed(lastAnalysed);
    }

    public boolean shouldAnalyseOnBurnouts(DateTime date) {
        return mSchedule.shouldAnalyseOnBurnouts(date);
    }

    public static void saveAll(Context context, Map<String, Record> records) {
        JSONArray objects = new JSONArray();
        for (Map.Entry<String, Record> record : records.entrySet()) {
            record.getValue().save(objects);
        }
        Utils.saveJSON(context, FILE_NAME, objects);
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Record: ");
        result.append(mSource.getId());
        result.append(" (");
        result.append(isExercise() ? "exercise" : "workout");
        result.append(')');
        if (isDeleted()) {
            result.append(" deleted");
        }
        return result.toString();
    }

    //-----------------------------------------------------------------------------

    public interface OnLoadedListener {
        void onLoaded(Record record);
    }

    private Schedule mSchedule;
    private Source mSource;

    private static final String FIELD_PLAN="plan";
    private static final String FILE_NAME="rec";
}
