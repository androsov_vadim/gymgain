package com.vaisoft.health.gymgain.sync;

import androidx.core.util.Pair;

import com.vaisoft.health.gymgain.data.Lifecycle;

import java.util.ArrayList;
import java.util.List;

public class Merger {

    private static <T extends Lifecycle> T getSameObject(List<T> container, T target) {
        for (T entry: container) {
            if (entry.isSameObject(target)) {
                return entry;
            }
        }
        return null;
    }

    private static <T extends Lifecycle> void replace(List<T> container, T source, T target) {
        int index = container.indexOf(source);
        container.set(index, target);
    }

    private static <T extends Lifecycle> List<T> mergeLists(List<T> source, List<T> target) {
        int size = source.size() + target.size();
        List<T> mergedList = new ArrayList<>(size);
        int iSource = 0;
        int iTarget = 0;
        while ((iSource + iTarget) < size) {
            if (iSource >= source.size()) {
                T cloudEntry = target.get(iTarget);
                mergedList.add(cloudEntry);
                ++iTarget;
            } else if (iTarget >= target.size()) {
                T localEntry = source.get(iSource);
                mergedList.add(localEntry);
                ++iSource;
            } else {
                T localEntry = source.get(iSource);
                T cloudEntry = target.get(iTarget);
                if (cloudEntry.isNewer(localEntry)) {
                    mergedList.add(cloudEntry);
                    ++iTarget;
                } else {
                    mergedList.add(localEntry);
                    ++iSource;
                }
            }
        }
        return mergedList;
    }

    public static <T extends Lifecycle> List<T> merge(List<T> local, List<T> cloud, Statistics statistics) {
        List<T> toAdd = new ArrayList<>();
        for (T cloudEntry: cloud) {
            T localEntry = getSameObject(local, cloudEntry);
            if (localEntry == null) {
                toAdd.add(cloudEntry);
            } else {
                boolean rename = !cloudEntry.getId().equals(localEntry.getId());
                if (cloudEntry.isNewer(localEntry)) {
                    statistics.onChange();
                    replace(local, localEntry, cloudEntry);
                    if (rename) {
                        statistics.onRename(localEntry, cloudEntry);
                    }
                }  else if (localEntry.isNewer(cloudEntry)) {
                    statistics.onChange();
                    if (rename) {
                        statistics.onRename(cloudEntry, localEntry);
                    }
                }
            }
        }
        if (!toAdd.isEmpty()) {
            statistics.onChange();
            return mergeLists(local, toAdd);
        } else {
            return local;
        }
    }

    public static <T extends Lifecycle> List<T> getDeletingItems(List<T> local, List<T> cloud) {
        List<T> toDelete = new ArrayList<>();
        for (T cloudEntry: cloud) {
            T localEntry = getSameObject(local, cloudEntry);
            if ((localEntry != null) && (localEntry.isDeleted() != cloudEntry.isDeleted())) {
                if (cloudEntry.isDeleted() && cloudEntry.isNewer(localEntry)) {
                    toDelete.add(localEntry);
                } else if (localEntry.isDeleted() && (!cloudEntry.isNewer(localEntry))) {
                    toDelete.add(cloudEntry);
                }
            }
        }
        return toDelete;
    }

    public static class Statistics {

        public void onChange() {
            mChanged = true;
        }

        public boolean isChanged() {
            return mChanged;
        }

        public void onRename(Lifecycle from, Lifecycle to) {
            mRenamed.add(new Pair<>(from, to));
        }

        public void getRenamed(List<Pair<Lifecycle, Lifecycle>> target) {
            target.addAll(mRenamed);
        }

        private boolean mChanged = false;
        private List<Pair<Lifecycle, Lifecycle>> mRenamed = new ArrayList<>();
    }
}
