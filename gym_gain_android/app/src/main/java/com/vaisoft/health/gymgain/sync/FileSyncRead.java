package com.vaisoft.health.gymgain.sync;

import androidx.annotation.NonNull;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveId;
import com.google.android.gms.drive.Metadata;
import com.google.android.gms.drive.MetadataBuffer;
import com.google.android.gms.drive.query.Filters;
import com.google.android.gms.drive.query.Query;
import com.google.android.gms.drive.query.SearchableField;

import java.io.InputStream;


public class FileSyncRead extends FileSync {

    public interface Listener extends FileSync.Listener {
        void read(InputStream is);
        void noFile();
    }

    public FileSyncRead(GoogleApiClient drive, String fileName, Listener listener) {
        super(drive, fileName, listener);
        mListener = listener;
        query();
    }

    private void query() {
        Query query = new Query.Builder()
                .addFilter(Filters.eq(SearchableField.TITLE, getFileName()))
                .build();

        Drive.DriveApi.query(getDrive(), query)
                .setResultCallback(new ResultCallback<DriveApi.MetadataBufferResult>() {
                    @Override
                    public void onResult(DriveApi.MetadataBufferResult result) {
                        applyConnectionResult(result);
                    }
                });
    }

    private void applyConnectionResult(DriveApi.MetadataBufferResult result) {
        boolean fileFound = false;
        if (result != null && result.getStatus().isSuccess()) {
            MetadataBuffer metaDataBuffer = result.getMetadataBuffer();
            if (metaDataBuffer != null) {
                for (Metadata metadata : metaDataBuffer) {
                    if (metadata == null || !metadata.isDataValid()) {
                        continue;
                    }
                    DriveId driveId = metadata.getDriveId();
                    DriveFile file = driveId.asDriveFile();
                    file.open(getDrive(), DriveFile.MODE_READ_ONLY, null).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
                        @Override
                        public void onResult(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
                            onFileDataReceived(driveContentsResult);
                        }
                    });
                    fileFound = true;
                    break;
                }
                metaDataBuffer.release();
            }
        }
        if (!fileFound) {
            mListener.noFile();
        }
    }

    private void onFileDataReceived(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
        if (!driveContentsResult.getStatus().isSuccess()) {
            onFail(driveContentsResult.getStatus());
            return;
        }
        DriveContents contents = driveContentsResult.getDriveContents();
        mListener.read(contents.getInputStream());
    }

    private Listener mListener;
}
