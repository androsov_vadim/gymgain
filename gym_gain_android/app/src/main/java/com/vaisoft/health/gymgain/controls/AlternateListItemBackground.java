package com.vaisoft.health.gymgain.controls;

import android.graphics.Rect;
import androidx.annotation.ColorInt;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

public class AlternateListItemBackground extends RecyclerView.ItemDecoration {
    public AlternateListItemBackground(boolean portrait, @ColorInt int color1, @ColorInt int color2) {
        mPortrait = portrait;
        mColor1 = color1;
        mColor2 = color2;
    }

    private boolean isColorOne(int position) {
        if (mPortrait) {
            return position % 2 == 0;
        } else {
            if ((position % 2) == 0) {
                return (position / 2) % 2 == 0;
            } else {
                return (position / 2) % 2 > 0;
            }
        }
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        int position = parent.getChildAdapterPosition(view);
        view.setBackgroundColor(isColorOne(position) ? mColor1 : mColor2);
    }

    private boolean mPortrait;
    private @ColorInt int mColor1;
    private @ColorInt int mColor2;
}
