package com.vaisoft.health.gymgain.workout;

import com.vaisoft.health.gymgain.controls.CoachFragment;
import com.vaisoft.health.gymgain.data.Workout;

public abstract class WorkoutFragment extends CoachFragment {

    protected boolean isWorkoutActive() {
        WorkoutActivity activity = (WorkoutActivity) getActivity();
        return activity.isWorkoutActive();
    }

    protected Workout getWorkout() {
        WorkoutActivity activity = (WorkoutActivity) getActivity();
        return activity.getWorkout();
    }

    protected Workout getPreviousWorkout() {
        WorkoutActivity activity = (WorkoutActivity) getActivity();
        return activity.getPreviousWorkout();
    }

    protected void notifyChanged() {
        WorkoutActivity activity = (WorkoutActivity) getActivity();
        activity.notifyChanged();
    }

    public static String SET = "set";
    public static String EXERCISE = "ex";
}
