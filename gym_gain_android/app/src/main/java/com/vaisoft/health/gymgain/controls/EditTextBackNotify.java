package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.KeyEvent;

public class EditTextBackNotify extends AppCompatEditText {
    public EditTextBackNotify(Context context) {
        super(context);
    }

    public EditTextBackNotify(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextBackNotify(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (mBackPressListener != null) {
                mBackPressListener.onBackPressed();
                return true;
             }
        }
        return super.onKeyPreIme(keyCode, event);
    }

    public void setOnBackPressListener(BackPressListener listener) {
        mBackPressListener = listener;
    }

    public interface BackPressListener {
        void onBackPressed();
    }

    private BackPressListener mBackPressListener;
}
