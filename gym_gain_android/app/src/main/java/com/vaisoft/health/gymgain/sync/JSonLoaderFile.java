package com.vaisoft.health.gymgain.sync;

import android.content.Context;

import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSonLoaderFile implements JSONLoader {

    public JSonLoaderFile(Context context) {
        mContext = context;
    }

    @Override
    public JSONObject loadObject(String id, Utils.FileTarget target) {
        return Utils.loadJSONObject(mContext, id, target);
    }

    @Override
    public JSONArray loadArray(String id, Utils.FileTarget target) {
        return Utils.loadJSONArray(mContext, id, target);
    }

    private Context mContext;
}
