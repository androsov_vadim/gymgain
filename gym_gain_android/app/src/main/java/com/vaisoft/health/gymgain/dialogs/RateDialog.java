package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;

public class RateDialog extends DialogFragment
{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        String message = getString(R.string.rate_message);
        builder.setTitle(R.string.rate_caption);
        builder.setMessage(message);
        builder.setIcon(R.drawable.ic_star_border_black_24dp);
        builder.setPositiveButton(R.string.rate_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rateNow();
            }
        });
        builder.setNeutralButton(R.string.rate_later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rateLater();
            }
        });
        builder.setNegativeButton(R.string.rate_never, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rateNever();
            }
        });

        TextView textView = new TextView(getActivity());
        textView.setMaxLines(6);
        builder.setView(textView);

        return builder.create();
    }

    private void rateNow() {
        getListener().onRateMeNow();
    }

    private void rateLater() {
        getListener().onRateMeLater();
    }

    private void rateNever() {
        getListener().onRateMeNever();
    }

    private Listener getListener() {
        return (Listener)getActivity();
    }

    public interface Listener {
        void onRateMeNow();
        void onRateMeLater();
        void onRateMeNever();
    }
}
