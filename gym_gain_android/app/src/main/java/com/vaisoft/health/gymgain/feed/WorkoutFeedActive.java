package com.vaisoft.health.gymgain.feed;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.controls.FeedItem;
import com.vaisoft.health.gymgain.controls.TextTimeTicker;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.data.ProgressCounter;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.workout.WorkoutActivityView;
import com.vaisoft.health.gymgain.main.FeedFragment;

public class WorkoutFeedActive extends Feed {

    public WorkoutFeedActive(Context context, Workout workout, Coach coach) {
        super(context);
        mWorkout = workout;
        mCoach = coach;
    }

    @Override
    public void bind(ContextViewHolder viewHolder, Coach coach) {
        ViewHolder holder = (ViewHolder) viewHolder;
        holder.textTicker.setInitialDate(mWorkout.getStartedDate());
        FeedItem item = holder.getItem();
        item.setProgress(ProgressCounter.GOOD);
        item.setCaption(coach.getLocalizedName(getContext(), mWorkout));
        item.setItemBackgroundColor(R.color.feed_active_workout_background);
        item.setIcon(R.drawable.ic_av_timer_white_36dp, R.color.feed_icon);
    }

    @Override
    public void clicked() {
        Context context = getContext();
        Intent intent = new Intent(context, WorkoutActivityView.class);
        intent.putExtra(WorkoutActivityView.WORKOUT_ACTIVE, mCoach.getActiveWorkoutId(mWorkout));
        context.startActivity(intent);
    }

    @Override
    public void deleted(Coach coach) {
        mCoach.deleteActiveWorkout(getContext(), mWorkout);
    }

    @Override
    public int getViewType() {
        return VIEW_TYPE;
    }

    @Override
    protected long getEffectiveTime() {
        return mWorkout.getEffectiveDate().getMillis();
    }

    //-----------------------------------------------------------------------------

    public static class ViewHolder extends Feed.ViewHolder {

        public ViewHolder(View view, FeedFragment.FeedAdapter adapter) {
            super(view, adapter);
            TextView target = getItem().getNeutralField();
            textTicker = new TextTimeTicker(target);
        }

        @Override
        public void start() {
            super.start();
            textTicker.start();
        }

        @Override
        public void stop() {
            super.stop();
            textTicker.stop();
        }

        public TextTimeTicker textTicker;
    }

    private Workout mWorkout;
    private Coach mCoach;

    public static final int VIEW_TYPE = R.layout.feed_item_workout_active;
}
