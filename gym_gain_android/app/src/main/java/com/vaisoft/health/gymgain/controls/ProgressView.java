package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Progress;


public class ProgressView extends View {

    public ProgressView(Context context) {
        super(context);
        init();
    }

    public ProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    protected void init() {
        mGood = new Paint();
        mSoso = new Paint();
        mBad = new Paint();
        mGood.setColor(ContextCompat.getColor(getContext(), R.color.chart_good));
        mSoso.setColor(ContextCompat.getColor(getContext(), R.color.chart_soso));
        mBad.setColor(ContextCompat.getColor(getContext(), R.color.chart_bad));
     }

    protected Paint getBrush(Progress.Estimate estimate) {
        switch (estimate) {
            case GOOD: return mGood;
            case SOSO: return mSoso;
            case BAD: return mBad;
        }
        return null;
    }

    public void notifyDataChanged() {
        invalidate();
    }

    private Paint mGood;
    private Paint mBad;
    private Paint mSoso;
}