package com.vaisoft.health.gymgain.settings;

import com.vaisoft.health.gymgain.controls.CoachFragment;

public abstract class SettingsTabFragment extends CoachFragment {
    public void onTabSelected() {

    }

    protected int getCurrentTabIndex() {
        SettingsActivity activity = (SettingsActivity) getActivity();
        return activity.getActiveTab();
    }
}
