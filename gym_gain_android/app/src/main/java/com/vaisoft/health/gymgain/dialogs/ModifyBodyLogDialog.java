package com.vaisoft.health.gymgain.dialogs;

import android.os.Bundle;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.BodyMeasure;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.edit.BodyLogActivity;

public class ModifyBodyLogDialog extends BodyLogDialog {

    @Override
    protected void onOk(BodyMeasure.Pattern pattern) {
        BodyLogActivity activity = (BodyLogActivity) getActivity();
        activity.modifyPattern(mInitialName, pattern);
    }

    @Override
    protected int getCaption() {
        return R.string.edit_metric;
    }

    @Override
    protected int getLayout() {
        return R.layout.modify_body_metric;
    }


    @Override
    public void onCoachConnected() {
        super.onCoachConnected();

        Bundle args = getArguments();

        mInitialName = args.getString(METRIC_NAME);
        setName(mInitialName);

        Unit unit = Unit.valueOf(args.getString(UNIT));
        setUnit(unit, false);

        boolean targetInc = Value.getTargetValue(args.getString(GROW));
        setTargetInc(targetInc);
    }

    @Override
    protected boolean isNameInUse(String name) {
        String localized = getCoach().getName(mInitialName);
        if (name.equals(localized)) {
            return false;
        }
        return super.isNameInUse(name);
    }

    private String mInitialName;

    public static final String METRIC_NAME = "mname";
    public static final String UNIT = "unit";
    public static final String GROW = "grow";
}
