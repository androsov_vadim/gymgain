package com.vaisoft.health.gymgain.feed;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.controls.FeedItem;
import com.vaisoft.health.gymgain.data.BodyMeasure;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.ProgressStatistics;
import com.vaisoft.health.gymgain.edit.BodyLogActivity;
import com.vaisoft.health.gymgain.main.FeedFragment;
import com.vaisoft.health.gymgain.utils.Utils;

public class BodyLogFeed extends Feed {

    public BodyLogFeed(Context context, BodyMeasure.LogEntry entry, BodyMeasure measurer) {
        super(context);
        mEntry = entry;
        mMeasurer = measurer;
    }

    @Override
    public void bind(ContextViewHolder viewHolder, Coach coach) {
        ViewHolder holder = (ViewHolder) viewHolder;
        ProgressStatistics statistics = mMeasurer.getProgressStatistics(mEntry);
        FeedItem item = holder.getItem();
        item.reset();
        item.setProgress(statistics.getCounter());
        item.setCaption(holder.getString(R.string.feed_body_log));
        int totalProgress = statistics.getTotal();
        item.applyDescription(statistics, coach.getLocalizer());
        if (totalProgress == 0) {
            item.setIcon(R.drawable.ic_directions_walk_white_36dp, R.color.feed_icon);
        } else {
            item.showThumb(Progress.positiveSummaryIndex(totalProgress).getEstimate());
        }
        item.setItemBackgroundColor(R.color.feed_body_log_background);
        String date = Utils.formatDate(mEntry.date);
        item.setNeutralText(date, null);
    }

    @Override
    public void clicked() {
        Context context = getContext();
        Intent intent = new Intent(context, BodyLogActivity.class);
        intent.putExtra(BodyLogActivity.ITEM, mMeasurer.getEntryIndex(mEntry));
        context.startActivity(intent);
    }

    @Override
    public void deleted(Coach coach) {
        coach.deleteBodyLog(getContext(), mEntry);
    }

    @Override
    public int getViewType() {
        return VIEW_TYPE;
    }

    @Override
    protected long getEffectiveTime() {
        return mEntry.date.getMillis();
    }

    //-----------------------------------------------------------------------------

    public static class ViewHolder extends Feed.ViewHolder {

        public ViewHolder(View view, FeedFragment.FeedAdapter adapter) {
            super(view, adapter);
        }
    }

    private BodyMeasure.LogEntry mEntry;
    private BodyMeasure mMeasurer;

    public static final int VIEW_TYPE = R.layout.feed_item_body;
}
