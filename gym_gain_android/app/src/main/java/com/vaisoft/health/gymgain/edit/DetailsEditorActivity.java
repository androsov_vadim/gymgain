package com.vaisoft.health.gymgain.edit;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.ButtonPanel;
import com.vaisoft.health.gymgain.data.IdGenerator;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.dialogs.ScheduleDialog;
import com.vaisoft.health.gymgain.schedule.Schedule;
import com.vaisoft.health.gymgain.utils.Utils;
import com.vaisoft.health.gymgain.workout.WorkoutActivityView;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.List;

public abstract class DetailsEditorActivity extends EditorActivity
    implements ButtonPanel.Listener, ScheduleDialog.Listener
{
    protected abstract void onIdChanged(String id);
    protected abstract String getId();
    protected abstract Workout createWorkout();
    protected abstract boolean isUseless();

    @Override
    protected void onStart() {
        super.onStart();
        mIdDefined = false;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(FIELD_EDIT_TARGET, mEditTarget);
        outState.putBoolean(FIELD_ID_DEFINED, mIdDefined);
        if (mInitialId != null) {
            outState.putString(FIELD_INITIAL_ID, mInitialId);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mEditTarget = savedInstanceState.getInt(FIELD_EDIT_TARGET);
        mIdDefined = savedInstanceState.getBoolean(FIELD_ID_DEFINED);
        mInitialId = savedInstanceState.getString(FIELD_INITIAL_ID);
    }

    protected List<ButtonPanel> createButtonPanels(int layoutId, List<String> items, boolean editable, boolean selectable) {
        List<ButtonPanel> buttons = new ArrayList<>();
        float dp = 2.f;
        int marginPx = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dp,
                getResources().getDisplayMetrics()
        );

        FlowLayout fl = findViewById(layoutId);
        for(String part:items) {
            FlowLayout.LayoutParams lp = new FlowLayout.LayoutParams(FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
            lp.rightMargin = marginPx;
            lp.topMargin = marginPx;
            ButtonPanel bp = new ButtonPanel(this);
            bp.setTag(part);
            bp.setTitleText(getCoach().getLocalized(part));
            bp.setDeletable(editable);
            bp.setSelected(false);
            bp.setSelectable(selectable);
            bp.setEventsListener(this);
            fl.addView(bp, lp);
            buttons.add(bp);
        }
        return  buttons;
    }

    @Override
    @Deprecated
    protected void initCommonInterface(String id) {
        super.initCommonInterface(id);
        initIdEditor(id);
        initScheduler();
        mEditTarget = R.string.empty_string;
    }

    protected void initCommonInterface(String id, int editTarget) {
        super.initCommonInterface(id);
        initIdEditor(id);
        initScheduler();
        mEditTarget = editTarget;
        initSaveAllButton();
    }

    private void initSaveAllButton() {
        View saveAll = findViewById(R.id.save_all);
        if (!isEditable()) {
            saveAll.setVisibility(View.GONE);
        }
        saveAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isCoach()) {
                    return;
                }
                if (isChanged()) {
                    onSaveChanges();
                } else {
                    goBack();
                }
            }
        });
    }

    @Override
    protected int getEditTargetId() {
        return mEditTarget;
    }

    private void initScheduler() {
        View schedule = findViewById(R.id.plan_training);
        schedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPlanTrainingClick();
            }
        });
        View start = findViewById(R.id.quick_start_training);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onQuickStartClick();
            }
        });
        mSchedulerButton = findViewById(R.id.schedule);
        mSchedulerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPlanTrainingClick();
            }
        });
        updateSchedule();
    }

    @Override
    public void onScheduleUpdated(@NonNull Schedule schedule) {
        Record record = getRecord();
        boolean doSave = false;
        if (record == null) {
            if (getInitialId() == null) {
               if (checkAndNotifyWrongId()) {
                   return;
               }
               doSave = true;
            }
            record = createRecord();
        }
        getCoach().setRecordSchedule(this, record, schedule);
        updateSchedule();
        if (doSave) {
            forceSave();
        }
    }

    @Override
    public void onScheduleDeleted() {
        deleteSchedule();
        updateSchedule();
    }

    private void updateSchedule() {
        ImageView icon = findViewById(R.id.plan_training_icon);
        Record record = getRecord();
        int color;
        boolean planned = record != null;
        if (planned) {
            color = ContextCompat.getColor(this, R.color.positive_color);
            hideScheduleBigButton();
        } else {
            color = ContextCompat.getColor(this, android.R.color.transparent);
            showScheduleBigButton();
        }
        icon.setColorFilter(color);
    }

    private void showScheduleBigButton() {
        if (mSchedulerButton.getVisibility() != View.VISIBLE) {
            mSchedulerButton.show();
        }
    }

    private void hideScheduleBigButton() {
        if (mSchedulerButton.getVisibility() == View.VISIBLE) {
            mSchedulerButton.hide();
        }
    }

    private void onPlanTrainingClick() {
        if (!isCoach()) {
            return;
        }
        if (isUseless()) {
            Utils.showErrorDialog(getSupportFragmentManager(), R.string.error_no_use);
            return;
        }

        Record record = getRecord();
        showRecordDialog(record);
    }

    protected boolean checkAndNotifyWrongId() {
        if (getId().isEmpty()) {
            Utils.showErrorDialog(getSupportFragmentManager(), R.string.error_id_wrong);
            return true;
        }
        return false;
    }

    private void onQuickStartClick() {
        if (!isCoach()) {
            return;
        }
        if (isUseless()) {
            Utils.showErrorDialog(getSupportFragmentManager(), R.string.error_no_use);
            return;
        }
        if (isChanged()) {
            confirm(R.string.confirm_save_quick_start, R.string.confirm_start_caption, CONFIRM_SAVE_CODE);
        } else {
            onConfirmed(CONFIRM_SAVE_CODE);
        }
    }

    @Override
    public boolean onConfirmed(int code) {
        if (super.onConfirmed(code)) {
            return true;
        }
        if (code != CONFIRM_SAVE_CODE) {
            return false;
        }
        if (isChanged()) {
            if (saveChanges()) {
                setChanged(false);
                onQuickStartTraining();
            }
        } else {
            onQuickStartTraining();
        }
        return true;
    }

    private void onQuickStartTraining() {
        getCoach().prepareWorkout(createWorkout());
        Intent intent = new Intent(this, WorkoutActivityView.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finish();
        startActivity(intent);
    }

    protected void removeButtonPanel(ButtonPanel button)
    {
        ViewGroup parent = (ViewGroup) button.getParent();
        parent.removeView(button);
    }

    private void initIdEditor(String textId) {
        if (isEditable()) {
            final EditText idInput = findViewById(R.id.id_input);
            findViewById(R.id.foo).requestFocus();
            idInput.setText(getCoach().getName(textId));
            idInput.setSelectAllOnFocus(!isIdDefined());
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            idInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String text = s.toString();
                    if (getSupportActionBar() != null) {
                        getSupportActionBar().setTitle(text);
                    }
                    onIdChanged(text);
                    mIdDefined = true;
                    idInput.setSelectAllOnFocus(false);
                    setChanged(true);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            idInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if ((actionId & EditorInfo.IME_MASK_ACTION) != 0) {
                        findViewById(R.id.foo).requestFocus();
                        View focused = getCurrentFocus();
                        if (focused != null) {
                            imm.hideSoftInputFromWindow(focused.getWindowToken(), 0);
                        }
                    }
                    return false;
                }
            });
        } else {
            final View idInputTab = findViewById(R.id.id_input_card);
            idInputTab.setVisibility(View.GONE);
        }
    }

    protected boolean isIdDefined() {
        return mIdDefined;
    }

    protected void setInitialId(String id) {
        mInitialId = id;
        mIdDefined = true;
    }

    protected String getInitialId() {
        return mInitialId;
    }

    @Override
    protected void forceSave() {
        boolean changes = isChanged();
        super.forceSave();
        if (changes) {
            setInitialId(getId());
        }
    }

    private int mEditTarget;
    private boolean mIdDefined;
    private String mInitialId;
    private FloatingActionButton mSchedulerButton;

    private static final String FIELD_EDIT_TARGET = "target";
    private static final String FIELD_ID_DEFINED = "isId";
    private static final String FIELD_INITIAL_ID = "id";

    private static final int CONFIRM_SAVE_CODE = IdGenerator.generateCode();
}
