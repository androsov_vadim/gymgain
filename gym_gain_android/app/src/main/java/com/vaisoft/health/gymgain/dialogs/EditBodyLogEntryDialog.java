package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import android.view.View;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.BoostNumberEditor;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;

public class EditBodyLogEntryDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        String caption = getCaption();
        builder.setTitle(caption);
        builder.setIcon(R.drawable.ic_event_available_black_24dp);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                long newValue = mEditor.accessValue();
                if (mValue != newValue) {
                    Listener listener = (Listener) getActivity();
                    listener.onEntryValueChanged(mSelectedIndex, newValue);
                }
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        builder.setNeutralButton(R.string.delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Listener listener = (Listener) getActivity();
                listener.onEntryDeleted(mSelectedIndex);
            }
        });
        builder.setView(createInterface());
        return builder.create();
    }

    private String getCaption() {
        String caption = getArguments().getString(ARG_CAPTION);
        if ((caption == null) || (caption.isEmpty())) {
            return "";
        }
        return caption.substring(0, 1).toUpperCase() + caption.substring(1);
    }

    private View createInterface() {
        View view = View.inflate(getActivity(), R.layout.edit_body_metric, null);
        mEditor = (BoostNumberEditor) view.findViewById(R.id.value_editor);

        Bundle bundle = getArguments();
        mSelectedIndex = bundle.getInt(ARG_SELECTED);
        mUnit = Unit.valueOf(bundle.getString(ARG_UNIT));
        mValue = bundle.getLong(ARG_VALUE);
        mEditor.setValue(mUnit, mValue, Value.Status.Applied);

        return view;
    }

    //------------------------------------------------------------------------------

    public interface Listener {
        void onEntryValueChanged(int index, long value);
        void onEntryDeleted(int index);
    }

    private BoostNumberEditor mEditor;
    private long mValue;
    private Unit mUnit;
    private int mSelectedIndex;

    public static final String ARG_UNIT = "unit";
    public static final String ARG_VALUE = "value";
    public static final String ARG_SELECTED = "selected";
    public static final String ARG_CAPTION = "param";
}
