package com.vaisoft.health.gymgain.edit;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.ButtonPanel;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.data.Training;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.dialogs.ExerciseDialog;
import com.vaisoft.health.gymgain.dialogs.ExerciseSimpleDialog;
import com.vaisoft.health.gymgain.dialogs.MultySelectDialog;
import com.vaisoft.health.gymgain.dialogs.SetConfigDialog;
import com.vaisoft.health.gymgain.utils.Utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TrainingDetailActivity extends DetailsEditorActivity
        implements  ExerciseDialog.Listener,
                    ExerciseSimpleDialog.Listener,
                    MultySelectDialog.Listener,
                    SetConfigDialog.Listener
{

    @Override
    public void onCoachConnected() {
        setContentView(R.layout.activity_training_detail);
        initTarget();
        initCommonInterface(mTraining.getId(), R.string.edit_target_training);
        initExpanders();
        initOrderer();
        initExercisesList();
        initWorkoutInfo();
        initExercisesDialog();
        initSetsList();
    }

    private void initTarget() {
        mTraining = getCoach().getEditTarget(Training.class);
        if (mTraining == null) {
            Intent intent = getIntent();
            if (intent.hasExtra(ITEM)) {
                try {
                    String trainingId = intent.getStringExtra(ITEM);
                    mTraining = (Training) getCoach().getTraining(trainingId).clone();
                    setInitialId(mTraining.getId());
                } catch (CloneNotSupportedException e) {
                    Log.e(getClass().getName(), e.getMessage());
                }
            } else {
                mTraining = new Training();
                mTraining.setId(getCoach().getDefaultTrainingId(getResources().getString(R.string.new_training_prefix)));
            }
            setEditTarget(mTraining);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(FIELD_SELECTED_SET, mSelectedSetIndex);
        outState.putInt(FIELD_SELECTED_PLAN_ITEM, mSelectedPlanItemIndex);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mSelectedSetIndex = savedInstanceState.getInt(FIELD_SELECTED_SET);
        mSelectedPlanItemIndex = savedInstanceState.getInt(FIELD_SELECTED_PLAN_ITEM);
    }

    private void initOrderer() {
        ListView ol = findViewById(R.id.tr_order);
        mOrderAdapter = new OrderAdapter();
        ol.setAdapter(mOrderAdapter);
        ol.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSelectedPlanItemIndex = position;
                configSetsDialog();
            }
        });
    }

    private void initExercisesList() {
        ListView el = findViewById(R.id.tr_exercises_list);
        mExercisesAdapter = new ExercisesAdapter();
        el.setAdapter(mExercisesAdapter);
    }

    private void configSetsDialog() {
        Training.PlanEntry planItem = mOrderAdapter.getPlanItem(mSelectedPlanItemIndex);
        SetConfigDialog setsDialog = new SetConfigDialog();
        Bundle args = new Bundle();
        if (planItem.getPrecisePlan() != null) {
            args.putIntArray(SetConfigDialog.DATA, planItem.getPrecisePlan());
        }
        if (planItem.getComment() != null) {
            args.putString(SetConfigDialog.COMMENT, planItem.getComment());
        }
        args.putBoolean(SetConfigDialog.REPS, supportsReps(planItem));
        setsDialog.setArguments(args);
        setsDialog.show(getSupportFragmentManager(), "SetConfigDialog");
    }

    @Override
    public void onSetDataChanged(int[] data, String comment) {
        Training.PlanEntry planItem = mOrderAdapter.getPlanItem(mSelectedPlanItemIndex);
        planItem.updatePlan(data, comment);
        mOrderAdapter.notifyDataSetChanged();
        setChanged(true);
    }

    private boolean supportsReps(Training.PlanEntry planItem) {
        List<String> exercises = planItem.getItems();
        for (String exerciseId: exercises) {
            Exercise exercise = getCoach().getExercise(exerciseId);
            if (!exercise.isParameter(Unit.reps)) {
                return false;
            }
        }
        return true;
    }

    private void initExpanders() {
        mScrollPanel = findViewById(R.id.root_scroll);

        mExercisesExpander = findViewById(R.id.tr_expand_ex);
        mExEditEnabled = new ArrayList<>();
        mExEditDisabled = new ArrayList<>();
        mExEditEnabled.add(findViewById(R.id.tr_edit_ex));
        mExEditDisabled.add(findViewById(R.id.tap_to_edit_ex));
        mExEditDisabled.add(findViewById(R.id.short_descr_ex));

        mSetsExpander = findViewById(R.id.tr_expand_set);
        mSetEditEnabled = new ArrayList<>();
        mSetEditDisabled = new ArrayList<>();
        mSetEditEnabled.add(findViewById(R.id.tr_edit_set));
        mSetEditDisabled.add(findViewById(R.id.tap_to_edit_sets));
        mSetEditDisabled.add(findViewById(R.id.short_descr_sets));

        mOrderExpander = findViewById(R.id.tr_expand_order);
        mOrderEditEnabled = new ArrayList<>();
        mOrderEditDisabled = new ArrayList<>();
        mOrderEditEnabled.add(findViewById(R.id.tr_edit_order));
        mOrderEditDisabled.add(findViewById(R.id.tap_to_edit_order));
        mOrderEditDisabled.add(findViewById(R.id.short_descr_order));

        mOnLayoutChange = new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                mScrollPanel.getDrawingRect(scrollRect);
                mAutoFitView.getHitRect(viewRect);
                if (scrollRect.bottom < viewRect.bottom) {
                    mScrollPanel.smoothScrollBy(0, viewRect.bottom - scrollRect.bottom);
                }
                mAutoFitView.removeOnLayoutChangeListener(mOnLayoutChange);
            }

            private Rect scrollRect = new Rect();
            private Rect viewRect = new Rect();
        };

        mExEditor = findViewById(R.id.tr_edit_ex_tab);
        mExEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isExpanded(mExEditEnabled)) {
                    setExerciseEditorExpanded(true);
                }
            }
        });

        mSetEditor = findViewById(R.id.tr_edit_set_tab);
        mSetEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isExpanded(mSetEditEnabled)) {
                    setSetEditorExpanded(true);
                }
            }
        });

        mOrderEditor = findViewById(R.id.tr_edit_order_tab);
        mOrderEditor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isExpanded(mOrderEditEnabled)) {
                    setOrderEditorExpanded(true);
                }
            }
        });

        View exToggler = findViewById(R.id.tr_toggle_ex);
        exToggler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setExerciseEditorExpanded(!isExpanded(mExEditEnabled));
            }
        });

        View setToggler = findViewById(R.id.tr_toggle_set);
        setToggler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSetEditorExpanded(!isExpanded(mSetEditEnabled));
            }
        });

        View orderToggler = findViewById(R.id.tr_toggle_order);
        orderToggler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setOrderEditorExpanded(!isExpanded(mOrderEditEnabled));
            }
        });
    }

    private void initWorkoutInfo() {
        List<String> muscles = getCoach().getMusclesForExercises(mTraining.getExercises());
        List<String> parts = getCoach().getAffectedByMusclesParts(muscles);
        mPartsButtons = createButtonPanels(R.id.tr_parts_list, parts , false, true);
        mMusclesButtons = createButtonPanels(R.id.tr_muscles_list,
                muscles, false, false);
        mExEditDisabled.add(updateExShortDescription());
        mSetEditDisabled.add(updateSetsShortDescription());
    }

    private View updateExShortDescription() {
        TextView shortDescription = findViewById(R.id.short_descr_ex);
        if (mTraining.getExercises().isEmpty()) {
            shortDescription.setText(R.string.empty);
        } else {
            shortDescription.setText(Utils.convertListToString(
                    mTraining.getExercises(), getCoach().getLocalizer()));
        }
        return shortDescription;
    }

    private View updateSetsShortDescription() {
        TextView shortDescription = findViewById(R.id.short_descr_sets);
        if (mTraining.getSetsCount() == 0) {
            shortDescription.setText(R.string.empty);
        } else {
            StringBuilder text = new StringBuilder();
            boolean coma = false;
            for (int i = 0; i < mTraining.getSetsCount(); i++) {
                List<String> set = mTraining.getSet(i);
                if (coma) {
                    text.append(", ");
                } else {
                    coma = true;
                }
                String setText = Utils.convertListToString(set, getCoach().getLocalizer());
                text.append('[');
                text.append(setText);
                text.append(']');
            }
            shortDescription.setText(text.toString());
        }
        return shortDescription;
    }

    private void initExercisesDialog() {
        Button addExercises = findViewById(R.id.tr_add_exercise);
        addExercises.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddExerciseDialog();
            }
        });
        mExEditEnabled.add(addExercises);
    }

    private void initSetsDialog() {
        Button addSet = findViewById(R.id.tr_add_set);
        addSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSetsDialog(-1);
            }
        });
        mSetEditEnabled.add(addSet);
    }

    private void showSetsDialog(int selectedIndex) {
        mSelectedSetIndex = selectedIndex;
        Bundle bundle = new Bundle();
        ArrayList<String> exercises = mTraining.getExercisesNotInSets(selectedIndex);
        bundle.putStringArrayList(MultySelectDialog.DATA,
                getCoach().getLocalized(exercises));
        ExerciseSimpleDialog dialog = new ExerciseSimpleDialog();
        if (selectedIndex >= 0) {
            ArrayList<Integer> indexes = getSelectedExercisesIndexes(exercises, selectedIndex);
            bundle.putIntegerArrayList(MultySelectDialog.DATA_SELECTED, indexes);
            bundle.putBoolean(ExerciseSimpleDialog.SET_SELECTED, true);
        }
        dialog.setArguments(bundle);
        dialog.show(getSupportFragmentManager(), "ExerciseSimpleDialog");
    }

    @Override
    public void onChosen(List<Integer> data) {
        ArrayList<String> exercises = mTraining.getExercisesNotInSets(mSelectedSetIndex);
        if (data.size() >= 2) {
            List<String> toAdd = new ArrayList<>();
            for (Integer index : data) {
                toAdd.add(exercises.get(index.intValue()));
            }
            if (mSelectedSetIndex >= 0) {
                mTraining.removeSet(mSelectedSetIndex);
            }
            mTraining.addSet(toAdd);
            notifySetsChanged();
        } else {
            if (mSelectedSetIndex >= 0) {
                removeSet(mSelectedSetIndex);
            }
        }
    }

    @Override
    public void onSetDeleteConfirmed() {
        removeSet(mSelectedSetIndex);
    }

    private void removeSet(int selectedIndex) {
        mTraining.removeSet(selectedIndex);
        notifySetsChanged();
    }

    private ArrayList<Integer> getSelectedExercisesIndexes(List<String> exercises, int selectedIndex) {
        ArrayList<Integer> indexes = new ArrayList<>();
        List<String> inSet = mTraining.getSet(selectedIndex);
        for (String ex: inSet) {
            indexes.add(exercises.indexOf(ex));
        }
        return indexes;
    }

    private void initSetsList() {
        ListView sl = findViewById(R.id.tr_sets);
        mSetsAdapter = new SetsAdapter();
        sl.setAdapter(mSetsAdapter);
        initSetsDialog();
        sl.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showSetsDialog(position);
            }
        });
    }

    private void showAddExerciseDialog() {
        Bundle bundle = new Bundle();
        bundle.putStringArrayList(ExerciseDialog.DATA, mTraining.getExercises());
        ExerciseDialog exerciseDialog = new ExerciseDialog();
        exerciseDialog.setArguments(bundle);
        exerciseDialog.show(getSupportFragmentManager(), "ExerciseDialog");
    }

    @Override
    protected void deleteObjectConfirmed() {
        if (isIdDefined()) {
            String id = getInitialId();
            if (id != null) {
                getCoach().deleteTraining(this, id);
            }
        }
        setChanged(false);
        goBack();
    }

    @Override
    protected Record createRecord() {
        String id = getInitialId();
        if (id == null) {
            return getCoach().createRecord(mTraining);
        }
        return getCoach().createTrainingRecord(id);
    }

    @Override
    protected Record getRecord() {
        String id = getInitialId();
        if (id == null) {
            return null;
        }
        return getCoach().getRecordForTraining(id);
    }

    @Override
    protected Workout createWorkout() {
        Record record = new Record(mTraining);
        return record.createWorkout(getCoach());
    }

    @Override
    protected boolean isUseless() {
        return mTraining.isUseless();
    }

    @Override
    public void onSelectionChange(String id, boolean selected) {
        if (selected) {
            for (ButtonPanel ptButton: mPartsButtons) {
                if (!ptButton.getTag().equals(id)) {
                    ptButton.setChecked(false);
                }
            }
            List<String> exercises = getCoach().getExercisesForBodyPart(mTraining, id);
            mExercisesAdapter.setSelectedExercises(exercises);
            List<String> muscles = getCoach().getMusclesForPart(id);
            for (ButtonPanel msButton: mMusclesButtons) {
                msButton.setChecked(muscles.contains(msButton.getTag()));
            }
        } else {
            mExercisesAdapter.setSelectedExercises(null);
            for (ButtonPanel msButton: mMusclesButtons) {
                msButton.setChecked(false);
            }
        }
    }

    @Override
    public void onDeleteItem(String id) {
        mTraining.removeExercise(id);
        updateExShortDescription();
        notifySetsChanged();
        List<String> muscles = getCoach().getMusclesForExercises(mTraining.getExercises());
        List<String> parts = getCoach().getAffectedByMusclesParts(muscles);
        for (Iterator<ButtonPanel> iButton = mPartsButtons.iterator(); iButton.hasNext();) {
            ButtonPanel bp = iButton.next();
            if (!parts.contains(bp.getTag())) {
                removeButtonPanel(bp);
                iButton.remove();
            }
        }

        for (Iterator<ButtonPanel> iButton = mMusclesButtons.iterator(); iButton.hasNext();) {
            ButtonPanel bp = iButton.next();
            if (!muscles.contains(bp.getTag())) {
                removeButtonPanel(bp);
                iButton.remove();
            }
        }
    }

    void notifySetsChanged() {
        updateSetsShortDescription();
        mSetsAdapter.notifyDataSetChanged();
        mOrderAdapter.notifyDataSetChanged();
        mExercisesAdapter.notifyDataSetChanged();
        setChanged(true);
    }

    @Override
    public void onExercisesSelected(List<String> exercises) {
        if (exercises.isEmpty()) {
            return;
        }
        List<String> muscles = getCoach().getMusclesForExercises(exercises);
        List<String> parts = getCoach().getAffectedByMusclesParts(muscles);
        List<String> musclesBefore = getCoach().getMusclesForExercises(mTraining.getExercises());
        List<String> partsBefore = getCoach().getAffectedByMusclesParts(musclesBefore);
        muscles.removeAll(musclesBefore);
        parts.removeAll(partsBefore);
        mPartsButtons.addAll(createButtonPanels(R.id.tr_parts_list, parts, false, true));
        mMusclesButtons.addAll(createButtonPanels(R.id.tr_muscles_list, muscles, false, false));
        mTraining.addExercises(exercises);
        setChanged(true);

        mExercisesAdapter.notifyDataSetChanged();
        mOrderAdapter.notifyDataSetChanged();

        updateExShortDescription();
    }

    @Override
    protected void onIdChanged(String id) {
        mTraining.setId(id);
    }

    @Override
    protected String getId() {
        return mTraining.getId();
    }

    @Override
    public boolean saveChanges() {
        if (checkAndNotifyWrongId()) {
            return false;
        }
        String initialId = getInitialId();
        if ((initialId == null) || (!initialId.equals(mTraining.getId()))) {
            if (!getCoach().isTrainingIdFree(mTraining.getId())) {
                Utils.showErrorDialog(getSupportFragmentManager(), R.string.error_id_exists);
                return false;
            }
        }
        getCoach().saveTraining(this, mTraining, initialId);
        return true;
    }

    @Override
    protected boolean isDeletable() {
        return  true;
    }

    @Override
    protected boolean canAutoSave() {
        return false;
    }

    @Override
    protected boolean isEditable() {
        return true;
    }

    private Context getActivityContext() {
        return this;
    }

    private void switchExpansion(boolean enabled, List<View> enabledItems, List<View> disabledItems) {
        for (View view : enabledItems) {
            view.setVisibility(enabled ? View.VISIBLE : View.GONE);
        }
        for (View view : disabledItems) {
            view.setVisibility(enabled ? View.GONE : View.VISIBLE);
        }
    }

    private Drawable getExpandDrawable(boolean expanded) {
        if (expanded) {
            return ContextCompat.getDrawable(getActivityContext(), R.drawable.ic_expand_less_black_24dp);
        } else {
            return ContextCompat.getDrawable(getActivityContext(), R.drawable.ic_expand_more_black_24dp);
        }
    }

    private void setExerciseEditorExpanded(boolean expanded) {
        turnAutoFitOn(mExEditor);
        switchExpansion(expanded, mExEditEnabled, mExEditDisabled);
        mExercisesExpander.setImageDrawable(getExpandDrawable(expanded));
    }

    private void setSetEditorExpanded(boolean expanded) {
        turnAutoFitOn(mSetEditor);
        switchExpansion(expanded, mSetEditEnabled, mSetEditDisabled);
        mSetsExpander.setImageDrawable(getExpandDrawable(expanded));
    }

    private void setOrderEditorExpanded(boolean expanded) {
        turnAutoFitOn(mOrderEditor);
        switchExpansion(expanded, mOrderEditEnabled, mOrderEditDisabled);
        mOrderExpander.setImageDrawable(getExpandDrawable(expanded));
    }

    private void turnAutoFitOn(View target) {
        mAutoFitView = target;
        mAutoFitView.addOnLayoutChangeListener(mOnLayoutChange);
    }

    private boolean isExpanded(List<View> enabledItems) {
        return enabledItems.get(0).getVisibility() == View.VISIBLE;
    }

    //--------------------------------------------------------------------------------------------

    private class SetsAdapter extends BaseAdapter {

        public SetsAdapter() {
            mInflater = LayoutInflater.from(getActivityContext());
        }

        @Override
        public int getCount() {
            return mTraining.getSetsCount();
        }

        @Override
        public Object getItem(int position) {
            return mTraining.getSet(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            if (convertView == null) {
                view = mInflater.inflate(android.R.layout.two_line_list_item, parent, false);
            } else {
                view = convertView;
            }
            TextView text1 = view.findViewById(android.R.id.text1);
            TextView text2 = view.findViewById(android.R.id.text2);
            List<String> entry = (List<String>) getItem(position);
            List<String> muscles = getCoach().getMusclesForExercises(entry);
            text2.setText(toText(muscles));
            text1.setText(toText(entry));
            return view;
        }

        private String toText(List<String> data) {
            return Utils.convertListToString(data, getCoach().getLocalizer());
        }

        private final LayoutInflater mInflater;
    }


    private class OrderAdapter extends BaseAdapter {

        public OrderAdapter() {
            mInflater = LayoutInflater.from(getActivityContext());
        }

        @Override
        public int getCount() {
            return mTraining.getPlanSize();
        }

        @Override
        public Object getItem(int position) {
            return getPlanItem(position);
        }

        public Training.PlanEntry getPlanItem(int position) {
            return mTraining.getPlanItemContent(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final View view;
            if (convertView == null) {
                view = mInflater.inflate(R.layout.training_order_item, parent, false);
            } else {
                view = convertView;
            }

            TextView textNumber = view.findViewById(R.id.text_number);
            textNumber.setText("" + (position + 1));

            Training.PlanEntry item = getPlanItem(position);
            List<String> data = item.getItems();

            TextView textDescription = view.findViewById(R.id.text_description);
            textDescription.setText(Utils.convertListToString(data, getCoach().getLocalizer()));

            TextView textPlan = view.findViewById(R.id.text_precise);
            int[] plan = item.getPrecisePlan();
            String planText = getPrecisePlanText(plan);
            textPlan.setText(planText);

            TextView textComment = view.findViewById(R.id.text_comment);
            if (item.getComment() != null) {
                textComment.setVisibility(View.VISIBLE);
                textComment.setText(item.getComment());
            } else {
                textComment.setVisibility(View.GONE);
            }

            createPositionControls(view, position);

            return view;
        }

        @NonNull
        private String getPrecisePlanText(int[] plan) {
            StringBuilder result = new StringBuilder();
            result.append('[');
            if (plan == null) {
                result.append(getResources().getText(R.string.no_precise_plan));
            } else {
                boolean separator = false;
                for (int i = 0; i < plan.length; i++) {
                    if (separator) {
                        result.append('/');
                    } else {
                        separator = true;
                    }
                    if (plan[i] == 0) {
                        result.append('?');
                    } else {
                        result.append(plan[i]);
                    }
                }
            }
            result.append(']');
            return result.toString();
        }

        private void createPositionControls(View view, final int position) {
            ImageButton up = view.findViewById(R.id.move_up);
            if (position > 0) {
                up.setVisibility(View.VISIBLE);
                up.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swap(position, position - 1);
                    }
                });
            } else {
                up.setVisibility(View.GONE);
            }

            ImageButton down = view.findViewById(R.id.move_down);
            if (position < (getCount() - 1)) {
                down.setVisibility(View.VISIBLE);
                down.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        swap(position, position + 1);
                    }
                });
            } else {
                down.setVisibility(View.GONE);
            }
        }

        private void swap(int x, int y) {
            mTraining.swapPlanItems(x, y);
            notifySetsChanged();
            setChanged(true);
        }

        private final LayoutInflater mInflater;
    }


    private class ExercisesAdapter extends BaseAdapter {

        public ExercisesAdapter() {
            mInflater = LayoutInflater.from(getActivityContext());
        }

        @Override
        public int getCount() {
            return mTraining.getExercises().size();
        }

        @Override
        public String getItem(int position) {
            return mTraining.getExercises().get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void setSelectedExercises(List<String> exercises) {
            mSelectedExercises = exercises;
            notifyDataSetChanged();
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final View view;
            if (convertView == null) {
                view = mInflater.inflate(R.layout.exercises_list_item, parent, false);
            } else {
                view = convertView;
            }

            TextView name = view.findViewById(android.R.id.text1);
            String exerciseId = getItem(position);
            name.setText(getCoach().getName(exerciseId));

            int color;
            if ((mSelectedExercises != null) && (mSelectedExercises.contains(exerciseId))) {
                color = ContextCompat.getColor(getActivityContext(), R.color.button_panel_selected);
            } else {
                color = ContextCompat.getColor(getActivityContext(), android.R.color.background_light);
            }
            view.setBackgroundColor(color);

            createControls(view, exerciseId);

            return view;
        }

        private void createControls(View view, final String id) {
            ImageButton up = view.findViewById(R.id.delete);
            up.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onDeleteItem(id);
                }
            });
        }

        private final LayoutInflater mInflater;
        private List<String> mSelectedExercises;
    }
    //--------------------------------------------------------------------------------------------

    private Training mTraining;

    private List<ButtonPanel> mPartsButtons;
    private List<ButtonPanel> mMusclesButtons;

    private SetsAdapter mSetsAdapter;
    private OrderAdapter mOrderAdapter;
    private ExercisesAdapter mExercisesAdapter;

    private NestedScrollView mScrollPanel;
    private View.OnLayoutChangeListener mOnLayoutChange;
    private View mExEditor;
    private View mSetEditor;
    private View mOrderEditor;
    private View mAutoFitView;

    private ImageView mExercisesExpander;
    private ImageView mSetsExpander;
    private ImageView mOrderExpander;

    private List<View> mExEditEnabled;
    private List<View> mExEditDisabled;
    private List<View> mSetEditEnabled;
    private List<View> mSetEditDisabled;
    private List<View> mOrderEditEnabled;
    private List<View> mOrderEditDisabled;

    private int mSelectedSetIndex = -1;//save
    private int mSelectedPlanItemIndex = -1;//save

    public static final String ITEM = "training";

    public static final String FIELD_SELECTED_SET = "set";
    public static final String FIELD_SELECTED_PLAN_ITEM = "plan_item";
}
