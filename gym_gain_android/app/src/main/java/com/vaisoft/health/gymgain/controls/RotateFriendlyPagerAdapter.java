package com.vaisoft.health.gymgain.controls;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.PagerAdapter;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

public abstract class RotateFriendlyPagerAdapter extends PagerAdapter {

    protected abstract CoachFragment getItem(int position);

    public RotateFriendlyPagerAdapter(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
        mFragments = new SparseArray<>();
    }

    public Fragment getFragment(int position) {
        String tag = getFragmentTag(position);
        return mFragmentManager.findFragmentByTag(tag);
    }

    private String getFragmentTag(int position) {
        return "android:switcher:" + position;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        CoachFragment fragment = getItem(position);
        mFragments.put(position, fragment);
        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }
        String tag = getFragmentTag(position);
        mCurTransaction.add(container.getId(), fragment, tag);
        return fragment;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        if (mCurTransaction == null) {
            mCurTransaction = mFragmentManager.beginTransaction();
        }
        mCurTransaction.detach(mFragments.get(position));
        mFragments.remove(position);
    }

    @Override
    public boolean isViewFromObject(View view, Object fragment) {
        return ((Fragment) fragment).getView() == view;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        if (mCurTransaction != null) {
            mCurTransaction.commitAllowingStateLoss();
            mCurTransaction = null;
            mFragmentManager.executePendingTransactions();
        }
    }

    public void invalidate() {
        for(int i = 0; i < mFragments.size(); i++) {
            int key = mFragments.keyAt(i);
            CoachFragment fragment = mFragments.get(key);
            fragment.invalidate();
        }
    }

    private FragmentManager mFragmentManager;
    private SparseArray<CoachFragment> mFragments;
    private FragmentTransaction mCurTransaction;
}
