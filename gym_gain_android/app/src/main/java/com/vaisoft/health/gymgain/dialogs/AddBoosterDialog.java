package com.vaisoft.health.gymgain.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.BodyMap;
import com.vaisoft.health.gymgain.controls.NumbersListAdapter;
import com.vaisoft.health.gymgain.controls.SpinnerSimpleAdapter;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.coach.CoachConnect;
import com.vaisoft.health.gymgain.coach.CoachTalk;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.schedule.Schedule;

import java.util.Calendar;
import java.util.List;

public class AddBoosterDialog extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.booster_add);
        builder.setIcon(R.drawable.ic_fast_forward_black_24dp);

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                createBoosterAndStart();
            }
        });
        builder.setNegativeButton(R.string.cancel, null);
        final View view = createInterface();
        builder.setView(view);

        AlertDialog dialog = builder.create();

        mCoachTalk = new CoachTalk(getContext());
        mCoachTalk.bind(new CoachConnect() {
            @Override
            public void onCoachConnected() {
                init(view);
            }
        });

        return dialog;
    }

    private void createBoosterAndStart() {
        Schedule schedule = createSchedule();
        Record record = getCoach().createRecord(mExercise);
        record.setSchedule(schedule);
        getCoach().saveRecords(getContext());
        record.startNewWorkout(getContext(), getCoach());
    }

    private Schedule createSchedule() {
        switch (mSelectedButton.getId()) {
            case R.id.schedule_every_day:
                return createEveryDaySchedule();
            case R.id.schedule_every_week_day:
                return createEveryWeekDaySchedule();
            case R.id.schedule_one_time_days:
                return createOneTimeDaysSchedule();
            case R.id.schedule_times_a_week:
                return createTimesAWeekSchedule();
        }
        return null;
    }

    private Schedule createTimesAWeekSchedule() {
        int times = mTimesPerWeekWorkouts.getSelectedItemPosition() + 1;
        return Schedule.createFlexible(Schedule.PeriodType.WEEK, 1, times);
    }

    private Schedule createOneTimeDaysSchedule() {
        int days = mOneTimeDaysCount.getSelectedItemPosition() + 1;
        return Schedule.createFlexible(Schedule.PeriodType.DAY, days, 1);
    }

    private Schedule createEveryWeekDaySchedule() {
        int days[] = new int[] {Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY,
            Calendar.THURSDAY, Calendar.FRIDAY};
        return Schedule.createWeekly(days);
    }

    private Schedule createEveryDaySchedule() {
        return Schedule.createFlexible(Schedule.PeriodType.DAY, 1, 1);
    }

    @Override
    public void onStart() {
        super.onStart();
        AlertDialog dialog = (AlertDialog) getDialog();
        mButtonOK = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        updateOkStatus();
    }

    private void updateOkStatus() {
        mButtonOK.setEnabled(false);
    }

    private void init(View view) {
        mExercises = getCoach().getFilteredExercises(null);
        initOptions(view);
        initExerciseSelector(view);
        initPerDays(view);
        initPerWeek(view);
    }

    private void initOptions(View view) {
        mEveryDay = (RadioButton) view.findViewById(R.id.schedule_every_day);
        initRadio(mEveryDay);

        mEveryWeekDay = (RadioButton) view.findViewById(R.id.schedule_every_week_day);
        initRadio(mEveryWeekDay);

        mOneTimeDays = (RadioButton) view.findViewById(R.id.schedule_one_time_days);
        initRadio(mOneTimeDays);
        View oneTimeDays = view.findViewById(R.id.schedule_one_time_days_clicker);
        oneTimeDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClicked(mOneTimeDays);
            }
        });

        mTimesPerWeek = (RadioButton) view.findViewById(R.id.schedule_times_a_week);
        initRadio(mTimesPerWeek);
        View timesPerWeek = view.findViewById(R.id.schedule_times_a_week_clicker);
        timesPerWeek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClicked(mTimesPerWeek);
            }
        });

        onClicked(mEveryDay);
    }

    private void initRadio(final RadioButton button) {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClicked(button);
            }
        });
    }

    private void initPerWeek(View view) {
        mTimesPerWeekWorkouts = (Spinner) view.findViewById(R.id.workouts_count);
        NumbersListAdapter listAdapter = new NumbersListAdapter(getContext(), 7, false);
        mTimesPerWeekWorkouts.setAdapter(listAdapter);
        final TextView timesPerText = (TextView) view.findViewById(R.id.timer_per);
        TextView periodName = (TextView) view.findViewById(R.id.period_name);
        String name = getResources().getString(R.string.booster_week_target);
        periodName.setText(name);
        mTimesPerWeekWorkouts.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String name = getResources().getQuantityString(R.plurals.times_per, i + 1);
                boolean isInit = isFirstInitialization(timesPerText);
                timesPerText.setText(name);
                if (!isInit) {
                    onClicked(mTimesPerWeek);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mTimesPerWeekWorkouts.setSelection(0);
    }

    private boolean isFirstInitialization(TextView text) {
        return text.getText().length() == 0;
    }

    private void initPerDays(View view) {
        mOneTimeDaysCount = (Spinner) view.findViewById(R.id.days_count);
        NumbersListAdapter listAdapter = new NumbersListAdapter(getContext(), 100, false);
        mOneTimeDaysCount.setAdapter(listAdapter);
        final TextView daysText = (TextView) view.findViewById(R.id.days_name);
        mOneTimeDaysCount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String name = getResources().getQuantityString(R.plurals.days, i + 1);
                boolean isInit = isFirstInitialization(daysText);
                daysText.setText(name);
                if (!isInit) {
                    onClicked(mOneTimeDays);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        mOneTimeDaysCount.setSelection(1);
    }

    private void initExerciseSelector(View view) {
        mBodyMap = (BodyMap) view.findViewById(R.id.body_map);
        ImageView iconBackground = (ImageView) mBodyMap.findViewById(R.id.body_background);
        int color = ContextCompat.getColor(view.getContext(), R.color.notify_hot);
        iconBackground.setColorFilter(color);
        Spinner exercises = (Spinner) view.findViewById(R.id.exercise);
        ExercisesAdapter adapter = new ExercisesAdapter(getContext());
        exercises.setAdapter(adapter);
        exercises.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                mButtonOK.setEnabled(i > 0);
                updateBodyMap(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void updateBodyMap(int exerciseIndex) {
        if (exerciseIndex > 0) {
            mExercise = mExercises.get(exerciseIndex - 1);
            List<String> affected = getCoach().getAffectedParts(mExercise);
            mBodyMap.apply(affected);
        } else {
            mBodyMap.reset();
            mExercise = null;
        }
    }

    private View createInterface() {
        return View.inflate(getActivity(), R.layout.add_booster_dialog, null);
    }

    @Override
    public void onDestroy() {
        mCoachTalk.unbind();
        super.onDestroy();
    }

    private Coach getCoach() {
        return mCoachTalk.getCoach();
    }

    public void onClicked(RadioButton button) {
        if (mSelectedButton == button) {
            return;
        }
        mSelectedButton = button;
        updateStatus(mEveryDay);
        updateStatus(mEveryWeekDay);
        updateStatus(mOneTimeDays);
        updateStatus(mTimesPerWeek);
    }

    private void updateStatus(RadioButton button) {
        button.setChecked(button == mSelectedButton);
    }


    //-----------------------------------------------------------------------------------

    private class ExercisesAdapter extends SpinnerSimpleAdapter
    {
        public ExercisesAdapter(Context context) {
            super(context);
        }

        @Override
        public int getCount() {
            return mExercises.size() + 1;
        }

        @Override
        public String getItem(int position) {
            if (position == 0) {
                return getString(R.string.booster_exercise_not_selected);
            }
            --position;
            Exercise exercise = mExercises.get(position);
            return getCoach().getName(exercise);
        }
    }

    private List<Exercise> mExercises;
    private Button mButtonOK;
    private CoachTalk mCoachTalk;
    private BodyMap mBodyMap;
    private RadioButton mEveryDay;
    private RadioButton mEveryWeekDay;
    private RadioButton mOneTimeDays;
    private RadioButton mTimesPerWeek;
    private RadioButton mSelectedButton;
    private Spinner mTimesPerWeekWorkouts;
    private Spinner mOneTimeDaysCount;
    private Exercise mExercise;
}
