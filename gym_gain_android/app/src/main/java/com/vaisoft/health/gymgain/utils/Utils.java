package com.vaisoft.health.gymgain.utils;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.ColorInt;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.AlternateListItemBackground;
import com.vaisoft.health.gymgain.data.Localizer;
import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.dialogs.ShowErrorDialog;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class Utils {

    public static JSONArray loadJSONArray(Context context, String fileName, FileTarget target) {
        try {
            String json = getJSON(context, fileName, target);
            return new JSONArray(json);
        } catch (Exception ex) {
            return null;
        }
    }

    public static JSONObject loadJSONObject(Context context, String fileName, FileTarget target) {
        try {
            String json = getJSON(context, fileName, target);
            return new JSONObject(json);
        } catch (Exception ex) {
            return null;
        }
    }

    static private String getJSON(Context context, String fileName) throws Exception {
        try {
            return getJSON(context, fileName, FileTarget.FILE_TARGET_PRIVATE);
        } catch (Exception ex) {
            return getJSON(context, fileName, FileTarget.FILE_TARGET_ASSETS);
        }
    }

    static public String getJSON(Context context, String fileName, FileTarget target) throws Exception {
        if (target == FileTarget.FILE_TARGET_DYNAMIC) {
            return getJSON(context, fileName);
        }
        String fullFileName = fileName + ".json";
        InputStream is = null;
        switch (target) {
            case FILE_TARGET_ASSETS:
                is = context.getAssets().open(fullFileName);
                break;
            case FILE_TARGET_PRIVATE:
                is = context.openFileInput(fullFileName);
                break;
            default:
                Log.e(Utils.class.getName(), "Unknown file target");
        }
        return readString(is);
    }

    public static boolean isFileExist(Context context, String fileName) {
        fileName += ".json";
        File file = context.getFileStreamPath(fileName);
        if  ((file != null) && file.exists()) {
            return true;
        }
        try {
            InputStream stream = context.getAssets().open(fileName);
            stream.close();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static String readString(InputStream is) throws IOException {
        BufferedInputStream bis = new BufferedInputStream(is);
        int size = bis.available();
        byte[] buffer = new byte[size];
        bis.read(buffer);
        bis.close();
        return new String(buffer, "UTF-8");
    }

    public static void saveJSON(Context context, String fileName, Object data)
    {
        try {
            FileOutputStream outputStream = context.openFileOutput(fileName + ".json", Context.MODE_PRIVATE);
            writeString(outputStream, data);
        } catch (Exception ex) {
            Log.e(Utils.class.getName(), ex.getMessage());
        }
    }

    public static void writeString(OutputStream os, Object data) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(os);
        bos.write(data.toString().getBytes());
        bos.close();
    }

    public static void showErrorDialog(FragmentManager fragmentManager, int message)
    {
        ShowErrorDialog err = new ShowErrorDialog();
        Bundle bundle = new Bundle();
        bundle.putInt(ShowErrorDialog.DATA, message);
        err.setArguments(bundle);
        err.show(fragmentManager, "ShowErrorDialog");
    }

    public static void openStore(Context context)
    {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
        }
    }

    public static void openTutorial(Context context) {
        String video = context.getString(R.string.tutorial_video);
        String uri = "http://www.youtube.com/watch?v=" + video;
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
    }

    public static void openSocial(Context context) {
        String page = context.getString(R.string.social_page);
        String uri = "http://fb.me/" + page;
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(uri)));
    }

    public static String convertListToString(List<String> data, Localizer localizer)
    {
        if ((data == null) || data.isEmpty()) {
            return "";
        }
        boolean coma = false;
        StringBuilder result = new StringBuilder();
        for (String item: data) {
            if (coma) {
                result.append(", ");
            } else {
                coma = true;
            }
            if (localizer != null) {
                result.append(localizer.translate(item));
            } else {
                result.append(item);
            }
        }
        return result.toString();
    }

    public static long getHours(long diffSec) {
        return diffSec / 60 / 60;
    }

    public static long getMins(long diffSec) {
        return (diffSec / 60) % 60;
    }

    public static long getSecs(long diffSec) {
        return diffSec % 60;
    }

    public static long getTime(long h, long min, long sec) {
        return (sec + min * 60 + h * 60 * 60) * 1000;
    }

    public static boolean isStringDefined(String s) {
        return (s != null) && (!s.isEmpty());
    }

    public static String getElapsedFormat(long diffMs, boolean whitespaces, boolean precise) {
        long diffSec = diffMs / 1000;
        long sec = Utils.getSecs(diffSec);
        long min = Utils.getMins(diffSec);
        long hrs = Utils.getHours(diffSec);
        StringBuilder timeText = new StringBuilder();
        if ((!precise) || (hrs > 0)) {
            appendNumber(timeText, hrs);
            timeText.append(getTimeSeparator(whitespaces));
        }
        appendNumber(timeText, min);
        timeText.append(getTimeSeparator(whitespaces));
        appendNumber(timeText, sec);
        if (precise) {
            appendTenth(timeText, diffMs, whitespaces);
        }
        return timeText.toString();
    }

    private static char getDecimalSeparator() {
        if (sDecimalSeparator == 0) {
            sDecimalSeparator = '.';
            NumberFormat nf = NumberFormat.getInstance();
            if (nf instanceof DecimalFormat) {
                DecimalFormatSymbols sym = ((DecimalFormat) nf).getDecimalFormatSymbols();
                sDecimalSeparator = sym.getDecimalSeparator();
            }
        }
        return sDecimalSeparator;
    }

    private static void appendTenth(StringBuilder timeText, long ms, boolean whitespaces) {
        long tenth = (long) ((ms % 1000) / 100.f + 0.5f);
        if (tenth > 9) {
            tenth = 9;
        }
        char separator = getDecimalSeparator();
        timeText.append(separator);
        if (whitespaces) {
            timeText.append(' ');
        }
        timeText.append(tenth);
    }

    public static long parseElapsedFormat(String time) {
        if (time.isEmpty()) {
            return 0;
        }
        long result = 0;
        int tenthSeparator = getTenthSeparatorIndex(time);
        if (tenthSeparator != -1) {
            if (tenthSeparator < (time.length() - 1)) {
                String sTenth = "0." + time.substring(tenthSeparator + 1);
                float fTenth = Float.parseFloat(sTenth);
                result = (long) (fTenth * 1000);
            }
            time = time.substring(0, tenthSeparator);
        }
        String chunks[] = time.split(getTimeSeparator(false));
        if (chunks.length > 0) {
            long sec = Long.parseLong(chunks[chunks.length - 1]);
            result += sec * 1000;
        }
        if (chunks.length > 1) {
            long min = Long.parseLong(chunks[chunks.length - 2]);
            result += min * 60 * 1000;
        }
        if (chunks.length > 2) {
            long hrs = Long.parseLong(chunks[chunks.length - 3]);
            result += hrs * 60 * 60 * 1000;
        }
        return result;
    }

    private static int getTenthSeparatorIndex(String time) {
        int index = time.lastIndexOf('.');
        if (index != -1) {
            return index;
        }
        return time.lastIndexOf(',');
    }

    public static String getElapsedFormat(DateTime startDate, DateTime finishDate, boolean whitespaces) {
        return getElapsedFormat(finishDate.getMillis() - startDate.getMillis(), whitespaces, false);
    }

    private static String getTimeSeparator(boolean whitespaces) {
        if (whitespaces) {
            return " : ";
        } else {
            return ":";
        }
    }

    private static void appendNumber(StringBuilder target, long number) {
        if (number < 10) {
            target.append(0);
        }
        target.append(number);
    }

    public static String toHexForHTML(int color) {
        return  String.format("#%06X", (0xFFFFFF & color));
    }

     public static int mapCalendarWeekDayToJoda(int day) {
         switch (day) {
             case Calendar.MONDAY:
                 return DateTimeConstants.MONDAY;
             case Calendar.TUESDAY:
                 return DateTimeConstants.TUESDAY;
             case Calendar.WEDNESDAY:
                 return DateTimeConstants.WEDNESDAY;
             case Calendar.THURSDAY:
                 return DateTimeConstants.THURSDAY;
             case Calendar.FRIDAY:
                 return DateTimeConstants.FRIDAY;
             case Calendar.SATURDAY:
                 return DateTimeConstants.SATURDAY;
             case Calendar.SUNDAY:
                 return DateTimeConstants.SUNDAY;
         }
         return 0;
     }

    public static boolean isBeforeOrEqualDay(DateTime source, DateTime target) {
        if (source.isBefore(target)) {
            return true;
        }
        return isEqualDay(source, target);
    }

    public static boolean isAfterOrEqualDay(DateTime source, DateTime target) {
        if (source.isAfter(target)) {
            return true;
        }
        return isEqualDay(source, target);
    }

    public static boolean isEqualDay(DateTime source, DateTime target) {
        return (source.year().equals(target.year())) &&
                (source.monthOfYear().equals(target.monthOfYear())) &&
                (source.dayOfMonth().equals(target.dayOfMonth()));
    }


    public static List<DateTime> clampDates(List<DateTime> source, DateTime start, DateTime finish) {
        List<DateTime> result = new ArrayList<>();
        for (DateTime date: source) {
            if (date.isBefore(start) || date.isAfter(finish)) {
                continue;
            }
            result.add(date);
        }
        return result;
    }

    public static String convertValuesToString(List<Value> data, Localizer localizer) {
        StringBuilder result = new StringBuilder();
        boolean comaAims = false;
        for (Value value: data) {
            if (comaAims) {
                result.append(", ");
            } else {
                comaAims = true;
            }
            result.append(value.toString());
            result.append(' ');
            result.append(localizer.translate(value.unit().toString()));
        }
        result.append(')');
        return result.toString();
    }

    public static int getEstimateColor(Progress.Estimate estimate, Context context) {
        switch (estimate) {
            case GOOD:
                return ContextCompat.getColor(context, R.color.chart_good);
            case BAD:
                return ContextCompat.getColor(context, R.color.chart_bad);
            case SOSO:
                return ContextCompat.getColor(context, R.color.chart_soso);
        }
        return 0;
    }

    public static String getHtmlColor(int colorResource, Context context) {
        int color = ContextCompat.getColor(context, colorResource);
        return Utils.toHexForHTML(color);
    }

    public static String getEstimateColorHtml(Progress.Estimate estimate, Context context) {
        switch (estimate) {
            case GOOD:
                if (mColorGoodHtml == null) {
                    mColorGoodHtml = getHtmlColor(R.color.chart_good, context);
                }
                return mColorGoodHtml;
            case BAD:
                if (mColorBadHtml == null) {
                    mColorBadHtml = getHtmlColor(R.color.chart_bad, context);
                }
                return mColorBadHtml;
            case SOSO:
                if (mColorSosoHtml == null) {
                    mColorSosoHtml = getHtmlColor(R.color.chart_soso, context);
                }
                return mColorSosoHtml;
        }
        return null;
    }

    private static NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    public static boolean isNetworkConnection(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return info != null && info.isConnected();
    }

    public static boolean isNetworkConnectionWifi(Context context) {
        NetworkInfo info = getNetworkInfo(context);
        return info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI;
    }

    @SuppressWarnings("deprecation")
    public static Spanned html(String source) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(source);
        }
    }

    public static void appendHint(StringBuilder target, String text) {
        target.append(" <small>/<i>");
        target.append(text);
        target.append("</i>/</small>");
    }

    public static String formatDateTime(DateTime dateTime) {
        DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM,
                Locale.getDefault());
        return formatter.format(dateTime.toDate());
    }

    public static String formatDate(DateTime dateTime) {
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault());
        return formatter.format(dateTime.toDate());
    }


    public static void initOrientationAwareList(RecyclerView target) {
        Context context = target.getContext();
        boolean isPortrait = context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
        int columnsCount = isPortrait ? 1 : 2;
        target.setLayoutManager(new GridLayoutManager(context, columnsCount));

        @ColorInt int color1 = ContextCompat.getColor(context, R.color.window_background);
        @ColorInt int color2 = ContextCompat.getColor(context, R.color.window_background_dark);
        RecyclerView.ItemDecoration decorator = new AlternateListItemBackground(isPortrait, color1, color2);
        target.addItemDecoration(decorator);
    }

    public static DateTime now() {
        return DateTime.now();
    }

    public static Activity getActivity(Context context) {
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity)context;
            }
            context = ((ContextWrapper)context).getBaseContext();
        }
        return null;
    }


    public enum FileTarget
    {
        FILE_TARGET_ASSETS,
        FILE_TARGET_PRIVATE,
        FILE_TARGET_DYNAMIC
    }

    private static String mColorBadHtml;
    private static String mColorGoodHtml;
    private static String mColorSosoHtml;
    private static char sDecimalSeparator = 0;

}
