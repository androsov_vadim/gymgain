package com.vaisoft.health.gymgain.edit;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.core.app.NavUtils;
import androidx.core.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Record;
import com.vaisoft.health.gymgain.dialogs.ConfirmDialog;
import com.vaisoft.health.gymgain.dialogs.DeleteConfirmDialog;
import com.vaisoft.health.gymgain.dialogs.SaveConfirmDialog;
import com.vaisoft.health.gymgain.dialogs.ScheduleDialog;
import com.vaisoft.health.gymgain.main.MainActivity;
import com.vaisoft.health.gymgain.utils.Utils;

public abstract class EditorActivity extends CoachActivity
    implements SaveConfirmDialog.Listener, ConfirmDialog.Listener, DeleteConfirmDialog.Listener {

    protected abstract boolean isEditable();
    protected abstract boolean isDeletable();
    protected abstract boolean canAutoSave();
    protected abstract void deleteObjectConfirmed();
    protected abstract int getEditTargetId();
    protected abstract boolean saveChanges();

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        autoSave();
        outState.putBoolean(CHANGED, mChanged);
    }


    protected void deleteSchedule() {
        Record record = getRecord();
        if (record == null) {
            return;
        }
        getCoach().deleteRecord(this, record);
        getCoach().saveRecords(this);
    }


    private void autoSave() {
        if (canAutoSave()) {
            forceSave();
        }
    }

    protected void forceSave() {
        if (isChanged()) {
            boolean ok = saveChanges();
            if (ok) {
                setChanged(false);
            }
        }
    }

    protected Record getRecord() {
        return null;
    }

    protected Record createRecord() {
        return null;
    }

    protected void showRecordDialog(Record record) {
        ScheduleDialog dialog = new ScheduleDialog();
        if (record != null) {
            Bundle bundle = new Bundle();
            bundle.putString(ScheduleDialog.RECORD, record.getSource().getKey());
            dialog.setArguments(bundle);
        }
        dialog.show(getSupportFragmentManager(), "ScheduleDialog");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mChanged = savedInstanceState.getBoolean(CHANGED);
    }

    protected void setHomeButtonAsUp() {
        if (getSupportActionBar() != null) {
            mHomeEnabled = true;
            Drawable homeButton = ContextCompat.getDrawable(this, R.drawable.ic_home_white_24dp);
            getSupportActionBar().setHomeAsUpIndicator(homeButton);
        }
    }

    public void notifyChanged() {
        setChanged(true);
    }

    protected void setChanged(boolean changed) {
        mChanged = changed;
    }

    protected boolean isChanged() {
        return mChanged;
    }

    @Override
    public void onBackPressed() {
        if (!isCoach()) {
            setChanged(false);
        }
        goBack();
    }

    @Override
    public boolean onConfirmed(int code) {
        return false;
    }

    @Override
    public boolean onCanceled(int code) {
        return false;
    }

    protected void goBack() {
        mGoHome = false;
        returnFromActivity();
    }

    protected void returnFromActivity() {
        autoSave();
        if (isChanged()) {
            SaveConfirmDialog dialog = new SaveConfirmDialog();
            dialog.show(getSupportFragmentManager(), "SaveConfirmDialog");
            return;
        }
        super.returnFromActivity();
        if (mHomeEnabled && mGoHome) {
            Intent intent = NavUtils.getParentActivityIntent(this);
            intent.putExtra(MainActivity.ACTIVE_TAB, MainActivity.TAB_FEED);
            NavUtils.navigateUpTo(this, intent);
        } else {
            finish();
        }
    }

    @Override
    public boolean onSaveChanges() {
        boolean ok = saveChanges();
        if (ok) {
            setChanged(false);
            returnFromActivity();
        }
        return ok;
    }

    @Override
    public void onDiscardChanges() {
        setChanged(false);
        returnFromActivity();
    }

    public void onDeleteEditedObject() {
        DeleteConfirmDialog confirm = new DeleteConfirmDialog();
        Bundle args = new Bundle();
        args.putInt(DeleteConfirmDialog.MESSAGE, getEditTargetId());
        confirm.setArguments(args);
        confirm.show(getSupportFragmentManager(), "DeleteConfirmDialog");
    }

    @Override
    public void onDeleteConfirmed() {
        setChanged(false);
        deleteObjectConfirmed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isEditable()) {
            getMenuInflater().inflate(R.menu.editor_actions, menu);
        }
        return true;
    }

    protected void goHome() {
        mGoHome = true;
        returnFromActivity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (isCoach()) {
            if (processMenuCommand(item.getItemId())) {
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean processMenuCommand(int itemId) {
        switch (itemId) {
            case android.R.id.home:
                goHome();
                return true;
            case R.id.editor_action_save:
                onSaveChanges();
                return true;
            case R.id.editor_action_discard:
                onDiscardChanges();
                return true;
            case R.id.editor_action_delete:
                if (isDeletable()) {
                    onDeleteEditedObject();
                } else {
                    Utils.showErrorDialog(getSupportFragmentManager(), R.string.error_object_used);
                }
                return true;
        }
        return false;
    }

    private boolean mChanged;
    private boolean mGoHome;
    private boolean mHomeEnabled;

    private static final String CHANGED = "changed";
}
