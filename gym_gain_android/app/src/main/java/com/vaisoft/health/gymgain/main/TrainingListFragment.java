package com.vaisoft.health.gymgain.main;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.BodyMap;
import com.vaisoft.health.gymgain.controls.CoachListFragment;
import com.vaisoft.health.gymgain.controls.ContextViewHolder;
import com.vaisoft.health.gymgain.coach.Coach;
import com.vaisoft.health.gymgain.data.Training;
import com.vaisoft.health.gymgain.edit.TrainingDetailActivity;

import java.util.List;

public class TrainingListFragment extends MainActivityFragment {

    @Override
    public void onContextMenu(int id) {
        switch (id) {
            case ContextViewHolder.MENU_EDIT:
                mAdapter.edit(mAdapter.getContextItem());
                break;
            case ContextViewHolder.MENU_DELETE:
                mAdapter.deleteContextItem();
                break;
        }
    }

    @Override
    protected Adapter createAdapter() {
        mAdapter = new TrainingRecyclerViewAdapter();
        return mAdapter;
    }

    public class TrainingRecyclerViewAdapter
            extends CoachListFragment.Adapter<TrainingRecyclerViewAdapter.ViewHolder>
    {
        @Override
        public void updateData(boolean force) {
            if (force || getCoach().shouldUpdateTrainingList()) {
                mData = getCoach().getFilteredTrainings(getFilter());
                notifyDataSetChanged();
            }
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
        {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.training_item, parent, false);
            return new ViewHolder(view, this);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position)
        {
            Training training = mData.get(position);

            Coach coach = getCoach();
            holder.textViewName.setText(coach.getName(training.getId()));

            List<String> parts = coach.getAffectedByExercisesParts(training.getExercises());
            holder.textViewAffected.setText(coach.getAffectedString(parts));
            holder.bodyMap.apply(parts);
            holder.bodyMap.setBackgroundColor(R.color.training_background);
        }

        @Override
        public int getItemCount()
        {
            if (mData != null) {
                return mData.size();
            } else {
                return 0;
            }
        }

        public void edit(int item) {
            Context context = getContext();
            Intent intent = new Intent(context, TrainingDetailActivity.class);
            intent.putExtra(TrainingDetailActivity.ITEM, getTrainingId(item));

            context.startActivity(intent);
        }

        public void deleteContextItem() {
            Context context = getContext();
            String trainingId = getTrainingId(getContextItem());
            getCoach().deleteTraining(context, trainingId);
            updateData(true);
        }

        private String getTrainingId(int index) {
            return mData.get(index).getId();
        }

        public class ViewHolder extends ContextViewHolder
        {
            public final TextView textViewName;
            public final TextView textViewAffected;
            public final BodyMap bodyMap;

            @Override
            protected void clicked() {
                int item = getAdapterPosition();
                if (item != -1) {
                    edit(item);
                }
            }

            @Override
            protected boolean isDeletable() {
                return true;
            }

            public ViewHolder(View view, Adapter adapter) {
                super(view, adapter);
                textViewName = view.findViewById(R.id.name);
                textViewAffected = view.findViewById(R.id.affected);
                bodyMap = view.findViewById(R.id.body_map);
            }
        }

        private List<Training> mData;
    }

    private TrainingRecyclerViewAdapter mAdapter;
}
