package com.vaisoft.health.gymgain.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;

import com.vaisoft.health.gymgain.R;

import java.util.ArrayList;
import java.util.List;

public class AddActionDialog extends DialogFragment
{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        initList();
        CharSequence[] cs = mList.toArray(new CharSequence[mList.size()]);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setItems(cs, new SelectionHandler());
        builder.setNegativeButton(R.string.cancel, null);
        builder.setTitle(R.string.add_feed);
        return builder.create();
    }

    private void initList() {
        mList = new ArrayList<>();
        mList.add(getResources().getString(R.string.body_log_new));
        mList.add(getResources().getString(R.string.booster_add));
        Bundle bundle = getArguments();
        ArrayList<String> schedules = bundle.getStringArrayList(DATA);
        mSchedulesExist = !schedules.isEmpty();
        if (mSchedulesExist) {
            mList.addAll(schedules);
        } else {
            mList.add(getResources().getString(R.string.list_trainings));
            mList.add(getResources().getString(R.string.list_exercises));
        }
    }

    class SelectionHandler implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Intent intent  = new Intent();
            if (which == 0) {
                intent.putExtra(BODY, true);
            } else if (which == 1) {
                intent.putExtra(BOOSTER, true);
            } else {
                if (mSchedulesExist) {
                    intent.putExtra(RECORD, which - 2);
                } else {
                    intent.putExtra(getListId(which - 1), true);
                }
            }
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        }
    }

    private String getListId(int item) {
        switch (item) {
            case 1: return LIST_TRAININGS;
            case 2: return LIST_EXERCISES;
        }
        return null;
    }

    private List<String> mList;
    private boolean mSchedulesExist = false;

    public static final String DATA = "items";
    public static final String RECORD = "record";
    public static final String LIST_EXERCISES = "exercises";
    public static final String LIST_TRAININGS = "trainings";
    public static final String BODY = "body";
    public static final String BOOSTER = "booster";
}
