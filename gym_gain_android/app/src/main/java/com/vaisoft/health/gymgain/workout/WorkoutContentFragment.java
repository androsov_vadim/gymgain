package com.vaisoft.health.gymgain.workout;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.FragmentManager;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.controls.CoachFragment;
import com.vaisoft.health.gymgain.controls.ExerciseResultEditor;
import com.vaisoft.health.gymgain.controls.InfoSearcher;
import com.vaisoft.health.gymgain.controls.RotateFriendlyPagerAdapter;
import com.vaisoft.health.gymgain.controls.ViewPagerWrapContent;
import com.vaisoft.health.gymgain.data.Exercise;
import com.vaisoft.health.gymgain.data.Unit;
import com.vaisoft.health.gymgain.data.Value;
import com.vaisoft.health.gymgain.data.Workout;
import com.vaisoft.health.gymgain.dialogs.ViewPreviousWorkoutDialog;
import com.vaisoft.health.gymgain.utils.Utils;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class WorkoutContentFragment extends WorkoutFragment
        implements ExerciseResultEditor.OnValueChangedListener {

    public void setExerciseChangeListener(ActiveExerciseChangeListener listener) {
        mExerciseChangeListener = listener;
    }

    @Override
    protected int getRootResource() {
        return R.layout.activity_workout_edit_page;
    }

    @Override
    protected void init() {
        Bundle args = getArguments();
        mSetId = args.getInt(SET);
        mCurrentRep = args.getInt(EXERCISE);
        mWorkout = getWorkout();
        mEntry = mWorkout.getEntry(mSetId);
        Workout previousWorkout = getPreviousWorkout();
        if (previousWorkout != null) {
            mPreviousEntry = previousWorkout.getSameEntry(mEntry);
        }
        initButtons();
        initSetsList();
        initTabs();
        initComments();
    }

    private void initComments() {
        TextView prevComment = getView().findViewById(R.id.prevComment);
        mPostComment = getView().findViewById(R.id.postComment);
        TextView setComment = getView().findViewById(R.id.setComment);

        if (mPreviousEntry != null) {
            WorkoutActivity.setHideableComment(mPreviousEntry.postComment, prevComment);
        } else {
            WorkoutActivity.setHideableComment(null, prevComment);
        }

        WorkoutActivity.setHideableComment(mEntry.setComment, setComment);
        initPostComment();
    }

    private void initPostComment() {
        WorkoutActivity.setAlwaysVisibleComment(mEntry.postComment, mPostComment);
        mPostComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WorkoutActivity parent = getWorkoutActivity();
                parent.showPostCommentDialog(mEntry.postComment, mSetId);
            }
        });
    }

    private WorkoutActivity getWorkoutActivity() {
        return (WorkoutActivity) getActivity();
    }

    private void initSetsList() {
        mResults = getView().findViewById(R.id.set_results);
        mResults.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        mResultsAdapter = new EditableSetResultAdapter();
        mResults.setAdapter(mResultsAdapter);
        if (mCurrentRep == -1) {
            mCurrentRep = mResultsAdapter.getCount() - 1;
        }
        select(mCurrentRep);
        mResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onSelected(i);
            }
        });
        if (mResultsAdapter.canBeActive()) {
            mTicker = new WorkoutTicker(mResults, mResultsAdapter);
            setTickerActive(true);
        }
    }

    private void onSelected(int index) {
        if (index == mResultsAdapter.getCount() - 1) {
            createSet(true);
        } else if (index != mCurrentRep) {
            select(index);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setTickerActive(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        setTickerActive(false);
    }

    private void setTickerActive(boolean active) {
        if (mTicker != null) {
            mTicker.setActive(active);
        }
    }

    private void initButtons() {
        mNextSetButton = getView().findViewById(R.id.next_set_button);
        mNextSetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onNextSet();
            }
        });

        ImageButton cancelSet = getView().findViewById(R.id.cancel_set_button);
        cancelSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCurrentRep < mResultsAdapter.getCount() - 1) {
                    deleteSet();
                }
            }
        });

        ImageButton previousWorkoutView = getView().findViewById(R.id.view_prev_button);
        previousWorkoutView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewPreviousWorkout();
            }
        });

        InfoSearcher informer = getView().findViewById(R.id.ex_info);
        informer.setListener(new InfoSearcher.Listener() {
            @Override
            public void searchInfo() {
                String exercise = mPagerAdapter.getCurrentExercise();
                getCoach().searchInfo(exercise, getActivity());
            }
        });
    }

    private void viewPreviousWorkout() {
        Workout previousWorkout = getPreviousWorkout();
        if (previousWorkout != null) {
            ViewPreviousWorkoutDialog dialog = new ViewPreviousWorkoutDialog();
            Bundle bundle = new Bundle();
            bundle.putInt(SET, mSetId);
            getWorkoutActivity().writeWorkoutTo(bundle);
            dialog.setArguments(bundle);
            dialog.show(getFragmentManager(), "ViewPreviousWorkoutDialog");
        } else {
            Utils.showErrorDialog(getFragmentManager(), R.string.error_no_previous_workout);
        }
    }

    private void onNextSet() {
        if (isNoCurrentHasPrevious()) {
            loadPreviousResultsToCurrentSet();
            notifyResultsChanged();
            updateNextSetButtonName();
            if (isNextSet()) {
               selectNext();
            }
        } else if (isNextSet()) {
            selectNext();
        } else {
            createSet(true);
        }
        updateHighlightForSetsCount();
    }

    private void selectNext() {
        select(mCurrentRep + 1);
    }

    private boolean isNextSet() {
        return mEntry.isSet(mCurrentRep + 1);
    }

    private void updateHighlightForSetsCount() {
        boolean isResults = getEntry().isResults();
        WorkoutActivityEdit editor = (WorkoutActivityEdit) getActivity();
        editor.setActiveTabHighlight(!isResults);
    }

    private void deleteSet() {
        mEntry.removeSet(mCurrentRep);
        mResultsAdapter.notifyDataSetChanged();
        select(mCurrentRep);
        notifyChanged();
        updateHighlightForSetsCount();
    }

    private void createSet(boolean notify) {
        if (isAddNewSelected()) {
            createSetWithAddNew(notify);
            return;
        }
        if (isEmptySets()) {
            loadPreviousResultsToLastSet();
            createSets(false);
        } else {
            createSets(true);
        }
        if (notify) {
            selectFromEnding(1);
            notifyResultsChanged();
        }
    }

    private void createSetWithAddNew(boolean notify) {
        createSets(false);
        loadPreviousResultsToLastSet();
        if (notify) {
            selectFromEnding(0);
            if (mPagerAdapter != null) {
                mPagerAdapter.invalidate();
            }
            notifyResultsChanged();
        }
    }

    private void loadPreviousResultsToLastSet() {
        loadPreviousResultsToSet(-1);
    }

    private void loadPreviousResultsToCurrentSet() {
        loadPreviousResultsToSet(mCurrentRep);
    }

    private void loadPreviousResultsToSet(int index) {
        Workout.Entry compatibleEntry = getCompatibleEntryFromPreviousSet();
        for (Workout.ExerciseLogItem item : mEntry.log) {
            int itemIndex = (index != -1) ? index : (item.sets.size() - 1);
            Workout.Set set = null;
            if (compatibleEntry != null) {
                set = compatibleEntry.getExerciseSet(item.exercise, itemIndex);
            }
            if (set == null) {
                if (itemIndex == 0) {
                    continue;
                }
                set = mEntry.getExerciseSet(item.exercise, itemIndex - 1);
            }
            Workout.Set compatibleSet = item.getSet(itemIndex);
            compatibleSet.apply(set);
        }
    }

    private Workout.Entry getCompatibleEntryFromPreviousSet() {
        Workout.Entry previousEntry = getPreviousEntry();
        if (previousEntry == null) {
            return null;
        }
        if (!previousEntry.containsExercisesOf(mEntry)) {
            return null;
        }
        return previousEntry;
    }

    private boolean isAddNewSelected() {
        return mCurrentRep == (mResults.getCount() - 1);
    }

    private boolean isEmptySets() {
        for (Workout.ExerciseLogItem item: mEntry.log) {
            Workout.Set lastSet = item.getLastSet();
            if ((lastSet != null) && lastSet.isResults()) {
                return false;
            }
        }
        return true;
    }

    private void createSets(boolean activesPossible) {
        getWorkout().applyActiveValuesExcept(mEntry);
        for (Workout.ExerciseLogItem item: mEntry.log) {
            Workout.Set newSet = createSet(item, activesPossible);
            item.sets.add(newSet);
        }
    }

    private Workout.Set createSet(Workout.ExerciseLogItem item, boolean activesPossible) {
        Workout.Set newSet = new Workout.Set();
        if (activesPossible && (!item.sets.isEmpty())) {
            Workout.Set set = item.getLastSet();
            Set<Value> applied = set.applyActiveValues(true);
            if (applied != null) {
                newSet.activateValues(applied);
            }
        }
        return newSet;
    }

    private void notifyResultsChanged() {
        mResultsAdapter.notifyDataSetChanged();
        notifyChanged();
    }

    private void selectFromEnding(int shift) {
        select(mResultsAdapter.getCount() - 1 - shift);
    }

    private void select(int position) {
        mResults.setItemChecked(position, true);
        mCurrentRep = position;
        updateNextSetButtonName();
        if ((mPagerAdapter != null) && (mCurrentRep < mResults.getCount())) {
            mPagerAdapter.invalidate();
        }
    }

    private void updateNextSetButtonName() {
        if (isNoCurrentHasPrevious()) {
            mNextSetButton.setText(R.string.set_apply);
        } else {
            mNextSetButton.setText(R.string.set_next);
        }
    }

    private boolean isNoCurrentHasPrevious() {
        boolean isPrevious = (mCurrentRep > 0) || ((mPreviousEntry != null) &&
                mPreviousEntry.isSet(mCurrentRep));
        return isPrevious &&
                mEntry.isSet(mCurrentRep) &&
                mEntry.isSetEmpty(mCurrentRep);
    }

    private void initTabs() {
        mPager = getView().findViewById(R.id.exercises_editors);
        mPagerAdapter = new Adapter(getChildFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        TabLayout tabs = getView().findViewById(R.id.exercises_tabs);
        if (mPagerAdapter.getCount() == 1) {
            tabs.setVisibility(View.GONE);
        } else {
            tabs.setupWithViewPager(mPager);
            tabs.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mPager) {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    super.onTabSelected(tab);
                    onExerciseSelected(tab.getPosition());
                }
            });
        }
    }

    public Workout.Entry getEntry() {
        return mEntry;
    }

    public Workout.Entry getPreviousEntry() {
        return mPreviousEntry;
    }

    public int getCurrentRep() {
        return mCurrentRep;
    }

    public Workout.Set getPreviousSet(String exercise, int rep) {
        Workout.Entry previousEntry = getPreviousEntry();
        Workout.Set result = null;
        if (previousEntry != null) {
            result = previousEntry.getExerciseSet(exercise, rep);
        }
        if ((result == null) && (rep > 0)) {
            result = mEntry.getExerciseSet(exercise, rep - 1);
        }
        return result;
    }

    private Unit getActualUnits(Unit base) {
        return getCoach().getConfig().getUnits(base);
    }

    private void apply(String exerciseId, Unit parameter, long rawValue, Value.Status status) {
        Unit unit = getActualUnits(parameter);
        Workout.Set set = mEntry.getExerciseSet(exerciseId, mCurrentRep);
        Value value = set.getValue(unit);
        Exercise exercise = getCoach().getExercise(exerciseId);
        if (value != null) {
            value.setStatus(status);
            value.set(unit, rawValue);
        } else {
            boolean shouldGrow = exercise.getParameter(parameter).shouldGrow();
            value = new Value(unit, rawValue, shouldGrow, status);
            set.putValue(value);
        }
        updateHighlightForSetsCount();

        Workout.Set previousSet = getPreviousSet(exerciseId, mCurrentRep);
        if (previousSet != null) {
            for (Map.Entry<Unit, Value> entry : previousSet.result.entrySet()) {
                Unit key = entry.getKey();
                Unit previousUnit = entry.getValue().unit();
                if (set.getValue(previousUnit) == null) {
                    boolean shouldGrow = exercise.getParameter(key).shouldGrow();
                    Unit actualUnit = getActualUnits(key);
                    long prevValue = entry.getValue().get(actualUnit);
                    value = new Value(actualUnit, prevValue, shouldGrow, status);
                    set.putValue(value);
                }
            }
        }

        updateNextSetButtonName();
    }

    @Override
    public void onChanged(String exerciseId, Unit unit, long value, Value.Status status) {
        boolean addingNew = mCurrentRep == mResultsAdapter.getCount() - 1;
        if (addingNew) {
            createSet(false);
        }
        apply(exerciseId, unit, value, status);
        if (addingNew) {
            selectFromEnding(1);
        }
        notifyResultsChanged();
        if (Value.canBeActive(unit)) {
            notifyExerciseUpdate();
        }
    }

    public void setPostComment(String text) {
        mPostComment.setText(text);
        mEntry.postComment = text;
    }

    public String getExercise(int index) {
        return getEntry().log.get(index).exercise;
    }

    public void onExerciseSelected(int position) {
        mCurrentExercise = position;
        notifyExerciseUpdate();
    }

    private void notifyExerciseUpdate() {
        if (mExerciseChangeListener != null) {
            mExerciseChangeListener.onActiveExerciseChanged(mCurrentExercise);
        }
    }

    public int getCurrentExercise() {
        return mCurrentExercise;
    }

//----------------------------------------------------------------

    private class EditableSetResultAdapter extends SetResultsAdapter {

        public EditableSetResultAdapter() {
            super(mEntry, mPreviousEntry, getCoach(), getContext());
        }

        @Override
        public int getCount() {
            return super.getCount() + 1;
        }

        @Override
        protected boolean isData(int position) {
            return position < (getCount() - 1);
        }

        @Override
        public boolean tick(View view) {
            boolean isActive = super.tick(view);
            int position = getPosition(view);
            if (isActive && (position == mCurrentRep)) {
                mPagerAdapter.invalidate();
            }
            return isActive;
        }

        @Override
        protected void applyData(View marker, TextView text, int position) {
            if (isData(position)) {
                super.applyData(marker, text, position);
            } else {
                text.setText(R.string.add_new_set);
                marker.setVisibility(View.INVISIBLE);
            }
        }
    }

    private class Adapter extends RotateFriendlyPagerAdapter {

        public Adapter(FragmentManager fm) {
            super(fm);
            mExercises = mWorkout.getLogContent(mSetId);
        }

        @Override
        public CoachFragment getItem(int position) {
            SetFragment fragment = new SetFragment();
            fragment.setParent(WorkoutContentFragment.this);
            Bundle args = new Bundle();
            args.putInt(EXERCISE, position);
            fragment.setArguments(args);
            return fragment;
        }

        public String getCurrentExercise() {
            return mExercises.get(mCurrentExercise);
        }

        @Override
        public int getCount() {
            return mExercises.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String exerciseId = mExercises.get(position);
            String title = getCoach().getLocalized(exerciseId);
            Exercise exercise = getCoach().getExercise(exerciseId);
            List<Value> targeted = exercise.getTargetedParameters();
            if (!targeted.isEmpty()) {
                title += " (" + Utils.convertValuesToString(targeted, getCoach().getLocalizer()) + ")";
            }
            return title;
        }

        private List<String> mExercises;
    }

    private Workout mWorkout;
    private int mSetId = -1;
    private Workout.Entry mEntry;
    private Workout.Entry mPreviousEntry;
    private EditableSetResultAdapter mResultsAdapter;
    private ListView mResults;
    private Adapter mPagerAdapter;
    private ViewPagerWrapContent mPager;
    private int mCurrentRep;
    private TextView mPostComment;
    private WorkoutTicker mTicker;
    private int mCurrentExercise;
    private ActiveExerciseChangeListener mExerciseChangeListener;
    private Button mNextSetButton;
}
