package com.vaisoft.health.gymgain.billing;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import androidx.annotation.StyleRes;
import android.util.Log;

import com.android.vending.billing.IInAppBillingService;
import com.vaisoft.health.gymgain.BuildConfig;
import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.utils.ShopActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

public class Billing implements ServiceConnection {

    public Billing(Activity activity, boolean fullData, ReadyListener listener) {
        mActivity = activity;
        mListener = listener;
        mFullData = fullData;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mService = IInAppBillingService.Stub.asInterface(service);
        loadPurchases();
    }

    public void update() {
        loadPurchases();
    }

    private void readyBasic() {
        if (mFullData && mOk) {
            loadPurchasesDetails();
        } else {
            ready();
        }
    }

    private void loadPurchases() {
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                readyBasic();
            }
        };
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                mPurchaseIndex = -1;
                loadPurchases(handler, null);
            }
        });
        thread.start();
    }

    private void loadPurchases(Handler handler, String token) {
        Bundle ownedItems = null;
        try {
            ownedItems = mService.getPurchases(3, mActivity.getPackageName(), "inapp", token);
        } catch (RemoteException e) {
            error(e.getMessage(), handler);
            return;
        }

        int response = ownedItems.getInt("RESPONSE_CODE");
        if (response != 0) {
            error("Billing details response fail: " + response, handler);
            return;
        }

        loadPurchases(ownedItems, handler);

        String continuationToken =
                ownedItems.getString("INAPP_CONTINUATION_TOKEN");

        if ((continuationToken != null) && (!continuationToken.isEmpty())) {
            loadPurchases(handler, continuationToken);
        } else {
            mOk = true;
            handler.sendEmptyMessage(0);
        }
    }

    public boolean isOk() {
        return mOk;
    }

    public boolean isBasicOrBetter() {
        return mPurchaseIndex >= ID_BASIC;
    }

    public boolean isProOrBetter() {
        return mPurchaseIndex >= ID_PRO;
    }

    public boolean isPremiumOrBetter() {
        return mPurchaseIndex >= ID_PREMIUM;
    }

    public boolean isBasicPurchased() {
        return mPurchaseIndex == ID_BASIC;
    }

    public boolean isProPurchased() {
        return mPurchaseIndex == ID_PRO;
    }

    public boolean isPremiumPurchased() {
        return mPurchaseIndex == ID_PREMIUM;
    }

    public String getPriceBasic() {
        return mPrices[ID_BASIC];
    }

    public String getPricePro() {
        return mPrices[ID_PRO];
    }

    public String getPricePremium() {
        return mPrices[ID_PREMIUM];
    }

    public @StyleRes int getTheme() {
        switch (mPurchaseIndex) {
            case -1:
                return R.style.Theme_Free;
            case 0:
                return R.style.Theme_Basic;
            case 1:
                return R.style.Theme_Pro;
            case 2:
                return R.style.Theme_Premium;
            default:
                return R.style.Theme_AppDesign;
        }
    }

    public boolean isAdsEnabled() {
        return !(isOk() && isBasicOrBetter());
    }

    public boolean doShowShopButton() {
        return !(isOk() && isProOrBetter());
    }

    private void loadPurchases(Bundle ownedItems, Handler handler) {
        ArrayList<String> ownedSkus =
                ownedItems.getStringArrayList("INAPP_PURCHASE_ITEM_LIST");
        ArrayList<String>  signatureList =
                ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");
        ArrayList<String>  purchaseDataList =
                ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");

         for (int i = 0; i < ownedSkus.size(); ++i) {
            String signature = signatureList.get(i);
            String data = purchaseDataList.get(i);
            if (!checkSignature(data, signature)) {
                error("Signature fail", handler);
                return;
            }
            //if (consume(data)) continue; //TODO ATTENTION! NEVER COMMIT!!
            String sku = ownedSkus.get(i);
            int index = getIndex(sku);
            if (index != -1) {
                if (index > mPurchaseIndex) {
                    mPurchaseIndex = index;
                }
            } else {
                error("Unknown item " + sku, handler);
                return;
            }
        }
    }

    private boolean consume(String data) {
        if (!BuildConfig.DEBUG) {
            Log.e(getClass().getName(), "Purchases drop is available in debug mode only");
            return false;
        }
        try {
            JSONObject dataObject = new JSONObject(data);
            String token = dataObject.getString("purchaseToken");
            mService.consumePurchase(3, mActivity.getPackageName(), token);
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage());
        }
        return true;
    }

    private boolean checkSignature(String data, String signature) {
        String key = getKey();
        return Security.verifyPurchase(key, data, signature);
    }

    private void ready() {
        mListener.onBillingReady();
    }

    private void loadPurchasesDetails() {
        ArrayList<String> ids = new ArrayList<>(getItemsCount());
        for (int i = mPurchaseIndex + 1; i < getItemsCount(); ++i) {
            ids.add(getItem(i));
        }
        if (!ids.isEmpty()) {
            final Bundle query = new Bundle();
            query.putStringArrayList("ITEM_ID_LIST", ids);
            final Handler handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    ready();
                }
            };
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    loadPurchasesDetails(query, handler);
                }
            });
            thread.start();
        } else {
            ready();
        }
    }

    private void error(String message, Handler handler) {
        Log.e(getClass().getName(), message);
        mOk = false;
        if (handler != null) {
            handler.sendEmptyMessage(1);
        }
    }

    private void error(String message) {
        error(message, null);
        ready();
    }

    private void loadPurchasesDetails(Bundle query, Handler handler) {
        mPrices = new String[getItemsCount()];
        Bundle details = null;
        try {
            details = mService.getSkuDetails(3,
                    mActivity.getPackageName(), "inapp", query);
        } catch (RemoteException e) {
            error(e.getMessage(), handler);
            return;
        }

        int response = details.getInt("RESPONSE_CODE");
        if (response != 0) {
            error("Billing details response fail: " + response, handler);
            return;
        }

        ArrayList<String> responseList = details.getStringArrayList("DETAILS_LIST");
        try {
            for (String thisResponse : responseList) {
                JSONObject object = new JSONObject(thisResponse);
                String id = object.getString("productId");
                String price = object.getString("price");
                int i = getIndex(id);
                if (i != -1) {
                    mPrices[i] = price;
                } else {
                    error("Unknown item " + id, handler);
                    return;
                }
            }
        } catch (JSONException e) {
            error(e.getMessage(), handler);
            return;
        }

        mOk = true;
        handler.sendEmptyMessage(0);
    }

    private int getIndex(String id) {
        for (int i = 0; i < getItemsCount(); ++i) {
            if ((getItem(i).equals(id))) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        unbind();
    }

    public void bind() {
        Intent serviceIntent =
                new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        mActivity.bindService(serviceIntent, this, Context.BIND_AUTO_CREATE);
    }

    public boolean isBound() {
        return mService != null;
    }

    public void unbind() {
        if (isBound()) {
            mActivity.unbindService(this);
            mService = null;
        }
    }

    private int getItemsCount() {
        return 3;
    }

    private String getItem(int index) {
        switch (index) {
            case 0:
                return "gymgain.base";
            case 1:
                return "gymgain.pro";
            case 2:
                return "gymgain.premium";
        }
        return null;
    }

    public void purchaseBasic() {
        purchase(ID_BASIC);
    }

    public void purchasePro() {
        purchase(ID_PRO);
    }

    public void purchasePremium() {
        purchase(ID_PREMIUM);
    }

    private void purchase(final int id) {
        if ((id < mPurchaseIndex) || (id >= getItemsCount())) {
            error("Wrong purchase index " + id);
            return;
        }
        String purchaseId = getItem(id);
        String payload = getRandomString();
        Bundle buyIntentBundle = null;
        try {
            buyIntentBundle = mService.getBuyIntent(3, mActivity.getPackageName(),
                    purchaseId, "inapp", payload);
        } catch (RemoteException e) {
            error(e.getMessage());
            return;
        }
        PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
        try {
            mActivity.startIntentSenderForResult(pendingIntent.getIntentSender(),
                    BUY_REQUEST_CODE, new Intent(), Integer.valueOf(0), Integer.valueOf(0),
                    Integer.valueOf(0));
        } catch (IntentSender.SendIntentException e) {
            error(e.getMessage());
            return;
        }
    }

    private String getRandomString() {
        Random rng = new Random();
        char[] text = new char[rng.nextInt(32)];
        for (int i = 0; i < text.length; i++) {
            text[i] = PUBLIC_KEY1.charAt(rng.nextInt(PUBLIC_KEY1.length()));
        }
        return new String(text);
    }

    public void handleBuyResult(int requestCode, Intent data) {
        handleBuyResult(data);
    }

    public boolean canHandle(int requestCode) {
        return requestCode == BUY_REQUEST_CODE;
    }

    private void handleBuyResult(Intent data) {
        int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
        if (responseCode != 0) {
            error("Billing purchase response fail: " + responseCode);
            return;
        }

        String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
        String signature = data.getStringExtra("INAPP_DATA_SIGNATURE");
        if (!checkSignature(purchaseData, signature)) {
            error("Signature fail");
            return;
        }
        String itemId = null;
        try {
            JSONObject dataObject = new JSONObject(purchaseData);
            itemId = dataObject.getString("productId");
        } catch (JSONException e) {
            error(e.getMessage());
            return;
        }
        int id = getIndex(itemId);
        if (id > mPurchaseIndex) {
            mPurchaseIndex = id;
            ready();
        }
    }

    private String getKey() {
        return  getKey("euMX()");
    }

    private String getKey(String text) {
        String result = PUBLIC_KEY1 + text + PUBLIC_KEY2;
        result = result.replace('@', 'd');
        result = result.replace(')', 'm');
        result = result.replace('(', 'A');
        result = result.replace('$', 'V');
        result = result.replace('*', 'y');
        result = result.replace('%', 'a');
        return result;
    }

    public static void startShop(Activity starter) {
        Intent intent = new Intent(starter, ShopActivity.class);
        starter.startActivity(intent);
    }

    //------------------------------------------------------------------------------------

    public interface ReadyListener
    {
        void onBillingReady();
    }

    private IInAppBillingService mService;
    private int mPurchaseIndex;
    private boolean mFullData;
    private String mPrices[];
    private ReadyListener mListener;
    private Activity mActivity;
    private boolean mOk;

    private static final int ID_BASIC = 0;
    private static final int ID_PRO = 1;
    private static final int ID_PREMIUM = 2;

    private static final int BUY_REQUEST_CODE = 1;

    private static String PUBLIC_KEY1 = "MIIBIj(NBgkqhkiG9w0B(QEF((OC(Q8(MIIBCgKC(QE(nkX+GWlZ6ON*wpEOpil)qRu0ivb3%joqb+ObMtCD/ZJ%RgjK8fje8brEftpnIhCqChLq$$6qbtG%1";
    private static String PUBLIC_KEY2 = "ijwNFMSH2uxJDC@3L+Qw@f2l)+hOMxbBRfTEw0Jt+NQtzz/xxYNCi12I1c/ZTWL3HpbCx8v+FnMP3(CoJg4xP5tvru6kZ8)@F4kDF+YcqCOHirKoSr3X+2q1FQoOYiL9MZDO2@GF%8IB$1E7NJ2vH53GW/8ZgY21w4z7TRJsWw3wJ6Tl%jRQI550qb051)RPbwt4vsj%M8%p1bKuBD8ZXi89h@)8C87oSkZo3%YpNg5KT6Xg%9U9O%JP$xshr5MFzzwID(Q(B";

}
