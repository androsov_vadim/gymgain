package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.vaisoft.health.gymgain.data.Progress;
import com.vaisoft.health.gymgain.data.ProgressCounter;


public class ProgressBar extends ProgressView {

    public ProgressBar(Context context) {
        super(context);
    }

    public ProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setData(ProgressCounter data) {
        mData = data;
        notifyDataChanged();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float badSoso = mData.getBadSosoLevel() * getHeight();
        float sosoGood = mData.getSosoGoodLevel() * getHeight();
        drawValue(canvas, getBrush(Progress.Estimate.BAD), 0, badSoso);
        drawValue(canvas, getBrush(Progress.Estimate.SOSO), badSoso, sosoGood);
        drawValue(canvas, getBrush(Progress.Estimate.GOOD), sosoGood, getHeight());
    }

    private void drawValue(Canvas canvas, Paint brush, float y1, float y2) {
        canvas.drawRect(0, y1, getWidth(), y2, brush);
    }

    private ProgressCounter mData = ProgressCounter.UNDEFINED;
}