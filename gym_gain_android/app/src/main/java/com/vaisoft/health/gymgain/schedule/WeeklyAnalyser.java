package com.vaisoft.health.gymgain.schedule;

import com.vaisoft.health.gymgain.utils.Utils;

import org.joda.time.DateTime;
import org.joda.time.Days;

import java.util.Set;

public class WeeklyAnalyser extends AbstractScheduleAnalyser
{
    public WeeklyAnalyser(DateTime date, DateTime lastUpdatedDate, Set<Integer> weekDays, DateTime lastAnalysed) {
        super(date, lastUpdatedDate, lastAnalysed);
        mSchedule = weekDays;
        init();
    }

    @Override
    protected DateTime getCurrentPeriodStart(DateTime checkDate) {
        return getWeeklyPeriodStart(checkDate);
    }

    @Override
    protected DateTime analyseWorkoutsFrom() {
        return getCurrentPeriodStart().minusWeeks(1);
    }

    @Override
    protected int getPlannedCount(DateTime start, DateTime finish) {
        if (Days.daysBetween(start, finish).getDays() >= 7) {
            return mSchedule.size();
        }
        int count = 0;
        DateTime current = new DateTime(start);
        while (Utils.isBeforeOrEqualDay(current, finish)) {
            if (isPlannedOn(current)) {
                count++;
            }
            current = current.plusDays(1);
        }
        return count;
    }

    @Override
    protected boolean isPlannedOn(DateTime date) {
        return mSchedule.contains(date.getDayOfWeek());
    }

    @Override
    protected boolean isLegacyActual() {
        return true;
    }

    @Override
    protected boolean isSingleDayPlan() {
        return false;
    }

    private Set<Integer> mSchedule;
}
