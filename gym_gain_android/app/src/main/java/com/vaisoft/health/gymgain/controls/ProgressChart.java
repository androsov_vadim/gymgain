package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;

import com.vaisoft.health.gymgain.R;
import com.vaisoft.health.gymgain.data.Progress;


public class ProgressChart extends ProgressView {

    public ProgressChart(Context context) {
        super(context);
    }

    public ProgressChart(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProgressChart(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void init() {
        super.init();
        mCellSpacing = getResources().getDimensionPixelSize(R.dimen.chart_spacing);
        mMarker = new Paint();
        mColorBack = ContextCompat.getColor(getContext(), R.color.chart_background);

        mMarker.setColor(ContextCompat.getColor(getContext(), R.color.accent));
        mMarker.setStyle(Paint.Style.FILL);
     }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(mColorBack);
        if (mAdapter == null) {
            return;
        }

        int fullCellX = mCellWidth + mCellSpacing;
        int fullCellY = (mCellWidth / 2) + mCellSpacing;
        int paddingX = (getWidth() - fullCellX * mColumnsCount + mCellSpacing) / 2;
        mMarker.setStrokeWidth(paddingX / 2);

        int level = mFirstColumnHeight;
        int markerIndex = mAdapter.getMarkerIndex();
        for (int i = 0; i < mColumnsCount; i++) {
            int x = paddingX + i * fullCellX;
            Progress progress = mAdapter.getProgress(mStartIndex + i);
            Paint brush = getBrush(progress.getEstimate());
            if (i > 0) {
                level = applyProgress(level, progress);
            }
            if ((i + mStartIndex) == markerIndex) {
                canvas.drawRect(x - mCellSpacing,
                        getHeight() - (level * fullCellY),
                        x + fullCellX,
                        getHeight(), mMarker);
            }
            for (int j = 0; j < level; j++) {
                int y = getHeight() - (j * fullCellY);
                canvas.drawRect(x, y - mCellWidth / 2, x + mCellWidth, y, brush);
            }
        }
    }

    public void setAdapter(Adapter adapter) {
        mAdapter = adapter;
        notifyDataChanged();
    }

    @Override
    public void notifyDataChanged() {
        super.notifyDataChanged();
        requestLayout();
    }

    private void calculateColumnMaxHeight(int maxCount) {
        mStartIndex = 0;
        if (maxCount < mAdapter.getCount()) {
            mStartIndex = mAdapter.getCount() - maxCount;
        }
        mColumnsCount = mAdapter.getCount() - mStartIndex;
        int level = 1;
        int bottom = 1;
        mMaxColumnElementsCount = 1;
        for (int i = 1; i < mColumnsCount; ++i) {
            Progress progress = mAdapter.getProgress(i + mStartIndex);
            switch (progress.getChange()) {
                case MORE:
                    ++level;
                    if (level > mMaxColumnElementsCount) {
                        mMaxColumnElementsCount = level;
                    }
                    break;
                case LESS:
                    --level;
                    if (level < bottom) {
                        bottom = level;
                    }
                    break;
            }
        }
        mFirstColumnHeight = 1;
        if (bottom < 1) {
            mFirstColumnHeight -= (bottom - 1);
            mMaxColumnElementsCount -= (bottom - 1);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        mCellWidth = getCellWidth(widthSize);
        int visibleColumnsCount = (widthSize - mCellSpacing) / (mCellWidth + mCellSpacing);
        calculateColumnMaxHeight(visibleColumnsCount);
        int desiredHeight = (mCellWidth / 2 + mCellSpacing) * mMaxColumnElementsCount + mCellSpacing;

        int height;

        if (heightMode == MeasureSpec.EXACTLY) {
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            height = Math.min(desiredHeight, heightSize);
        } else {
            height = desiredHeight;
        }

        setMeasuredDimension(widthSize, height);
    }

    private int getCellWidth(int totalWidth) {
        int min = getResources().getDimensionPixelSize(R.dimen.chart_cell_min);
        int max = getResources().getDimensionPixelSize(R.dimen.chart_cell_max);
        int count = mAdapter.getCount();
        if (count == 0) {
            return max;
        }
        int cellSize = totalWidth / mAdapter.getCount() - mCellSpacing * 2;
        if (cellSize > max) {
            return max;
        }
        if (cellSize < min) {
            return min;
        }
        return cellSize;
    }

    private int applyProgress(int value, Progress progress) {
        int result = value;
        switch (progress.getChange()) {
            case MORE:
                result = value + 1;
                break;
            case LESS:
                result = value - 1;
                break;
        }
        return result;
    }

    //---------------------------------------------------------------------------------------

    public interface Adapter {
        int getCount();
        Progress getProgress(int index);
        int getMarkerIndex();
    }

    private int mCellSpacing;
    private Paint mMarker;
    private int mColorBack;
    private int mFirstColumnHeight;
    private int mCellWidth;
    private int mStartIndex;
    private int mColumnsCount;
    private int mMaxColumnElementsCount;
    private Adapter mAdapter;
}