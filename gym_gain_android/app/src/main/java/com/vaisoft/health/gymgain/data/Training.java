package com.vaisoft.health.gymgain.data;

import android.content.Context;
import android.util.Log;

import com.vaisoft.health.gymgain.sync.JSONLoader;
import com.vaisoft.health.gymgain.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Training extends Lifecycle implements Cloneable {

    public  Training() {

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Training newTraining = (Training) super.clone();
        newTraining.mExercises = new ArrayList<>(mExercises);
        newTraining.mSets = new ArrayList<>(mSets);
        if (mPlan != null) {
            newTraining.mPlan = new ArrayList<>(mPlan.size());
            for (PlanEntry item : mPlan) {
                newTraining.mPlan.add(item.clone());
            }
        }
        return newTraining;
    }

    private Training(JSONObject object) {
        try {
            mId = object.getString(ID);
            loadLifecycle(object);
            if (!isDeleted()) {
                initLifecycleUniqueId();
                JSONArray exercises = object.getJSONArray(EXERCISES);
                readExercises(exercises);
                if (object.has(SETS)) {
                    JSONArray sets = object.getJSONArray(SETS);
                    readSets(sets);
                }
                if (object.has(PLAN)) {
                    mManualPlan = true;
                    JSONArray plan = object.getJSONArray(PLAN);
                    readPlan(plan);
                }
            }
        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
    }

    private void initLifecycleUniqueId() {
        if (getLifecycleUniqueId() == 0) {
            ++UNIQUE_ID_GENERATOR;
            setLifecycleUniqueId(UNIQUE_ID_GENERATOR);
        }
    }

    public static void load(JSONLoader loader, OnLoadedListener listener) {
        JSONArray trains = loader.loadArray(FILE_NAME, Utils.FileTarget.FILE_TARGET_DYNAMIC);
        if (trains == null) {
            return;
        }
        try {
            for (int i = 0; i < trains.length(); i++) {
                JSONObject theObject = trains.getJSONObject(i);
                Training training = new Training(theObject);
                listener.onLoaded(training);
            }
        } catch (JSONException e) {
            Log.e(Training.class.getName(), e.getMessage());
        }
    }

    public static void addSourceFilesToList(List<String> list) {
        list.add(FILE_NAME);
    }

    private void readPlan(JSONArray plan) throws JSONException {
         mPlan = new ArrayList<>();
         for (int i = 0; i < plan.length(); i++) {
            JSONObject itemObject = plan.getJSONObject(i);
            PlanEntry item = new PlanEntry();
            if (itemObject.has(PLAN_EXERCISE)) {
                item.mType = PlanItemType.EXERCISE;
                item.mIndex = itemObject.getInt(PLAN_EXERCISE);
            } else if (itemObject.has(PLAN_SET)) {
                item.mType = PlanItemType.SET;
                item.mIndex = itemObject.getInt(PLAN_SET);
            } else {
                item = null;
                Log.e(getClass().getName(), "Unknown plan item type");
            }
             if (itemObject.has(PLAN_PRECISE)) {
                 JSONArray data = itemObject.getJSONArray(PLAN_PRECISE);
                 item.mPrecisePlan = new int[data.length()];
                 for (int p = 0; p < data.length(); p++) {
                     item.mPrecisePlan[p] = data.getInt(p);
                 }
             }
             if (itemObject.has(PLAN_COMMENT)) {
                 item.mComment = itemObject.getString(PLAN_COMMENT);
             }
            mPlan.add(item);
        }
    }

    private void readSets(JSONArray sets) throws JSONException {
        for (int s = 0; s < sets.length(); s++) {
            JSONArray exercises = sets.getJSONArray(s);
            List<String> exList = new ArrayList<>();
            for (int i = 0; i < exercises.length(); i++) {
                String exId = exercises.getString(i);
                exList.add(exId);
            }
            mSets.add(exList);
        }
    }

    private void readExercises(JSONArray exercises) throws JSONException {
        for (int i=0; i < exercises.length(); i++) {
            String exId = exercises.getString(i);
            mExercises.add(exId);
        }
    }

    public JSONObject writeToJson()
    {
        JSONObject object = new JSONObject();
        try {
            object.put(ID, mId);

            saveLifecycle(object);

            if (!isDeleted()) {
                JSONArray exercises = new JSONArray();
                for (String exercise : mExercises) {
                    exercises.put(exercise);
                }
                object.put(EXERCISES, exercises);

                JSONArray sets = writeSetsToJson();
                object.put(SETS, sets);

                if (mManualPlan) {
                    JSONArray plan = writePlanToJson();
                    object.put(PLAN, plan);
                }
            }

        } catch (JSONException e) {
            Log.e(getClass().getName(), e.getMessage());
        }
        return  object;
    }

    private JSONArray writePlanToJson() throws JSONException {
        JSONArray plan = new JSONArray();
        for (PlanEntry item : mPlan) {
            JSONObject itemObject = new JSONObject();
            switch (item.mType) {
                case EXERCISE:
                    itemObject.put(PLAN_EXERCISE, item.mIndex);
                    break;
                case SET:
                    itemObject.put(PLAN_SET, item.mIndex);
                    break;
            }
            if (item.mPrecisePlan != null) {
                JSONArray data = new JSONArray();
                for (int i = 0; i < item.mPrecisePlan.length; i++) {
                    data.put(i, item.mPrecisePlan[i]);
                }
                itemObject.put(PLAN_PRECISE, data);
            }
            if (item.mComment != null) {
                itemObject.put(PLAN_COMMENT, item.mComment);
            }
            plan.put(itemObject);
        }
        return plan;
    }

    private JSONArray writeSetsToJson() {
        JSONArray sets = new JSONArray();
        for (List<String> setEntry : mSets) {
            JSONArray setEx = new JSONArray();
            for (String exId : setEntry) {
                setEx.put(exId);
            }
            sets.put(setEx);
        }
        return sets;
    }

    public ArrayList<String> getExercises() {
        return mExercises;
    }

    public ArrayList<String> getExercisesNotInSets(int except) {
        ArrayList<String> result = new ArrayList<>(mExercises);
        int index = 0;
        for (List<String> set: mSets) {
            if (index != except) {
                result.removeAll(set);
            }
            ++index;
        }
        return result;
    }

    private List<String> getSetWithExercise(String id) {
        for (List<String> set : mSets) {
            if (set.contains(id)) {
                return set;
            }
        }
        return null;
    }

    public void removeExercise(String id) {
        int exIndex = mExercises.indexOf(id);
        if (exIndex == -1) {
            Log.e(getClass().getName(), "Attempt to remove non-existing exercise " + id);
            return;
        }

        int emptiedSet = -1;
        List<String> set = getSetWithExercise(id);
        if (set != null) {
            set.remove(id);
            if (set.size() < 2) {
                emptiedSet = mSets.indexOf(set);
            }
        }

        if (emptiedSet != -1) {
            mSets.remove(emptiedSet);
        }
        mExercises.remove(id);

        if (mPlan != null) {
            if (emptiedSet != -1) {
                removeFromPlan(PlanItemType.SET, emptiedSet);
                addNotUsedExercisesToPlan();
                fixPlanIndexes(PlanItemType.EXERCISE, exIndex);
            } else {
                removeFromPlan(PlanItemType.EXERCISE, exIndex);
            }
        }
    }

    private void addNotUsedExercisesToPlan() {
        for (int i = 0; i < mExercises.size(); ++i) {
            boolean found = false;
            for (PlanEntry entry: mPlan) {
                if (entry.isExercise(i)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                List<String> set = getSetWithExercise(mExercises.get(i));
                if (set == null) {
                    mPlan.add(new PlanEntry(PlanItemType.EXERCISE, i));
                }
            }
        }
    }

    private void removeFromPlan(PlanItemType type, int index) {
        PlanEntry toRemove = new PlanEntry(type, index);
        mPlan.remove(toRemove);
        fixPlanIndexes(type, index);
    }

    private void fixPlanIndexes(PlanItemType type, int index) {
        for (PlanEntry entry: mPlan) {
            entry.decreaseIndexIfGreater(type, index);
        }
    }

    public void addExercises(List<String> exercises) {
        for (String id : exercises) {
            mExercises.add(id);
            if (mPlan != null) {
                mPlan.add(new PlanEntry(PlanItemType.EXERCISE, mExercises.size() - 1));
            }
        }
    }

    public void addSet(List<String> set) {
        mSets.add(set);
        if (mPlan != null) {
            List<PlanEntry> toRemove = new ArrayList<>();
            for (PlanEntry item : mPlan) {
                if (item.mType == PlanItemType.EXERCISE) {
                    String exId = mExercises.get(item.mIndex);
                    if (set.contains(exId)) {
                        toRemove.add(item);
                    }
                }
            }
            mPlan.removeAll(toRemove);
            mPlan.add(new PlanEntry(PlanItemType.SET, mSets.size() - 1));
        }
    }

    public void removeSet(int index) {
        mSets.remove(index);
        if (mPlan != null) {
            removeFromPlan(PlanItemType.SET, index);
            addNotUsedExercisesToPlan();
        }
    }

    public int getSetsCount() {
        return mSets.size();
    }

    public List<String> getSet(int index) {
        return mSets.get(index);
    }

    private void buildAutomaticPlan() {
        List<Integer> plannedSets = new ArrayList<>();
        for (int e = 0; e < mExercises.size(); e++) {
            String ex = mExercises.get(e);
            List<Integer> setsToPlan = new ArrayList<>();
            for (int s = 0; s < mSets.size(); s++) {
                List<String> set = mSets.get(s);
                if (set.contains(ex) && (!plannedSets.contains(s))) {
                   setsToPlan.add(s);
                }
            }
            if (setsToPlan.isEmpty()) {
                PlanEntry item = new PlanEntry();
                item.mType = PlanItemType.EXERCISE;
                item.mIndex = e;
                mPlan.add(item);
            } else {
                plannedSets.addAll(setsToPlan);
                for (Integer setIndex : setsToPlan) {
                    PlanEntry item = new PlanEntry();
                    item.mType = PlanItemType.SET;
                    item.mIndex = setIndex;
                    mPlan.add(item);
                }
            }
        }
    }

    public int getPlanSize() {
        return getPlan().size();
    }

    private List<PlanEntry> getPlan() {
        if (mPlan.isEmpty()) {
            buildAutomaticPlan();
        }
        return mPlan;
    }

    public PlanEntry getPlanItemContent(int index) {
        return mPlan.get(index);
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public void swapPlanItems(int x, int y) {
        PlanEntry xItem = mPlan.get(x);
        mPlan.set(x, mPlan.get(y));
        mPlan.set(y, xItem);
        mManualPlan = true;
    }

    public static void saveAll(Context context, List<Training> trainings) {
        if (context != null) {
            JSONArray objects = new JSONArray();
            for (Training training : trainings) {
                objects.put(training.writeToJson());
            }
            Utils.saveJSON(context, FILE_NAME, objects);
        }
    }

    public boolean usesExercise(String exerciseId) {
        return mExercises.contains(exerciseId);
    }

    public boolean isUseless() {
        return mExercises.isEmpty();
    }

    public boolean renameExercises(String id, String previousId) {
        if (!mExercises.contains(previousId)) {
            return false;
        }
        mExercises.remove(previousId);
        mExercises.add(id);
        return true;
    }

//----------------------------------------------------------------------------------------

    public enum PlanItemType {
        EXERCISE,
        SET
    }

    public class PlanEntry implements Cloneable {

        PlanEntry() {
            mIndex = -1;
        }

        PlanEntry(PlanItemType type, int index) {
            mType = type;
            mIndex = index;
        }

        @Override
        protected PlanEntry clone() throws CloneNotSupportedException {
            PlanEntry newItem = (PlanEntry) super.clone();
            newItem.mType = mType;
            newItem.mIndex = mIndex;
            if (mPrecisePlan != null) {
                newItem.mPrecisePlan = mPrecisePlan.clone();
            }
            return newItem;
        }

        public List<String> getItems() {
            List<String> result = new ArrayList<>();
            switch (mType) {
                case EXERCISE:
                    result.add(mExercises.get(mIndex));
                    break;
                case SET:
                    result.addAll(mSets.get(mIndex));
                    break;
            }
            return result;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof PlanEntry) {
                PlanEntry item = (PlanEntry) o;
                return (item.mType == mType) && (item.mIndex == mIndex);
            }
            return false;
        }

        public void decreaseIndexIfGreater(PlanItemType type, int index) {
            if ((mType == type) && (mIndex > index)) {
                --mIndex;
            }
        }

        public boolean isExercise(int index) {
            return (mType == PlanItemType.EXERCISE) && (mIndex == index);
        }

        public String getComment() {
            return mComment;
        }

        public int[] getPrecisePlan() {
            return mPrecisePlan;
        }

        public void updatePlan(int[] plan, String comment) {
            mPrecisePlan = plan;
            mComment = comment;
            mManualPlan = true;
        }

        private PlanItemType mType;
        private int mIndex;
        private int[] mPrecisePlan;
        private String mComment;
    }

    public interface OnLoadedListener {
        void onLoaded(Training training);
    }

    private String mId;
    private boolean mManualPlan;

    private ArrayList<PlanEntry> mPlan = new ArrayList<>();
    private ArrayList<String> mExercises = new ArrayList<>();
    private List<List<String>> mSets = new ArrayList<>();

    private static final String ID = "id";
    private static final String EXERCISES = "exercises";
    private static final String SETS = "sets";
    private static final String PLAN = "plan";
    private static final String PLAN_PRECISE = "precise";
    private static final String PLAN_COMMENT = "comment";
    private static final String PLAN_EXERCISE = "ex";
    private static final String PLAN_SET = "set";

    private static final String FILE_NAME="train";

    private static long UNIQUE_ID_GENERATOR = 0;
}
