package com.vaisoft.health.gymgain.controls;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import androidx.core.content.ContextCompat;
import androidx.cardview.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vaisoft.health.gymgain.R;

public class ButtonPanel extends LinearLayout {

    public ButtonPanel(Context context)
    {
        super(context);
        init(context, null);
    }

    public ButtonPanel(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context, attrs);
    }

    public ButtonPanel(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs)
    {
        mChecked = false;
        mDeletable = false;
        mSelectable = false;
        View.inflate(context, R.layout.button_panel, this);
        setDescendantFocusability(FOCUS_BLOCK_DESCENDANTS);
        mTitle = findViewById(R.id.button_panel_text);
        mButton = findViewById(R.id.button_panel_button);
        mBody = findViewById(R.id.button_panel_body);

        mBody.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (mSelectable) {
                        setChecked(!mChecked);
                        if (mListener != null) {
                            mListener.onSelectionChange(getData(), mChecked);
                        }
                    }
                }
                return true;
            }
        });

        mButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mDeletable && (mListener != null)) {
                    mListener.onDeleteItem(getData());
                }
            }
        });

        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(
                    attrs, R.styleable.ButtonPanel, 0, 0);
            applyParameters(a);
            a.recycle();
        }

    }

    private String getData()
    {
        return (String) getTag();
    }

    private void applyParameters(TypedArray a)
    {
        String titleText = "";
        boolean deletable = false;
        boolean selectable = false;
        boolean checked = false;
        try {
            titleText = a.getString(R.styleable.ButtonPanel_bpText);
            checked = a.getBoolean(R.styleable.ButtonPanel_bpChecked, false);
            deletable = a.getBoolean(R.styleable.ButtonPanel_bpDeleteable, false);
            selectable = a.getBoolean(R.styleable.ButtonPanel_bpSelectable, false);
        } catch (Exception e) {
            Log.e(getClass().getName(), "There was an error loading attributes.");
        }

        setTitleText(titleText);
        setChecked(checked);
        setDeletable(deletable);
        setSelectable(selectable);
    }

    public void setChecked(boolean checked)
    {
        int colorId;
        if (checked) {
            colorId = R.color.button_panel_selected;
        } else {
            colorId = R.color.button_material_light;
        }
        mBody.setCardBackgroundColor(ContextCompat.getColor(getContext(), colorId));
        mChecked = checked;
    }

    public boolean isChecked()
    {
        return mChecked;
    }

    public void setTitleText(String text)
    {
        mTitle.setText(text);
    }

    public void setDeletable(boolean deletable)
    {
        mDeletable = deletable;
        if (mDeletable) {
            mButton.setVisibility(View.VISIBLE);
            mButton.setFocusable(true);
            mButton.setFocusableInTouchMode(true);
            mTitle.setTypeface(null, Typeface.BOLD);
        } else {
            mButton.setVisibility(View.GONE);
            mTitle.setTypeface(null, Typeface.NORMAL);
        }
    }

    public void setSelectable(boolean selectable)
    {
        mSelectable = selectable;
        setFocusable(selectable);
        setFocusableInTouchMode(selectable);
    }

    public void setEventsListener(Listener listener)
    {
        if (mListener != null) {
            Log.w(getClass().getName(), "Button panel events listener changed: " + mButton.getTag());
        }
        mListener = listener;
    }

    //-----------------------------------------------------------------------------------
    public interface Listener
    {
        void onSelectionChange(String id, boolean selected);
        void onDeleteItem(String id);
    }

    private ImageButton mButton;
    private TextView mTitle;
    private CardView mBody;
    private boolean mChecked;
    private boolean mDeletable;
    private boolean mSelectable;
    private Listener mListener;
}
