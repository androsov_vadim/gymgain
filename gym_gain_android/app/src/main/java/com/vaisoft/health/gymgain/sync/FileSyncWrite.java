package com.vaisoft.health.gymgain.sync;

import androidx.annotation.NonNull;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.drive.DriveApi;
import com.google.android.gms.drive.DriveContents;
import com.google.android.gms.drive.DriveFile;
import com.google.android.gms.drive.DriveFolder;
import com.google.android.gms.drive.MetadataChangeSet;

import java.io.OutputStream;


public class FileSyncWrite extends FileSync {

    public interface Listener extends FileSync.Listener {
        boolean write(OutputStream os);
        void done();
    }

    public FileSyncWrite(GoogleApiClient drive, String fileName, String mimeType, Listener listener) {
        super(drive, fileName, listener);
        mListener = listener;
        mMimeType = mimeType;
        query();
    }

    private void query() {
        Drive.DriveApi.newDriveContents(getDrive())
                .setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
                    @Override
                    public void onResult(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
                        onFileCreateQuery(driveContentsResult);
                    }
                });
    }

    private void onFileCreateQuery(@NonNull final DriveApi.DriveContentsResult driveContentsResult) {
        if (!driveContentsResult.getStatus().isSuccess()) {
            onFail(driveContentsResult.getStatus());
            return;
        }
        MetadataChangeSet changeSet = new MetadataChangeSet.Builder()
                .setTitle(getFileName())
                .setMimeType(mMimeType).build();
        Drive.DriveApi.getAppFolder(getDrive())
                .createFile(getDrive(), changeSet, driveContentsResult.getDriveContents())
                .setResultCallback(new ResultCallback<DriveFolder.DriveFileResult>() {
                    @Override
                    public void onResult(@NonNull DriveFolder.DriveFileResult driveFileResult) {
                        onFileCreateResult(driveFileResult);
                    }
                });
    }

    private void onFileCreateResult(@NonNull DriveFolder.DriveFileResult driveFileResult) {
        if (!driveFileResult.getStatus().isSuccess()) {
            onFail(driveFileResult.getStatus());
            return;
        }
        driveFileResult.getDriveFile().open(getDrive(), DriveFile.MODE_WRITE_ONLY, null).setResultCallback(new ResultCallback<DriveApi.DriveContentsResult>() {
            @Override
            public void onResult(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
                writeFile(driveContentsResult);
            }
        });
    }

    private void writeFile(@NonNull DriveApi.DriveContentsResult driveContentsResult) {
        if (!driveContentsResult.getStatus().isSuccess()) {
            onFail(driveContentsResult.getStatus());
            return;
        }
        DriveContents contents = driveContentsResult.getDriveContents();
        OutputStream os = contents.getOutputStream();
        if (!mListener.write(os)) {
            return;
        }
        contents.commit(getDrive(), null).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                onChangesCommit(status);
            }
        });
    }

    private void onChangesCommit(@NonNull Status status) {
        if (status.isSuccess()) {
            mListener.done();
        } else {
            onFail(status);
        }
    }

    private Listener mListener;
    private String mMimeType;
}
